from encodings import utf_8
import h5py
from config import resolution,startCoords,stopCoords,step_size,axes
import json
import os

class CoordinateAxes:
    def __init__(self,axes):
        self.axes = axes
    
    def index(self,val):
        return self.axes.index(val)

class Coordinate:
    def __init__(self,value):
        self.value = value
    
    def isAxisValue(self,CoordinateAxes,*axis):
        for point in axis:
            index = CoordinateAxes.index(point[0])
            if point[1]!= self.value[index]:
                return False
        return True 

class CoordinateContext:
    '''
    '''
    def __init__(self,DataPath):
        if os.path.exists(DataPath + "ui_data.json"):
            f = open(DataPath + "ui_data.json", "r")
            UIData = json.load(f)
            f.close()
            self.axes = UIData["axes"]
            self.startCoords = UIData["startCoords"]
            self.stopCoords = UIData["stopCoords"]
            self.stepSize = UIData["step_size"]
            self.resolution = UIData["resolution"]
        else:
            self.axes = axes # axes as stored in the hdf5 file
            self.startCoords = startCoords
            self.stopCoords = stopCoords
            self.stepSize = step_size
            self.resolution = resolution
        #configData = DataFile.get('config')
        self.buildAxesDict()
        self.createDimensionPoints()
        self.createAllCoords()
    
    def __repr__(self):
        return f"This is the context object at this point the values are: \n \
         axes : {self.axes} \n \
         axisDict : {self.index_dict} \n  \
        startCoords : {self.startCoords} \n \
        stopCoords : {self.stopCoords} \n \
        stepSize : {self.stepSize} \n \
        allCoords: {self.allCoords} \n \
        dimpoints : {self.dimPoints} \n "


    def createAllCoords(self):
        count = 0
        allCoords = [list(self.startCoords)]
        while count< len(self.axes):
            newCoord = []
            for index in range(len(allCoords)):
                newCoord+=self.fitPoints(allCoords[index],count,self.dimPoints[count])
            allCoords = newCoord.copy()
            count+=1
        self.allCoords = allCoords.copy()
        
    def createDimensionPoints(self): # creates an individual list of all the coordinates for all the axis
        self.dimPoints = []
        for coord in range(len(self.axes)):
            self.dimPoints.append(self.generateCoords(coord))


    def fitPoints(self,coords,index,extensions): # put full cooord, and all extension coords that need to be fit in there and the index at which they need to be fit at
        newCoords = []
        for point in extensions:
            temp = coords.copy()
            temp[index] = point
            newCoords.append(temp)
        return newCoords

    def generateCoords(self,index): ## generates a list of coordniates for a single index in the coordinates lists
        coords = [x/self.resolution for x in range(int(self.startCoords[index]*self.resolution),int(self.stopCoords[index]*self.resolution+self.stepSize[index]*self.resolution),int(self.stepSize[index]*self.resolution))]
        return coords

    def generateCustomCoords(self,index,start,stop):
        coords = []
        #print(f"{int(self.startCoords[index]*resolution)},{int(self.stopCoords[index]*resolution+self.stepSize[index]*resolution)},{self.stepSize[index]}")
        for i in range(int(start*self.resolution),int(stop*self.resolution+self.stepSize[index]*self.resolution),int(self.stepSize[index]*self.resolution)):
            #print(i/resolution)
            coords.append(i/self.resolution)
        return coords
    
    def buildAxesDict(self): # WHY do we need this ???
        ## returns the indices of the coordinates and axis dictinary mapping coorinates to indices.
        self.index_dict = {}
        for i in range(len(self.axes)):
            index = self.axes.index(self.axes[i])
            self.index_dict[self.axes[i]] = index
        return self.index_dict

    def getAxisIndex(self,axisName):
        return self.index_dict[axisName]
        

if __name__=="__main__":
    file = h5py.File("OscData.hdf5", "r")
    context = CoordinateContext(file)
    print(context)
   #print(f"the context of the coordinates are axes:{context.axes},start:{context.startCoords},stop:{context.stopCoords},stepSize:{context.stepSize}")
    #print(context.dimPoints)
    #print(context.getAxisIndex(b'R1'))
    #print(context.fitPoints([1,'' ,'','' ,34],1,[1,1,1,11]))