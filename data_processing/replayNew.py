from numpy.core.fromnumeric import size
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg 
import os
import config
import h5py
from scipy import stats
from random import randint


from DataHandler import DataHandler
dh = DataHandler()
# dimensions = int(config.moduleType[0])
# slice = int(input(f"The experiment has {dimensions} dimensions please choose how many dimesnsions you want in the output replay"))
# if slice == 1:
#     axis = input("Enter the axis name you want to explore")
#     a = config.axes
#     a.remove(axis)
#     b = a.copy()
#     for i in range(len(a)):
#         a[i] = int(input(f"Enter the static value of {a[i]}"))
#     print(reader.get1DSlice(b,a))
all_Data = dh.getallData()
all_Coords = dh.context.allCoords
def get_data(count):
    return all_Data[count]
count = 0



app = QtGui.QApplication([])
win = pg.GraphicsLayoutWidget(show=True, title="Replay")
win.resize(2000,1200)
win.setWindowTitle('Replay')
p6 = win.addPlot(title="Recorded data")
p6.addLegend()
p6.setRange(yRange=[-10,20])
curve = p6.plot(pen='y')
plotItem = pg.PlotDataItem(name='dataset')
p6.addItem(plotItem)

background = get_data(count)
count+=1
def update():
    
    global curve, data, ptr, p6,count,iter,plotItem,background
    a = get_data(count)
    #super_threshold_indices = a < 0.000001
    #a[super_threshold_indices] = 0

    p6.legend.removeItem(plotItem)
    p6.legend.addItem(plotItem, f"{all_Coords[count]}")
    curve.setData(a)

    #if ptr == 0:
     #   p6.enableAutoRange('', False)  ## stop auto-scaling after the first data set is plotted
    count += 1
    print(count)
    if count >len(all_Data)-1:
        count = 0


if __name__ == "__main__":
    timer = QtCore.QTimer() 
    timer.timeout.connect(update)
    speed = input("enter speed (ms)")
    timer.start(int(speed))
    QtGui.QApplication.instance().exec_()
