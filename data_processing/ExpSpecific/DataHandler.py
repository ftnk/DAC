"""
@author: Arwinder
edited by: Rune
"""

from msilib.schema import Error
import h5py
from Context import CoordinateContext
import numpy as np
import enum
import os
import datetime
import collections.abc
def makeDir(filepath:'str',count:'int') -> 'str':
    '''
    Creates a directory and if the directory already exists, it creates a directory with t numbered name to avoid conflict
    #Input args : path to directory, current count
    #output : new path to file 

    '''
    while os.path.isdir(f"{filepath}-{count}"):
        count += 1
    try:                                  # if there is no naming conflict already
        filePath = f"{filepath}-{count}"
        os.mkdir(filePath)
    except:                               # if threre is a naming conflict then increase the count
        filePath = makeDir(filepath,count+1)
    return filePath
class Accumulator(enum.Enum):
    Raw = 0
    Integrate = 1
    Max = 2
    Sum = 3
    Peak = 4



class DataHandler:
    """
    Data is saved in "OscData.hdf5" and "HistData.hdf5".
    Format of "OscData.hdf5": (All TimeBases are the same) (len(TimeBase) = 10000) (len(YData) = 10000)
        {key_1: [[TimeBase], [YData for key_1]],
         key_2: [[TimeBase], [YData for key_2]],
         ...
         key_n: [[TimeBase], [YData for key_n]]}
    
    Format of "HistData.hdf5": (All TimeValues are the same) (len(TimeValues) = 1000) (len(HistogramValues) = 1000)
        {key_1: [[TimeValues], [HistogramValues for key_1]],
         key_2: [[TimeValues], [HistogramValues for key_2]],
         ...
         key_n: [[TimeValues], [HistogramValues for key_n]]}
    """
    
    def __init__(self, data_path=None):
        if data_path == None:
            preFix = ""
        else:
            preFix = data_path + "/"
        self.lockinExp = False
        self.file = None
        self.hFile = None
        self.lFile = None
        self.ampfile = None
        
        if os.path.exists(preFix + "OscData.hdf5"):
            self.file = h5py.File(preFix + "OscData.hdf5", "r") #Default file name for the hdf5 file is always OscData
        if os.path.exists(preFix + "AmpData.hdf5"):
            self.ampfile = h5py.File(preFix + "AmpData.hdf5", "r")
        if os.path.exists(preFix + "HistData.hdf5"):
            self.hFile = h5py.File(preFix + "HistData.hdf5", "r")
        if os.path.exists(preFix + "LockinData.hdf5"):
            self.lockinExp = True
            self.lFile = h5py.File(preFix + "LockinData.hdf5", "r")
        self.fileDict = {"osc": self.file, "hist": self.hFile, "amp": self.ampfile, "lock": self.lFile}
        self.histExist = os.path.exists(preFix + "HistData.hdf5")
        
        self.context = CoordinateContext(preFix) #creates the context for the file
        self.acc = Accumulator.Raw
        self.type = 0
        self.voltmap =   {0: [1.80099390582226, -0.0122911940964699, 0.000236801422346918], 50: [1.64239277714055, -0.0170954257408607, 0.000332224047279585],
                          100: [1.74423472064667, -0.0146333638479751, 0.000293418297082765], 200: [1.62872257610746, -0.0170552158138173, 0.000326084416459131],
                          300: [1.62044558446214, -0.0169427454118967, 0.000320087496423759], 500: [1.71417421019867, -0.0156754736172359, 0.000315725056140291],
                          700: [1.69476425255195, -0.0162459184100424, 0.000326389738052270], 1000: [1.67081224770819, -0.0167879741430848, 0.000333921673681089],
                          1500: [1.76710767011358, -0.0137398501511253, 0.000272612399719351], 2000: [1.65625704485442, -0.0170017138044564, 0.000334748847337431]}
        self.cutoffmap = {0: 0.006145597048, 50: 0.008547712870, 100: 0.007316681924, 200: 0.008527607907, 300: 0.008471372706, 500: 0.007837736809,
                          700: 0.008122959205, 1000: 0.008393987072, 1500: 0.006869925076, 2000: 0.008500856902}
    
    def getFile(self, dSource):
        return self.fileDict[dSource]

    def getIndex(self, axis):
        file = None
        for i in [self.file, self.hFile, self.lFile, self.ampfile]:
            if i is not None:
                file = i
                break
        axes = [x.decode() for x in list(file.get("axes"))]
        #print(f"Axes: {axes}")
        #print(type(axes[0]))
        #print(type(axis))
        axisIndex = axes.index(axis)
        
        #print(f"axisIndex: {axisIndex}")
        return axisIndex

    def sortCustomKeys(self, keys: list, unique: int, n_times: int):
        stripped_keys = []
        for i in keys:
            i_num = i[i.index("_") + 1:]
            stripped_keys.append(int(i_num))
        stripped_keys.sort()
        sortedKeys = []
        for i in stripped_keys:
            for j in keys:
                if int(j[j.index("_") + 1:]) == i:
                    sortedKeys.append(j)
                    continue
        return sortedKeys

    def getUniqueKeys(self, keys: list = None):
        if keys == None:
            keys = self.getKeys()
        uniqueKeys = []
        for i in keys:
            if i == "axes" or i == "config":
                continue
            i_key = float(i[: i.index("_")].replace("[", "").replace("]", ""))
            if not i_key in uniqueKeys:
                uniqueKeys.append(i_key)
        return uniqueKeys
    
    def customExport(self, forPlot=False):
        keys = self.getKeys()
        decimals = self.getDecimals()
        unique = 1
        n_times = 0
        sss = list(self.file.get('config'))
        curr = float(sss[0])
        while curr != sss[1]:
            unique += 1
            curr = round(curr + float(sss[2]), decimals)
        
        n_times = int((len(keys) - 2) / unique)

        mean_int = ["mean_int"]
        mean_max = ["mean_max"]
        std_int = ["std_int"]
        std_max = ["std_max"]
        sortedKeys = self.sortCustomKeys(keys[:-2], unique, n_times)
        
        for i in range(unique):
            mean_int_curr = []
            mean_max_curr = []
            
            for j in range(n_times):
                key = sortedKeys[(i * n_times) + j]
                self.setAccumulator(1)
                mean_int_curr.append(self.getDataPoint(key, "amp"))
                self.setAccumulator(2)
                mean_max_curr.append(self.getDataPoint(key, "amp"))
            mean_int.append(np.mean(mean_int_curr))
            mean_max.append(np.mean(mean_max_curr))
            std_int.append(np.std(mean_int_curr))
            std_max.append(np.std(mean_max_curr))
            
            
        fileName = "CustomExport.txt"
        fileName = self.getFreeName(fileName)
        final_arr = np.c_[mean_int, mean_max, std_int, std_max]
        if not forPlot:
            f = open(fileName, "a+")
            for i in final_arr:
                temp = ""
                for j in i:
                    temp += j + ", "
                temp = temp[:-2]
                f.write(temp + "\n")
            f.close()
        if forPlot:
            return final_arr

    
    """
    Uses the stepsize to determine how many decimals we should round to.
    Example: if the step sizes are [0.1, 0.5, 0.25] It will be 2 decimals (0.25)

    returns: int, the number of decimals
    """
    def getDecimals(self):
        file = None
        for i in [self.file, self.hFile, self.lFile, self.ampfile]:
            if i is not None:
                file = i
                break
        sss = list(file.get("config"))
        start = sss[0]
        stop = sss[1]
        steps = sss[2]
        maxDigits = -1

        for i in range(len(steps)):
            curr_step = steps[i]
            curr_start = start[i]
            curr_stop = stop[i]

            dotindx_step = str(curr_step).find(".")+1
            dotindx_start = str(curr_start).find(".")+1
            dotindx_stop = str(curr_stop).find(".")+1
            
            if len(str(curr_step)[dotindx_step:]) > maxDigits:
                maxDigits = len(str(curr_step)[dotindx_step:])

            if len(str(curr_start)[dotindx_start:]) > maxDigits:
                maxDigits = len(str(curr_start)[dotindx_start:])

            if len(str(curr_stop)[dotindx_stop:]) > maxDigits:
                maxDigits = len(str(curr_stop)[dotindx_stop:])
    
        return maxDigits


    """
    Converts 1 timevalue to electron-volts.

    x: float, The timevalue to be converted
    volts: int, the voltage to use (see self.voltmap)

    returns: float, the result in electron-volts
    """
    def energyCal(self, x, volts):
        ''''
        Based on some voltage, peform calculations on x and return the result
        '''
        if x < self.cutoffmap[volts]:
            return None
        
        values = self.voltmap[volts]
        res = (values[0])/(x**2 + values[1] * x + values[2])
        return res

    """
    Performs energycalibration on a list of timevalues.

    x: list, the list of timevalues to be converted
    volts: int, the voltage to use (see self.voltmap)

    returns: list, the converted list, which has been flipped because of the nature of the function
    """
    def energyCalibration(self, x, volts=0):
        ''''
        Performs calculations on all elements in x based on some voltage
        '''
        validVolts = [0, 50, 100, 200, 300, 500, 700, 1000, 1500, 2000]
        if volts not in validVolts:
            print(f"Energycalibration was called with input: volts={volts} which is not valid")
            print(f"Valid inputs are: {validVolts}")
            return None
        res = []
        excluded_indexes = []
        for i in range(1, len(x)):
            eValue = self.energyCal(x[i]*1E6, volts)
            if eValue != None:
                res.append(eValue)
            else:
                excluded_indexes.append(i-1)
        res = np.flip(res)
        return res, excluded_indexes
    
    def getKeys(self):
        file = None
        for i in [self.file, self.hFile, self.lFile, self.ampfile]:
            if i is not None:
                file = i
                break
        return list(file.keys())

    def getallData(self):
        data = []
        for coord in self.context.allCoords:
            data.append(self.getDataPoint(coord,"osc"))
        return data

    def get_all_raw_data(self):
        '''
        Gets all the data without accumulator
        only needed for oscilloscope class
        '''
        file = None
        for i in [self.file, self.hFile, self.lFile, self.ampfile]:
            if i is not None:
                file = i
                break
        keys = file.keys()
        data = []
        for key in  keys:
            data.append(file.get(key))
        return data
    
    def getDataPoint(self,coords,dSource,ignoreList = False):
        ''''
        Extracts data from OscData.hdf5 or HistData.hdf5 based on parameter 'hist'
        The 'coords' parameter is the coordinate to get data from, which also acts as a key in the data files
        '''
        formatFunctions = {
            Accumulator.Integrate: self.formatIntegrate,
            Accumulator.Max: self.formatMax,
            Accumulator.Sum: self.formatSum,
            Accumulator.Raw: self.formatRaw,
            Accumulator.Peak: self.formatPeak
        }
        if type(coords) == list or ignoreList:
            hist = dSource == "hist"
            amp = dSource == "amp"
            osc = dSource == "osc"
            lock = dSource == "lock"
            if hist:
                print(f"Getting HISTOGRAM data")
                data = np.array(self.hFile.get(str(coords)))
                return formatFunctions[self.acc](data)
            elif amp:
                print(f"Getting AMMETER data")
                data = np.array(self.ampfile.get(str(coords)))
                return data
            elif osc:
                print(f"Getting OSCILLOSCOPE data")
                data =  np.array(self.file.get(str(coords)))
                formatFunctions[Accumulator.Sum] = self.formatIntegrate
                data[1] = self.removeBackground(data[1])
                return formatFunctions[self.acc](data)
            elif lock:
                print(f"Getting LOCKIN data")
                data =  np.array(self.lFile.get(str(coords)))
                formatFunctions[Accumulator.Sum] = self.formatIntegrate
                return data
        else:
            print(f"The type of coords must be list , which is not the case with {coords}")
            raise(f"The type of coords must be list , which is not the case with {coords}")

    def formatIntegrate(self,data):
        return [np.trapz(data[1],dx= data[0][1]-data[0][0])]
    def formatSum(self,data):
        return [np.sum(data[1])]
    def formatMax(self,data):
        return [np.max(data[1])]
    def formatRaw(self,data):
        return data[1]
    def formatPeak(self,data):
        peakIndex = np.argmax(data[1])
        res = -1
        if data[1][peakIndex] == 0.0:
            res = None
        else:
            res = data[0][peakIndex]
        return [res]

    def checkInput(self,axes_names,coords,dimensions):
        if len(axes_names) != len(coords):
            raise ValueError("Number of axis names and the coords do not match")
        diff = len(self.context.axes) - len(axes_names)

        if diff == dimensions:
           return True
        return False


    def getPlaceHolder(self,axesName,axesValues):
        '''
        returns a placeholder coordinate with a value coordinate list with the missing 
        coordinate replace with a None and all others having fixed value.
        '''
        placeHolder = [None]*len(self.context.startCoords)
        for i in range(len(axesName)):
            index = self.context.getAxisIndex(axesName[i])
            placeHolder[index] = float(axesValues[i])
        return placeHolder
    

    def findMissing(self,placeHolder):
        '''
        returns the missing coordinates in the placeholder.


        '''
        missing = []
        for i in range(len(placeHolder)):
            if placeHolder[i] == None:
                missing.append(i)
        return missing

    def get1DRangedSlice(self, axesName, axesValues, dSource, rStart, rStop, rIndex):
        ''''
            returns a one dimensional slice of data, which is limited by a range on an axis
            the data is compressed using an accumulator,
            the inputs are the names and values of the fixed axes
            and wether or not to use histogram data
            the output includes the data,
            the coordinates, the name of the missing axes and 
            name of the plotted axes
        
        '''
        if self.checkInput(axesName,axesValues,1): 
            placeHolder = self.getPlaceHolder(axesName,axesValues)
            missing = self.findMissing(placeHolder)
            genCoords = self.context.generateCoords(missing[0])
            coords = self.context.fitPoints(placeHolder,missing[0],genCoords)
            newCoords = []
            for coord in coords:
                if coord[rIndex] >= rStart and coord[rIndex] <= rStop:
                    newCoords.append(coord)
            data = []
            for coord in newCoords:
                res = self.getDataPoint(coord,dSource)
                data.append(res)
            return data,newCoords,[self.context.axes[missing[0]]]
        else:
            print("Input-check failed")

    def get1DSlice(self,axesName,axesValues,rStart=None, rStop=None, rIndex=None, dSource=None):
        ''''
            returns a one dimensional slice of data,
            the data is compressed using an accumulator,
            the inputs are the names and values of the fixed axes
            and wether or not to use histogram data
            the output includes the data,
            the coordinates, the name of the missing axes and 
            name of the plotted axes
        
        '''
        if self.checkInput(axesName,axesValues,1): 
            placeHolder = self.getPlaceHolder(axesName,axesValues)
            missing = self.findMissing(placeHolder)
            genCoords = self.context.generateCoords(missing[0])
            coords = self.context.fitPoints(placeHolder,missing[0],genCoords)
            newCoords = []
            for coord in coords:
                if rIndex != None:
                    if self.isBetween(coord[rIndex], rStart, rStop):
                        newCoords.append(coord)
                else:
                    newCoords.append(coord)
            
            data = []
            for coord in newCoords:
                res = self.getDataPoint(coord,dSource)
                data.append(res)
            return data,newCoords,[self.context.axes[missing[0]]]
        else:
            print("Input-check failed")
    

    def get2DSlice(self,axesName,axesValues,dSource, rStart=None, rStop=None, rIndex=None, axisIndex=None):
        '''
        same get 1-D slice but with two dimesnions
        
        '''
        if self.checkInput(axesName,axesValues,2): 
            rangeSlice = rStart != None
            placeHolder = self.getPlaceHolder(axesName,axesValues)
            data   = []
            genCoordsa = self.context.generateCoords(axisIndex["X"])
            genCoordsb = self.context.generateCoords(axisIndex["Y"])
            coordsa = self.context.fitPoints(placeHolder,axisIndex["X"],genCoordsa)
            coordsb = []

            newcoords = []
            #print(f"genCoordsB: {genCoordsb}")
            for coordb in genCoordsb:
                temp = []
                for coorda in coordsa:
                    res = list(np.array(coorda).copy())
                    res[axisIndex["Y"]] = coordb
                    temp.append(res)
                coordsb.append(temp)
            #print(f"CoordsB: {coordsb}")
            
            for subcoord in coordsb:
                temp = []
                for coord in subcoord:
                    if rangeSlice:
                        if self.isBetween(coord[rIndex[0]], rStop[0], rStart[0]) and self.isBetween(coord[rIndex[1]], rStop[1], rStart[1]):
                            temp.append(coord)
                        #if coord[rIndex[0]] >= rStart[0] and coord[rIndex[0]] <= rStop[0] and coord[rIndex[1]] >= rStart[1] and coord[rIndex[1]] <= rStop[1]:
                        #    temp.append(coord)
                    else:
                        temp.append(coord)
                if len(temp) > 0:
                    newcoords.append(temp)
            #print(f"newcoords: {newcoords}")
            for subcoord in newcoords:
                data1= []
                for pt in subcoord:
                    data1.append(self.getDataPoint(pt,dSource))
                data.append(data1)

            return data,newcoords,[self.context.axes[axisIndex["X"]],self.context.axes[axisIndex["Y"]]]
        else:
            print("Input validation failed for 'get2DSlice'")
            

    def isBetween(self, val, a, b):
        if a > b:
            return val >= b and val <= a
        else:
            return val <= b and val >= a


    def removeBackground(self,data):
        ''''
        Averages the first 10 datapoints and subtracts that from the rest,
        to remove background-noise
        '''
        initAvgValue = np.sum(data[0:10])/10
        data = data -initAvgValue
        min_val = min(data)
        max_val = max(data)
        if abs(min_val) > abs(max_val):
            data = -1*data
        return data

    def getCompletePoint(self,coords, dSource): 
        ''''
        Get data from hdf5-files without passing it through an accumulator
        '''
        if type(coords) == list:
            coords = [float(x) for x in coords]
            data = np.array(self.fileDict[dSource].get(str(coords)))
            return data
        else:
            raise(f"The type of coords must be list , which is not the case with {coords}")
    
    def getVariableData(self, axes, axdict, vals, hist):
        if hist:
            file = self.hFile
        else:
            file = self.file
        data = {}
        for i in range(len(axdict)):
            if list(axdict.keys())[i] not in axes:
                vals.insert(i, None)
        for i in list(file.keys()):
            if i != "axes" and i != "config":
                key = i.replace("[", "").replace("]", "").replace(" ", "").split(",")
                key = [float(j) for j in key]
                addKey = True
                for j in axes:
                    if key[axdict[j]] != vals[axdict[j]]:
                        addKey = False
                if addKey:
                    data[str(key)] = (list(file[i]))
        return data

    """
    axes: list of axes with a set value
    axdict: dictionary of type: {'axes': index_in_keys}
    vals: list of set values for each axes in the 'axes'-param
    hist: boolean indicating wether to use HistData.hdf5 or OscData.hdf5
    rStart: float, lowest coordinate of the variable axes
    rStop: float, highest coordinate of the variable axes
    rIndex: int, the variable axes' index in the data keys

    returns: dictinary of type: {'key': data} (key could be "[0.1, 1.0, 1.0]", data is [[timevalues], [histogram-/oscilloscope- values]])
    """
    def getVariableRangeData(self, axes, axdict, vals, hist, rStart, rStop, rIndex):
        if hist:
            file = self.hFile
        else:
            file = self.file
        data = {}
        newVals = list(np.array(vals).copy())
        for i in range(len(axdict)):
            if list(axdict.keys())[i] not in axes:
                newVals.insert(i, None)
        for i in list(file.keys()):
            if i != "axes" and i != "config":
                key = i.replace("[", "").replace("]", "").replace(" ", "").split(",")
                key = [float(j) for j in key]
                addKey = True
                for j in axes:
                    if key[axdict[j]] != newVals[axdict[j]]:
                        addKey = False
                if addKey:
                    if self.isBetween(key[rIndex], rStart, rStop):
                        data[str(key)] = (list(file[i]))
                    #if rStart > rStop and key[rIndex] >= rStop and key[rIndex] <= rStart:
                    #    data[str(key)] = (list(file[i]))
                    #if rStart < rStop and key[rIndex] >= rStart and key[rIndex] <= rStop:
                    #    data[str(key)] = (list(file[i]))
                    
                #if addKey and key[rIndex] >= rStart and key[rIndex] <= rStop:
                #    data[str(key)] = (list(file[i]))
        return data
    
    """
    In the 'data'-parameter (dict), in the list of keys all axes but 'varAxis' is fixed.
    This function returns the floats of data[somekey][0] in reverse order in reltaion to the varAxis.
    ValuesAnno are the coords the varaxis varies over.

    data: {[coord_1, ..., coord_n]: [[float (len=1000)],[int (len=1000)]], [coord_1+step, ..., coord_n]: [[float (len=1000)],[int (len=1000)]]} (typically from getVariableRangeData)
    varAxis: String, the variable axes (fx. "VY")
    axes: list of axes with a set value (all axes except the varAxis)
    axdict: dictionary of type: {'axes': index_in_keys}
    vals: list of set values for each axes in the 'axes'-param
    SSS (Start, Stop, Step): 2D array: [[startcoords], [stopcoords], [stepcoords]] (Example: [[0.1, -3.0, -3.0], [0.15, 3.0, 3.0], [0.05, 0.25, 0.25]])
    digits: int, indicating the amount of digits to round to (typically from getDecimals)

    returns: 
        formattedData: 2D array of type [[data from the stop-coord], [data from the second to last coord], ..., [data from the second coord], [data from the start coord]]
        valuesAnno: array with the values for the variable axes (if the var axis starts at -3.0, ends at 0 with step 0.5, this would be [0.0, -0.5, -1.0, ... , -2.5, -3.0])
    """
    def extractVarDataFromDict(self, data, varAxis, axes, axdict, vals, SSS, digits):
        vals = [i for i in vals if i != None]
        #for i in [varAxis, axes, axdict, vals, SSS, digits]: print(i)
        formattedData = []
        currVal = SSS[1][axdict[varAxis]]
        start = SSS[0][axdict[varAxis]]
        step = SSS[2][axdict[varAxis]]
        arrStr = []
        usedAxes = 0
        for i in axes:
            arrStr.insert(axdict[i], round(float(vals[usedAxes]), digits))
            usedAxes += 1
        arrStr.insert(axdict[varAxis], round(float(currVal), digits))
        valuesAnno = []
        while round(abs(currVal - start), digits) > 0:
            valuesAnno.append(currVal)
            tempData = data[str(arrStr)][1]
            formattedData.append(tempData)

            currVal -= step
            """
            if SSS[0][axdict[varAxis]] < SSS[1][axdict[varAxis]]:
                if step > 0:
                    currVal -= step
                else:
                    currVal += step
            else:
                if step > 0:
                    currVal += step
                else:
                    currVal -= step
            """
            currVal = round(float(currVal), digits)
            if currVal == -0.0:
                currVal = 0.0
            arrStr[axdict[varAxis]] = currVal
        else:
            valuesAnno.append(currVal)
            tempData = data[str(arrStr)][1]
            formattedData.append(tempData)
        return formattedData, valuesAnno
    
    def removeFirstDataPoint2D(self, data):
        newData = []
        for i in data:
            newData.append(i[1:])
        return newData
            
    
    def save1D_Bulk(self,axesName,axesValues,start,stop,dSource,digits):
        '''
            Input Arguments:
            Variable axis: the axis which is varying 
            The values of fixed coordinates
            The range of values of the varibale axis
            Output Arguments:
            None
        '''
        placeHolder = self.getPlaceHolder(axesName,axesValues)
        missing = self.findMissing(placeHolder)
        genCoords = self.context.generateCustomCoords(missing[0],start,stop)
        coords = self.context.fitPoints(placeHolder,missing[0],genCoords) #all coords that need to be plotted.
        exportName = 'EXPORT-Axis'
        if dSource == "hist":
            exportName = 'EXPORT_HIST-Axis'
        directoryPath = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))),os.path.join(exportName+str(self.context.axes[missing[0]])+'-'+str(datetime.datetime.now().date())+'From-'+str(start)+'To-'+str(stop)))
        directoryPath = makeDir(directoryPath,0)
        for coord in coords:
            self.saveSingleData(coord,dSource,digits,filePath=directoryPath)
            #self.save1Ddata(axesNames=self.context.axes,axesValues=coord,filePath=directoryPath)
    
    def getFreeName(self, name, filePath=None):
        counter = 1
        if filePath is None:
            toCheck = name
        else:
            toCheck = os.path.join(filePath, name)
        if os.path.exists(toCheck):
            dotindx = toCheck.rfind(".")
            while os.path.exists(toCheck[:dotindx] + "_" + str(counter) + toCheck[dotindx:]):
                counter += 1
            return name[:dotindx] + "_" + str(counter) + name[dotindx:]
        else:
            return name




    def save1Ddata(self,axesNames, axes_original, axesValues, dSource=None, filePath=None, rangeMap=None):
        hist = dSource == "hist"
        amp = dSource == "amp"
        if hist:
            name = f"1DHistData1-{self.acc.name}-{axesNames}-{axesValues}-.txt".replace("[", "").replace("]", "").replace("'", "")
        elif amp:
            name = f"1DAmpData1-{self.acc.name}-{axesNames}-{axesValues}-.txt".replace("[", "").replace("]", "").replace("'", "")
        else:
            name = f"1DData1-{self.acc.name}-{axesNames}-{axesValues}-.txt".replace("[", "").replace("]", "").replace("'", "")
        name = self.getFreeName(name)
        a = open(name,"a+")
        data = self.get1DSlice(axesNames,axesValues, rangeMap["start"], rangeMap["stop"], rangeMap["index"], dSource)
        nonConst = -1
        #print(data)
        for i in range(len(axesNames)):
            if axes_original[i] != axesNames[i]:
                nonConst = i
                break
        #print(data)
        for i in range(len(data[0])):
            #value = f"{data[1][i][nonConst]},{data[0][i][0]} \n"
            value = str(data[1][i][nonConst]) + "," + str(data[0][i][0]) + " \n"
            value = value.replace("[", "").replace("]", "")
            a.write(value)
        a.close()
        
    def save2Ddata(self,axesNames,axesValues,dSource,rangeMap=None, axisMap=None):
        amp = dSource == "amp"
        hist = dSource == "hist"
        if self.acc == Accumulator.Raw and not self.lockinExp and not amp:
            raise Error
        else:
            if hist:
                name = f"2DHistData-{self.acc.name}-{axesNames}-{axesValues}.txt".replace("[", "").replace("]", "").replace("'", "")
            elif amp:
                name = f"2DAmpData"
                if len(axesNames) > 0:
                    name += f"-{axesNames}-{axesValues}"
                name += ".txt"
                name = name.replace("[", "").replace("]", "").replace("'", "")
            else:
                name = f"2DData-{self.acc.name}-{axesNames}-{axesValues}.txt".replace("[", "").replace("]", "").replace("'", "")
            name = self.getFreeName(name)
            a = open(name,"a+")

            axisIndex = {}
            decoded_axes = [i.decode() for i in list(self.fileDict[dSource].get("axes"))]
            for key in axisMap:
                axisIndex[key] = decoded_axes.index(axisMap[key])

            data = self.get2DSlice(axesNames, axesValues, dSource, rangeMap["start"], rangeMap["stop"], rangeMap["index"], axisIndex)
            #if rangeMap == None:
            #    data = self.get2DSlice(axesNames,axesValues,hist)
            #else:
            #    data = self.get2DRangedSlice(axesNames, axesValues, hist, rangeMap["start"], rangeMap["stop"], rangeMap["index"], axisIndex)
            formattedData = []
            for data1 in data[0]:
                temp = ""
                for j in data1:
                    if amp or self.lockinExp:
                        res = j[0]
                    else:
                        res = j
                    if (isinstance(res, collections.abc.Sequence)):
                        temp+=str(res[0])+","
                    else:
                        temp+=str(res)+","
                temp= temp[:-1]+"\n"   
                a.write(temp)
            #print(formattedData)
            #print(data[2])
            a.close()


    def saveSingleData(self,coord,dSource,digits,filePath=None):
        data = self.getCompletePoint(coord, dSource)
        coord = [str(round(i, digits)) for i in coord]
        name = f"datapoint-{coord}.txt".replace("[", "").replace("]", "").replace("'", "")
        if dSource == "hist":
            name = "histogram_" + name
        name = self.getFreeName(name, filePath)
        if filePath!=None:
            mainFile = os.path.join(filePath, name)
            a = open(mainFile,"a+")
        else:
            a = open(name,"a+")
        for i in range(int(len(data[0]))):
            value = str(data[0][i]) + "," + str(data[1][i]) + "\n"
            #a.write(value)
            value = value.replace("[", "").replace("]", "")
            a.write(value)

        a.close()
    
    def saveSingleDataEnergyConv(self, coord, dSource, digits,voltage,filePath=None):

        data = self.getCompletePoint(coord, dSource)
        newData0, exclude_indexes = self.energyCalibration(data[0], voltage)
        newData0 = np.array(newData0)
        newData1 = [i for i in data[1][1:]]
        newData1 = self.removeIndexesFromData(newData1, exclude_indexes, 1)
        newData1 = np.flip(newData1)
        coord = [str(round(i, digits)) for i in coord]
        name = f"datapoint-{coord}-{voltage}V.txt".replace("[", "").replace("]", "").replace("'", "")
        if dSource == "hist":
            name = "histogram_" + name
        name = self.getFreeName(name)
        if filePath!=None:
            mainFile = os.path.join(filePath, name)
            a = open(mainFile,"a+")
        else:
            a = open(name,"a+")
        for i in range(int(len(newData0))):
            value = str(newData0[i]) + "," + str(newData1[i]) + "\n"
            #a.write(value)
            value = value.replace("[", "").replace("]", "")
            a.write(value)

        a.close()


    def getHistogramTimevalues(self):
        if self.histExist:
            return self.hFile.get(list(self.hFile.keys())[0])[0]
        else:
            print("Could not locate histogram data file (HistData.hdf5)")
    

    """
    Gets valid values for all axes. If 'X' moves from -2.0 to 2.0 in steps of 0.5, validvals for 'X' is: [-2.0, -1.5, -1.0, ... , 1.0, 1.5, 2.0]

    axes: list, axes to get valid vals for
    axdict: dictionary of type: {'axes': index_in_keys}
    hist: boolean indicating wether to use HistData.hdf5 or OscData.hdf5 to get the keys from (They should be the same)

    returns: dict of type: {'axis_1': [valid vals for 'axis_1'], 'axis_n': [valid vals for 'axis_n']}
    """
    def getValidVals(self, axes, axdict, hist):
        validVals = {}
        if hist:
            file = self.hFile
        else:
            file = self.file
        for i in range(len(axes)):
            validVals[axes[i]] = []
            indx = axdict[axes[i]]
            for j in list(file.keys()):
                if j != "axes" and j != "config":
                    key = j.replace("[", "").replace("]", "").replace(" ", "").split(",")
                    key = [float(j) for j in key]
                    if key[indx] not in validVals[axes[i]]:
                        validVals[axes[i]].append(key[indx])
        return validVals



        
    def setAccumulator(self,num):
        if num == 0:
            self.acc = Accumulator.Raw
        if num == 1:
            self.acc = Accumulator.Integrate
        if num == 2:
            self.acc = Accumulator.Max
        if num == 3:
            self.acc = Accumulator.Sum
        if num == 4:
            self.acc = Accumulator.Peak
    
    def removeIndexesFromData(self, data, indexes, dimensions):
        res = []
        if dimensions == 2:
            for i in range(len(data)):
                temp = []
                for j in range(len(data[i])):
                    if j not in indexes:
                        temp.append(data[i][j])
                res.append(temp)
        elif dimensions == 1:
            for i in range(len(data)):
                if i not in indexes:
                    res.append(data[i])
        return res


    """
    Get all data needed for an energy heatmap.

    varAxis: String, the variable axes (fx. "VY")
    axes: list of axes with a set value (all axes except the varAxis)
    axdict: dictionary of type: {'axes': index_in_keys}
    vals: list of set values for each axes in the 'axes'-param
    SSS (Start, Stop, Step): 2D array: [[startcoords], [stopcoords], [stepcoords]] (Example: [[0.1, -3.0, -3.0], [0.15, 3.0, 3.0], [0.05, 0.25, 0.25]])
    voltage: int, which voltage should be used for energycalibration (see energyCalibration)
    digits: int, indicating the amount of digits to round to (typically from getDecimals)
    rangeMap: dict, {'start': float, 'stop': float, 'index': int}
        'start' is the starting coordinate of the variable axes (this can be anywhere, but to get the whole plot, 'start'=first coord)
        'stop' is the stopping coordinate of the variable axes (this can be anywhere, but to get the whole plot, 'stop'=last coord)
        'index' is which index the variable axes has in the keys
    
    returns:
        energyvals: list of floats with length 999 (timevalues after energycalibration, which means it is flipped)
        valuesAnno: list of coords for the variable axes (in flipped order)
        formattedData: 2D array of the data in reverse order:
            [[Data from the last key (flipped)          ], (len=999)
             [Data from the second to last key (flipped)], (len=999)
             ...,
             [Data from the second key (flipped)        ], (len=999)
             [Data from the first key (flipped)         ]] (len=999)
    """
    def getEnergyHeatMap(self, varAxis, axes, axdict, vals, SSS, voltage, digits, rangeMap=None):
        timevals = self.getHistogramTimevalues()
        energyvals, exclude_indexes = self.energyCalibration(timevals, voltage)
        if rangeMap == None:
            data = self.getVariableData(axes, axdict, vals, True)
        else:
            data = self.getVariableRangeData(axes, axdict, vals, True, rangeMap["start"], rangeMap["stop"], rangeMap["index"])
            SSS[0][rangeMap["index"]] = rangeMap["start"]
            SSS[1][rangeMap["index"]] = rangeMap["stop"]
        formattedData, valuesAnno = self.extractVarDataFromDict(data, varAxis, axes, axdict, vals, SSS, digits)
        formattedData = self.removeFirstDataPoint2D(formattedData)
        formattedData = self.removeIndexesFromData(formattedData, exclude_indexes, 2)
        formattedData = [np.flip(i) for i in formattedData]
        #formattedData = np.flip(formattedData, axis=0)
        return [energyvals, valuesAnno, formattedData]
    
    def saveEnergyHeatMap(self,varAxis, axes, axdict, vals, SSS, voltage, digits, rangeMap=None):
        allData = self.getEnergyHeatMap(varAxis, axes, axdict, vals, SSS, voltage, digits, rangeMap)
        energyvals = allData[0]
        valuesAnno = allData[1]
        formattedData = allData[2]
        dirname = f"EnergyHeatmap-{varAxis}"
        usedAxes = 0
        for i in range(len(vals)):
            if vals[i] != None:
                dirname += f"-{axes[usedAxes]}={vals[i]}"
                usedAxes += 1
        dirpath = makeDir(dirname, 0)
        name = f"{dirpath}\heatmapValues.txt"
        metaName = f"{dirpath}\metadata.txt"
        np.savetxt(name, formattedData, fmt="%d")
        f = open(metaName, "a+")
        if len(energyvals) > len(valuesAnno):
            longest = energyvals
            shortest = valuesAnno
            firstline = f"eV, {varAxis}\n"
        else:
            longest = valuesAnno
            shortest = energyvals
            firstline = f"{varAxis}, eV\n"
        while len(shortest) != len(longest):
            shortest.append(np.NaN)
        f.write(firstline)
        for i, j in zip(longest, shortest):
            f.write(f"{i}, {j}\n")
        f.close()
        axisDataFile = f"{dirpath}/axisData.txt"
        f = open(axisDataFile, "a+")
        firstline = ""
        for i in axes:
            firstline += i + ", "
        firstline += varAxis + "\n"
        secondline = ""
        for i in vals:
            if i != None:
                secondline += str(i) + ", "
        secondline = secondline[:-2]
        f.write(firstline)
        f.write(secondline)
        f.close()
    

