from msilib.schema import Error
import h5py
from matplotlib.pyplot import axes
from Context import CoordinateContext
import numpy as np
import enum
import os
import datetime
import collections.abc


def makeDir(filepath:'str',count:'int') -> 'str':
    '''
    Creates a directory and if the directory already exists, it creates a directory with t numbered name to avoid conflict
    #Input args : path to directory, current count
    #output : new path to file 

    '''
    try:                                  # if there is no naming conflict already
        filePath = f"{filepath}-{count}"
        os.mkdir(filePath)
    except:                               # if threre is a naming conflict then increase the count
        filePath = makeDir(filepath,count+1)
    return filePath



class Accumulator(enum.Enum):
    Raw = 0
    Integrate = 1
    Max = 2
    Sum = 3


class DataHandler:
    
    def __init__(self, data_path=None):
        if data_path == None:
            preFix = ""
        else:
            preFix = data_path + "/"
        self.file = h5py.File("LockinData.hdf5", "r") #Default file name for the hdf5 file is always OscData
        self.context = CoordinateContext(preFix) #creates the context for the file
        self.histExist = False
        self.type = 1
        self.acc = Accumulator.Raw 

    def getOnlyAmp(self):
        return False

    def getKeys(self):
        return list(self.file.keys())
    
    def getValidVals(self, axes, axdict, hist=False):
        validVals = {}
        file = self.file
        for i in range(len(axes)):
            validVals[axes[i]] = []
            indx = axdict[axes[i]]
            for j in list(file.keys()):
                if j != "axes" and j != "config":
                    key = j.replace("[", "").replace("]", "").replace(" ", "").split(",")
                    key = [float(j) for j in key]
                    if key[indx] not in validVals[axes[i]]:
                        validVals[axes[i]].append(key[indx])
        return validVals
    
    def getallData(self):
        
        data = []
        for coord in self.context.allCoords:
            data.append(self.getDataPoint(coord))
        return data

    def get_all_raw_data(self):
        '''
        Gets all the data without accumulator
        only needed for oscilloscope class
        '''
        keys = self.file.keys()
        data = []
        for key in  keys:
            data.append(self.file.get(key))
        return data
    
    def getDataPoint(self,coords):

        if type(coords) == list:
            data =  np.array(self.file.get(str(coords)))
            
            return data
        else:
            raise(f"The type of coords must be list , which is not the case with {coords}")


    def checkInput(self,axes_names,coords,dimensions):
        if len(axes_names) != len(coords):
            raise ValueError("Number of axis names and the coords do not match")
        diff = len(self.context.axes) - len(axes_names)
        if diff == dimensions:
           return True
        return False


    def getPlaceHolder(self,axesName,axesValues):
        '''
        returns a placeholder coordinate with a value coordinate list with the missing 
        coordinate replace with a None and all others having fixed value.
        '''
        placeHolder = [None]*len(self.context.startCoords)
        for i in range(len(axesName)):
            index = self.context.getAxisIndex(axesName[i])
            placeHolder[index] = axesValues[i]
        return placeHolder
    

    def findMissing(self,placeHolder):
        '''
        returns the missing coordinates in the placeholder.
        '''
        missing = []
        for i in range(len(placeHolder)):
            if placeHolder[i] == None:
                missing.append(i)
        return missing
    
    def isBetween(self, val, a, b):
        if a > b:
            return val >= b and val <= a
        else:
            return val <= b and val >= a


    def get1DSlice(self, axesName, axesValues, hist, rStart=None, rStop=None, rIndex=None):
        ''''

            returns a one dimensional slice of data,
            the data is compressed using an accumulator,
            the inputs are the names and values of the fixed axes
            the output includes the data,
            the coordinates, the name of the missing axes and 
            name of the plotted axes
        
        '''
        if self.checkInput(axesName,axesValues,1): 
            placeHolder = self.getPlaceHolder(axesName,axesValues)
            missing = self.findMissing(placeHolder)
            genCoords = self.context.generateCoords(missing[0])
            coords = self.context.fitPoints(placeHolder,missing[0],genCoords) 
            data = []
            for coord in coords:
                data.append(self.getDataPoint(coord))
            return data,coords,[self.context.axes[missing[0]]]
    

    def get2DSlice(self,axesName,axesValues,hist):
        '''
        same get 1-D slice but with two dimesnions
        
        '''
        if self.checkInput(axesName,axesValues,2): 
            placeHolder = self.getPlaceHolder(axesName,axesValues)
            missing = self.findMissing(placeHolder)
            data   = []
            genCoordsa = self.context.generateCoords(missing[0])
            genCoordsb = self.context.generateCoords(missing[1])
            coordsa = self.context.fitPoints(placeHolder,missing[0],genCoordsa)
            coordsb = []
            coords = []

            for coord in coordsa:
                fitpt = self.context.fitPoints(coord,missing[1],genCoordsb)
                coordsb.append(fitpt)
                coords+=fitpt

            for subcoord in coordsb:
                data1= []
                for pt in subcoord:
                    data1.append(self.getDataPoint(pt))
                data.append(data1)
                
            return data,coordsb,[self.context.axes[missing[0]],self.context.axes[missing[1]]]


    def getCompletePoint(self,coords): 
        if type(coords) == list:
            data =  np.array(self.file.get(str(coords)))
            return data
        else:
            raise(f"The type of coords must be list , which is not the case with {coords}")
    


    def save1D_Bulk(self,axesName,axesValues,start,stop,hist,digits):
        '''
            Input Arguments:
            Variable axis: the axis which is varying 
            The values of fixed coordinates
            The range of values of the varibale axis
            Output Arguments:
            None
        '''
        placeHolder = self.getPlaceHolder(axesName,axesValues)
        missing = self.findMissing(placeHolder)
        genCoords = self.context.generateCustomCoords(missing[0],start,stop)
        coords = self.context.fitPoints(placeHolder,missing[0],genCoords) #all coords that need to be plotted.
        
        directoryPath = os.path.join(os.path.dirname(os.path.realpath(__file__)),os.path.join('EXPORT-Axis'+str(self.context.axes[missing[0]])+'-'+str(datetime.datetime.now().date())+'From-'+str(start)+'To-'+str(stop)))
        directoryPath = makeDir(directoryPath,0)
        mainFileName = f"datapoint-{start}-{stop}.txt".replace("[", "").replace("]", "")
        mainFile = os.path.join(directoryPath,mainFileName)
        a = open(mainFile,"a+")
        for coord in coords:
            data = self.getCompletePoint(coord)
            value = f"{coord};{data} \n"
            value = value.replace("[", "").replace("]", "")
            a.write(value)
        a.close()

        

    def save1Ddata(self,axesNames, axes_original,axesValues,hist,filePath=None):
        name = f"1DData-{axesNames}-{axesValues}.txt".replace("[", "").replace("]", "")
        a = open(name,"a+")
        if filePath!=None:
            mainFileName = f"1DData-{axesNames}-{str(axesValues)}-{datetime.datetime.now()}.txt".replace("[", "").replace("]", "")
            mainFile = os.path.join(filePath,mainFileName)
        else:
            mainFile = f"1DData1-{axesNames}-{axesValues}-.txt".replace("[", "").replace("]", "")
        #a = open(mainFile,"a+")
        data = self.get1DSlice(axesNames,axesValues, hist)
        nonConst = -1
        for i in range(len(axesNames)):
            if axes_original[i] != axesNames[i]:
                nonConst = i
                break
        for i in range(len(data[0])):
            value = str(data[1][i][nonConst]) + ";" + str(data[0][i][0]) + "\n"
            value = value.replace("[", "").replace("]", "")
            a.write(value)
        a.close()

        
    def save2Ddata(self,axesNames,axesValues,hist):
        name = f"2DData-Lockin-{axesNames}-{axesValues}.txt".replace("[", "").replace("]", "")
        a = open(name,"a+")
        data = self.get2DSlice(axesNames,axesValues,hist)
        for data1 in data[0]:
            temp = ""
            for j in data1:
                if (isinstance(j, collections.abc.Sequence)) or type(j) is np.ndarray:
                    temp+=str(j[0])+","
                else:
                    temp+=str(j)+","
            temp= temp[:-1]+"\n"   
            a.write(temp)
        a.close()


    def saveSingleData(self,coord,hist,digits,filePath=None):
        data = self.getCompletePoint(coord)
        coord = [str(i) for i in coord]
        for i in range(len(coord)):
            dot = coord[i].find(".")+1
            while len(coord[i][dot:]) < digits:
                coord[i] = coord[i] + "0"
        mainFileName = f"datapoint-{coord}.txt".replace("[", "").replace("]", "").replace("'", "")
        if filePath!=None:
            mainFile = os.path.join(filePath,mainFileName)
            a = open(mainFile,"a+")
        else:
            a = open(mainFileName,"a+")
        #for i in range(int(len(data[0]))):
        value = str(data) + "\n"
        value = value.replace("[", "").replace("]", "")
        a.write(value)

        a.close()
    
    def setAccumulator(self,num):
        if num == 0:
            self.acc = Accumulator.Raw
        if num == 1:
            self.acc = Accumulator.Integrate
        if num == 2:
            self.acc = Accumulator.Max
        if num == 3:
            self.acc = Accumulator.Sum   
            


if __name__ == "__main__":
    dh = DataHandler()
    #print(dh.getallData()) 
    #print(dh.context.axes)
    #print(dh.get1DSlice(['X'],[2.0]))
    #dh.get2DSlice([],[])

    value = dh.get_all_raw_data()
    #print(value)
    value = dh.getDataPoint([1000.0,0.3])
    value = dh.get1DSlice(["S0"],[1000.0],False)
    value = dh.get2DSlice([],[],False)
    print(value)
    #print(dh.get2DSlice(['X','Y','R1'],[16.0,2.0,50.0]))
    #print(dh.get1DSlice(['X','Y','R1','R2'],[16.0,2.0,50.0,50.0]))
    #print(dh.get_all_raw_data())
    dh.save1D_Bulk(['S0'],[750.0],0.0,0.5)
    dh.save1Ddata(['S0'],[750.0])
    dh.save2Ddata([],[])
    #print(dh.getCompletePoint([7.0,20.0]))
    #dh.saveSingleData([16.0,2.0,10.0,50.0,50.0])


