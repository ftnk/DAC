import tkinter as tk
from tkinter import ttk
import numpy as np
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from mpl_interactions import panhandler, zoom_factory
from matplotlib import patches
import os

class SDP(tk.Frame):
    def __init__(self, dataHandler, coord, dSource):
        #super().__init__()
        self.root = tk.Tk()
        self.root.title("Single Data Point")
        self.dh = dataHandler
        self.voltage = 0
        self.xEnergy = False
        self.coord = coord
        self.dSource = dSource
        self.hist = dSource == "hist"
        self.data = None
        self.x_min = None
        self.x_max = None
        self.ax = None
        self.x_values = None
        self.y_values = None
        self.noUpdate = False
        self.abs_x_Min = None
        self.abs_x_Max = None
        self.abs_y_Min = None
        self.abs_y_Max = None
        self.zoom_corners = [[None, None], [None, None]]
        self.zoom_rect = None
        self.x_padding = [None, None]
        self.y_padding = [None, None]
        self.log_x = False
        self.log_y = False
        self.use_pad = int(self.hist)
        self.get_data()
        self.create_widgets()
    
    def get_data(self):
        coord = self.coord
        data = self.dh.getCompletePoint(coord, self.dSource)
        self.data = data
        self.x_min = min(data[0])
        self.x_max = max(data[0])
        self.x_values = data[0]
        
        self.y_values = data[1]
    
    def get_padding(self):
        x_pad = self.ax.get_xlim()
        y_pad = self.ax.get_ylim()
        self.x_padding = [x_pad[0] - min(self.x_values), x_pad[1] - max(self.x_values)]
        self.y_padding = [y_pad[0] - min(self.y_values), y_pad[1] - max(self.y_values)]

    def setLimits(self, energy=False):
        y_limits = self.ax.get_ylim()
        if energy:
            x_limits = [np.min(self.x_values), np.max(self.x_values)]
        else:
            x_limits = self.ax.get_xlim()
        self.abs_x_Min = x_limits[0]
        self.abs_x_Max = x_limits[1]
        self.abs_y_Min = y_limits[0]
        self.abs_y_Max = y_limits[1]
        self.noUpdate = True
        self.ymin_sv.set(np.min(self.y_values))
        self.ymax_sv.set(np.max(self.y_values))
        self.x_min = np.min(self.x_values)
        self.x_max = np.max(self.x_values)
        if energy:
            self.x_padding = [-5, 5]
            self.xmin_sv.set(str(x_limits[0]))
            self.xmax_sv.set(str(x_limits[1]))
            self.noUpdate = False
            self.limit_change(xmin=np.min(self.x_values), xmax=np.max(self.x_values))
            self.noUpdate = True
        else:
            self.xmin_sv.set(self.x_min)
            self.xmax_sv.set(self.x_max)
        
        
        self.noUpdate = False
    
    def plotPoint(self):
        self.create_widgets()
        if hasattr(self, 'canvas'):
            self.canvas.get_tk_widget().destroy()
        coord = self.coord
        hist = self.hist
        self.x_values = self.data[0]
        if hist: title = f"Histogram data at point {coord}"
        else: title = f"Oscilloscope trace at point {coord}"
        #fig = Figure(num=title)
        fig = Figure()
        ax = fig.add_subplot(111)
        self.ax = ax
        ax.set_title(title)
        if self.dh.type == 0:
            ax.set_xlabel("Time [s]")
            ax.set_title(title)
            if hist:
                ax.set_ylabel(f"Counts")
                ax.stem(self.data[0], self.data[1])
            else:
                ax.set_ylabel(f"Volts [v]")
                ax.plot(self.data[0], self.data[1])
            ax.set_xscale('linear')
        
        if self.x_padding[0] == None:
            self.get_padding()
        
        self.setLimits()

        disconnect_zoom = zoom_factory(ax)
        pan_handler = panhandler(fig)

        self.canvas = FigureCanvasTkAgg(fig, master=self.root)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(fill="both", expand=True)

        self.canvas.mpl_connect('button_press_event', self.on_click)
    
    def on_click(self, event):
        if event.button == 3:
            self.zoom(event.xdata, event.ydata)
    
    def zoom(self, x, y):
        if self.zoom_corners[0][0] == None:
            self.zoom_corners[0][0] = x
            self.zoom_corners[0][1] = y
        elif self.zoom_corners[1][0] == None:
            self.zoom_corners[1][0] = x
            self.zoom_corners[1][1] = y
            bottom_left_corner = (min(self.zoom_corners[0][0], self.zoom_corners[1][0]), min(self.zoom_corners[0][1], self.zoom_corners[1][1]))
            width = max(self.zoom_corners[0][0], self.zoom_corners[1][0]) - min(self.zoom_corners[0][0], self.zoom_corners[1][0])
            height = max(self.zoom_corners[0][1], self.zoom_corners[1][1]) - min(self.zoom_corners[0][1], self.zoom_corners[1][1])
            self.zoom_rect = patches.Rectangle(bottom_left_corner, width, height, color='yellow', alpha=0.5)
            self.ax.add_patch(self.zoom_rect)
            self.canvas.draw()
        else:
            self.zoom_rect.remove()
            if x >= min(self.zoom_corners[0][0], self.zoom_corners[1][0]) and x <= max(self.zoom_corners[0][0], self.zoom_corners[1][0]) and y >= min(self.zoom_corners[0][1], self.zoom_corners[1][1]) and y <= max(self.zoom_corners[0][1], self.zoom_corners[1][1]):
                self.ax.set_xlim(min(self.zoom_corners[0][0], self.zoom_corners[1][0]), max(self.zoom_corners[0][0], self.zoom_corners[1][0]))
                self.ax.set_ylim(min(self.zoom_corners[0][1], self.zoom_corners[1][1]), max(self.zoom_corners[0][1], self.zoom_corners[1][1]))
                self.noUpdate = True
                self.xmin_sv.set(min(self.zoom_corners[0][0], self.zoom_corners[1][0]))
                self.xmax_sv.set(max(self.zoom_corners[0][0], self.zoom_corners[1][0]))
                self.ymin_sv.set(min(self.zoom_corners[0][1], self.zoom_corners[1][1]))
                self.ymax_sv.set(max(self.zoom_corners[0][1], self.zoom_corners[1][1]))
                self.noUpdate = False
            self.zoom_corners = [[None, None], [None, None]]
            self.canvas.draw()

    def trimData(self, data, l):
        currLen = len(data)
        rem = []
        for i in range(l, currLen):
            rem.append(i)
        data = np.delete(data, rem, axis=0)
        return data
    
    def plotPointEnergyConv(self):
        self.create_widgets()
        if hasattr(self, 'canvas'):
            self.canvas.get_tk_widget().destroy()
        coord = self.coord
        hist = self.hist
        voltage = self.voltage
        newData0, exclude_indexes = self.dh.energyCalibration(self.data[0], voltage)
        newData0 = np.array(newData0)
        self.x_values = newData0
        newData1 = [i for i in self.data[1][1:]]
        newData1 = self.dh.removeIndexesFromData(newData1, exclude_indexes, 1)
        newData1 = np.flip(newData1)
        if len(newData1) != len(newData0):
            newData1 = self.trimData(newData1, len(newData0))
        self.y_values = newData1
        if hist: title = f"Histogram data at point {coord}"
        else: title = f"Oscilloscope trace at point {coord}"
        fig = Figure()
        ax = fig.add_subplot(111)
        self.ax = ax
        if self.dh.type == 0:
            ax.set_xlabel("Electron-volts (eV)")
            ax.set_title(title)
            if hist:
                ax.set_ylabel(f"Counts")
                ax.stem(newData0, newData1)
                ax.set_xscale('log')
            else:
                ax.set_ylabel(f"Volts [v]")
                ax.plot(newData0,newData1)
        
        if self.x_padding[0] == None:
            self.get_padding()
        
        self.setLimits(energy=True)

        disconnect_zoom = zoom_factory(ax)
        pan_handler = panhandler(fig)
        
        self.canvas = FigureCanvasTkAgg(fig, master=self.root)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(fill="both", expand=True)

        self.canvas.mpl_connect('button_press_event', self.on_click)
    
    def toggleX(self):
        self.xEnergy = not self.xEnergy
        self.noUpdate = True
        self.x_padding = [None, None]
        self.y_padding = [None, None]
        if self.xEnergy:
            self.plotPointEnergyConv()
        else:
            self.plotPoint()
        self.canvas.draw()

        self.noUpdate = False

    def changeV(self, c):
        self.voltage = int(c)
        if self.xEnergy:
            self.noUpdate = True
            self.plotPointEnergyConv()
            self.noUpdate = False
    
    def export(self):
        digits = self.dh.getDecimals()
        if self.xEnergy:
            self.dh.saveSingleDataEnergyConv(self.coord, self.dSource, digits, self.voltage)
        else:
            self.dh.saveSingleData(self.coord, self.dSource, digits)
    
    def isFloat(self, f):
        try:
            float(f)
            return True
        except:
            return False
    
    def limit_change(self, var=None, index=None, mode=None, xmin=None, xmax=None):
        if self.noUpdate: return
        if xmin == None:
            new_x_min = self.xmin_sv.get()
        else:
            new_x_min = xmin
        if xmax == None:
            new_x_max = self.xmax_sv.get()
        else:
            new_x_max = xmax
        if self.isFloat(new_x_min) and self.isFloat(new_x_max):
            #self.x_padding = [-(abs(float(new_x_max) - float(new_x_min)) / 20), (abs(float(new_x_max) - float(new_x_min)) / 20)]
            #self.ax.set_xlim(float(new_x_min) + (self.x_padding[0] * self.use_pad), float(new_x_max) + (self.x_padding[1] * self.use_pad))
            self.ax.set_xlim(float(new_x_min), float(new_x_max))
            self.canvas.draw()
        
    def y_limit_change(self, var=None, index=None, mode=None):
        if self.noUpdate: return
        new_y_min = float(self.ymin_sv.get())
        new_y_max = float(self.ymax_sv.get())
        #self.y_padding = [-(abs(new_y_max - new_y_min) / 20), (abs(new_y_max - new_y_min) / 20)]
        #self.ax.set_ylim(new_y_min + (self.y_padding[0] * self.use_pad), new_y_max + (self.y_padding[1] * self.use_pad))
        self.ax.set_ylim(new_y_min, new_y_max)
        self.canvas.draw()
            
    def reset_plot(self):
        if self.xEnergy:
            self.x_min = np.min(self.x_values)
            self.x_max = np.max(self.x_values)
            self.x_padding = [-5, 5]
            self.ax.set_xlim(self.x_min, self.x_max)
        else:
            self.x_min = self.abs_x_Min
            self.x_max = self.abs_x_Max
            self.x_padding = [-(abs(np.max(self.x_values) - np.min(self.x_values)) / 20), (abs(np.max(self.x_values) - np.min(self.x_values)) / 20)]
            self.ax.set_xlim(np.min(self.x_values) + (self.x_padding[0]), np.max(self.x_values) + (self.x_padding[1]))
        
        self.y_padding = [-(abs(np.max(self.y_values) - np.min(self.y_values)) / 20), (abs(np.max(self.y_values) - np.min(self.y_values)) / 20)]
        self.ax.set_ylim(np.min(self.y_values) + (self.y_padding[0]), np.max(self.y_values) + (self.y_padding[1]))
        
        self.noUpdate = True
        if self.hist:
            self.pad_btn.select()
        self.xmin_sv.set(self.x_min)
        self.xmax_sv.set(self.x_max)
        self.ymin_sv.set(self.abs_y_Min)
        self.ymax_sv.set(self.abs_y_Max)
        self.noUpdate = False
        self.canvas.draw()
    
    def padding_change(self, var=None, index=None, mode=None):
        self.use_pad = int(self.pad_var.get())
        if self.ax != None and not self.noUpdate:
            self.y_limit_change()
            self.limit_change()

    def runMainLoop(self):
        self.root.mainloop()
    
    def destroy_all_widgets(self):
        for widget in self.root.winfo_children():
            widget.destroy()
    
    def get_filename(self, fileName):
        count = 1
        while os.path.exists(fileName):
            dotIndx = str(fileName).rfind(".")
            fileName = fileName[:dotIndx] + "_" + str(count) + fileName[dotIndx:]
            count += 1
        return fileName
    
    def save_as_png(self, current=True):
        if not os.path.isdir("./Saved_Figures"):
            os.mkdir("./Saved_Figures")
        coord = [str(i) for i in self.coord]
        if self.xEnergy:
            fileName = f"datapoint-{coord}-{self.voltage}V.png".replace("[", "").replace("]", "").replace("'", "")
        else:
            fileName = f"datapoint-{coord}.png".replace("[", "").replace("]", "").replace("'", "")
        if self.hist:
            fileName = "hist-" + fileName
        
        fileName = os.path.join("./Saved_Figures", fileName)
        fileName = self.get_filename(fileName)
        if current:
            self.canvas.print_png(fileName)
        else:
            x_min = np.min(self.x_values)
            x_max = np.max(self.x_values)
            y_min = np.min(self.y_values)
            y_max = np.max(self.y_values)
            x_pad = abs(x_max - x_min) / 20
            y_pad = abs(y_max - y_min) / 20
            old_lim = [self.ax.get_xlim(), self.ax.get_ylim()]
            self.ax.set_xlim(x_min - x_pad, x_max + x_pad)
            self.ax.set_ylim(y_min - y_pad, y_max + y_pad)
            self.canvas.print_png(fileName)
            self.ax.set_xlim(old_lim[0])
            self.ax.set_ylim(old_lim[1])
            self.canvas.draw()
    
    def toggleY_linlog(self):
        if self.log_y:
            self.ax.set_yscale('linear')
        else:
            self.ax.set_yscale('log')
        self.log_y = not self.log_y
        self.canvas.draw()
    
    def toggleX_linlog(self):
        if self.log_x:
            self.ax.set_xscale('linear')
        else:
            self.ax.set_xscale('log')
        self.log_x = not self.log_x
        self.canvas.draw()
    
    def close(self):
        exit(0)
    
    def create_widgets(self):
        self.destroy_all_widgets()

        self.menu = tk.Menu(self.root)
        self.root.config(menu=self.menu)

        self.fileMenu = tk.Menu(self.menu, tearoff=0)

        self.saveMenu = tk.Menu(self.fileMenu, tearoff=0)
        self.saveMenu.add_command(label="Current Plot", command=lambda x=True: self.save_as_png(x))
        self.saveMenu.add_command(label="Whole Plot", command=lambda x=False: self.save_as_png(x))
        
        self.fileMenu.add_cascade(label="Save", menu=self.saveMenu)
        self.fileMenu.add_separator()
        self.fileMenu.add_command(label="Exit", command=self.close)

        self.menu.add_cascade(label="File", menu=self.fileMenu)

        self.buttons_frame = tk.Frame(self.root)
        self.buttons_frame.pack(side=tk.TOP)

        self.reset_button = tk.Button(self.buttons_frame, text="Reset", command=self.reset_plot)
        self.reset_button.pack(side=tk.LEFT)

        self.spacer_label0 = tk.Label(self.buttons_frame, text="        ")
        self.spacer_label0.pack(side=tk.LEFT)

        self.toggle_axis_btn = tk.Button(self.buttons_frame, text="Toggle X-axis (Energy/Time)", command=self.toggleX)
        self.toggle_axis_btn.pack(side=tk.LEFT)

        self.toggle_axis_linlog_btn = tk.Button(self.buttons_frame, text="Toggle X-axis (Lin/Log)", command=self.toggleX_linlog)
        self.toggle_axis_linlog_btn.pack(side=tk.LEFT)

        self.toggle_linlog_axis_btn = tk.Button(self.buttons_frame, text="Toggle Y-axis (Lin/Log)", command=self.toggleY_linlog)
        self.toggle_linlog_axis_btn.pack(side=tk.LEFT)

        self.voltage_label = tk.Label(self.buttons_frame, text="Voltage:")
        self.voltage_label.pack(side=tk.LEFT)

        self.volt_selection = tk.StringVar(self.buttons_frame)
        self.volt_selection.set(str(self.voltage))

        options = ["0", "50", "100", "200", "300", "500", "700", "1000", "1500", "2000"]

        self.volt_dropdown = tk.OptionMenu(self.buttons_frame, self.volt_selection, *options, command=self.changeV)
        self.volt_dropdown.pack(side=tk.LEFT)

        self.spacer_label = tk.Label(self.buttons_frame, text="        ")
        self.spacer_label.pack(side=tk.LEFT)
        
        self.export_btn = tk.Button(self.buttons_frame, text="Export", command=self.export)
        self.export_btn.pack(side=tk.RIGHT)

        self.config_x_frame = tk.Frame(self.root)
        self.config_x_frame.pack(side=tk.BOTTOM, pady=10)

        self.xmin_label = tk.Label(self.config_x_frame, text="Min x:")
        self.xmin_label.pack(side=tk.LEFT)

        self.xmin_sv = tk.StringVar()
        self.xmin_sv.trace_add("write", callback=self.limit_change)

        self.xmin_entry = tk.Entry(self.config_x_frame, textvariable=self.xmin_sv)
        self.xmin_entry.pack(side=tk.LEFT)

        self.spacer_label2 = tk.Label(self.config_x_frame, text="        ")
        self.spacer_label2.pack(side=tk.LEFT)

        self.xmax_label = tk.Label(self.config_x_frame, text="Max x:")
        self.xmax_label.pack(side=tk.LEFT)

        self.xmax_sv = tk.StringVar()
        self.xmax_sv.trace_add("write", callback=self.limit_change)

        self.xmax_entry = tk.Entry(self.config_x_frame, textvariable=self.xmax_sv)
        self.xmax_entry.pack(side=tk.LEFT)

        self.config_y_frame = tk.Frame(self.root)
        self.config_y_frame.pack(side=tk.BOTTOM, pady=10)

        y_floats = list(dict.fromkeys([float(y) for y in self.y_values]))
        y_floats.sort()

        self.ymin_label = tk.Label(self.config_y_frame, text="Min y:")
        self.ymin_label.pack(side=tk.LEFT)

        self.ymin_sv = tk.StringVar()
        self.ymin_sv.trace_add("write", callback=self.y_limit_change)

        self.ymin_entry = tk.Entry(self.config_y_frame, textvariable=self.ymin_sv)
        self.ymin_entry.pack(side=tk.LEFT)

        self.spacer_label3 = tk.Label(self.config_y_frame, text="        ")
        self.spacer_label3.pack(side=tk.LEFT)

        self.ymax_label = tk.Label(self.config_y_frame, text="Max y:")
        self.ymax_label.pack(side=tk.LEFT)
        
        self.ymax_sv = tk.StringVar()
        self.ymax_sv.trace_add("write", callback=self.y_limit_change)

        self.ymax_entry = tk.Entry(self.config_y_frame, textvariable=self.ymax_sv)
        self.ymax_entry.pack(side=tk.LEFT)

        self.pad_var = tk.IntVar()
        self.pad_var.trace_add("write", callback=self.padding_change)

        self.pad_btn = tk.Checkbutton(self.root, width=10, variable=self.pad_var, text="Axes padding")
        self.pad_btn.select()
        self.pad_btn.pack(side=tk.BOTTOM)


if __name__ == "__main__":
    from DataHandler import DataHandler
    dh = DataHandler()
    s_d_p = SDP(dh, [0.1, 1.0, 1.0], True)
    s_d_p.plotPoint()
    s_d_p.runMainLoop()
