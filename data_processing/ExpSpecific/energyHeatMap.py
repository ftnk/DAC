import tkinter as tk
from tkinter import ttk
import numpy as np
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.colors import LogNorm
import matplotlib.ticker as ticker
from mpl_interactions import panhandler, zoom_factory
from matplotlib import patches
import os


class EnergyHeatMap(tk.Frame):
    def __init__(self, varAxis, axes, axdict, vals, digits, SSS, rangeMap, dataHandler, pointPlotFunc, root=None):
        #for i, j in zip([varAxis, axes, axdict, vals, digits, SSS, rangeMap], 
        #                ["varAxis", "axes", "axdict", "vals", "digits", "SSS", "rangeMap"]):
        #    print(j + ":\n" + str(type(i)) + "\n" + str(i) + "\n")
        if root is None:
            self.root = tk.Tk()
            self.root.title("Energy Heatmap")
        else:
            self.root = root
        self.initParams = [varAxis, np.array(axes).copy().tolist(), dict(axdict), np.array(vals).copy().tolist(), digits, np.array(SSS).copy().tolist(), dict(rangeMap), dataHandler, pointPlotFunc]
        self.voltage = 0
        self.xEnergy = False
        self.varAxis = varAxis
        self.axes = axes
        self.axDict = axdict
        self.vals = vals
        self.digits = digits
        self.SSS = SSS
        self.rangeMap = rangeMap
        self.dh = dataHandler
        self.plotPointFunc = pointPlotFunc
        self.axisData = self.load_axis_data()
        self.data = None
        self.axVals = None
        self.log_color_scale_var = False
        self.ax = None
        self.x_values = None
        self.y_values = None
        self.heatmap = None
        self.get_data()
        self.x_min = None
        self.x_max = None
        self.start_limits = [None, None]
        self.start_y_limits = [None, None]
        self.zoom_corners = [[None, None], [None, None]]
        self.x_padding = [None, None]
        self.y_padding = [None, None]
        self.zoom_rect = None
        self.use_pad = 1
        if root is not None:
            allW = self.all_children(self.root)
            for item in allW:
                item.pack_forget()
        self.create_widgets()
        self.noUpdate = False
        self.first = True
        self.log_x = False
    
    def get_data(self):
        allData = self.dh.getEnergyHeatMap(self.varAxis, self.axes, self.axDict, self.vals, self.SSS, self.voltage, self.digits, self.rangeMap)
        self.x_values = allData[0]
        self.y_values = allData[1]
        self.heatmap = np.array(allData[2])
        currLen = self.heatmap.shape[1]
        wantedLen = len(self.x_values)
        rem = []
        for i in range(wantedLen, currLen):
            rem.append(i)
        self.heatmap = np.delete(self.heatmap, rem, axis=1)
    
    def get_padding(self):
        x_pad = self.ax.get_xlim()
        y_pad = self.ax.get_ylim()
        self.x_padding = [x_pad[0] - min(self.x_values), x_pad[1] - max(self.x_values)]
        self.y_padding = [y_pad[0] - min(self.y_values), y_pad[1] - max(self.y_values)]
        
    
    def normalize(self, arr, t_min, t_max):
        norm_arr = []
        diff = t_max - t_min
        diff_arr = max(arr) - min(arr)
        for i in arr:
            temp = (((i - min(arr))*diff)/diff_arr) + t_min
            norm_arr.append(temp)
        return norm_arr
    
    def getDecimals(self, stepArr):
        maxDigits = 1
        for i in stepArr:
            dotIndx = str(i).find(".")
            zeroIndx = str(i).find("000")-1
            if zeroIndx == -1:
                continue
            if zeroIndx - dotIndx > maxDigits:
                maxDigits = zeroIndx - dotIndx
        return maxDigits
    
    def load_axis_data(self):
        axisData = {}
        usedVals = 0
        for key in self.axDict:
            if key != self.varAxis:
                axisData[key] = self.vals[usedVals]
                usedVals += 1
            else:
                axisData[key] = []
        
        varCurr = self.SSS[0][self.axDict[self.varAxis]]
        varStop = self.SSS[1][self.axDict[self.varAxis]]
        varStep = self.SSS[2][self.axDict[self.varAxis]]

        while varCurr <= varStop:
            axisData[self.varAxis].append(varCurr)
            varCurr = round(varCurr + varStep, self.digits)
        
        axisData[self.varAxis] = axisData[self.varAxis]
        return axisData
    
    def getTicks(self, data, tickamount, decimals=-1):
        ticks = []
        tickLocations = []
        delta = round(len(data) / (tickamount-1))
        for i in range(0, delta*(tickamount-1), delta):
            if decimals == -1:
                ticks.append(data[i])
            else:
                ticks.append(round(data[i], decimals))
            tickLocations.append(i)
        if decimals == -1:
            ticks.append(data[-1])
        else:
            ticks.append(round(data[-1], decimals))
        tickLocations.append(len(data) - 1)
        newTicks = ['{:0.3e}'.format(i) for i in ticks]
        return newTicks, tickLocations
        
    def plot_pcolormap(self):
        if hasattr(self, 'canvas'):
            self.canvas.get_tk_widget().destroy()
        
        axVals = {}
        usedAxes = 0
        for i in range(len(self.axes)):
            axVals[self.axes[i]] = self.vals[usedAxes]
            usedAxes += 1

        x_values = self.x_values
        y_values = self.y_values
        heatmap = self.heatmap
        varAxis = self.varAxis

        title = f"EnergyHeatmap for {varAxis}\n"

        for key in axVals:
            title += f"{key} = {axVals[key]} &"
        title = title[:-2]

        fig = Figure(figsize=(6, 5), dpi=100)
        ax = fig.add_subplot()
        ax.set_title(title)

        if self.log_color_scale_var:
            norm = LogNorm()
        else:
            norm = None
        
        cax = ax.pcolormesh(x_values, y_values, heatmap, shading='auto', cmap='viridis', norm=norm)

        self.colorbar = fig.colorbar(cax)
        self.colorbar.set_label("Counts")
        ax.tick_params(axis='x', which='major', pad=3.5, labelrotation=-75)

        self.ax = ax

        ax.yaxis.set_major_locator(ticker.MaxNLocator(len(y_values)))
        ax.set_xlabel("eV")
        ax.set_ylabel(varAxis)
        fig.tight_layout()

        if self.x_padding[0] == None:
            self.get_padding()

        disconnect_zoom = zoom_factory(ax)
        pan_handler = panhandler(fig)

        self.canvas = FigureCanvasTkAgg(fig, master=self.root)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(fill="both", expand=True)

        self.canvas.mpl_connect('button_press_event', self.on_click)

        if self.first:
            self.start_limits[0] = self.ax.get_xlim()[0]
            self.start_limits[1] = self.ax.get_xlim()[1]
            self.start_y_limits[0] = self.ax.get_ylim()[0]
            self.start_y_limits[1] = self.ax.get_ylim()[1]
            self.first = False

        self.noUpdate = True
        self.xmin_sv.set(np.min(x_values))
        self.xmax_sv.set(np.max(x_values))
        self.noUpdate = False
    
    def all_children (self, root) :
        _list = root.winfo_children()

        for item in _list :
            if item.winfo_children() :
                _list.extend(item.winfo_children())

        return _list
    
    def reset_plot(self):
        self.__init__(self.initParams[0], self.initParams[1], self.initParams[2], self.initParams[3], self.initParams[4], self.initParams[5], self.initParams[6], self.initParams[7], self.initParams[8], root=self.root)
        self.plot_pcolormap()
        self.runMainLoop()
        """
        self.ax.set_ylim(np.min(self.y_values) + self.y_padding[0], np.max(self.y_values) + self.y_padding[1])
        self.ax.set_xlim(np.min(self.x_values) + self.x_padding[0], np.max(self.x_values) + self.x_padding[1])
        self.noUpdate = True
        self.pad_btn.select()
        self.ymin_combo_box.set(float(np.min(self.y_values)))
        self.ymax_combo_box.set(float(np.max(self.y_values)))
        self.xmin_sv.set(self.start_limits[0])
        self.xmax_sv.set(self.start_limits[1])
        self.noUpdate = False
        self.canvas.draw()
        """

    def on_click(self, event):
        if event.inaxes and event.xdata >= 0:
            x = round(event.xdata)
            y = round(event.ydata * 2) / 2
            if event.dblclick and event.button == 1:
                self.plotPoint(x, y)
        if event.button == 3:
            self.zoom(event.xdata, event.ydata)

    def zoom(self, x, y):
        if self.zoom_corners[0][0] == None:
            self.zoom_corners[0][0] = x
            self.zoom_corners[0][1] = y
        elif self.zoom_corners[1][0] == None:
            self.zoom_corners[1][0] = x
            self.zoom_corners[1][1] = y
            bottom_left_corner = (min(self.zoom_corners[0][0], self.zoom_corners[1][0]), min(self.zoom_corners[0][1], self.zoom_corners[1][1]))
            width = max(self.zoom_corners[0][0], self.zoom_corners[1][0]) - min(self.zoom_corners[0][0], self.zoom_corners[1][0])
            height = max(self.zoom_corners[0][1], self.zoom_corners[1][1]) - min(self.zoom_corners[0][1], self.zoom_corners[1][1])
            self.zoom_rect = patches.Rectangle(bottom_left_corner, width, height, color='yellow', alpha=0.5)
            self.ax.add_patch(self.zoom_rect)
            self.canvas.draw()
        else:
            self.zoom_rect.remove()
            if x >= min(self.zoom_corners[0][0], self.zoom_corners[1][0]) and x <= max(self.zoom_corners[0][0], self.zoom_corners[1][0]) and y >= min(self.zoom_corners[0][1], self.zoom_corners[1][1]) and y <= max(self.zoom_corners[0][1], self.zoom_corners[1][1]):
                self.ax.set_xlim(min(self.zoom_corners[0][0], self.zoom_corners[1][0]), max(self.zoom_corners[0][0], self.zoom_corners[1][0]))
                self.ax.set_ylim(min(self.zoom_corners[0][1], self.zoom_corners[1][1]), max(self.zoom_corners[0][1], self.zoom_corners[1][1]))
                self.noUpdate = True
                self.xmin_sv.set(min(self.zoom_corners[0][0], self.zoom_corners[1][0]))
                self.xmax_sv.set(max(self.zoom_corners[0][0], self.zoom_corners[1][0]))
                self.ymin_sv.set(min(self.zoom_corners[0][1], self.zoom_corners[1][1]))
                self.ymax_sv.set(max(self.zoom_corners[0][1], self.zoom_corners[1][1]))
                self.noUpdate = False
            self.zoom_corners = [[None, None], [None, None]]
            self.canvas.draw()

    def plotPoint(self, x, y):
        histFile = self.dh.hFile
        if histFile == None:
            print("Unable to locate Histogram data file (HistData.hdf5)")
            return
        axes = [x.decode() for x in list(histFile.get(f"axes"))]
        varAxisIndex = axes.index(self.varAxis)
        finalKey = []
        allAxes = list(self.axisData.keys())
        for _ in range(len(allAxes)):
            finalKey.append(None)
        for i in range(len(axes)):
            if axes[i] != self.varAxis:
                finalKey[i] = self.axisData[axes[i]]
            else:
                finalKey[varAxisIndex] = y
        #print(f"Final key: {finalKey}")
        self.plotPointFunc(finalKey, True)
    
    def makeDir(self, filepath:'str',count:'int') -> 'str':
        '''
        Creates a directory and if the directory already exists, it creates a directory with t numbered name to avoid conflict
        #Input args : path to directory, current count
        #output : new path to file 

        '''
        while os.path.isdir(f"{filepath}-{count}"):
            count += 1
        try:                                  # if there is no naming conflict already
            filePath = f"{filepath}-{count}"
            os.mkdir(filePath)
        except:                               # if threre is a naming conflict then increase the count
            filePath = self.makeDir(filepath,count+1)
        return filePath
    
    def export(self):
        allData = self.dh.getEnergyHeatMap(self.varAxis, self.axes, self.axDict, self.vals, self.SSS, self.voltage, self.digits, self.rangeMap)
        energyvals = list(allData[0])
        valuesAnno = list(allData[1])
        formattedData = allData[2]
        dirname = f"EnergyHeatmap-{self.varAxis}"
        usedAxes = 0
        for i in range(len(self.vals)):
            if self.vals[i] != None:
                dirname += f"-{self.axes[usedAxes]}={self.vals[i]}"
                usedAxes += 1
        dirpath = self.makeDir(dirname, 0)
        name = f"{dirpath}\heatmapValues.txt"
        metaName = f"{dirpath}\metadata.txt"
        np.savetxt(name, formattedData, fmt="%d")
        f = open(metaName, "a+")
        if len(energyvals) > len(valuesAnno):
            longest = energyvals
            shortest = valuesAnno
            firstline = f"eV, {self.varAxis}\n"
        else:
            longest = valuesAnno
            shortest = energyvals
            firstline = f"{self.varAxis}, eV\n"
        while len(shortest) != len(longest):
            shortest.append(np.NaN)
        f.write(firstline)
        for i, j in zip(longest, shortest):
            f.write(f"{i}, {j}\n")
        f.close()
        axisDataFile = f"{dirpath}/axisData.txt"
        f = open(axisDataFile, "a+")
        firstline = ""
        for i in self.axes:
            firstline += i + ", "
        firstline += self.varAxis + "\n"
        secondline = ""
        for i in self.vals:
            if i != None:
                secondline += str(i) + ", "
        secondline = secondline[:-2]
        f.write(firstline)
        f.write(secondline)
        f.close()
        
    def runMainLoop(self):
        self.root.mainloop()

    def toggle_log_color_scale(self):
        self.log_color_scale_var = not self.log_color_scale_var
        self.plot_pcolormap()
    
    def changeV(self, c):
        self.voltage = int(c)
        self.plot_pcolormap()
    
    def limit_change(self, var=None, index=None, mode=None):
        if self.noUpdate: return
        new_min = self.xmin_entry.get()
        new_max = self.xmax_entry.get()
        if new_min.replace(".", "").isnumeric() and new_max.replace(".", "").isnumeric() and new_min.count(".") <= 1 and new_max.count(".") <= 1:
            new_min = float(new_min)
            new_max = float(new_max)
            if new_min > new_max:
                return
            self.x_min = new_min
            self.x_max = new_max
            self.ax.set_xlim(self.x_min + (self.x_padding[0] * self.use_pad), self.x_max + (self.x_padding[1] * self.use_pad))
            self.canvas.draw()
    
    def y_limit_change(self, var=None, index=None, mode=None):
        if self.noUpdate: return
        y_min = float(self.ymin_combo_box.get())
        y_max = float(self.ymax_combo_box.get())
        if y_min != y_max:
            self.ax.set_ylim(y_min + (self.y_padding[0] * self.use_pad), y_max + (self.y_padding[1] * self.use_pad))
            self.canvas.draw()
    
    def padding_change(self, var=None, index=None, mode=None):
        self.use_pad = int(self.pad_var.get())
        if self.ax != None and not self.noUpdate:
            self.y_limit_change()
            self.limit_change()
    
    def get_filename(self, fileName):
        count = 1
        while os.path.exists(fileName):
            dotIndx = str(fileName).rfind(".")
            fileName = fileName[:dotIndx] + "_" + str(count) + fileName[dotIndx:]
            count += 1
        return fileName
    
    def save_as_png(self, current=True):
        if not os.path.isdir("./Saved_Figures"):
            os.mkdir("./Saved_Figures")
        fileName = "EnergyHeatmap_" + str(self.varAxis) + "_"
        for i in self.axes:
            fileName += i + "=" + str(self.vals[self.axDict[i]]) + "_"
        fileName = fileName[:-1] + ".png"
        fileName = os.path.join("./Saved_Figures", fileName)
        fileName = self.get_filename(fileName)
        if current:
            self.canvas.print_png(fileName)
        else:
            old_lim = [self.ax.get_xlim(), self.ax.get_ylim()]
            self.ax.set_xlim(self.start_limits[0], self.start_limits[1])
            self.ax.set_ylim(self.start_y_limits[0], self.start_y_limits[1])
            self.canvas.print_png(fileName)
            self.ax.set_xlim(old_lim[0])
            self.ax.set_ylim(old_lim[1])
            self.canvas.draw()
    
    def toggleX_linlog(self):
        if self.log_x:
            self.ax.set_xscale('linear')
        else:
            self.ax.set_xscale('log')
        self.log_x = not self.log_x
        #self.flip_axis_padding()
        self.canvas.draw()
        
    def close(self):
        exit(0)

    def create_widgets(self):

        self.menu = tk.Menu(self.root)
        self.root.config(menu=self.menu)

        self.fileMenu = tk.Menu(self.menu, tearoff=0)

        self.saveMenu = tk.Menu(self.fileMenu, tearoff=0)
        self.saveMenu.add_command(label="Current Plot", command=lambda x=True: self.save_as_png(x))
        self.saveMenu.add_command(label="Whole Plot", command=lambda x=False: self.save_as_png(x))
        
        self.fileMenu.add_cascade(label="Save", menu=self.saveMenu)
        self.fileMenu.add_separator()
        self.fileMenu.add_command(label="Exit", command=self.close)

        self.menu.add_cascade(label="File", menu=self.fileMenu)

        self.buttons_frame = tk.Frame(self.root)
        self.buttons_frame.pack(side=tk.TOP)

        self.reset_button = tk.Button(self.buttons_frame, text="Reset", command=self.reset_plot)
        self.reset_button.pack(side=tk.LEFT)

        self.spacer_label0 = tk.Label(self.buttons_frame, text="        ")
        self.spacer_label0.pack(side=tk.LEFT)

        self.log_color_button = tk.Button(self.buttons_frame, text="Toggle Log Color Scale", command=self.toggle_log_color_scale)
        self.log_color_button.pack(side=tk.LEFT)

        self.toggle_axis_linlog_btn = tk.Button(self.buttons_frame, text="Toggle X-axis (Lin/Log)", command=self.toggleX_linlog)
        self.toggle_axis_linlog_btn.pack(side=tk.LEFT)

        self.voltage_label = tk.Label(self.buttons_frame, text="Voltage:")
        self.voltage_label.pack(side=tk.LEFT)

        self.volt_selection = tk.StringVar(self.buttons_frame)
        self.volt_selection.set("0")

        options = ["0", "50", "100", "200", "300", "500", "700", "1000", "1500", "2000"]

        self.volt_dropdown = tk.OptionMenu(self.buttons_frame, self.volt_selection, *options, command=self.changeV)
        self.volt_dropdown.pack(side=tk.LEFT)

        self.spacer_label = tk.Label(self.buttons_frame, text="        ")
        self.spacer_label.pack(side=tk.LEFT)
        
        self.export_btn = tk.Button(self.buttons_frame, text="Export", command=self.export)
        self.export_btn.pack(side=tk.RIGHT)

        self.config_x_frame = tk.Frame(self.root)
        self.config_x_frame.pack(side=tk.BOTTOM, pady=10)

        self.xmin_label = tk.Label(self.config_x_frame, text="Min x:")
        self.xmin_label.pack(side=tk.LEFT)

        self.xmin_sv = tk.StringVar()
        self.xmin_sv.trace_add("write", callback=self.limit_change)

        self.xmin_entry = tk.Entry(self.config_x_frame, textvariable=self.xmin_sv)
        self.xmin_entry.pack(side=tk.LEFT)

        self.spacer_label2 = tk.Label(self.config_x_frame, text="        ")
        self.spacer_label2.pack(side=tk.LEFT)

        self.xmax_label = tk.Label(self.config_x_frame, text="Max x:")
        self.xmax_label.pack(side=tk.LEFT)

        self.xmax_sv = tk.StringVar()
        self.xmax_sv.trace_add("write", callback=self.limit_change)

        self.xmax_entry = tk.Entry(self.config_x_frame, textvariable=self.xmax_sv)
        self.xmax_entry.pack(side=tk.LEFT)

        self.config_y_frame = tk.Frame(self.root)
        self.config_y_frame.pack(side=tk.BOTTOM, pady=10)

        y_floats = [float(y) for y in self.y_values]

        self.ymin_label = tk.Label(self.config_y_frame, text="Min y:")
        self.ymin_label.pack(side=tk.LEFT)

        self.ymin_sv = tk.StringVar()
        self.ymin_sv.set(min(y_floats))
        self.ymin_sv.trace_add("write", callback=self.y_limit_change)
        
        self.ymin_combo_box = ttk.Combobox(self.config_y_frame, values=y_floats, width=10, textvariable=self.ymin_sv)
        self.ymin_combo_box.pack(side=tk.LEFT)

        self.spacer_label3 = tk.Label(self.config_y_frame, text="        ")
        self.spacer_label3.pack(side=tk.LEFT)

        self.ymax_label = tk.Label(self.config_y_frame, text="Max y:")
        self.ymax_label.pack(side=tk.LEFT)

        self.ymax_sv = tk.StringVar()
        self.ymax_sv.set(max(y_floats))
        self.ymax_sv.trace_add("write", callback=self.y_limit_change)
        
        self.ymax_combo_box = ttk.Combobox(self.config_y_frame, values=y_floats, width=10, textvariable=self.ymax_sv)
        self.ymax_combo_box.pack(side=tk.LEFT)

        self.pad_var = tk.IntVar()
        self.pad_var.trace_add("write", callback=self.padding_change)

        self.pad_btn = tk.Checkbutton(self.root, width=10, variable=self.pad_var, text="Axes padding")
        self.pad_btn.select()
        self.pad_btn.pack(side=tk.BOTTOM)
    

if __name__ == "__main__":
    SSS = [np.array([-5, -5], dtype=float), np.array([5, 5], dtype=float), np.array([0.5, 0.5], dtype=float)]
    rangeMap = {'start': -5.0, 'stop': 5.0, 'index': 1}
    from DataHandler import DataHandler
    dh = DataHandler()
    def p_point(coord, hist):
        print(f"PlotPoint: {coord} (hist: {hist})")
    ehmap = EnergyHeatMap("VX", ["VZ"], {"VZ": 0, "VX": 1}, [1.0], 1, SSS, rangeMap, dh, p_point)
    ehmap.plot_pcolormap()
    ehmap.runMainLoop()