import tkinter as tk
from tkinter import ttk
import numpy as np
import os
import collections.abc
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from mpl_interactions import panhandler, zoom_factory
from matplotlib.colors import LogNorm
from matplotlib import patches



class HeatMap(tk.Frame):
    def __init__(self, axesNames, axesValues, dSource, rangeMap, labelMap, datahandler, pointPlotFunc, axisMap):
        #for i in [axesNames, axesValues, hist, rangeMap, labelMap, axisMap]: print(i)
        self.root = tk.Tk()
        self.root.title("Heatmap")
        self.axesNames = axesNames
        self.axesValues = axesValues
        self.dSource = dSource
        self.rangeMap = rangeMap
        self.labelMap = labelMap
        self.dh = datahandler
        self.plotPointFunc = pointPlotFunc
        self.axisMap = axisMap
        self.xCoords = None
        self.yCoords = None
        self.axIndex = None
        self.XY_Names = {"X": None, "Y": None}
        self.accMap = {"Max": 2, "Sum": 3, "Peak": 4}
        self.acc = "Sum"
        self.data = None
        self.formattedData = None
        self.formattedData_max = None
        self.formattedCoordsx = None
        self.formattedCoordsy = None
        self.noUpdate = False
        self.x_min = None
        self.x_max = None
        self.ax = None
        self.x_padding = [None, None]
        self.y_padding = [None, None]
        self.original_limits = [None, None]
        self.first = True
        self.zoom_corners = [[None, None], [None, None]]
        self.zoom_rect = None
        self.use_pad = 1
        self.curr_x_lim = [None, None]
        self.curr_y_lim = [None, None]
        self.stepSizes = [None, None]
        self.lastPlot = [None, None, None]
        self.logColorScale = False
        self.file = self.dh.getFile(self.dSource)
        self.get_data()
        self.create_widgets()
    
    """
    def get_curr_data(self):
        if self.acc == "Sum":
            return self.formattedData_sum
        elif self.acc == "Max":
            return self.formattedData_max
        else:
            print(f"Accumulator error! Current accumulator: {self.acc} should be either Sum or Max")
            return None
    """
    
    def getDecimals(self, stepArr):
        maxDigits = 1
        for i in stepArr:
            dotIndx = str(i).find(".")
            zeroIndx = str(i).find("000")-1
            if zeroIndx == -1:
                continue
            if zeroIndx - dotIndx > maxDigits:
                maxDigits = zeroIndx - dotIndx
        return maxDigits
    
    def getTicks(self, data, tickamount, decimals=-1):
        ticks = []
        tickLocations = []
        delta = round(len(data) / (tickamount-1))
        for i in range(0, delta*(tickamount-1), delta):
            if decimals == -1:
                ticks.append(data[i])
            else:
                ticks.append(round(data[i], decimals))
            tickLocations.append(i)
        if decimals == -1:
            ticks.append(data[-1])
        else:
            ticks.append(round(data[-1], decimals))
        tickLocations.append(len(data) - 1)
        return ticks, tickLocations
    
    
    def NoneToZero(self, array3d):
        data = array3d
        for i in range(len(data)):
            for j in range(len(data[i])):
                for k in range(len(data[i][j])):
                    if data[i][j][k] == None:
                        data[i][j][k] = 0.0
        return data
    """
    
    def get_data_from_acc(self):
        self.formattedData_sum = []
        self.formattedData_max = []
        for i in self.data[0]:
            temp_sum = []
            temp_max = []
            for j in i:
                temp_sum.append(np.sum(j))
                temp_max.append(np.max(j))
            self.formattedData_sum.append(temp_sum)
            self.formattedData_max.append(temp_max)
    """

    def get_padding(self):
        x_pad = self.ax.get_xlim()
        y_pad = self.ax.get_ylim()
        self.x_padding = [x_pad[0] - min(self.xCoords), x_pad[1] - max(self.xCoords)]
        self.y_padding = [y_pad[0] - min(self.yCoords), y_pad[1] - max(self.yCoords)]

    def get_data(self):
        axisIndex = {}
        decoded_axes = [i.decode() for i in list(self.file.get("axes"))]
        for key in self.axisMap:
            axisIndex[key] = decoded_axes.index(self.axisMap[key])

        self.data = self.dh.get2DSlice(self.axesNames, self.axesValues, self.dSource, rStart=self.rangeMap["start"], rStop=self.rangeMap["stop"], rIndex=self.rangeMap["index"], axisIndex=axisIndex)

        self.formattedCoordsx = []
        self.formattedCoordsy = []

        self.axis_namex = self.axisMap["X"]
        self.axis_namey = self.axisMap["Y"]

        
        self.XY_Names["X"] = self.axis_namex
        self.XY_Names["Y"] = self.axis_namey

        index_x = axisIndex["X"]
        index_y = axisIndex["Y"]
        self.axIndex = axisIndex
        
        if self.acc == "Peak":
            res = self.NoneToZero(self.data[0])
            self.formattedData = [[i[0] for i in res[j] if i[0] != None] for j in range(len(res))]
        else:
            self.formattedData = [[i[0] for i in self.data[0][j] if i[0] != None] for j in range(len(self.data[0]))]
        

        #self.formattedData = [[i[0] for i in self.data[0][j] if i[0] != None] for j in range(len(self.data[0]))]

        for i in self.data[1][0]:
            self.formattedCoordsx.append(i[index_x])
        for i in self.data[1]:
            self.formattedCoordsy.append(i[0][index_y])

        self.xCoords = self.formattedCoordsx
        self.yCoords = self.formattedCoordsy

        self.curr_x_lim = [np.min(self.xCoords), np.max(self.xCoords)]
        self.curr_y_lim = [np.min(self.yCoords), np.max(self.yCoords)]
        
        self.stepSizes[0] = abs(self.xCoords[1] - self.xCoords[0])
        self.stepSizes[1] = abs(self.yCoords[1] - self.yCoords[0])

        self.x_min = min(self.xCoords)
        self.x_max = max(self.xCoords)

        self.xLabelsIndex = np.linspace(0, len(self.formattedCoordsx), num=len(self.formattedCoordsx), endpoint=False)
        self.xLabelsIndex = [int(x) for x in self.xLabelsIndex]
        self.yLabelsIndex = np.linspace(0, len(self.formattedCoordsy), num=len(self.formattedCoordsy), endpoint=False)
        self.yLabelsIndex = [int(x) for x in self.yLabelsIndex]

        
    def plot_pcolormap(self):
        self.plot_matrix(self.formattedData, self.xCoords, self.yCoords)
    
    def plot_matrix(self, matrix, xVals, yVals):
        self.lastPlot = [matrix, xVals, yVals]
        if hasattr(self, 'canvas'):
            self.canvas.get_tk_widget().destroy()
        if self.dSource == "hist":
            title = "HIST HeatMap"
        else:
            title = "HeatMap"
        if len(self.axesNames) > 0:
            title += "\n"
            for i in range(len(self.axesNames)):
                title += self.axesNames[i] + "=" + str(self.axesValues[i]) + " & "
            title = title[:-3]

        fig = Figure()
        ax = fig.add_subplot(111)

        self.ax = ax
        #print("MATRIX:")
        #for i in matrix: print(i)
        if self.logColorScale:
            norm = LogNorm()
        else:
            norm = None
        im = ax.pcolormesh(xVals, yVals, matrix, shading='auto', cmap='viridis', norm=norm)
        ax.set_title(title)

        ax.set_ylabel(self.labelMap[self.axis_namey])
        ax.set_xlabel(self.labelMap[self.axis_namex])

        cbar = ax.figure.colorbar(im, ax=ax)
        if self.acc == "Sum":
            ylabel = "Integrated Area [v * s]"
        elif self.acc == "Max":
            ylabel = "Peak Value [v]"
        elif self.acc == "Peak":
            ylabel = "Peak Value [v] ???"
        else:
            ylabel = "ERROR"
        cbar.ax.set_ylabel(ylabel, rotation=-90, va="bottom")

        fig.tight_layout()

        if self.x_padding[0] == None:
            self.get_padding()
        
        if self.first:
            self.original_limits[0] = self.ax.get_xlim()
            self.original_limits[1] = self.ax.get_ylim()
            self.first = False
        
        self.set_x_min_max(str(np.min(xVals)), str(np.max(xVals)), False)
        self.set_y_min_max(str(np.min(yVals)), str(np.max(yVals)), False)
        

        disconnect_zoom = zoom_factory(ax)
        pan_handler = panhandler(fig)

        self.canvas = FigureCanvasTkAgg(fig, master=self.root)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(fill="both", expand=True)

        self.canvas.mpl_connect('button_press_event', self.on_click)


    def set_x_min_max(self, minx: str, maxx: str, update: bool):
        if not update:
            self.noUpdate = True
        self.xmin_sv.set(minx)
        self.xmax_sv.set(maxx)
        if not update:
            self.noUpdate = False
    
    def set_y_min_max(self, miny: str, maxy: str, update: bool):
        if not update:
            self.noUpdate = True
        self.ymin_sv.set(miny)
        self.ymax_sv.set(maxy)
        if not update:
            self.noUpdate = False
        
    
    def on_click(self, event):
        if event.inaxes:
            if event.button == 1 and event.dblclick:
                if not self.dh.lockinExp:
                    self.plotPoint(event.xdata, event.ydata)
        if event.button == 3:
            self.zoom(event.xdata, event.ydata)
    
    def get_coordrange(self, start, stop, step):
        curr = start
        vals = []
        while curr != stop:
            vals.append(curr)
            curr += step
        vals.append(curr)
        return vals


    def get_vals_in_zoom(self, corners):
        min_x = corners[0]
        max_x = corners[1]
        min_y = corners[2]
        max_y = corners[3]
        min_x_index = self.xCoords.index(min_x)
        min_y_index = self.yCoords.index(min_y)
        max_x_index = self.xCoords.index(max_x)
        max_y_index = self.yCoords.index(max_y)
        values = []
        data = self.formattedData
        for i in range(min_y_index, max_y_index + 1, 1):
            temp = []
            for j in range(min_x_index, max_x_index + 1, 1):
                #print(f"Appending row: {i}, col: {j}")
                temp.append(data[i][j])
            values.append(temp)
        return values

    
    def zoom(self, x, y):
        _, closest_x_indx = self.closest(x, self.xCoords)
        _, closes_y_indx = self.closest(y, self.yCoords)
        if self.zoom_corners[0][0] == None:
            self.zoom_corners[0][0] = self.xCoords[closest_x_indx]
            self.zoom_corners[0][1] = self.yCoords[closes_y_indx]
        elif self.zoom_corners[1][0] == None:
            self.zoom_corners[1][0] = self.xCoords[closest_x_indx]
            self.zoom_corners[1][1] = self.yCoords[closes_y_indx]
            if self.zoom_corners[0][0] < self.zoom_corners[1][0]:
                self.zoom_corners[0][0] = self.zoom_corners[0][0] + self.x_padding[0]
                self.zoom_corners[1][0] = self.zoom_corners[1][0] + (self.x_padding[1])
            elif self.zoom_corners[0][0] > self.zoom_corners[1][0]:
                self.zoom_corners[1][0] = self.zoom_corners[1][0] + self.x_padding[0]
                self.zoom_corners[0][0] = self.zoom_corners[0][0] + (self.x_padding[1])

            if self.zoom_corners[0][1] < self.zoom_corners[1][1]:
                self.zoom_corners[0][1] = self.zoom_corners[0][1] + self.y_padding[0]
                self.zoom_corners[1][1] = self.zoom_corners[1][1] + (self.y_padding[1])
            elif self.zoom_corners[0][1] > self.zoom_corners[1][1]:
                self.zoom_corners[1][1] = self.zoom_corners[1][1] + self.y_padding[0]
                self.zoom_corners[0][1] = self.zoom_corners[0][1] + (self.y_padding[1])

            bottom_left_corner = (min(self.zoom_corners[0][0], self.zoom_corners[1][0]), min(self.zoom_corners[0][1], self.zoom_corners[1][1]))
            width = max(self.zoom_corners[0][0], self.zoom_corners[1][0]) - min(self.zoom_corners[0][0], self.zoom_corners[1][0])
            height = max(self.zoom_corners[0][1], self.zoom_corners[1][1]) - min(self.zoom_corners[0][1], self.zoom_corners[1][1])
            self.zoom_rect = patches.Rectangle(bottom_left_corner, width, height, color='yellow', alpha=0.5)
            self.ax.add_patch(self.zoom_rect)
            self.canvas.draw()
        else:
            zoom_corners = self.zoom_rect.get_corners()
            self.zoom_rect.remove()
            if x >= min(self.zoom_corners[0][0], self.zoom_corners[1][0]) and x <= max(self.zoom_corners[0][0], self.zoom_corners[1][0]) and y >= min(self.zoom_corners[0][1], self.zoom_corners[1][1]) and y <= max(self.zoom_corners[0][1], self.zoom_corners[1][1]):
                self.set_x_min_max(str(min(self.zoom_corners[0][0], self.zoom_corners[1][0])), str(max(self.zoom_corners[0][0], self.zoom_corners[1][0])), False)
                self.set_y_min_max(str(min(self.zoom_corners[0][1], self.zoom_corners[1][1])), str(max(self.zoom_corners[0][1], self.zoom_corners[1][1])), False)
                corners = [zoom_corners[0][0] + self.x_padding[1], zoom_corners[1][0] + self.x_padding[0], zoom_corners[0][1] + self.y_padding[1], zoom_corners[2][1] + self.y_padding[0]]
                vals = self.get_vals_in_zoom(corners)
                xStart = min(self.zoom_corners[0][0], self.zoom_corners[1][0]) + self.x_padding[1]
                xStop = max(self.zoom_corners[0][0], self.zoom_corners[1][0]) + self.x_padding[0]
                yStart = min(self.zoom_corners[0][1], self.zoom_corners[1][1]) + self.y_padding[1]
                yStop = max(self.zoom_corners[0][1], self.zoom_corners[1][1]) + self.y_padding[0]
                xVals = self.get_coordrange(xStart, xStop, self.stepSizes[0])
                yVals = self.get_coordrange(yStart, yStop, self.stepSizes[1])
                self.curr_x_lim = [xStart, xStop]
                self.curr_y_lim = [yStart, yStop]       
                self.plot_matrix(vals, xVals, yVals)
            self.zoom_corners = [[None, None], [None, None]]
            self.canvas.draw()
    
    def closest(self, x, arr):
        lowest = abs(x - arr[0])
        index = 0
        for i in range(len(arr)):
            if abs(x - arr[i]) < lowest:
                lowest = abs(x - arr[i])
                index = i
        return lowest, index
    
    def plotPoint(self, x, y):
        _, min_x_index = self.closest(x, self.xCoords)
        _, min_y_index = self.closest(y, self.yCoords)
        axes = list(self.file.get("axes"))
        key = []
        for _ in axes:
            key.append(None)
        for _ in axes:
            key[self.axIndex["X"]] = self.xCoords[min_x_index]
            key[self.axIndex["Y"]] = self.yCoords[min_y_index]
        usedAxes = 0
        for i in range(len(key)):
            if key[i] == None:
                key[i] = self.axesValues[usedAxes]
                usedAxes += 1
        self.plotPointFunc(key, self.dSource)

    def changeAcc(self, c):
        self.acc = c
        self.dh.setAccumulator(self.accMap[self.acc])
        self.get_data()
        self.plot_pcolormap()
    
    def export(self):
        axes_original = [i.decode() for i in list(self.file.get("axes"))]
        axesDiff = list(set(axes_original) - set(self.axesNames))
        varAxis1 = axesDiff[0]
        varAxis2 = axesDiff[1]
        if self.dSource == "hist":
            name = f"2DHistData_{varAxis1}_{varAxis2}-{self.dh.acc.name}-".replace("[", "").replace("]", "").replace("'", "")
        else:
            name = f"2DData_{varAxis1}_{varAxis2}-{self.dh.acc.name}-".replace("[", "").replace("]", "").replace("'", "")
        for i in range(len(self.axesNames)):
            name += self.axesNames[i] + "=" + str(self.axesValues[i]) + "-"
        name = name[:-1] + ".txt"
        a = open(name,"a+")
        data = self.data
        for data1 in data[0]:
            temp = ""
            for j in data1:
                if (isinstance(j, collections.abc.Sequence)):
                    temp+=str(j[0])+","
                else:
                    temp+=str(j)+","
            temp= temp[:-1]+"\n"   
            a.write(temp)
        a.close()
    
    def x_limit_change(self, var=None, index=None, mode=None):
        if self.noUpdate: return
        new_min = self.xmin_sv.get()
        new_max = self.xmax_sv.get()
        if new_min.replace(".", "").replace("-", "").isnumeric() and new_max.replace(".", "").replace("-", "").isnumeric() and new_min.count(".") <= 1 and new_max.count(".") <= 1:
            new_min = float(new_min)
            new_max = float(new_max)
            self.curr_x_lim = [new_min, new_max]
            vals = self.get_vals_in_zoom([new_min, new_max, self.curr_y_lim[0], self.curr_y_lim[1]])
            xVals = self.get_coordrange(new_min, new_max, self.stepSizes[0])
            yVals = self.get_coordrange(self.curr_y_lim[0], self.curr_y_lim[1], self.stepSizes[1])
            self.plot_matrix(vals, xVals, yVals)

    def y_limit_change(self, var=None, index=None, mode=None):
        if self.noUpdate: return
        new_min = self.ymin_sv.get()
        new_max = self.ymax_sv.get()
        if new_min.replace(".", "").replace("-", "").isnumeric() and new_max.replace(".", "").replace("-", "").isnumeric() and new_min.count(".") <= 1 and new_max.count(".") <= 1:
            new_min = float(new_min)
            new_max = float(new_max)
            self.curr_y_lim = [new_min, new_max]
            vals = self.get_vals_in_zoom([self.curr_x_lim[0], self.curr_x_lim[1], new_min, new_max])
            xVals = self.get_coordrange(self.curr_x_lim[0], self.curr_x_lim[1], self.stepSizes[0])
            yVals = self.get_coordrange(new_min, new_max, self.stepSizes[1])
            self.plot_matrix(vals, xVals, yVals)

    def reset_plot(self):
        self.plot_matrix(self.formattedData, self.xCoords, self.yCoords)
    
    def get_filename(self, fileName):
        count = 1
        while os.path.exists(fileName):
            dotIndx = str(fileName).rfind(".")
            fileName = fileName[:dotIndx] + "_" + str(count) + fileName[dotIndx:]
            count += 1
        return fileName
    
    def save_as_png(self, current=True):
        if not os.path.isdir("./Saved_Figures"):
            os.mkdir("./Saved_Figures")
        axes_original = [i.decode() for i in list(self.file.get("axes"))]
        axesDiff = list(set(axes_original) - set(self.axesNames))
        varAxis1 = axesDiff[0]
        varAxis2 = axesDiff[1]
        if self.dSource == "hist":
            fileName = f"2DHistData_{varAxis1}_{varAxis2}-{self.dh.acc.name}-".replace("[", "").replace("]", "").replace("'", "")
        else:
            fileName = f"2DData_{varAxis1}_{varAxis2}-{self.dh.acc.name}-".replace("[", "").replace("]", "").replace("'", "")
        for i in range(len(self.axesNames)):
            fileName += self.axesNames[i] + "=" + str(self.axesValues[i]) + "-"
        fileName = fileName[:-1] + ".png"
        fileName = os.path.join("./Saved_Figures", fileName)
        fileName = self.get_filename(fileName)
        if current:
            self.canvas.print_png(fileName)
        else:
            old_matrix = self.lastPlot[0]
            old_xvals = self.lastPlot[1]
            old_yvals = self.lastPlot[2]
            self.reset_plot()
            self.canvas.print_png(fileName)
            self.plot_matrix(old_matrix, old_xvals, old_yvals)
    
    def toggle_lin_log(self):
        self.logColorScale = not self.logColorScale
        self.plot_pcolormap()

    def close(self):
        exit(0)
    
    def runMainLoop(self):
        self.root.mainloop()

    def create_widgets(self):

        self.menu = tk.Menu(self.root)
        self.root.config(menu=self.menu)

        self.fileMenu = tk.Menu(self.menu, tearoff=0)

        self.saveMenu = tk.Menu(self.fileMenu, tearoff=0)
        self.saveMenu.add_command(label="Current Plot", command=lambda x=True: self.save_as_png(x))
        self.saveMenu.add_command(label="Whole Plot", command=lambda x=False: self.save_as_png(x))
        
        self.fileMenu.add_cascade(label="Save", menu=self.saveMenu)
        self.fileMenu.add_separator()
        self.fileMenu.add_command(label="Exit", command=self.close)

        self.menu.add_cascade(label="File", menu=self.fileMenu)

        self.buttons_frame = tk.Frame(self.root)
        self.buttons_frame.pack(side=tk.TOP)

        self.reset_button = tk.Button(self.buttons_frame, text="Reset", command=self.reset_plot)
        self.reset_button.pack(side=tk.LEFT)

        self.spacer_label0 = tk.Label(self.buttons_frame, text="        ")
        self.spacer_label0.pack(side=tk.LEFT)

        self.lin_log = tk.Button(self.buttons_frame, text="Toggle lin/log", command=self.toggle_lin_log)
        self.lin_log.pack(side=tk.LEFT)

        self.spacer_label00 = tk.Label(self.buttons_frame, text="        ")
        self.spacer_label00.pack(side=tk.LEFT)

        self.acc_label = tk.Label(self.buttons_frame, text="Accumulator:")
        self.acc_label.pack(side=tk.LEFT)

        self.acc_selection = tk.StringVar(self.buttons_frame)
        self.acc_selection.set("Sum")

        if self.dSource == "hist":
            options = ["Sum", "Peak"]
        else:
            options = ["Sum", "Max"]

        self.acc_dropdown = tk.OptionMenu(self.buttons_frame, self.acc_selection, *options, command=self.changeAcc)
        self.acc_dropdown.pack(side=tk.LEFT)

        self.spacer_label = tk.Label(self.buttons_frame, text="        ")
        self.spacer_label.pack(side=tk.LEFT)
        
        self.export_btn = tk.Button(self.buttons_frame, text="Export", command=self.export)
        self.export_btn.pack(side=tk.RIGHT)

        self.config_x_frame = tk.Frame(self.root)
        self.config_x_frame.pack(side=tk.BOTTOM, pady=10)

        self.xmin_label = tk.Label(self.config_x_frame, text="Min x:")
        self.xmin_label.pack(side=tk.LEFT)

        x_floats = [float(x) for x in self.xCoords]

        self.xmin_sv = tk.StringVar()
        self.xmin_sv.set(min(x_floats))
        self.xmin_sv.trace_add("write", callback=self.x_limit_change)

        self.xmin_combo_box = ttk.Combobox(self.config_x_frame, values=x_floats, width=10, textvariable=self.xmin_sv)
        self.xmin_combo_box.pack(side=tk.LEFT)

        self.spacer_label2 = tk.Label(self.config_x_frame, text="        ")
        self.spacer_label2.pack(side=tk.LEFT)

        self.xmax_label = tk.Label(self.config_x_frame, text="Max x:")
        self.xmax_label.pack(side=tk.LEFT)

        self.xmax_sv = tk.StringVar()
        self.xmax_sv.set(max(x_floats))
        self.xmax_sv.trace_add("write", callback=self.x_limit_change)

        self.xmax_combo_box = ttk.Combobox(self.config_x_frame, values=x_floats, width=10, textvariable=self.xmax_sv)
        self.xmax_combo_box.pack(side=tk.LEFT)

        self.config_y_frame = tk.Frame(self.root)
        self.config_y_frame.pack(side=tk.BOTTOM, pady=10)

        y_floats = [float(y) for y in self.yCoords]

        self.ymin_label = tk.Label(self.config_y_frame, text="Min y:")
        self.ymin_label.pack(side=tk.LEFT)

        self.ymin_sv = tk.StringVar()
        self.ymin_sv.set(min(y_floats))
        self.ymin_sv.trace_add("write", callback=self.y_limit_change)
        
        self.ymin_combo_box = ttk.Combobox(self.config_y_frame, values=y_floats, width=10, textvariable=self.ymin_sv)
        self.ymin_combo_box.pack(side=tk.LEFT)

        self.spacer_label3 = tk.Label(self.config_y_frame, text="        ")
        self.spacer_label3.pack(side=tk.LEFT)

        self.ymax_label = tk.Label(self.config_y_frame, text="Max y:")
        self.ymax_label.pack(side=tk.LEFT)

        self.ymax_sv = tk.StringVar()
        self.ymax_sv.set(max(y_floats))
        self.ymax_sv.trace_add("write", callback=self.y_limit_change)
        
        self.ymax_combo_box = ttk.Combobox(self.config_y_frame, values=y_floats, width=10, textvariable=self.ymax_sv)
        self.ymax_combo_box.pack(side=tk.LEFT)


if __name__ == "__main__":
    labelMap = {'X': 'X - Position [mm]', 'Y': 'Y - Position [mm]', 'Z': 'Z - Position [mm]', 'VX': 'VX - Position [mm]', 'VY': 'VY - Position [mm]', 'VZ': 'VZ - Position [mm]', 'S0': 'S0 - Voltage [v]', 'S1': 'S1 - Voltage [v]', 'S2': 'S2 - Voltage [v]', 'S3': 'S3 - Voltage [v]', 'R1': 'R1 - PMT Angle [°]', 'R2': 'R2 - E-field [n.u]', 'R3': 'R3 - Wiregrid angle [°]', 'Sum': 'Volt-seconds [vs]', 'Peak': 'Volts [v]', 'Integrate': 'Volt-seconds [vs]', 'Max': 'Volts [v]'}
    from DataHandler import DataHandler
    dh = DataHandler()
    def p_point(coord, hist):
        print(f"PlotPoint: {coord} (hist: {hist})")
    
    hmap = HeatMap([], [], False, {'start': [-5.0, -3.0], 'stop': [5.0, 5.0], 'index': [0, 1]}, labelMap, dh, p_point, {'X': 'VZ', 'Y': 'VX'})
    hmap.plot_pcolormap()
    hmap.runMainLoop()