import tkinter as tk
from tkinter import ttk
import numpy as np
from mpl_interactions import panhandler, zoom_factory
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib import patches
import os


class D1plot(tk.Frame):
    def __init__(self, axesNames, axesValues, dSource, rangeMap, labelMap, dataHandler, pointPlotFunc, root=None):
        #for i in [axesNames, axesValues, hist, rangeMap, labelMap, dataHandler]: print(i)
        #for i, j in zip([axesNames, axesValues, hist, rangeMap, labelMap], 
        #                ["axesNames", "axesValues", "hist", "rangeMap", "labelMap"]):
        #    print(j + ":\n" + str(type(i)) + "\n" + str(i) + "\n")
        if root is None:
            self.root = tk.Tk()
            self.root.title("1D Plot")
        else:
            self.root = root
        
        self.initParams = [np.array(axesNames).copy().tolist(), np.array(axesValues).copy().tolist(), dSource, dict(rangeMap), dict(labelMap), dataHandler, pointPlotFunc]
        self.axesNames = axesNames
        self.axesValues = axesValues
        self.dSource = dSource
        self.rangeMap = rangeMap
        self.labelMap = labelMap
        self.dh = dataHandler
        self.plotPointFunc = pointPlotFunc
        self.acc = "Sum"
        self.accMap = {"Max": 2, "Sum": 3, "Peak": 4}
        self.varAxis = None
        self.ax = None
        self.data = None
        self.x_values = None
        self.y_values = None
        self.x_max = None
        self.x_min = None
        self.y_max = None
        self.y_min = None
        self.abs_x_Min = None
        self.abs_x_Max = None
        self.abs_y_Min = None
        self.abs_y_Max = None
        self.noUpdate = False
        self.log_y = False
        self.x_padding = [None, None]
        self.y_padding = [None, None]
        self.zoom_corners = [[None, None], [None, None]]
        self.zoom_rect = None
        self.use_pad = 1
        self.file = self.dh.getFile(self.dSource)
        self.get_data()
        if root is not None:
            allW = self.all_children(self.root)
            for item in allW:
                item.pack_forget()
        self.create_widgets()
        
    
    def get_data(self):
        self.data = self.dh.get1DSlice(self.axesNames,self.axesValues, self.rangeMap["start"], self.rangeMap["stop"], self.rangeMap["index"], self.dSource)
        
        index = self.dh.context.getAxisIndex(self.data[2][0])
        self.x_values = []
        for j in self.data[1]:
            self.x_values.append(j[index])
        self.x_min = min(self.x_values)
        self.x_max = max(self.x_values)
        
        self.y_values = []
        for i in self.data[0]:
            if i[0] != None:
                self.y_values.append(i[0])
        self.y_min = min(self.y_values)
        self.y_max = max(self.y_values)
    
    def get_padding(self):
        x_pad = self.ax.get_xlim()
        y_pad = self.ax.get_ylim()
        self.x_padding = [x_pad[0] - min(self.x_values), x_pad[1] - max(self.x_values)]
        self.y_padding = [y_pad[0] - min(self.y_values), y_pad[1] - max(self.y_values)]
        
        
    def setLimits(self):
        xLimits = self.ax.get_xlim()
        yLimits = self.ax.get_ylim()
        
        self.abs_x_Min = xLimits[0]
        self.abs_x_Max = xLimits[1]

        self.abs_y_Min = yLimits[0]
        self.abs_y_Max = yLimits[1]


    def plot_1_d(self):
        if hasattr(self, 'canvas'):
            self.canvas.get_tk_widget().destroy()
        
        axis_name = self.data[2][0]
        self.varAxis = axis_name
        index = self.dh.context.getAxisIndex(axis_name)

        formattedData = []
        formattedCoords = []

        amp = self.dSource == "amp"

        std = []
        for i in self.data[0]:
            formattedData.append(i[0])
            if amp:
                std.append(i[1] / 2)
        for j in self.data[1]:
            formattedCoords.append(j[index])
        hist = self.dSource == "hist"

        if hist:
            title = "HIST 1D Plot"
        elif amp:
            title = "AMMETER 1D Plot"
        else:
            title = "1D Plot"
        if len(self.axesNames) > 0:
            title += "\n"
            for i in range(len(self.axesNames)):
                title += self.axesNames[i] + "=" + str(self.axesValues[i]) + " & "
            title = title[:-3]

        fig = Figure()
        ax = fig.add_subplot(111)
        self.ax = ax
        ax.set_title(title)
        

        if amp:
            ax.errorbar(formattedCoords, formattedData, std, ecolor='red', capsize=3.0, capthick=1.0, color='blue')
        elif self.dh.type == 0 and self.dh.acc.name == "Peak":
            ax.scatter(formattedCoords,formattedData)
        else:
            ax.plot(formattedCoords,formattedData)
        ax.set_xlabel(self.labelMap[axis_name])

        if self.dh.type == 1:
            ax.set_ylabel(f"Voltage (V)")
        elif self.dh.type == 0:
            if hist:
                ax.set_ylabel(f"Electron Counts")
            else:
                ax.set_ylabel(self.dh.acc.name)
        
        self.setLimits()

        fig.tight_layout()

        if self.x_padding[0] == None:
            self.get_padding()
        
        disconnect_zoom = zoom_factory(ax)
        pan_handler = panhandler(fig)
        
        self.canvas = FigureCanvasTkAgg(fig, master=self.root)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(fill="both", expand=True)

        self.canvas.mpl_connect('button_press_event', self.on_click)
    
    def closest(self, x, arr):
        lowest = abs(x - arr[0])
        index = 0
        for i in range(len(arr)):
            if abs(x - arr[i]) < lowest:
                lowest = abs(x - arr[i])
                index = i
        return lowest, index

    def zoom(self, x, y):
        if self.zoom_corners[0][0] == None:
            self.zoom_corners[0][0] = x
            self.zoom_corners[0][1] = y
        elif self.zoom_corners[1][0] == None:
            self.zoom_corners[1][0] = x
            self.zoom_corners[1][1] = y
            bottom_left_corner = (min(self.zoom_corners[0][0], self.zoom_corners[1][0]), min(self.zoom_corners[0][1], self.zoom_corners[1][1]))
            width = max(self.zoom_corners[0][0], self.zoom_corners[1][0]) - min(self.zoom_corners[0][0], self.zoom_corners[1][0])
            height = max(self.zoom_corners[0][1], self.zoom_corners[1][1]) - min(self.zoom_corners[0][1], self.zoom_corners[1][1])
            self.zoom_rect = patches.Rectangle(bottom_left_corner, width, height, color='yellow', alpha=0.5)
            self.ax.add_patch(self.zoom_rect)
            self.canvas.draw()
        else:
            self.zoom_rect.remove()
            if x >= min(self.zoom_corners[0][0], self.zoom_corners[1][0]) and x <= max(self.zoom_corners[0][0], self.zoom_corners[1][0]) and y >= min(self.zoom_corners[0][1], self.zoom_corners[1][1]) and y <= max(self.zoom_corners[0][1], self.zoom_corners[1][1]):
                self.ax.set_xlim(min(self.zoom_corners[0][0], self.zoom_corners[1][0]), max(self.zoom_corners[0][0], self.zoom_corners[1][0]))
                self.ax.set_ylim(min(self.zoom_corners[0][1], self.zoom_corners[1][1]), max(self.zoom_corners[0][1], self.zoom_corners[1][1]))
                self.noUpdate = True
                self.xmin_sv.set(min(self.zoom_corners[0][0], self.zoom_corners[1][0]))
                self.xmax_sv.set(max(self.zoom_corners[0][0], self.zoom_corners[1][0]))
                self.ymin_sv.set(min(self.zoom_corners[0][1], self.zoom_corners[1][1]))
                self.ymax_sv.set(max(self.zoom_corners[0][1], self.zoom_corners[1][1]))
                self.noUpdate = False
            self.zoom_corners = [[None, None], [None, None]]
            self.canvas.draw()

    
    def on_click(self, event):
        if event.inaxes:
            x = event.xdata
            if event.button == 1 and event.dblclick:
                self.plotPoint(x)
            elif event.button == 3:
                self.zoom(event.xdata, event.ydata)
    
    def plotPoint(self, x):
        _, minIndex = self.closest(x, self.x_values)
        new_x = self.x_values[minIndex]
        axes = list(self.file.get("axes"))
        axOrder = {i.decode(): axes.index(i) for i in axes}
        key = []
        for i in axes:
            key.append(None)
        key[axOrder[self.varAxis]] = new_x
        insertAt = 0

        for i in range(len(self.axesNames)):
            while key[insertAt] != None:
                insertAt += 1
            key[insertAt] = float(self.axesValues[i])
            insertAt += 1
        self.plotPointFunc(key, self.dSource)

    
    def changeAcc(self, c):
        #self.reset_plot()
        self.acc = c
        self.dh.setAccumulator(self.accMap[self.acc])
        self.get_data()
        self.plot_1_d()
    
    def export(self):
        nonConst = -1
        axes_original = [i.decode() for i in list(self.file.get("axes"))]
        varAxis = list(set(axes_original) - set(self.axesNames))[0]
        hist = self.dSource == "hist"
        for i in range(len(self.axesNames)):
            if axes_original[i] != self.axesNames[i]:
                nonConst = i
                break
        if hist:
            name = f"1DHistData_{varAxis}-{self.dh.acc.name}-".replace("[", "").replace("]", "").replace("'", "")
        else:
            name = f"1DData_{varAxis}-{self.dh.acc.name}-".replace("[", "").replace("]", "").replace("'", "")
        for i in range(len(self.axesNames)):
            name += self.axesNames[i] + "=" + str(self.axesValues[i]) + "-"
        name = name[:-1] + ".txt"
        a = open(name,"a+")
        data = self.dh.get1DSlice(self.axesNames,self.axesValues, self.rangeMap["start"], self.rangeMap["stop"], self.rangeMap["index"], self.dSource)
        
        for i in range(len(data[0])):
            #value = f"{data[1][i][nonConst]},{data[0][i][0]} \n"
            value = str(data[1][i][nonConst]) + "," + str(data[0][i][0]) + " \n"
            value = value.replace("[", "").replace("]", "")
            a.write(value)
        a.close()
    
    def y_limit_change(self, var=None, index=None, mode=None):
        if self.noUpdate: return
        new_y_min = self.ymin_sv.get()
        new_y_max = self.ymax_sv.get()
        if new_y_min.replace(".", "").replace("-", "").isnumeric() and new_y_max.replace(".", "").replace("-", "").isnumeric():
            #self.ax.set_ylim(float(new_y_min) + (self.y_padding[0] * self.use_pad), float(new_y_max) + (self.y_padding[1] * self.use_pad))
            self.ax.set_ylim(float(new_y_min), float(new_y_max))
            self.canvas.draw()


    def x_limit_change(self, var=None, index=None, mode=None):
        if self.noUpdate: return
        new_x_min = float(self.xmin_combo_box.get())
        new_x_max = float(self.xmax_combo_box.get())
        self.x_min = new_x_min
        self.x_max = new_x_max
        #self.ax.set_xlim(new_x_min + (self.x_padding[0] * self.use_pad), new_x_max + (self.x_padding[1] * self.use_pad))
        self.ax.set_xlim(new_x_min, new_x_max)
        self.canvas.draw()

    def reset_plot(self):
        self.dh.setAccumulator(self.accMap["Sum"])
        self.__init__(self.initParams[0], self.initParams[1], self.initParams[2], self.initParams[3], self.initParams[4], self.initParams[5], self.initParams[6], self.root)
        self.plot_1_d()
        self.runMainLoop()
        """
        self.ax.set_xlim(min(self.x_values) + self.x_padding[0], max(self.x_values) + self.x_padding[1])
        self.ax.set_ylim(min(self.y_values) + self.y_padding[0], max(self.y_values) + self.y_padding[1])
        self.x_min = min(self.x_values)
        self.x_max = max(self.x_values)
        self.y_min = min(self.y_values)
        self.y_max = max(self.y_values)
        self.noUpdate = True
        self.pad_btn.select()
        self.xmin_sv.set(self.x_min)
        self.xmax_sv.set(self.x_max)
        self.ymin_sv.set(min(self.y_values))
        self.ymax_sv.set(max(self.y_values))
        self.noUpdate = False
        
        self.canvas.draw()
        """
    
    def padding_change(self, var=None, index=None, mode=None):
        self.use_pad = int(self.pad_var.get())
        if self.ax != None and not self.noUpdate:
            self.y_limit_change()
            self.x_limit_change()
    
    def get_filename(self, fileName):
        count = 1
        while os.path.exists(fileName):
            dotIndx = str(fileName).rfind(".")
            fileName = fileName[:dotIndx] + "_" + str(count) + fileName[dotIndx:]
            count += 1
        return fileName
    
    def save_as_png(self, current=True):
        if not os.path.isdir("./Saved_Figures"):
            os.mkdir("./Saved_Figures")
        hist = self.dSource == "hist"
        if hist:
            fileName = f"1DHistData_{self.varAxis}-{self.dh.acc.name}-".replace("[", "").replace("]", "").replace("'", "")
        else:
            fileName = f"1DData_{self.varAxis}-{self.dh.acc.name}-".replace("[", "").replace("]", "").replace("'", "")
        for i in range(len(self.axesNames)):
            fileName += self.axesNames[i] + "=" + str(self.axesValues[i]) + "-"
        fileName = fileName[:-1] + ".png"
        fileName = os.path.join("./Saved_Figures", fileName)
        fileName = self.get_filename(fileName)
        if current:
            self.canvas.print_png(fileName)
        else:
            x_min = np.min(self.x_values)
            x_max = np.max(self.x_values)
            y_min = np.min(self.y_values)
            y_max = np.max(self.y_values)
            x_pad = abs(x_max - x_min) / 20
            y_pad = abs(y_max - y_min) / 20
            old_lim = [self.ax.get_xlim(), self.ax.get_ylim()]
            self.ax.set_xlim(x_min - x_pad, x_max + x_pad)
            self.ax.set_ylim(y_min - y_pad, y_max + y_pad)
            self.canvas.print_png(fileName)
            self.ax.set_xlim(old_lim[0])
            self.ax.set_ylim(old_lim[1])
            self.canvas.draw()
    
    def close(self):
        exit(0)

    def toggleY_linlog(self):
        if self.log_y:
            self.ax.set_yscale("linear")
        else:
            self.ax.set_yscale("log")
        self.canvas.draw()
        self.log_y = not self.log_y
    
    def runMainLoop(self):
        self.root.mainloop()
    
    def all_children (self, root) :
        _list = root.winfo_children()

        for item in _list :
            if item.winfo_children() :
                _list.extend(item.winfo_children())

        return _list
    
    def create_widgets(self):

        self.menu = tk.Menu(self.root)
        self.root.config(menu=self.menu)

        self.fileMenu = tk.Menu(self.menu, tearoff=0)

        self.saveMenu = tk.Menu(self.fileMenu, tearoff=0)
        self.saveMenu.add_command(label="Current Plot", command=lambda x=True: self.save_as_png(x))
        self.saveMenu.add_command(label="Whole Plot", command=lambda x=False: self.save_as_png(x))
        
        self.fileMenu.add_cascade(label="Save", menu=self.saveMenu)
        self.fileMenu.add_separator()
        self.fileMenu.add_command(label="Exit", command=self.close)

        self.menu.add_cascade(label="File", menu=self.fileMenu)

        self.buttons_frame = tk.Frame(self.root)
        self.buttons_frame.pack(side=tk.TOP)

        self.reset_button = tk.Button(self.buttons_frame, text="Reset", command=self.reset_plot)
        self.reset_button.pack(side=tk.LEFT)

        self.spacer_label0 = tk.Label(self.buttons_frame, text="        ")
        self.spacer_label0.pack(side=tk.LEFT)

        self.acc_label = tk.Label(self.buttons_frame, text="Accumulator:")
        self.acc_label.pack(side=tk.LEFT)

        self.acc_selection = tk.StringVar(self.buttons_frame)
        self.acc_selection.set("Sum")

        hist = self.dSource == "hist"

        if hist:
            options = ["Sum", "Peak"]
        else:
            options = ["Sum", "Max"]

        self.acc_dropdown = tk.OptionMenu(self.buttons_frame, self.acc_selection, *options, command=self.changeAcc)
        self.acc_dropdown.pack(side=tk.LEFT)

        self.toggle_linlog_axis_btn = tk.Button(self.buttons_frame, text="Toggle Y-axis (Lin/Log)", command=self.toggleY_linlog)
        self.toggle_linlog_axis_btn.pack(side=tk.LEFT)

        self.spacer_label = tk.Label(self.buttons_frame, text="        ")
        self.spacer_label.pack(side=tk.LEFT)
        
        self.export_btn = tk.Button(self.buttons_frame, text="Export", command=self.export)
        self.export_btn.pack(side=tk.RIGHT)

        x_floats = [float(x) for x in self.x_values]

        self.config_x_frame = tk.Frame(self.root)
        self.config_x_frame.pack(side=tk.BOTTOM, pady=10)

        self.xmin_label = tk.Label(self.config_x_frame, text="Min x:")
        self.xmin_label.pack(side=tk.LEFT)

        self.xmin_sv = tk.StringVar()
        self.xmin_sv.set(min(x_floats))
        self.xmin_sv.trace_add("write", callback=self.x_limit_change)

        self.xmin_combo_box = ttk.Combobox(self.config_x_frame, values=x_floats, width=12, textvariable=self.xmin_sv)
        self.xmin_combo_box.pack(side=tk.LEFT)

        self.spacer_label2 = tk.Label(self.config_x_frame, text="        ")
        self.spacer_label2.pack(side=tk.LEFT)

        self.xmax_label = tk.Label(self.config_x_frame, text="Max x:")
        self.xmax_label.pack(side=tk.LEFT)

        self.xmax_sv = tk.StringVar()
        self.xmax_sv.set(max(x_floats))
        self.xmax_sv.trace_add("write", callback=self.x_limit_change)

        self.xmax_combo_box = ttk.Combobox(self.config_x_frame, values=x_floats, width=12, textvariable=self.xmax_sv)
        self.xmax_combo_box.pack(side=tk.LEFT)

        self.config_y_frame = tk.Frame(self.root)
        self.config_y_frame.pack(side=tk.BOTTOM, pady=10)

        self.ymin_label = tk.Label(self.config_y_frame, text="Min y:")
        self.ymin_label.pack(side=tk.LEFT)

        self.ymin_sv = tk.StringVar()
        self.ymin_sv.set(min(self.y_values))
        self.ymin_sv.trace_add("write", callback=self.y_limit_change)

        self.ymin_entry = tk.Entry(self.config_y_frame, textvariable=self.ymin_sv, width=15)
        self.ymin_entry.pack(side=tk.LEFT)

        self.spacer_label3 = tk.Label(self.config_y_frame, text="        ")
        self.spacer_label3.pack(side=tk.LEFT)

        self.ymax_label = tk.Label(self.config_y_frame, text="Max y:")
        self.ymax_label.pack(side=tk.LEFT)
        
        self.ymax_sv = tk.StringVar()
        self.ymax_sv.set(max(self.y_values))
        self.ymax_sv.trace_add("write", callback=self.y_limit_change)

        self.ymax_entry = tk.Entry(self.config_y_frame, textvariable=self.ymax_sv, width=15)
        self.ymax_entry.pack(side=tk.LEFT)

        self.pad_var = tk.IntVar()
        self.pad_var.trace_add("write", callback=self.padding_change)

        self.pad_btn = tk.Checkbutton(self.root, width=10, variable=self.pad_var, text="Axes padding")
        self.pad_btn.select()
        #self.pad_btn.pack(side=tk.BOTTOM)
        



if __name__ == "__main__":
    from DataHandler import DataHandler
    dh = DataHandler()
    labelMap = {'X': 'X - Position [mm]', 'Y': 'Y - Position [mm]', 'Z': 'Z - Position [mm]', 'VX': 'VX - Position [mm]', 'VY': 'VY - Position [mm]', 'VZ': 'VZ - Position [mm]', 'S0': 'S0 - Voltage [v]', 'S1': 'S1 - Voltage [v]', 'S2': 'S2 - Voltage [v]', 'S3': 'S3 - Voltage [v]', 'R1': 'R1 - PMT Angle [°]', 'R2': 'R2 - E-field [n.u]', 'R3': 'R3 - Wiregrid angle [°]', 'Sum': 'Volt-seconds [vs]', 'Peak': 'Volts [v]', 'Integrate': 'Volt-seconds [vs]', 'Max': 'Volts [v]'}
    def p_point(coord, hist):
        print(f"PlotPoint: {coord} (hist: {hist})")
    dh.setAccumulator(3)
    d1p = D1plot(['VZ'], [1.0], False, {'start': -5.0, 'stop': 5.0, 'index': 1}, labelMap, dh, p_point)
    d1p.plot_1_d()
    d1p.runMainLoop()
