"""
@author: Arwinder
edited by: Rune
"""

import matplotlib.pyplot as plt
from ExpSpecific.energyHeatMap import EnergyHeatMap
from ExpSpecific.SDP import SDP
from ExpSpecific.heatMap import HeatMap
from ExpSpecific.D1plot import D1plot

class DataVisualizer:
    def __init__(self,dataHandler):
        self.dataHandler = dataHandler
        self.labelMap = {"X": "X - Position [mm]", "Y": "Y - Position [mm]", "Z": "Z - Position [mm]",
                         "VX": "VX - Position [mm]", "VY": "VY - Position [mm]", "VZ": "VZ - Position [mm]",
                         "S0": "S0 - Voltage [v]", "S1": "S1 - Voltage [v]", "S2": "S2 - Voltage [v]",
                         "S3": "S3 - Voltage [v]", "R1": "R1 - PMT Angle [°]", "R2": "R2 - E-field [n.u]",
                         "R3": "R3 - Wiregrid angle [°]", "Sum": "Volt-seconds [vs]", "Peak": "Volts [v]",
                         "Integrate": "Volt-seconds [vs]", "Max": "Volts [v]", "TimeStage": "Time [s]"}
        #plt.ion()
    
    def closeAll(self):
        plt.close('all')
    
    def customPlot(self):
        data = self.dataHandler.customExport(True)
        keys = self.dataHandler.getUniqueKeys()

        int_y_vals = []
        max_y_vals = []
        int_deviation = []
        max_deviation = []
        for i in range(len(data)):
            if i == 0: continue
            int_y_vals.append(float(data[i][0]))
            max_y_vals.append(float(data[i][1]))
            int_deviation.append(float(data[i][2]) / 2)
            max_deviation.append(float(data[i][3]) / 2)
        
        plt.errorbar(keys, int_y_vals, yerr=int_deviation, ecolor='red', capsize=5.0, capthick=1.0, color='blue')
        #plt.errorbar(keys, max_y_vals, yerr=max_deviation, ecolor='red', capsize=5.0, capthick=1.0, color='green')
        plt.xlabel("R2")
        plt.show()

    def OneDPlot(self,axesNames,axesValues, dSource, rangeMap=None):
        #for i in [axesNames, axesValues, hist]: print(i)
        d1p = D1plot(axesNames, axesValues, dSource, rangeMap, self.labelMap, self.dataHandler, self.plotPoint)
        d1p.plot_1_d()
        d1p.runMainLoop()

    def heatMap(self,axesNames,axesValues,dSource, rangeMap=None, axisMap=None):
        #for i in [axesNames, axesValues, hist, rangeMap]: print(i)
        hmap = HeatMap(axesNames,axesValues,dSource, rangeMap, self.labelMap, self.dataHandler, self.plotPoint, axisMap)
        hmap.plot_pcolormap()
        hmap.runMainLoop()
    
    def energyHeatMap(self,varAxis, axes, axdict, vals, SSS, voltage, digits, rangeMap=None):
        #for i in [varAxis, axes, axdict, vals, SSS, voltage, digits, rangeMap]: print(i)
        ehmap = EnergyHeatMap(varAxis, axes, axdict, vals, digits, SSS, rangeMap, self.dataHandler, self.plotPoint)
        ehmap.plot_pcolormap()
        ehmap.runMainLoop()

    def conditionalAppend(self,mainList,value):
        if value in mainList:
            pass
        else:
            mainList.append(value)

    def plotPoint(self, coord, dSource):
        #for i in [coord, hist]: print(i)
        sdp = SDP(self.dataHandler, coord, dSource)
        sdp.plotPoint()
        sdp.runMainLoop()

if __name__ == "__main__":
    from DataHandler import DataHandler
    dh = DataHandler()
    dv = DataVisualizer(dh)
    #dv.heatMap([],[])
    dv.OneDPlot(['R2'],[0.5],False)
    #dv.plotPoint([16.0,2.0,10.0,50.0,50.0])
    