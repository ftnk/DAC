from config import readerType
import json
from DataVisualizer import DataVisualizer
import os
readerType1 = readerType
if os.path.exists("./ui_data.json"):
    f = open("./ui_data.json", "r")
    UIData = json.load(f)
    f.close()
    readerType1 = UIData["reader_type"]
if readerType1 == 1:
    from DataHandlerLockIn import DataHandler,Accumulator
if readerType1 == 0:
    from DataHandler import DataHandler,Accumulator
class DataManager():
    def __init__(self):

        self.dh = DataHandler()
        #print(Accumulator)
        self.dv = DataVisualizer(self.dh)
        self.useHist = False
        self.histExist = os.path.exists("HistData.hdf5")
        self.validVals = {}
        axes = self.dh.context.axes.copy()
        self.axdict = {}
        for i in range(len(axes)):
            self.axdict[axes[i]] = i
            self.validVals[axes[i]] = []
        self.validVals = self.dh.getValidVals(axes, self.axdict, False)
        self.digits = self.findDigits(self.dh.context.stepSize)
        #self.visualMethods = [self.oneDplot,self.heatMap,]

    
    def runPrompt(self):
        print("Welcome to the data manager , select one of the following options:")
        self.rootWords = ["visualize", "export data"]
        self.firstWords = ["visualize", "export"]
        self.secondWords = ["plot","data"]
        print("1) Visualize Data \n2) Export Data\n3) View Keys")
        
        self.type = int(input("Enter a number and press enter: "))-1
        if self.type == 2:
            keys = self.dh.getKeys()
            print(f"Keys: {keys}")
            print()
            print("1) Visualize Data \n2) Export Data")
            self.type = int(input("Enter a number and press enter: "))-1
        while self.type > len(self.rootWords)-1 or self.type < 0:
            print("Not a valid input!")
            self.type = int(input("Enter a number and press enter: "))-1
        self.firstLevelMethod()
            
        


    def firstLevelMethod(self):

        print(f"Selected to {self.rootWords[self.type]}")
        print(f"What do you want to {self.firstWords[self.type]}?")
        valid = [1,2,3]
        if self.dh.type == 0:
            print(f"1) One Dimensional {self.secondWords[self.type]}\n2) Two Dimensional {self.secondWords[self.type]} \n3) Single data point(oscilloscope trace at a point)")
        else:
            print(f"1) One Dimensional {self.secondWords[self.type]}\n2) Two Dimensional {self.secondWords[self.type]}")
        if self.type == 1 and self.dh.type ==0:
            valid.append(4)
            print("4) Export multiple single data points")
        self.secondLevel = int(input("Enter a number and press enter: "))
        while self.secondLevel not in valid:
            print("Invalid input!")
            self.secondLevel = int(input("Enter a number and press enter: "))
        if self.histExist:
            print("A histogram data-file exists in the current folder")
            print("(1) Use scope-trace data")
            print("(2) Use histogram data")
            inp = int(input("Enter a number and press enter: "))
            while inp not in [1, 2]:
                print("Invalid input!")
                inp = int(input("Enter a number and press enter: "))
            if inp == 2:
                self.useHist = True
            else:
                self.useHist = False
        self.secondLevelMethod()


    def secondLevelMethod(self):
        heatmapType = -1
        axes_original = self.dh.context.axes.copy()
        axes = self.dh.context.axes.copy()
        missing = len(axes) - 1 
        if self.secondLevel==1:
            missing = len(axes) - 1 
            if missing >0:
                print(f"All experimental axes are {axes}, you need to enter values of {missing} axes \nstart cooords are: {self.dh.context.startCoords} \nstop cooords are: {self.dh.context.stopCoords} \nstep size are: {self.dh.context.stepSize}")
                self.printAxes(axes)
                missingVal = input(f"Enter the index of the variable axis name that you want to {self.firstWords[self.type]} and press enter: ")
                axes.remove(axes[int(missingVal)])
            else:
                print(f"All axes are {axes}, \nstart cooords are: {self.dh.context.startCoords} \nstop cooords are: {self.dh.context.stopCoords} \nstep size are: {self.dh.context.stepSize}")
                axes = []
            inputValues = self.getInputAxes(axes)
            if self.dh.type == 0:
                self.getAccumulator()
            if self.type == 0:
                self.dv.OneDPlot(axes,inputValues,self.useHist)
            if self.type == 1:
                self.dh.save1Ddata(axes, axes_original, inputValues,self.useHist)
            self.start_again()
        
        if self.secondLevel==2:
            if self.useHist:
                valid = [1,2]
                print(f"Choose what kind of heatmap you want:")
                print(f"(1) Regular heatmap")
                print(f"(2) Energy heatmap")
                heatmapType = int(input("Enter a number and press enter: "))
                while heatmapType not in valid:
                    print("Invalid input!")
                    heatmapType = int(input("Enter a number and press enter: "))
            if heatmapType == 1 or not self.useHist:
                missing = len(axes) - 2 
                if missing == 4:
                    print(f"All exprimental axes are {axes}, you need to enter values of {missing} axes \nstart cooords are: {self.dh.context.startCoords} \nstop cooords are: {self.dh.context.stopCoords} \nstep size are: {self.dh.context.stepSize}")
                    self.printAxes(axes)
                    missingVal_a = input(f"Enter the index of the variable axes name that you want to {self.firstWords[self.type]}  and press enter: ")
                    remove_a = axes[int(missingVal_a)]
                    axes.remove(remove_a)
                    
                if missing >0:
                    print(f"Exprimental axes are {axes}, you need to enter values of {missing} axes \nstart cooords are: {self.dh.context.startCoords} \nstop cooords are: {self.dh.context.stopCoords} \nstep size are: {self.dh.context.stepSize}")
                    self.printAxes(axes)
                    missingVal_a = input(f"Enter the index of the variable axes name that you want to {self.firstWords[self.type]}  and press enter: ")
                    remove_a = axes[int(missingVal_a)]
                    missingVal_b = input("Enter the index of the second variable axes name that you want to plot and press enter: ")
                    remove_b = axes[int(missingVal_b)]
                    axes.remove(remove_a)
                    axes.remove(remove_b)
                    
                if missing<1:
                    print(f"All axes are {axes}, \nstart cooords are: {self.dh.context.startCoords} \nstop cooords are: {self.dh.context.stopCoords} \nstep size are: {self.dh.context.stepSize}")
                    axes=[]

                inputValues = self.getInputAxes(axes)
                #print(axes,inputValues)
                if self.dh.type == 0:
                    self.getAccumulator()
                if self.type == 0:
                    self.dv.heatMap(axes,inputValues,self.useHist)
                if self.type == 1:
                    self.dh.save2Ddata(axes,inputValues,self.useHist)
            elif heatmapType == 2:
                print(f"Experimental axes are {axes},\nstart cooords are: {self.dh.context.startCoords} \nstop cooords are: {self.dh.context.stopCoords} \nstep size are: {self.dh.context.stepSize}")
                self.printAxes(axes)
                varAxis = int(input(f"Enter the axis you want to {self.firstWords[self.type]} : "))
                axPlot = axes[varAxis]
                axes.remove(axes[varAxis])
                vals = self.getInputAxes(axes)
                validVolts = [0, 50, 100, 200, 300, 500, 700, 1000, 1500, 2000]
                voltage = -1
                print(f"Voltage options: {validVolts}")
                while voltage not in validVolts:
                    voltage = int(input("Enter a voltage from the list above: "))
                if self.type == 0:
                    self.dv.energyHeatMap(axPlot, axes, self.axdict, vals, [self.dh.context.startCoords, self.dh.context.stopCoords, self.dh.context.stepSize], voltage, self.digits)
                elif self.type == 1:
                    self.dh.saveEnergyHeatMap(axPlot, axes, self.axdict, vals, [self.dh.context.startCoords, self.dh.context.stopCoords, self.dh.context.stepSize], voltage, self.digits)
            self.start_again()

        if self.secondLevel==3:
            axes = self.dh.context.axes.copy()
            print(f"Experimental axes are {axes},\nstart cooords are: {self.dh.context.startCoords} \nstop cooords are: {self.dh.context.stopCoords} \nstep size are: {self.dh.context.stepSize} you need to enter coordinate values for all axes and press enter ")
            inputValues = self.getInputAxes(axes)
            acc = int(self.dh.acc.value)
            self.dh.setAccumulator(0)
            if self.type == 0:
                self.dv.plotPoint(inputValues, self.useHist)
            if self.type == 1:
                self.dh.saveSingleData(inputValues, self.useHist, self.digits)
            if self.dh.type == 0:
                self.dh.setAccumulator(acc)
            self.start_again()

        if self.secondLevel == 4: 
            axes = self.dh.context.axes.copy()
            dummy = [0]*len(axes)
            print(f"Experimental axes are {axes},\nstart cooords are: {self.dh.context.startCoords} \nstop cooords are: {self.dh.context.stopCoords} \nstep size are: {self.dh.context.stepSize} you need to enter coordinate values range for one axis and fix all other axes")
            self.printAxes(axes)
            a = int(input("Enter index of variable axis and press enter: "))

            ## Do the following
            ## To the generate coords and fit coords method in the context, 
            ## fitCoords(list of fixed coords with the index of variable coords left blank, index of the variable axis, values
            # to input to the variable axis block)
            remove = axes[int(a)]

            axes.remove(remove)
            inputValues = self.getInputAxes(axes)
            print(f"You need to enter the range of coords for the axis {remove} that you want to export")

            start  = float(input("Enter the start coords and press enter: "))
            stop = float(input("Enter the stop coords and press enter: "))
            
            print(f"Fixed axes are {axes}")
            print(f"Variable axis is {axes}")
            print(f"input values are {inputValues}")
            print(f"Start and Stop values are : {start} , {stop}")
            self.dh.save1D_Bulk(axes,inputValues,start,stop, self.useHist,self.digits)
            self.start_again()




                 




    def findDigits(self,step_size):
        maxDigits = -1
        for i in step_size:
            dotIndx = str(i).find(".")+1
            if len(str(i)[dotIndx:]) > maxDigits:
                maxDigits = len(str(i)[dotIndx:])
        return maxDigits


        
    def printAxes(self,axes):
        print("---------------------------------------------------")
        print("|Experimental Axes Name | Experimental axes Index |")
        print("---------------------------------------------------")
        for i in enumerate(axes):
            print(f"|           {i[1]}           |            {i[0]}            |")
            print("---------------------------------------------------")
    
    def getInputAxes(self,axes):
        val = []
        for i in axes:
            currVal = float(input(f"Enter the coordinate value for axis {i} and press enter "))
            while currVal not in self.validVals[i]:
                print("Invalid input!")
                currVal = float(input(f"Enter the coordinate value for axis {i} and press enter "))
            val.append(currVal)
        return val
    
    def getAccumulator(self):
        print("Select an Accumulator and press enter")
        if self.useHist:
            validAccumulators = [3, 4]
        else:
            validAccumulators = [1, 2]
        counter = 1
        for acc in enumerate(Accumulator):
            if acc[0] in validAccumulators:
                if acc[0] == 4 and self.useHist:
                    print(f"({counter}) {acc[1]}")
                elif acc[0] != 4:
                    print(f"({counter}) {acc[1]}")
                counter += 1
        accum = input(":")
        self.dh.setAccumulator(validAccumulators[int(accum)-1])
    
    def start_again(self):
        again = input("enter y and press enter to start again:")
        try:
            if str(again).lower() == 'y':
                self.runPrompt()
        except:
            pass
        
        

if __name__ == "__main__":
    dm = DataManager()
    dm.runPrompt()
    #dm.printAxes(['X','Y','Z'])