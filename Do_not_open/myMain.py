# This Python file uses the following encoding: utf-8
"""
@author: Rune
"""
import time
import sys
from ctypes import * 

from PySide6.QtWidgets import QApplication, QMainWindow, QFileDialog
from PySide6.QtCore import QThread, SIGNAL
from PySide6.QtCore import QRegularExpression as QRegExp
from PySide6.QtGui import QRegularExpressionValidator as QRegExpValidator


# Important:
# You need to run the following command to generate the ui_form.py file
#     pyside6-uic form.ui -o ui_form.py, or
#     pyside2-uic form.ui -o ui_form.py
from GUI.ui_form import Ui_MainWindow
from hardware.motion_controllers import KinesisController
from hardware.camera import Camera
from GUI.popUps import cameraPopup
from pipython import GCS2Device
from experiment import MainBackgroundThread
from experimentLocKIn import MainBackgroundThreadL
from hardware.Voltage_Supply.power_supply import PowerSupply
from subprocess import Popen
import threading
import math
import json
import numpy as np
import os
import datetime
import h5py

class stagePosBackgroundThread(QThread):
    def __init__(self, controllers, labels, currentStages):
        QThread.__init__(self)
        self.controllers = controllers
        self.labels = labels
        self.axMap = {"X": 0, "Y": 1, "Z": 2, "R1": 3, "R2": 4}
        self.currentStages = currentStages
    
    def getPositions(self):
        pos = []
        for i in range(len(self.currentStages)):
            pos.append(self.controllers[self.axMap[self.currentStages[i]]].get_position())
        return pos

    def run(self):
        while True:
            pos = self.getPositions()
            for i in range(len(self.currentStages)):
                if self.currentStages[i] == "R2":
                    pos[i] = round(math.cos(pos[i] * (math.pi/180)) ** 2, 4)
                self.labels[self.currentStages[i]].setText(str(pos[i]))
            time.sleep(1)
            if self.isInterruptionRequested():
                break

class threadManager():
    def __init__(self, stageDict, positions):
        self.moveThreads = {"X": None, "Y": None, "Z": None, "R1": None, "R2": None}
        self.stageDict = stageDict
        self.positions = positions

    def move(self, axis, pos):
        print(f"Moving {axis} to {pos}")
        if self.moveThreads[axis] != None and not self.moveThreads[axis].isFinished():
            self.moveThreads[axis].wait()
        self.moveThreads[axis] = moveStage(self.stageDict, self.positions, axis, pos)
        self.moveThreads[axis].start()

class moveStage(QThread):
    def __init__(self, stageDict, positions, axis, pos):
        QThread.__init__(self)
        self.stageDict = stageDict
        self.positions = positions
        self.axis = axis
        self.pos = pos
    
    def run(self):
        self.stageDict[self.axis].move_device(self.pos)
        self.positions[self.axis] = round(self.pos, 2)
        return

class stagesPopUp():
    def __init__(self, ui):
        self.ui = ui
        self.serials = {"X": c_char_p(b'27255894'), "Y": c_char_p(b'27256338'), "Z": c_char_p(b'27004364'),"R1": c_char_p(b'27005004'), "R2": c_char_p(b'27255354')}
        XController = KinesisController(serialNumber=self.serials["X"], HomeStage=False, stageName='X-Stage', stageType='linear', lateSetup=True)
        YController = KinesisController(serialNumber=self.serials["Y"], HomeStage=False, stageName='Y-Stage', stageType='linear', lateSetup=True)
        ZController = KinesisController(serialNumber=self.serials["Z"], HomeStage=False, stageName='Z-Stage', stageType='linear', lateSetup=True)
        R1Controller = KinesisController(serialNumber=self.serials["R1"], HomeStage=False, stageName='rotating', stageType='rotational', lateSetup=True)
        R2Controller = KinesisController(serialNumber=self.serials["R2"], HomeStage=False, stageName='wiregrid', stageType='field', lateSetup=True)
        self.positions = {"X": None, "Y": None, "Z": None, "R1": None, "R2": None}
        self.axMap = {"X": 0, "Y": 1, "Z": 2, "R1": 3, "R2": 4}
        self.revAxMap = {0: "X", 1: "Y", 2: "Z", 3: "R1", 4: "R2"}
        self.allStages = [XController, YController, ZController, R1Controller, R2Controller]
        self.stageDict = {"X": XController, "Y": YController, "Z": ZController, "R1": R1Controller, "R2": R2Controller}
        self.posLabels = {"X": self.ui.pos_x_l, "Y": self.ui.pos_y_l, "Z": self.ui.pos_z_l, "R1": self.ui.pos_r1_l, "R2": self.ui.pos_r2_l}
        self.connLabels = {"X": self.ui.x_con_l, "Y": self.ui.y_con_l, "Z": self.ui.z_con_l, "R1": self.ui.r1_con_l, "R2": self.ui.r2_con_l}
        self.curPos = {"X": self.ui.curPos_x_edit, "Y": self.ui.curPos_y_edit, "Z": self.ui.curPos_z_edit, "R1": self.ui.curPos_r1_edit, "R2": self.ui.curPos_r2_edit}
        self.currentStages = []
        self.tMan = threadManager(self.stageDict, self.positions)
        self.connected = False

        self.workerThread = stagePosBackgroundThread(self.allStages, self.posLabels, self.currentStages)
        
    
    def stagesConnect(self):
        if self.connected:
            self.ui.s_connect_btn.setText("Connect")
            self.closeWindow()
            for key in self.connLabels:
                self.connLabels[key].setText("Disconnected")
                self.connLabels[key].setStyleSheet("color: black;")
            self.connected = False
        else:
            self.ui.s_connect_btn.setText("Disconnect")
            self.setupStages(self.currentStages)
            self.setConnStatus(self.currentStages)
            self.connected = True
            self.workerThread.start()
    
    def connectSingleStage(self, stage):
        self.setupStages([stage])
        self.setConnStatus([stage])

    def setConnStatus(self, stages):
        for i in range(len(stages)):
            conn = self.allStages[self.axMap[stages[i]]].lib.CC_CheckConnection(self.allStages[self.axMap[stages[i]]].serialNumber)
            if conn == 1:
                self.connLabels[stages[i]].setText("Connected")
                self.connLabels[stages[i]].setStyleSheet("color: green;")
            else:
                self.connLabels[stages[i]].setText("Disconnected")
                self.connLabels[stages[i]].setStyleSheet("color: red;")
                

    def setupStages(self, stages):
        for i in range(len(stages)):
            self.allStages[self.axMap[stages[i]]].late_setup()
            pos = self.allStages[self.axMap[stages[i]]].get_position()
            if stages[i] == "R2":
                pos = round(math.cos(pos * (math.pi/180)) ** 2, 4)
            self.positions[self.currentStages[i]] = round(pos, 2)
            self.curPos[self.currentStages[i]].setText(str(round(pos, 2)))
            #self.posLabels[stages[i]].setText(str(pos))

    def goToPos(self, axis, pos):
        print(f"Axis {axis} going to {pos}")
        self.curPos[axis].setText(str(pos))
        if axis == "R2":
            radians = math.acos(math.sqrt(pos))
            pos = radians * (180/math.pi)
        self.tMan.move(axis, pos)
        print(self.positions)
        #self.stageDict[axis].move_device(pos)
        #self.positions[axis] = round(pos, 2)

    def goToPosX(self):
        self.goToPos("X", float(self.ui.curPos_x_edit.text()))
    
    def goToPosY(self):
        self.goToPos("Y", float(self.ui.curPos_y_edit.text()))
    
    def goToPosZ(self):
        self.goToPos("Z", float(self.ui.curPos_z_edit.text()))
    
    def goToPosR1(self):
        self.goToPos("R1", float(self.ui.curPos_r1_edit.text()))
    
    def goToPosR2(self):
        self.goToPos("R2", float(self.ui.curPos_r2_edit.text()))
    
    def moveBy(self, axis, val):
        if axis == "R2":
            newPos = round(math.cos(self.positions[axis] * (math.pi/180)) ** 2, 4) + val
        else:
            newPos = self.positions[axis] + val
        self.goToPos(axis, newPos)
    
    def leftX(self):
        self.moveBy("X", -float(self.ui.step_x_edit.text()))
    
    def rightX(self):
        self.moveBy("X", float(self.ui.step_x_edit.text()))
    
    def leftY(self):
        self.moveBy("Y", -float(self.ui.step_y_edit.text()))
    
    def rightY(self):
        self.moveBy("Y", float(self.ui.step_y_edit.text()))
    
    def leftZ(self):
        self.moveBy("Z", -float(self.ui.step_z_edit.text()))
    
    def rightZ(self):
        self.moveBy("Z", float(self.ui.step_z_edit.text()))
    
    def leftR1(self):
        self.moveBy("R1", -float(self.ui.step_r1_edit.text()))
    
    def rightR1(self):
        self.moveBy("R1", float(self.ui.step_r1_edit.text()))
    
    def leftR2(self):
        self.moveBy("R2", -float(self.ui.step_r2_edit.text()))
    
    def rightR2(self):
        self.moveBy("R2", float(self.ui.step_r2_edit.text()))
    
    def closeWindow(self):
        self.workerThread.requestInterruption()
        self.workerThread.wait()
        for i in self.allStages:
            i.clean_up_device()

class posBackgroundThread(QThread):
    def __init__(self, gcs, axis, labels, currentStages):
        QThread.__init__(self)
        self.gcs = gcs
        self.labels = labels
        self.axis = axis
        self.currentStages = currentStages
        self.AXmap = {'VX':'1','VY':'2','VZ':'3'}
        self.revAXmap = {'1': 'VX', '2': 'VY', '3': 'VZ'}

    def run(self):
        while True:
            ax = [self.AXmap[x] for x in sorted(self.currentStages)]
            pos = self.gcs.qPOS(ax)
            for i in pos:
                self.labels[int(i) - 1].setText(str(pos[i]))
            time.sleep(1)
            if self.isInterruptionRequested():
                break

class vacPopUp():
    def __init__(self, ui):
        self.ui = ui
        self.device = GCS2Device()
        self.AXmap = {'VX':'1','VY':'2','VZ':'3'}
        self.revAXmap = {'1': 'VX', '2': 'VY', '3': 'VZ'}

        self.curpos = {'1': self.ui.curPos_vx_edit, '2': self.ui.curPos_vy_edit, '3': self.ui.curPos_vz_edit}
        self.steps = {'1': self.ui.step_vx_edit, '2': self.ui.step_vy_edit, '3': self.ui.step_vz_edit}
        self.positions = {'VX': None, 'VY': None, 'VZ': None}
        for i in self.steps:
            self.steps[i].setText("0.5")

        self.currentStages = []

        self.allAxes = []
        self.allAxes_n = []

        updateLabels = [self.ui.pos_vx_l, self.ui.pos_vy_l, self.ui.pos_vz_l]

        self.connected = False

        self.workerThread = posBackgroundThread(self.device, self.allAxes_n, updateLabels, self.currentStages)
        
    
    def reference(self, axis):
        #self.device.REF(axis)
        
        self.device.FRF(axis)
        while not self.device.IsControllerReady():
            pass
        for i in axis:
            self.curpos[i].setText("0.0")
            self.positions[self.revAXmap[i]] = 0.0

    def referenceVX(self):
        self.reference("1")

    def referenceVY(self):
        self.reference("2")

    def referenceVZ(self):
        self.reference("3")
    
    def referenceALL(self):
        self.reference(["1", "2", "3"])

    def connectVac(self):
        self.device.ConnectTCPIP(ipaddress='192.168.1.3')
        self.connected = True
        self.ui.v_connect_btn.setText("Disconnect")
        self.workerThread.start()
        self.setupVac()
    
    def disconnectVac(self):
        self.workerThread.requestInterruption()
        self.workerThread.wait()
        axis = self.device.qSAI_ALL()
        axisNames = []
        for i in axis:
            axisNames.append(self.revAXmap[i])
        self.setConStatus(axisNames)
        self.device.close()
        self.ui.v_connect_btn.setText("Connect")
        self.connected = False
    
    def setupVac(self, mode=True):
        axis = self.device.qSAI_ALL()
        axisNames = []
        for i in axis:
            self.device.SVO(i, mode)
            axisNames.append(self.revAXmap[i])
        self.setConStatus(axisNames)
        if mode:
            self.getInitPos()
    
    def getInitPos(self):
        axNums = []
        for i in self.allAxes:
            axNums.append(self.AXmap[i])
        pos = self.getPos(axNums)
        for i in pos:
            self.curpos[i].setText(str(round(pos[i], 2)))
            self.positions[self.revAXmap[i]] = round(pos[i], 2)
        
    
    def getPos(self, axes):
        pos = self.device.qPOS(axes)
        return pos
    
    def setConStatus(self, axis):
        if "VX" in axis: 
            self.ui.vx_con_l.setText("Connected")
            self.ui.vx_con_l.setStyleSheet("color: green;")
            self.allAxes.append("VX")
            self.allAxes_n.append("1")
        else:
            self.ui.vx_con_l.setText("Disconnected")
            self.ui.vx_con_l.setStyleSheet("color: red;")
        if "VY" in axis: 
            self.ui.vy_con_l.setText("Connected")
            self.ui.vy_con_l.setStyleSheet("color: green;")
            self.allAxes.append("VY")
            self.allAxes_n.append("2")
        else:
            self.ui.vy_con_l.setText("Disconnected")
            self.ui.vy_con_l.setStyleSheet("color: red;")
        if "VZ" in axis: 
            self.ui.vz_con_l.setText("Connected")
            self.ui.vz_con_l.setStyleSheet("color: green;")
            self.allAxes.append("VZ")
            self.allAxes_n.append("3")
        else:
            self.ui.vz_con_l.setText("Disconnected")
            self.ui.vz_con_l.setStyleSheet("color: red;")
    
    def moveBy(self, axis, val):
        try:
            self.device.send(f"MVR {self.AXmap[axis]} {val}\n")
            self.positions[axis] = round(self.positions[axis] + val, 2)
            self.updateCurPos()
        except:
            print(f"Moving axis {axis} by {val} failed...")
    
    def updateCurPos(self):
        for i in self.curpos:
            self.curpos[i].setText(str(self.positions[self.revAXmap[i]]))

    def leftVX(self):
        if len(self.ui.step_vx_edit.text()) != 0: 
            self.moveBy("VX", -float(self.ui.step_vx_edit.text()))
    
    def rightVX(self):
        if len(self.ui.step_vx_edit.text()) != 0: 
            self.moveBy("VX", float(self.ui.step_vx_edit.text()))
    
    def leftVY(self):
        if len(self.ui.step_vy_edit.text()) != 0: 
            self.moveBy("VY", -float(self.ui.step_vy_edit.text()))
    
    def rightVY(self):
        if len(self.ui.step_vy_edit.text()) != 0: 
            self.moveBy("VY", float(self.ui.step_vy_edit.text()))
    
    def leftVZ(self):
        if len(self.ui.step_vz_edit.text()) != 0: 
            self.moveBy("VZ", -float(self.ui.step_vz_edit.text()))
    
    def rightVZ(self):
        if len(self.ui.step_vz_edit.text()) != 0: 
            self.moveBy("VZ", float(self.ui.step_vz_edit.text()))
    
    def getParent(self, parent):
        self.myParent = parent
        if not self.device.connected:
            self.device.ConnectTCPIP(ipaddress='192.168.1.3')
            self.getInitPos()
            self.workerThread.start()

    def goToPos(self, axis, pos):
        self.device.send(f'MOV {self.AXmap[axis]} {pos}\n')
        self.positions[axis] = pos
        self.updateCurPos()
        axNums = []
        for i in self.allAxes:
            axNums.append(self.AXmap[i])

    def goToPosVX(self):
        if len(self.ui.curPos_vx_edit.text()) != 0:
            self.goToPos("VX", float(self.ui.curPos_vx_edit.text()))
    
    def goToPosVY(self):
        if len(self.ui.curPos_vy_edit.text()) != 0:
            self.goToPos("VY", float(self.ui.curPos_vy_edit.text()))
    
    def goToPosVZ(self):
        if len(self.ui.curPos_vz_edit.text()) != 0:
            self.goToPos("VZ", float(self.ui.curPos_vz_edit.text()))
    
    def closeWindow(self):
        self.workerThread.requestInterruption()
        self.workerThread.wait()
        self.device.close()
    
class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle("Experiment")
        timePath = os.path.join(os.path.join("Do_not_open", "timing"), "avgTimeData.json")
        self.timeDict = None
        if os.path.exists(timePath):
            self.timeDict = json.load(open(timePath, "r"))

        self.checkboxes = [self.ui.x_c, self.ui.y_c, self.ui.z_c, self.ui.r1_c, self.ui.r2_c, self.ui.r3_c, 
                            self.ui.vx_c, self.ui.vy_c, self.ui.vz_c, self.ui.s0_c, self.ui.s1_c, self.ui.s2_c, 
                            self.ui.s3_c]
        self.home_checkboxes = [self.ui.home_x_c, self.ui.home_y_c, self.ui.home_z_c,
                                self.ui.home_r1_c, self.ui.home_r2_c, self.ui.home_r3_c,
                                self.ui.home_vx_c, self.ui.home_vy_c, self.ui.home_vz_c]
        self.home_after_checkboxes = [self.ui.home_vx_a_c, self.ui.home_vy_a_c, self.ui.home_vz_a_c]
        self.startframes = [self.ui.x_start_frame, self.ui.y_start_frame, self.ui.z_start_frame, 
                            self.ui.r1_start_frame, self.ui.r2_start_frame, self.ui.r3_start_frame, 
                            self.ui.vx_start_frame, self.ui.vy_start_frame, self.ui.vz_start_frame, 
                            self.ui.s0_start_frame, self.ui.s1_start_frame, self.ui.s2_start_frame, 
                            self.ui.s3_start_frame]
        self.stopframes = [self.ui.x_stop_frame, self.ui.y_stop_frame, self.ui.z_stop_frame, 
                            self.ui.r1_stop_frame, self.ui.r2_stop_frame, self.ui.r3_stop_frame, 
                            self.ui.vx_stop_frame, self.ui.vy_stop_frame, self.ui.vz_stop_frame, 
                            self.ui.s0_stop_frame, self.ui.s1_stop_frame, self.ui.s2_stop_frame, 
                            self.ui.s3_stop_frame]
        self.stepframes = [self.ui.x_step_frame, self.ui.y_step_frame, self.ui.z_step_frame, 
                            self.ui.r1_step_frame, self.ui.r2_step_frame, self.ui.r3_step_frame, 
                            self.ui.vx_step_frame, self.ui.vy_step_frame, self.ui.vz_step_frame, 
                            self.ui.s0_step_frame, self.ui.s1_step_frame, self.ui.s2_step_frame, 
                            self.ui.s3_step_frame]
        self.ordframes = [self.ui.x_ord_frame, self.ui.y_ord_frame, self.ui.z_ord_frame, 
                            self.ui.r1_ord_frame, self.ui.r2_ord_frame, self.ui.r3_ord_frame, 
                            self.ui.vx_ord_frame, self.ui.vy_ord_frame, self.ui.vz_ord_frame, 
                            self.ui.s0_ord_frame, self.ui.s1_ord_frame, self.ui.s2_ord_frame, 
                            self.ui.s3_ord_frame]
        self.spinboxes = [self.ui.x_spin, self.ui.y_spin, self.ui.z_spin, 
                            self.ui.r1_spin, self.ui.r2_spin, self.ui.r3_spin, 
                            self.ui.vx_spin, self.ui.vy_spin, self.ui.vz_spin, 
                            self.ui.s0_spin, self.ui.s1_spin, self.ui.s2_spin, 
                            self.ui.s3_spin]
        self.startinputs = [self.ui.x_start_inp, self.ui.y_start_inp, self.ui.z_start_inp, 
                            self.ui.r1_start_inp, self.ui.r2_start_inp, self.ui.r3_start_inp, 
                            self.ui.vx_start_inp, self.ui.vy_start_inp, self.ui.vz_start_inp, 
                            self.ui.s0_start_inp, self.ui.s1_start_inp, self.ui.s2_start_inp, 
                            self.ui.s3_start_inp]
        self.stopinputs = [self.ui.x_stop_inp, self.ui.y_stop_inp, self.ui.z_stop_inp, 
                            self.ui.r1_stop_inp, self.ui.r2_stop_inp, self.ui.r3_stop_inp, 
                            self.ui.vx_stop_inp, self.ui.vy_stop_inp, self.ui.vz_stop_inp, 
                            self.ui.s0_stop_inp, self.ui.s1_stop_inp, self.ui.s2_stop_inp, 
                            self.ui.s3_stop_inp]
        self.stepinputs = [self.ui.x_step_inp, self.ui.y_step_inp, self.ui.z_step_inp, 
                            self.ui.r1_step_inp, self.ui.r2_step_inp, self.ui.r3_step_inp, 
                            self.ui.vx_step_inp, self.ui.vy_step_inp, self.ui.vz_step_inp, 
                            self.ui.s0_step_inp, self.ui.s1_step_inp, self.ui.s2_step_inp, 
                            self.ui.s3_step_inp]
        self.kinesisStages = [self.ui.x_c, self.ui.y_c, self.ui.z_c, self.ui.r1_c, self.ui.r2_c]
        self.vaccumStages = [self.ui.vx_c, self.ui.vy_c, self.ui.vz_c]
        self.labelMap = {"X": 0, "Y": 1, "Z": 2, "R1": 3, "R2": 4, "R3": 5, "VX": 6, "VY": 7, "VZ": 8, "S0": 9, "S1": 10, "S2": 11, "S3": 12}
        self.revLabelMap = {0: "X", 1: "Y", 2: "Z", 3: "R1", 4: "R2", 5: "R3", 6: "VX", 7: "VY", 8: "VZ", 9: "S0", 10: "S1", 11: "S2", 12: "S3"}
        self.actives = []
        self.activespins = []
        self.activeframes = []
        self.frameX = []
        self.errormsg = ""
        reg_ex = QRegExp("-{0,1}[0-9]+(\.[0-9]+){1}$")
        self.englishWords = ["Oscilloscope", "Dummy", "Experiment Name", "Linear stages", "Home", "Rotation stages",
                             "Home", "Wait time:", "Minimum exp time:", "Coordinates & step size", "Total Steps: ", "Starting coordinates",
                             "Select stages/special axes to begin...", "Stopping coordinates", "Select stages/special axes to begin...", "Step size",
                             "Select stages/special axes to begin...", "Axes ordering",
                             "Select which stage turns first by selecting '1' on that stage, the stage with '2' selected will turn second etc.",
                             "Select stages/special axes to begin...", "Save options", "Load options", "Stop experiment",
                             "Run experiment"]
        self.labelsToTranslate = [self.ui.osc_radio, self.ui.dummy_radio, self.ui.label, self.ui.label_10, self.ui.label_57, self.ui.label_53,
                                  self.ui.label_58, self.ui.label_54, self.ui.label_3, self.ui.label_6, self.ui.totalsteps_text_l, self.ui.label_7,
                                  self.ui.nostart_l, self.ui.label_22, self.ui.nostop_l, self.ui.label_36,
                                  self.ui.nostep_l, self.ui.label_50,
                                  self.ui.label_51,
                                  self.ui.noord_l, self.ui.save_btn, self.ui.load_btn, self.ui.stop_exp_btn,
                                  self.ui.run_btn]
        self.englishErrorMesseges = ["Please select either the Oscilloscope, Lockin or Dummy from the top-left side", "Please give the experiment a name at the top of the window",
                                     "Please check at least one of the checkboxes", "Please make sure that all input fields have a valid value", "Please make sure that the axes ordering have the correct numbers (1 to {})",
                                     "Please input a wait-time in seconds", "Please close the Vaccum-Controller before running an experiment with vaccum-stages", "R2 is in normalized units, meaning it has to move in the interval [0;1]",
                                     "Since you are using the ammeter, make sure that\nIntegrationTime * Measurements = WaitTime", "Please let the current experiment finish before starting a new one"]
        inp_val = QRegExpValidator(reg_ex, self.ui.SST_edit)
        self.ui.SST_edit.setValidator(inp_val)
        self.ui.SST_edit.setText("0.5")

        self.ordering = []

        self.axes = []
        self.getInstrumentHandler = ""
        self.getScanner = ""
        self.experimentName = ""
        self.startCoords = ""
        self.stopCoords = ""
        self.step_size = ""
        self.moduleType = ""
        self.stageSettleTime = ""
        self.useAmperemeter = ""
        self.resolution = ""
        self.readerType = ""
        self.integrationTime = ""
        self.numberOfMeassurements = ""

        for i in range(len(self.startframes)):
            input_validator1 = QRegExpValidator(reg_ex, self.startinputs[i])
            input_validator2 = QRegExpValidator(reg_ex, self.stopinputs[i])
            input_validator3 = QRegExpValidator(reg_ex, self.stepinputs[i])
            self.startinputs[i].setValidator(input_validator1)
            self.stopinputs[i].setValidator(input_validator2)
            self.stepinputs[i].setValidator(input_validator3)
        
        self.ui.nostart_l.setVisible(False)
        for i in range(len(self.startframes)):
            self.frameX.append(self.startframes[i].x())
        self.ui.time_c.stateChanged.connect(self.updateTime)
        inp_val_wait = QRegExpValidator(reg_ex, self.ui.wait_inp)
        self.ui.wait_inp.setValidator(inp_val_wait)
        self.ui.vx_c.stateChanged.connect(self.vxToggled)
        self.ui.vy_c.stateChanged.connect(self.vyToggled)
        self.ui.vz_c.stateChanged.connect(self.vzToggled)

        for i in range(len(self.kinesisStages)):
            self.kinesisStages[i].stateChanged.connect(lambda state, x=i: self.kinesisChange(x))
        
        for i in range(len(self.vaccumStages)):
            self.vaccumStages[i].stateChanged.connect(lambda state, x=i: self.vaccumChange(x))
        
        for i in self.checkboxes:
            i.stateChanged.connect(self.checkOnlyR2)
            i.stateChanged.connect(self.updateStartFrames)
            i.stateChanged.connect(self.updateStopFrames)
            i.stateChanged.connect(self.updateStepFrames)
            i.stateChanged.connect(self.updateOrdFrames)
            i.stateChanged.connect(self.calcTotalSteps)
        self.ui.amp_c.stateChanged.connect(self.updateAmpInfo)
        self.ui.osc_radio.toggled.connect(self.radioChange)
        self.ui.lockin_radio.toggled.connect(self.radioChange)
        self.ui.dummy_radio.toggled.connect(self.radioChange)
        self.ui.onlyamp_radio.toggled.connect(self.radioChange)
        self.ui.onlyamp_radio.toggled.connect(self.ampChosen)
        
        for i, k, j in zip(self.startinputs, self.stopinputs, self.stepinputs):
            i.textChanged.connect(self.calcTotalSteps)
            k.textChanged.connect(self.calcTotalSteps)
            j.textChanged.connect(self.calcTotalSteps)
        self.ui.wait_inp.textChanged.connect(self.calcTotalSteps)
        self.ui.wait_inp.textChanged.connect(self.resetSave)
        self.ui.dummy_PS_c.stateChanged.connect(self.resetSave)
        self.ui.intTime_edit.textChanged.connect(self.resetSave)
        self.ui.nofm_spin.valueChanged.connect(self.resetSave)
        self.ui.SST_edit.textChanged.connect(self.resetSave)
        self.ui.SST_edit.textChanged.connect(self.calcMinTime)
        for i in range(len(self.spinboxes)):
            self.spinboxes[i].valueChanged.connect(self.resetSave)
            self.spinboxes[i].valueChanged.connect(lambda state, x=i: self.autoReorder(x))
            self.spinboxes[i].valueChanged.connect(self.calcMinTime)
            
        self.spinboxValues = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.activeAxes = 0
        for i in range(len(self.home_checkboxes)):
            self.home_checkboxes[i].stateChanged.connect(self.homeUpdate)
        for i in range(len(self.home_after_checkboxes)):
            self.home_after_checkboxes[i].stateChanged.connect(self.homeUpdate)
        
        self.ui.datManager_btn.clicked.connect(self.runDatManager)
        
        self.actionDataDict_rev = {0: "X", 1: "Y", 2: "Z", 3: "R1", 4: "R2", 5: "R3", 6: "VX", 7: "VY", 8: "VZ",
                                   9: "S0", 10: "S1", 11: "S2", 12: "S3", 13: "TimeStage"}
        self.stepTimes = {"X": [], "Y": [], "Z": [], "R1": [], "R2": [], "R3": [],
                          "VX": [], "VY": [], "VZ": [], "S0": [], "S1": [], "S2": [], "S3": [], "TimeStage": []}
        self.vacFrames = [self.ui.vx_frame, self.ui.vy_frame, self.ui.vz_frame]
        self.kinesisFrames = [self.ui.stage_x_frame, self.ui.stage_y_frame, self.ui.stage_z_frame, self.ui.stage_r1_frame, self.ui.stage_r2_frame]

        self.ui.datManager_btn.setVisible(False)

        self.currentProgress = 0

        self.updateTime()
        self.updateStartFrames()
        self.updateStopFrames()
        self.updateStepFrames()
        self.updateOrdFrames()
        self.updateAmpInfo()
        self.radioChange()
        self.checkOnlyR2()

        self.start_time = -1
        self.end_time = -1
        self.actual_time = -1
        self.time_dict = {}
        self.directory = ""
        self.totalActions = -1

        self.worker = None
        cwd = os.getcwd()
        self.stages = stagesPopUp(self.ui)
        os.chdir(cwd)

        self.ui.s_connect_btn.setVisible(len(self.stages.currentStages))

        for i in self.kinesisFrames:
            i.setVisible(False)

        self.ui.go_x_btn.clicked.connect(self.stages.goToPosX)
        self.ui.go_y_btn.clicked.connect(self.stages.goToPosY)
        self.ui.go_z_btn.clicked.connect(self.stages.goToPosZ)
        self.ui.go_r1_btn.clicked.connect(self.stages.goToPosR1)
        self.ui.go_r2_btn.clicked.connect(self.stages.goToPosR2)
        self.ui.left_x_btn.clicked.connect(self.stages.leftX)
        self.ui.right_x_btn.clicked.connect(self.stages.rightX)
        self.ui.left_y_btn.clicked.connect(self.stages.leftY)
        self.ui.right_y_btn.clicked.connect(self.stages.rightY)
        self.ui.left_z_btn.clicked.connect(self.stages.leftZ)
        self.ui.right_z_btn.clicked.connect(self.stages.rightZ)
        self.ui.left_r1_btn.clicked.connect(self.stages.leftR1)
        self.ui.right_r1_btn.clicked.connect(self.stages.rightR1)
        self.ui.left_r2_btn.clicked.connect(self.stages.leftR2)
        self.ui.right_r2_btn.clicked.connect(self.stages.rightR2)
        self.ui.ref_x_btn.clicked.connect(lambda: self.stages.goToPos("X", 0))
        self.ui.ref_y_btn.clicked.connect(lambda: self.stages.goToPos("Y", 0))
        self.ui.ref_z_btn.clicked.connect(lambda: self.stages.goToPos("Z", 0))
        self.ui.ref_r1_btn.clicked.connect(lambda: self.stages.goToPos("R1", 0))
        self.ui.ref_r2_btn.clicked.connect(lambda: self.stages.goToPos("R2", 1))

        self.ui.s_connect_btn.clicked.connect(self.stages.stagesConnect)

        for i in self.vacFrames:
            i.setVisible(False)

        self.vac = vacPopUp(self.ui)
        self.vacConnections = [False, False, False]

        self.ui.go_vx_btn.clicked.connect(self.vac.goToPosVX)
        self.ui.go_vy_btn.clicked.connect(self.vac.goToPosVY)
        self.ui.go_vz_btn.clicked.connect(self.vac.goToPosVZ)
        self.ui.left_vx_btn.clicked.connect(self.vac.leftVX)
        self.ui.right_vx_btn.clicked.connect(self.vac.rightVX)
        self.ui.left_vy_btn.clicked.connect(self.vac.leftVY)
        self.ui.right_vy_btn.clicked.connect(self.vac.rightVY)
        self.ui.left_vz_btn.clicked.connect(self.vac.leftVZ)
        self.ui.right_vz_btn.clicked.connect(self.vac.rightVZ)
        self.ui.ref_vx_btn.clicked.connect(self.vac.referenceVX)
        self.ui.ref_vy_btn.clicked.connect(self.vac.referenceVY)
        self.ui.ref_vz_btn.clicked.connect(self.vac.referenceVZ)

        self.ui.v_connect_btn.clicked.connect(self.connectVac)

        self.vacAXmap = {'VX':'1','VY':'2','VZ':'3'}
        self.vacRevAXmap = {'1': 'VX', '2': 'VY', '3': 'VZ'}
        
        self.vaccumToggled(0, 0)

        self.ui.run_btn.clicked.connect(self.runExp)
        self.ui.save_btn.clicked.connect(self.saveOptions)
        self.ui.load_btn.clicked.connect(self.loadOptions)
        self.ui.stop_exp_btn.clicked.connect(self.stopExp)

        self.ui.cam_conf_btn.clicked.connect(self.camPreview)
        self.ui.cam_snap_btn.clicked.connect(self.camSnapShot)

        self.snapShotPath = None
        self.camPopup = cameraPopup(self)
        self.cam = None

        self.ui.n_edit.setText("1")
    
    def autoReorder(self, c):
        new_val = self.spinboxes[c].value()
        old_val = self.spinboxValues[c]
        swap_axes = -1
        if old_val != 0:
            for i in range(len(self.spinboxValues)):
                if self.spinboxValues[i] == new_val:
                    swap_axes = i
                    break
            self.spinboxes[swap_axes].setValue(old_val)
            self.spinboxValues[swap_axes] = old_val
        self.spinboxValues[c] = new_val
    
    def ampChosen(self):
        state = self.ui.onlyamp_radio.isChecked()
        if state:
            self.ui.amp_c.setChecked(False)
        self.ui.amp_c.setVisible(not state)
        self.updateAmpInfo(overrideState=state)

    
    def checkOnlyR2(self):
        onlyR2 = self.ui.r2_c.isChecked()
        if onlyR2:
            for i in range(len(self.checkboxes)):
                if self.checkboxes[i].isChecked():
                    if i != 4:
                        onlyR2 = False
                        break

        self.ui.n_label.setVisible(onlyR2)
        self.ui.n_edit.setVisible(onlyR2)
                
    
    def getSnapshotPath(self):
        counter = 1
        imgName = f"Exposure-{self.ui.exposure_l.text()}_Gain-{self.ui.gain_l.text()}"
        name = os.path.join(self.snapShotPath, f"{imgName}.png")
        while os.path.exists(name):
            name = os.path.join(self.snapShotPath, f"{imgName}-{counter}.png")
            counter += 1
        return name
    
    def camSnapShot(self):
        if self.cam == None:
            self.cam = Camera()
        exposure = self.ui.exposure_l.text()
        gain = self.ui.gain_l.text()
        if self.isValid(exposure) and self.isValid(gain):
            if self.snapShotPath == None:
                self.snapShotPath = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
            self.cam.create_snapshot(exposure=float(exposure), gain=float(gain))
            res = self.cam.TakeSnapshot()
            path = self.getSnapshotPath()
            self.cam.SaveImage(res, path)
    
    def camPreview(self):
        
        if self.cam == None:
            self.cam = Camera()
        if self.cam.camera._displaying_window:
            self.cam.camera.StreamVideoControl("stop_streaming")
            self.cam.camera.DestroyDisplayWindow()
        self.cam.setProperty(20, int(self.ui.exposure_l.text()))
        self.cam.setProperty(40, int(self.ui.gain_l.text()))
        
        if not self.camPopup.shown:
            self.camPopup.show()
            self.camPopup.shown = True
        
        self.cam.camera.CreateDisplayWindow(title=b"Live Camera Preview")
        self.cam.camera.StreamVideoControl("start_display")
        
    
    def changeExposureLimits(self, fromMainWindow):
        lower = self.camPopup.ui.exposure_from_edit.text()
        upper = self.camPopup.ui.exposure_to_edit.text()
        if self.isValid(lower) and self.isValid(upper):
            self.camPopup.ui.exposure_slider.setMinimum(float(lower))
            self.camPopup.ui.exposure_slider.setMaximum(float(upper))
    
    def changeGainLimits(self, fromMainWindow):
        lower = self.camPopup.ui.gain_from_edit.text()
        upper = self.camPopup.ui.gain_to_edit.text()
        if self.isValid(lower) and self.isValid(upper):
            self.camPopup.ui.gain_slider.setMinimum(float(lower))
            self.camPopup.ui.gain_slider.setMaximum(float(upper))
    
    def changeExposure(self, slider):
        self.ui.exposure_l.setText(str(slider.value()))
        self.camPopup.ui.exposure_cur_edit.setText(str(slider.value()))
        if self.cam == None:
            return
        if self.isValid(str(slider.value())):
            self.cam.setProperty(20, slider.value())
    
    def changeGain(self, slider):
        self.ui.gain_l.setText(str(slider.value()))
        self.camPopup.ui.gain_cur_edit.setText(str(slider.value()))
        if self.cam == None:
            return
        if self.isValid(str(slider.value())):
            self.cam.setProperty(40, slider.value())
    
    def setExposure(self, fromMainWindow):
        exposure = self.camPopup.ui.exposure_cur_edit.text()
        if self.isValid(exposure):
            self.camPopup.ui.exposure_slider.setValue(int(exposure))
    
    def setGain(self, fromMainWindow):
        gain = self.camPopup.ui.gain_cur_edit.text()
        if self.isValid(gain):
            self.camPopup.ui.gain_slider.setValue(int(gain))

    def vaccumToggled(self, c, n):
        axName = self.vacRevAXmap[str(n+1)]
        if c:
            self.vacConnections[n] = True
            if not self.ui.v_connect_btn.isVisible():
                self.ui.v_connect_btn.setVisible(True)
            self.vacFrames[n].setVisible(True)
        else:
            self.vacConnections[n] = False
            if not any(self.vacConnections) == True:
                self.ui.v_connect_btn.setVisible(False)
            self.vacFrames[n].setVisible(False)
    
    def vxToggled(self, c):
        self.vaccumToggled(c, 0)

    def vyToggled(self, c):
        self.vaccumToggled(c, 1)
    
    def vzToggled(self, c):
        self.vaccumToggled(c, 2)
    
    def connectVac(self):
        if self.vac.connected:
            self.vac.disconnectVac()
        else:
            self.vac.connectVac()
    
    def closeEvent(self, event):
        if self.vac.connected:
            self.vac.closeWindow()
        if self.stages.connected:
            self.stages.closeWindow()
    
    def kinesisChange(self, c):
        self.kinesisFrames[c].setVisible(self.kinesisStages[c].isChecked())
        if self.kinesisStages[c].isChecked():
            self.stages.currentStages.append(self.actionDataDict_rev[c])
            if self.stages.connected:
                self.stages.connectSingleStage(self.actionDataDict_rev[c])
            #self.stages.allStages[c].late_setup()
        else:
            self.stages.currentStages.remove(self.actionDataDict_rev[c])
            #self.stages.allStages[c].clean_up_device()
        self.ui.s_connect_btn.setVisible(len(self.stages.currentStages))
        
    
    def vaccumChange(self, c):
        if self.vaccumStages[c].isChecked():
            self.vac.currentStages.append(self.actionDataDict_rev[c+6])
        else:
            self.vac.currentStages.remove(self.actionDataDict_rev[c+6])

    def stopExp(self):
        if self.worker == None:
            return
        self.worker.requestInterruption()

    def resetSave(self):
        self.ui.saved_l.setText("")
    
    def homeUpdate(self):
        self.resetSave()
        for i in range(len(self.home_checkboxes)):
            if self.home_checkboxes[i].isChecked() and not self.checkboxes[i].isChecked():
                self.home_checkboxes[i].setChecked(False)
        for i in range(len(self.home_after_checkboxes)):
            if self.home_after_checkboxes[i].isChecked() and not self.checkboxes[i + 6].isChecked():
                self.home_after_checkboxes[i].setChecked(False)
            
    
    def getNewFilePath(self, filepath:'str') -> 'str': #If filepath exists add "-n" before the file extension (Before: test.json After: test-1.json)
        count = 0
        if os.path.exists(filepath):
            count = 1
            dotindx = filepath[1:].index(".")
            while os.path.exists(f"{filepath[:dotindx + 1]}-{count}{filepath[dotindx + 1:]}"):
                count += 1
        else:
            return filepath
        return f"{filepath[:dotindx + 1]}-{count}{filepath[dotindx + 1:]}"

    def saveOptions(self, running=False):
        saveDict = {}
        expname = self.ui.name_inp.text()
        if len(expname) == 0:
            self.reportError(1)
            return
        saveDict["expname"] = expname
        if self.ui.osc_radio.isChecked(): saveDict["radio"] = "osc"
        if self.ui.lockin_radio.isChecked(): saveDict["radio"] = "lockin"
        if self.ui.dummy_radio.isChecked(): saveDict["radio"] = "dummy"
        if self.ui.onlyamp_radio.isChecked(): saveDict["radio"] = "onlyamp"
        
        checkboxValues = []
        home_checkboxValues = []
        home_after = []
        for i in range(len(self.checkboxes)):
           checkboxValues.append(self.checkboxes[i].isChecked())
        for i in range(len(self.home_checkboxes)):
            home_checkboxValues.append(self.home_checkboxes[i].isChecked())
        for i in range(len(self.home_after_checkboxes)):
            home_after.append(self.home_after_checkboxes[i].isChecked())
        saveDict["checkboxes"] = checkboxValues
        saveDict["home_checkboxes"] = home_checkboxValues
        saveDict["home_after_checkboxes"] = home_after
        saveDict["timebox"] = self.ui.time_c.isChecked()
        saveDict["SST"] = self.ui.SST_edit.text()
        saveDict["ampmeter"] = self.ui.amp_c.isChecked()
        saveDict["inttime"] = self.ui.intTime_edit.text()
        saveDict["meas"] = self.ui.nofm_spin.value()
        startVals = []
        stopVals = []
        stepVals = []
        ordVals = []
        for i in range(len(self.startinputs)):
            startVals.append(self.startinputs[i].text())
            stopVals.append(self.stopinputs[i].text())
            stepVals.append(self.stepinputs[i].text())
            ordVals.append(self.spinboxes[i].value())
        saveDict["start"] = startVals
        saveDict["stop"] = stopVals
        saveDict["step"] = stepVals
        saveDict["ord"] = ordVals
        saveDict["waittime"] = self.ui.wait_inp.text()
        saveDict["dummyps"] = self.ui.dummy_PS_c.isChecked()
        saveDict["exposure"] = self.ui.exposure_l.text()
        saveDict["gain"] = self.ui.gain_l.text()
        saveDict["camChecked"] = self.ui.cam_c.isChecked()
        saveDict["exposureMin"] = self.camPopup.ui.exposure_from_edit.text()
        saveDict["exposureMax"] = self.camPopup.ui.exposure_to_edit.text()
        saveDict["gainMin"] = self.camPopup.ui.gain_from_edit.text()
        saveDict["gainMax"] = self.camPopup.ui.gain_to_edit.text()
        axes = []
        for i in range(len(self.checkboxes)):
            if self.checkboxes[i].isChecked():
                axes.append(self.revLabelMap[i])
        if self.ui.time_c.isChecked():
            axes = ["TimeStage"]
        saveDict["axes"] = axes
        saveDict["oscChannel"] = self.ui.channel_spin.value()
        saveDict["noHist"] = self.ui.no_histogram_c.isChecked()
        saveDict["onlyamp"] = self.ui.onlyamp_radio.isChecked()
        if running:
            return saveDict
        if not os.path.isdir("./savedOptions"):
            os.mkdir("./savedOptions")
        path = self.getNewFilePath(f"./savedOptions/{expname}.json")
        with open(path, "w") as outfile:
            json.dump(saveDict, outfile)
        date = datetime.datetime.now().strftime("%H:%M:%S")
        self.ui.saved_l.setText(f"Saved {date}")
        
    def loadOptions(self):
        self.resetSave()
        filename, _ = QFileDialog.getOpenFileName(self, ("Open File"), "./savedOptions")
        file = open(filename, "r")
        saveDict = json.load(file)
        file.close()
        self.ui.name_inp.setText(saveDict["expname"])
        if saveDict["radio"] == "osc":
            self.ui.osc_radio.setChecked(True)
            self.ui.lockin_radio.setChecked(False)
            self.ui.dummy_radio.setChecked(False)
            self.ui.onlyamp_radio.setChecked(False)
        if saveDict["radio"] == "lockin":
            self.ui.osc_radio.setChecked(False)
            self.ui.lockin_radio.setChecked(True)
            self.ui.dummy_radio.setChecked(False)
            self.ui.onlyamp_radio.setChecked(False)
        if saveDict["radio"] == "dummy":
            self.ui.osc_radio.setChecked(False)
            self.ui.lockin_radio.setChecked(False)
            self.ui.dummy_radio.setChecked(True)
            self.ui.onlyamp_radio.setChecked(False)
        if saveDict["radio"] == "onlyamp":
            self.ui.osc_radio.setChecked(False)
            self.ui.lockin_radio.setChecked(False)
            self.ui.dummy_radio.setChecked(False)
            self.ui.onlyamp_radio.setChecked(True)
        
        checkboxes = saveDict["checkboxes"]
        for i in range(len(self.checkboxes)):
            self.checkboxes[i].setChecked(checkboxes[i])
        home_checkboxes = saveDict["home_checkboxes"]
        for i in range(len(self.home_checkboxes)):
            self.home_checkboxes[i].setChecked(home_checkboxes[i])
        self.ui.time_c.setChecked(saveDict["timebox"])
        self.ui.amp_c.setChecked(saveDict["ampmeter"])
        self.ui.intTime_edit.setText(saveDict["inttime"])
        self.ui.nofm_spin.setValue(saveDict["meas"])
        startVals = saveDict["start"]
        stopVals = saveDict["stop"]
        stepVals = saveDict["step"]
        ordVals = saveDict["ord"]
        for i in range(len(self.startinputs)):
            self.startinputs[i].setText(startVals[i])
            self.stopinputs[i].setText(stopVals[i])
            self.stepinputs[i].setText(stepVals[i])
            self.spinboxes[i].setValue(ordVals[i])
        self.ui.SST_edit.setText(saveDict["SST"])
        self.ui.wait_inp.setText(saveDict["waittime"])
        self.ui.dummy_PS_c.setChecked(saveDict["dummyps"])
        if "exposure" in list(saveDict.keys()):
            self.ui.exposure_l.setText(saveDict["exposure"])
            self.ui.gain_l.setText(saveDict["gain"])
            self.ui.cam_c.setChecked(saveDict["camChecked"])
            self.camPopup.setValues(saveDict["exposure"], saveDict["gain"])
            self.camPopup.setMinMax([saveDict["exposureMin"], saveDict["exposureMax"]], [saveDict["gainMin"], saveDict["gainMax"]])
        if "oscChannel" in list(saveDict.keys()):
            self.ui.channel_spin.setValue(saveDict["oscChannel"])
        if "noHist" in list(saveDict.keys()):
            self.ui.no_histogram_c.setChecked(saveDict["noHist"])
        if "onlyamp" in list(saveDict.keys()):
            self.ui.onlyamp_radio.setChecked(saveDict["onlyamp"])
        if "home_after_checkboxes" in list(saveDict.keys()):
            home_after = saveDict["home_after_checkboxes"]
            for i in range(len(self.home_after_checkboxes)):
                self.home_after_checkboxes[i].setChecked(home_after[i])

    def checkValues(self): #Checks values from inputs (eg r2 can't go over 1)
        r2start = self.startinputs[4].text()
        r2stop = self.stopinputs[4].text()
        err = False
        if len(r2start) > 0 and float(r2start) > 1.0:
            self.reportError(7)
            err = True
        if len(r2stop) > 0 and float(r2stop) > 1.0:
            self.reportError(7)
            err = True
        if not err:
            self.ui.err_l.setText("")

    def calcTotalSteps(self):
        self.checkValues()
        self.resetSave()
        startVals = []
        stopVals = []
        stepVals = []
        errorMargin = 0.0001
        self.actives = []
        anyActives = False
        for i in self.checkboxes:
            self.actives.append(i.isChecked())
            if i.isChecked():
                anyActives = True
        if not anyActives and not self.ui.time_c.isChecked():
            self.ui.totalsteps_l.setText("???")
            return
        if self.ui.time_c.isChecked():
            self.calcMinTime()
            return
        for i in range(len(self.startinputs)):
            if self.actives[i]:
                if len(self.startinputs[i].text()) == 0 or len(self.stopinputs[i].text()) == 0 or len(self.stepinputs[i].text()) == 0:
                    self.ui.totalsteps_l.setText("???")
                    return
                if float(self.stepinputs[i].text()) == 0.0:
                    self.ui.totalsteps_l.setText("???")
                    return
                startVals.append(float(self.startinputs[i].text()))
                stopVals.append(float(self.stopinputs[i].text()))
                stepVals.append(float(self.stepinputs[i].text()))
        steps = []
        for i in range(len(startVals)):
            steps.append((stopVals[i] - startVals[i]) / stepVals[i])
        for i in range(len(steps)):
            if abs(round(steps[i]) - steps[i]) < errorMargin:
                steps[i] = round(steps[i]) + 1
            else:
                self.ui.totalsteps_l.setText("???")
                return
        totalSteps = np.prod(steps)
        self.ui.totalsteps_l.setText(str(totalSteps))
        self.calcMinTime()


    def radioChange(self):
        self.resetSave()

    def updateTime(self):
        self.resetSave()
        state = self.ui.time_c.isChecked()
        if state:
            for i in self.checkboxes:
                i.setChecked(False)
        self.ui.scrollArea_2.setVisible(not state)
        self.updateStartFrames()
        self.updateStopFrames()
        self.updateStepFrames()
        self.updateOrdFrames()
        



    def updateAmpInfo(self, overrideState=False):
        self.resetSave()
        state = self.ui.amp_c.isChecked()
        if overrideState:
            state = overrideState
        objs = [self.ui.intTime_l, self.ui.intTime_edit, self.ui.nofm_l, self.ui.nofm_spin, self.ui.int_time_s_l]
        for i in objs:
            i.setVisible(state)

    
    def updateFrames(self, frames):
        self.resetSave()
        if self.ui.time_c.isChecked():
            for i in self.checkboxes:
                i.setChecked(False)
            return
        framemap = {"start": self.startframes, "stop": self.stopframes, "step": self.stepframes, "ord": self.ordframes}
        labelmap = {"start": self.ui.nostart_l, "stop": self.ui.nostop_l, "step": self.ui.nostep_l, "ord": self.ui.noord_l}
        active_frames = framemap[frames]
        self.actives = []
        shown = 0
        for i in self.checkboxes:
            self.actives.append(i.isChecked())
        for i in range(len(active_frames)):
            if self.actives[i]:
                active_frames[i].setVisible(True)
                shown += 1
            else:
                active_frames[i].setVisible(False)
        self.activeAxes = shown
        if shown == 0:
            labelmap[frames].setVisible(True)
        else:
            labelmap[frames].setVisible(False)
        self.homeUpdate()

    
    def updateStartFrames(self):
        self.updateFrames("start")
    
    def updateStopFrames(self):
        self.updateFrames("stop")

    def updateStepFrames(self):
        self.updateFrames("step")

    def updateOrdFrames(self):
        self.getLatestCheck()
        self.updateFrames("ord")
    
    def getLatestCheck(self):
        for i in range(len(self.checkboxes)):
            if self.checkboxes[i].isChecked():
                if self.revLabelMap[i] not in self.ordering:
                    self.ordering.append(self.revLabelMap[i])
        newOrder = []
        
        for i in range(len(self.ordering)):
            if self.checkboxes[self.labelMap[self.ordering[i]]].isChecked():
                newOrder.append(self.ordering[i])
        self.ordering = newOrder
        self.setOrdValues()

    def setOrdValues(self):
        for i in range(len(self.spinboxes)):
            if self.checkboxes[i].isChecked():
                val = self.ordering.index(self.revLabelMap[i]) + 1
                self.spinboxes[i].setValue(val)


    def reportError(self, c, data=None):
        self.errormsg = self.englishErrorMesseges[c]
        if data != None:
            self.errormsg = self.errormsg.format(data)
        self.ui.err_l.setText(self.errormsg)
    
    def validityCheck(self):
        if not self.ui.dummy_radio.isChecked() and not self.ui.osc_radio.isChecked() and not self.ui.lockin_radio.isChecked() and not self.ui.onlyamp_radio.isChecked():
            self.reportError(0)
            return False
        if len(self.ui.name_inp.text()) == 0:
            self.reportError(1)
            return False
        anychecked = False
        for i in self.checkboxes:
            if i.isChecked():
                anychecked = True
                break
        if self.ui.time_c.isChecked(): anychecked = True
        if not anychecked:
            self.reportError(2)
            return False
        emptyInp = False
        for i in range(len(self.startinputs)):
            if self.checkboxes[i].isChecked():
                if len(self.startinputs[i].text()) == 0: emptyInp = True
                elif self.startinputs[i].text()[-1] == '.' or self.startinputs[i].text()[-1] == '-': emptyInp = True
                if len(self.stopinputs[i].text()) == 0: emptyInp = True
                elif self.stopinputs[i].text()[-1] == '.' or self.stopinputs[i].text()[-1] == '-': emptyInp = True
                if len(self.stepinputs[i].text()) == 0: emptyInp = True
                elif self.stepinputs[i].text()[-1] == '.' or self.stepinputs[i].text()[-1] == '-': emptyInp = True
        if emptyInp:
            self.reportError(3)
            return False
        numberOfActives = 0
        for i in self.checkboxes:
            if i.isChecked():
                numberOfActives += 1
        controlArray = []
        for i in range(numberOfActives):
            controlArray.append(i+1)
        spinboxValues = []
        for i in range(len(self.spinboxes)):
            if self.actives[i]:
                spinboxValues.append(self.spinboxes[i].value())
        spinboxValues.sort()
        if spinboxValues != controlArray:
            self.reportError(4, numberOfActives)
            return False
        if self.ui.time_c.isChecked():
            if len(self.ui.wait_inp.text()) == 0:
                self.reportError(5)
                return False
        usedVac = False
        for i in [self.ui.vx_c, self.ui.vy_c, self.ui.vz_c]:
            if i.isChecked():
                usedVac = True
        if self.ui.osc_radio.isChecked() and self.ui.amp_c.isChecked():
            sst_text = self.ui.SST_edit.text()
            decimals = 0
            if '.' in sst_text:
                decimals = round(len(sst_text) - sst_text.index(".") - 1)
            mul_res = round(float(self.ui.nofm_spin.value()) * float(self.ui.intTime_edit.text()), decimals)
            if mul_res != float(sst_text):
                self.reportError(8)
                return False
        if self.worker is not None:
            if self.worker.isRunning():
                self.reportError(9)
                return False
        return True

    def makeDir(self, path):
        if os.path.exists(path):
            count = 1
            while os.path.exists(path + f"_{count}"):
                count += 1
            os.mkdir(path + f"_{count}")
            return path + f"_{count}"
        else:
            os.mkdir(path)
            return path
    
    def getTotalSteps(self):
        if self.ui.time_c.isChecked():
            return int(self.ui.wait_inp.text())
        else:
            stepstr = self.ui.totalsteps_l.text()
            return int(stepstr)
    
    def softValidityCheck(self):
        for i in range(len(self.checkboxes)):
            if self.actives[i]:
                if not self.isValid(self.startinputs[i].text()) or not self.isValid(self.stopinputs[i].text()) or not self.isValid(self.stepinputs[i].text()):
                    return False
        return True


    def calcMinTime(self):
        if not self.softValidityCheck() and not self.ui.time_c.isChecked():
            return -1
        if self.ui.time_c.isChecked():
            if not self.isValid(self.ui.SST_edit.text()) or not self.isValid(self.ui.wait_inp.text()):
                return -1
            minSeconds = float(self.ui.SST_edit.text()) * float(self.ui.wait_inp.text())
        else:
            if self.timeDict is None:
                if self.ui.totalsteps_l.text == "???" or len(self.ui.SST_edit.text()) == 0:
                    return -1
                minSeconds = self.getTotalSteps() * float(self.ui.SST_edit.text())
                if minSeconds < 0:
                    return
            else:
                stepDict = {}
                totalSteps = 0
                for i in range(len(self.checkboxes)):
                    if self.actives[i]:
                        axName = self.revLabelMap[i]
                        axStepAmount = round((float(self.stopinputs[i].text()) - float(self.startinputs[i].text())) / float(self.stepinputs[i].text())) + 1
                        stepDict[axName] = axStepAmount
                        totalSteps += axStepAmount

                ordArr = [None, None, None, None, None, None, None, None, None, None, None, None, None, None, None]
                for i in range(len(self.spinboxes)):
                    if self.actives[i]:
                        ordArr[self.spinboxes[i].value()] = self.revLabelMap[i]
                ordArr = [x for x in ordArr if x is not None]

                #Case: ordArr = ['X', 'Y', 'Z'], stepDict = {'Y': 4, 'Z': 6, 'X': 3}
                #newStepDict = {'Y': 24, 'Z': 6, 'X': 72}

                #Case: ordArr is ['X', 'R2'] and stepDict is {'X': 2, 'R2': 3}
                # Now, write code to update stepdict, since X moves first, X actually moves 6 times (2 * 3)
                #So now update stepDict to reflect ALL steps for each axis
                #In this case, stepDict should be update to: {'X': 6, 'R2': 3}

                currMult = 1
                newStepDict = {}
                for i in range(len(ordArr) - 1, -1, -1):
                    newStepDict[ordArr[i]] = stepDict[ordArr[i]] * currMult
                    currMult *= stepDict[ordArr[i]]
                            
                minSeconds = 0.0

                for key in list(newStepDict.keys()):
                    minSeconds += self.timeDict[key][0] * newStepDict[key]
                
                minSeconds += totalSteps * float(self.ui.SST_edit.text())

        earliestFinish = (datetime.datetime.now() + datetime.timedelta(seconds=minSeconds)).strftime("%H:%M:%S")
        self.ui.early_time_l.setText(earliestFinish)
        formattedTime = self.formatSeconds(minSeconds)
        timestring = str(formattedTime[0]) + ":" + str(formattedTime[1]) + ":" + str(formattedTime[2])
        self.ui.min_time_l.setText(timestring)
        return minSeconds
        

    def runExp(self):
        if self.vac.connected:
            self.vac.disconnectVac()
        self.ui.datManager_btn.setVisible(False)
        self.errormsg = ""
        self.ui.err_l.setText(self.errormsg)
        self.ui.exp_time_l.setText(f"Experiment took: ???")
        if not self.validityCheck():
            print("Validitycheck FAILED")
            return
        print("Running experiment...")
        expName = self.ui.name_inp.text()
        instruments = []
        model = ""
        readerAddress = ""
        name = ""
        wname = ""
        stage_name = "ScanStage"
        dummyMover = False
        if self.ui.time_c.isChecked() or self.ui.dummy_radio.isChecked():
            dummyMover = True
            stage_type = 'dummyMover'
        else:
            stage_type = "KinesisController"
        readerType1 = 0
        if self.ui.osc_radio.isChecked():
            model = "Tektronix_DPO4102B"
            readerAddress = ["192.168.1.5", self.ui.onlyamp_radio.isChecked()]
            name = "ScanOsc"
        if self.ui.lockin_radio.isChecked():
            model = "LockInSRS800"
            readerAddress = ['GPIB0','8']
            name = "ScanLockIn"
            readerType1 = 1
        if self.ui.dummy_radio.isChecked():
            model = "dummyReader"
            readerAddress = "192.168.1.5"
            name = "ScanOsc"
        if self.ui.time_c.isChecked():
            dummyMover = True
            stage_type = "timeMover"
        data = [model,name,readerAddress] 
        instruments.append(data)

        stageData= []

        if self.checkboxes[0].isChecked():
            stageData.append(['X-Stage','linear','27255894','X',self.ui.home_x_c.isChecked()])
        if self.checkboxes[1].isChecked():
            stageData.append(['Y-Stage','linear','27256338','Y',self.ui.home_y_c.isChecked()])
        if self.checkboxes[2].isChecked():
            stageData.append(['Z-Stage','linear','27004364','Z',self.ui.home_z_c.isChecked()])
        if self.checkboxes[3].isChecked():
            stageData.append(['rotating','rotational','27005004','R1',self.ui.home_r1_c.isChecked()])
        if self.checkboxes[4].isChecked():
            stageData.append(['wiregrid','field','27255354','R2',self.ui.home_r2_c.isChecked()])
        if self.checkboxes[5].isChecked():
            stageData.append(['wiregrid2','rotational','27255354','R3',self.ui.home_r3_c.isChecked()])
        
        stage_name = 'ScanStage'
        if self.ui.time_c.isChecked():
            stage_name = "TimeStage"

        data = [stage_type,stage_name,{'createStages':True,'stageData':stageData}]
        instruments.append(data)

        if self.ui.s0_c.isChecked() or self.ui.s1_c.isChecked() or self.ui.s2_c.isChecked() or self.ui.s3_c.isChecked():
            specialAxis = ['S0','S1','S2','S3']

            specialInst1Name = "VoltageCont"
            specialInst1Type = "VoltageController"
            data = [specialInst1Type,specialInst1Name,specialAxis]
            instruments.append(data)

        vaccumStageName = "VaccumStage"
        vaccumStageType = "VaccumStage"
        dummyVac = False
        if not (self.ui.vx_c.isChecked() or self.ui.vy_c.isChecked() or self.ui.vz_c.isChecked()):
            vaccumStageType = "DummyVaccum"
            dummyVac = True
        else:
            if self.ui.dummy_radio.isChecked():
                vaccumStageType = "DummyVaccum"
                dummyVac = True
            vaccumStageAxis = ['VX','VY','VZ']
            homeStage = [self.ui.home_vx_c.isChecked(),self.ui.home_vy_c.isChecked(),self.ui.home_vz_c.isChecked()]
            vaccumStageIp = '192.168.1.3'
            data = [vaccumStageType,vaccumStageName,[vaccumStageIp,homeStage]]
            instruments.append(data)

        if self.ui.onlyamp_radio.isChecked() or self.ui.amp_c.isChecked():
            instruments.append(["Ammeter", "Ammeter", [self.ui.nofm_spin.value(), float(self.ui.intTime_edit.text())]])

        axes1 = []
        spinboxValues = {}
        for i in range(len(self.spinboxes)):
            if self.checkboxes[i].isChecked():
                spinboxValues[self.revLabelMap[i]] = self.spinboxes[i].value()
        spinboxValuesSorted = dict(sorted(spinboxValues.items(), key=lambda item: item[1]))
        for i in spinboxValuesSorted.keys():
            axes1.insert(0, i)
        
        startCoords1 = []
        stopCoords1 = []
        step_size1 = []
        
        for i in axes1:
            startCoords1.append(float(self.startinputs[self.labelMap[i]].text()))
            stopCoords1.append(float(self.stopinputs[self.labelMap[i]].text()))
            step_size1.append(float(self.stepinputs[self.labelMap[i]].text()))
        
        if self.ui.time_c.isChecked():
            startCoords1 = [0.0]
            stopCoords1 = [round(float(self.ui.wait_inp.text()) - 1.0)]
            step_size1 = [1.0]
            axes1 = ["TimeStage"]


        self.axes = axes1
        self.experimentName = expName
        self.startCoords = startCoords1
        self.stopCoords = stopCoords1
        self.step_size = step_size1
        self.stageSettleTime = float(self.ui.SST_edit.text())
        self.readerType = readerType1
        self.useAmperemeter = self.ui.amp_c.isChecked()
        self.integrationTime = float(self.ui.intTime_edit.text())
        self.numberOfMeassurements = int(self.ui.nofm_spin.value())
        #dummyVac = not (self.ui.vx_c.isChecked() or self.ui.vy_c.isChecked() or self.ui.vz_c.isChecked())
        data_dict = self.saveOptions(running=True)
        #for i, j in zip([instruments, startCoords1, stopCoords1, step_size1, axes1, dummyVac, readerType1, stageData], ["instruments", "startCoords1", "stopCoords1", "step_size1", "axes1", "dummyVac", "readerType1", "stageData"]):
        #    print(f"{j}: \n {type(i)} \n {i} \n")
        data_dict["instruments"] = instruments
        data_dict["startCoords"] = startCoords1
        data_dict["stopCoords"] = stopCoords1
        data_dict["step_size"] = step_size1
        data_dict["axes"] = axes1
        data_dict["dummyVac"] = dummyVac
        data_dict["reader_type"] = readerType1
        data_dict["stage_data"] = stageData
        data_dict["resolution"] = 10000
        data_dict["interrupted"] = False
        data_dict["new_stop"] = []

        path = os.getcwd()
        dacFolder = os.path.abspath(path)

        

        if self.ui.onlyamp_radio.isChecked():
            outname = "outputs-AMP"
        else:
            outname = "outputs-PMT"
        outputs = os.path.join(dacFolder, outname)
        if not os.path.isdir(outputs):
            os.mkdir(outputs)
        
        self.directory = os.path.join(dacFolder,os.path.join(outname, self.experimentName+'-'+str(datetime.datetime.now().date())))
        self.directory = self.makeDir(self.directory)
        
        self.time_dict["timescan"] = self.ui.time_c.isChecked()
        self.time_dict["waittime"] = self.ui.wait_inp.text()
        self.time_dict["dummymover"] = dummyMover
        self.time_dict["dummyVac"] = dummyVac
        self.time_dict["steps"] = self.getTotalSteps()
        self.time_dict["SST"] = self.ui.SST_edit.text()
        self.time_dict["mintime"] = self.calcMinTime()
        self.time_dict["axes"] = axes1
        self.time_dict["stepTimes"] = self.stepTimes
        jsonpath = self.getNewFilePath(os.path.join(self.directory , "ui_data.json"))
        outfile = open(jsonpath, "w")
        json.dump(data_dict, outfile)
        outfile.close()
        self.start_time = time.time()
        if self.ui.osc_radio.isChecked() or self.ui.dummy_radio.isChecked() or self.ui.onlyamp_radio.isChecked():
            n_times = 1
            if self.ui.n_edit.isVisible():
                n_times = int(self.ui.n_edit.text())
            noHist = self.ui.no_histogram_c.isChecked()
            self.worker = MainBackgroundThread(expName, self.directory, dacFolder, n_times, noHist)
        elif self.ui.lockin_radio.isChecked():
            self.worker = MainBackgroundThreadL(expName, self.directory, dacFolder)
        else:
            print("No radiobutton selected!")
            exit(0)
        
        self.connect(self.worker, SIGNAL("finished()"), self.done)
        self.connect(self.worker, SIGNAL("totalProgress(int)"), self.totalProgress)
        self.connect(self.worker, SIGNAL("progressUpdate(int)"), self.progressUpdate)
        self.connect(self.worker, SIGNAL("getStepsTime(float,int)"), self.getStepsTime)
        #self.connect(self.worker, SIGNAL("haveBeenStopped()"), self.interruptCleanUp)
        #print(f"Starting worker")
        self.worker.start()
        self.ui.progress_l.setText("Running...")
        self.ui.progress_l.setStyleSheet("color: yellow;")
    
    def getStepsTime(self, steptime, axis):
        axisName = self.actionDataDict_rev[axis]
        self.stepTimes[axisName].append(steptime)

    def progressUpdate(self, c):
        self.currentProgress = c
        self.ui.progressBar.setValue(c)
    
    def zeroPad(self, n):
        res = ""
        if n < 10:
            res = "0" + str(n)
        else:
            res = str(n)
        return res
    
    def formatSeconds(self, s):
        hours = int(s/3600)
        s -= hours * 3600
        minutes = int(s/60)
        s -= minutes * 60
        seconds = int(round(s, 0))
        return [self.zeroPad(hours), self.zeroPad(minutes), self.zeroPad(seconds)]

    def totalProgress(self, c):
        self.totalActions = c
        self.ui.progressBar.setMaximum(c)
    
    def getCurrTime(self):
        date = datetime.datetime.now()
        actualTime = date.strftime("%H:%M:%S")
        return actualTime

    def runDatManager(self):
        cwd = os.getcwd()
        os.chdir(self.directory)
        Popen("DataManager_GUI.bat")
        os.chdir(cwd)
    
    def cleanKeys(self, keys):
        keys.remove("axes")
        keys.remove("config")
        newKeys = []
        for i in keys:
            temp = []
            key = i.replace("[", "").replace("]", "").replace(" ", "").split(",")
            for j in key:
                temp.append(float(j))
            newKeys.append(temp)
        return newKeys

    def wholeKeys(self, keys):
        newKeys = []
        curr = keys[0][0]
        temp = []
        for i in keys:
            if i[0] == curr:
                temp.append(i)
            else:
                curr = i[0]
                for j in temp:
                    newKeys.append(j)
                temp = [i]
        return newKeys
    
    def column(self, matrix, i):
        return [row[i] for row in matrix]

    def maxValsFromKeys(self, keys):
        maxVals = []
       
        for i in range(len(keys[0])):
            maxVals.append(max(self.column(keys, i)))
        return maxVals
    
    def rampDownAndWait(self, channels):
        ps = PowerSupply()
        for i in channels:
            ps.set_channel_voltage(i, 0)
        while ps.check_if_ramping(channels):
            time.sleep(0.1)
        for i in channels:
            ps.disable_channel(i)
        return True
    
    def cleanUp(self):
        used_v = False
        used_s = []
        for i in self.axes:
            if i[0] == "V": used_v = True
            if i[0] == "S": used_s.append(int(i[1]))
        if used_v:
            pass
            #self.worker.terminate()
            #self.worker.wait()
        if len(used_s) > 0:
            rampDownThread = threading.Thread(target=self.rampDownAndWait, args=(used_s,))
            rampDownThread.start()
        
    def interruptCleanUp(self):
        self.cleanUp()
        ui_file = open(os.path.join(self.directory, "ui_data.json"), "r")
        ui_data = json.load(ui_file)
        ui_file.close()
        ui_data["interrupted"] = True
        ui_file = open(os.path.join(self.directory, "ui_data.json"), "w")
        json.dump(ui_data, ui_file)
        ui_file.close()

    def done(self):
        currentTime = self.getCurrTime()
        self.end_time = time.time()
        self.actual_time = self.end_time - self.start_time
        formattedTime = self.formatSeconds(self.actual_time)
        if self.currentProgress == self.totalActions:
            self.time_dict["actualtime"] = self.actual_time
            self.time_dict["used"] = False
            timepath = self.getNewFilePath(os.path.join(self.directory , "time_data.json"))
            out_time_file = open(timepath, "w")
            json.dump(self.time_dict, out_time_file)
            out_time_file.close()
            self.ui.progress_l.setText(f"Done! ({currentTime})")
            self.ui.datManager_btn.setVisible(True)
            self.ui.progress_l.setStyleSheet("color: green;")
            print(f"Experiment {self.experimentName} has completed")
        else:
            self.ui.progress_l.setText(f"Interrupted! ({currentTime})")
            self.ui.progress_l.setStyleSheet("color: red;")
            print(f"Experiment {self.experimentName} has been interrupted")
            self.interruptCleanUp()

        self.ui.exp_time_l.setText(f"Experiment took: {formattedTime[0]}:{formattedTime[1]}:{formattedTime[2]}")
    
    """
    Takes a string and checks if it is a valid float (Not valid: '-', '4.', '')
    toTest: String of a number (fx '-4.2' or '1.62')
    """
    def isValid(self, toTest):
        if len(toTest) == 0:
            return False
        if len(toTest) == 1 and toTest[0] == '-':
            return False
        if toTest[-1] == ".":
            return False
        return True
        

        

if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = MainWindow()
    widget.show()
    sys.exit(app.exec())
