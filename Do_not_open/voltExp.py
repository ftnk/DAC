# This Python file uses the following encoding: utf-8
"""
@author: Rune
"""

import sys
from ctypes import *
from typing import Optional 

from PySide6.QtWidgets import QApplication, QMainWindow, QFileDialog
from PySide6.QtCore import QRect, QThread, Qt, SIGNAL, QReadWriteLock
from PySide6.QtCore import QRegularExpression as QRegExp
from PySide6.QtGui import QRegularExpressionValidator as QRegExpValidator
from PySide6 import QtCore, QtGui, QtWidgets



# Important:
# You need to run the following command to generate the ui_form.py file
#     pyside6-uic form.ui -o ui_form.py, or
#     pyside2-uic form.ui -o ui_form.py
from GUI.ui_voltForm import Ui_MainWindow
from GUI.popUps import cameraPopup
from hardware.motion_controllers import KinesisController
from hardware.Voltage_Supply.power_supply import PowerSupply
from voltageExperiment import Experiment
from hardware.camera import Camera
import matplotlib.pyplot as plt
import multiprocessing as mp
import json
import numpy as np
import os
import datetime
import time
import h5py
import pandas as pd
import math

class runExperiment(QThread):
    def __init__(self, axes, sss, statAxes, turningPointsValues, tip, mf, totalSteps, r2Stage, pSupply, mutex, useCam, dummy, camDict, outputDir) -> None:
        QThread.__init__(self)
        self.axes = axes
        self.sss = sss
        self.statAxes = statAxes
        self.turningPointsValues = turningPointsValues
        self.r2Stage = r2Stage
        self.pSupply = pSupply
        self.tip = tip
        self.mf = mf
        self.totalSteps = totalSteps
        self.useCam = useCam
        self.dummy = dummy
        self.camDict = camDict
        self.outputDir = outputDir
        self.exp = Experiment(sss, axes, statAxes, turningPointsValues, tip, mf, totalSteps, useCam=useCam, volt=pSupply, stage=r2Stage, workerThread=self, mutex=mutex, dummy=dummy, camDict=camDict, outputDir=outputDir)
    
    def run(self):
        self.exp.run()
        return

class MySwitch(QtWidgets.QPushButton): #https://stackoverflow.com/questions/56806987/switch-button-in-pyqt
    def __init__(self, parent = None):
        super().__init__(parent)
        self.setCheckable(True)
        self.setMinimumWidth(66)
        self.setMinimumHeight(22)
        self.setMaximumHeight(22)

    def paintEvent(self, event):
        label = "ON" if self.isChecked() else "OFF"
        bg_color = Qt.green if self.isChecked() else Qt.red

        radius = 9
        width = 32
        center = self.rect().center()

        painter = QtGui.QPainter(self)
        painter.setRenderHint(QtGui.QPainter.Antialiasing)
        painter.translate(center)
        painter.setBrush(QtGui.QColor(0,0,0))

        pen = QtGui.QPen(Qt.black)
        pen.setWidth(2)
        painter.setPen(pen)

        painter.drawRoundedRect(QRect(-width, -radius, 2*width, 2*radius), radius, radius)
        painter.setBrush(QtGui.QBrush(bg_color))
        sw_rect = QRect(-radius, -radius, width + radius, 2*radius)
        if not self.isChecked():
            sw_rect.moveLeft(-width)
        painter.drawRoundedRect(sw_rect, radius, radius)
        painter.drawText(sw_rect, Qt.AlignCenter, label)

class stagePosBackgroundThread(QThread):
    def __init__(self, controller, label):
        QThread.__init__(self)
        self.controller = controller
        self.label = label
    
    def getPositions(self):
        pos = self.controller.get_position()
        #print(f"R2 pos: {pos}")
        return pos

    def run(self):
        while True:
            pos = self.getPositions()
            pos = round(math.cos(pos * (math.pi/180)) ** 2, 4)
            self.label.setText(str(pos))
            time.sleep(1)
            if self.isInterruptionRequested():
                break
        return

class stageController():
    def __init__(self, ui):
        self.ui = ui
        self.serial = c_char_p(b'27255354')
        self.R2Controller = KinesisController(serialNumber=self.serial, HomeStage=False, stageName='wiregrid', stageType='field', lateSetup=True)
        self.position = None
        self.posLabel = self.ui.pos_r2_l
        self.connLabel = self.ui.r2_con_l
        self.curPos = self.ui.curPos_r2_edit
        self.connected = False

        self.workerThread = stagePosBackgroundThread(self.R2Controller, self.posLabel)

        self.connectSignals()
    
    """
    Connects signals from buttons, checkboxes etc. to the correct methods
    """
    def connectSignals(self):
        self.ui.r_connect_btn.clicked.connect(self.connectDisconnect)
        self.ui.go_r2_btn.clicked.connect(lambda: self.move(float(self.ui.curPos_r2_edit.text())))
        self.ui.left_r2_btn.clicked.connect(lambda: self.move(float(self.ui.curPos_r2_edit.text()) - float(self.ui.step_r2_edit.text())))
        self.ui.right_r2_btn.clicked.connect(lambda: self.move(float(self.ui.curPos_r2_edit.text()) + float(self.ui.step_r2_edit.text())))
        
    """
    Moves the stage to a given value: 'val'
    """
    def move(self, val):
        radians = math.acos(math.sqrt(val))
        pos = radians * (180/math.pi)
        self.R2Controller.move_device(pos)
    
    """
    Connects/Disconnects from the R2 stage
    """
    def connectDisconnect(self):
        if self.connected:
            self.workerThread.requestInterruption()
            self.workerThread.wait()
            self.R2Controller.clean_up_device()
            self.ui.r_connect_btn.setText("Connect")
            self.connected = False
            self.connLabel.setText("Disconnected")
            self.connLabel.setStylesheet("color: black;")
        else:
            self.R2Controller.late_setup()
            pos = self.R2Controller.get_position()
            self.position = round(pos, 2)
            self.workerThread.start()
            self.curPos.setText(str(round(pos, 2)))
            self.ui.r_connect_btn.setText("Disconnect")
            self.connected = True
            self.connLabel.setText("Connected")
            self.connLabel.setStylesheet("color: green;")

class voltageBackgroundThread(QThread):
    def __init__(self, pSupply, labels):
        QThread.__init__(self)
        self.pSupply = pSupply
        self.labels = labels
    
    def run(self):
        while True:
            if self.isInterruptionRequested():
                break
            for i in range(4):
                pos = self.pSupply.get_channel_voltage(i)
                if pos == '':
                    continue
                if len(str(pos)) > 8:
                    #print(f"Rounding S{i} to {len(str(pos)) - 16} decimals")
                    pos = round(pos, 3)
                self.labels[i].setText(str(pos))
            time.sleep(1)
        return

class turnOffVoltagesBackgroundThread(QThread):
    def __init__(self, pSupply, usedChannels):
        QThread.__init__(self)
        self.pSupply = pSupply
        self.usedChannels = usedChannels
    
    def run(self):
        if not self.pSupply.isSetUp:
            return
        if len(self.usedChannels) > 0:
            self.turnOffVoltages()
        return

    """
    Sets all channels to 0V after synchronizing them
    """
    def turnOffVoltages(self):
        if 0 in self.usedChannels and 1 in self.usedChannels:
            self.syncVoltages([0, 1])
        self.syncVoltages(list(np.array(self.usedChannels).copy()))
        for i in self.usedChannels:
            self.pSupply.disable_channel(i)
        for i in self.usedChannels:
            self.pSupply.set_channel_voltage(i, 0)
            self.pSupply.enable_channel(i)
        time.sleep(2)
        while self.pSupply.check_if_ramping(self.usedChannels):
            time.sleep(0.5)
        for i in self.usedChannels:
            self.pSupply.disable_channel(i)

    """
    Synchronizes channels in channelList, by setting them all to the highest/lowest
    voltage in the list (if maxVoltage = True, it will use the highest voltage to sync)
    """
    def syncVoltages(self, channelList, maxVoltage=True):
        if 3 in channelList:
            newList = channelList.remove(3)
        else:
            newList = channelList
        if newList == None or len(newList) < 2:
            return
        volts = []
        for i in newList:
            volts.append(self.pSupply.get_channel_voltage(i))
            self.pSupply.disable_channel(i)
        if maxVoltage:
            targetV = max(volts)
        else:
            targetV = min(volts)

        for i in newList:
            self.pSupply.set_channel_voltage(i, targetV)
            self.pSupply.enable_channel(i)
        time.sleep(2)
        while self.pSupply.check_if_ramping(newList):
            time.sleep(0.5)

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle("Experiment")
        self.s0_status_btn = MySwitch()
        self.s1_status_btn = MySwitch()
        self.s2_status_btn = MySwitch()
        self.s3_status_btn = MySwitch()

        self.s0_status_btn.clicked.connect(lambda: self.sToggle(self.s0_status_btn.isChecked(), "S0"))
        self.s1_status_btn.clicked.connect(lambda: self.sToggle(self.s1_status_btn.isChecked(), "S1"))
        self.s2_status_btn.clicked.connect(lambda: self.sToggle(self.s2_status_btn.isChecked(), "S2"))
        self.s3_status_btn.clicked.connect(lambda: self.sToggle(self.s3_status_btn.isChecked(), "S3"))
        
        

        self.ui.gridLayout_4.addWidget(self.s0_status_btn, 2, 4, 1, 1)
        self.ui.gridLayout_4.addWidget(self.s1_status_btn, 3, 4, 1, 1)
        self.ui.gridLayout_4.addWidget(self.s2_status_btn, 4, 4, 1, 1)
        self.ui.gridLayout_4.addWidget(self.s3_status_btn, 5, 4, 1, 1)

        self.pSupply = PowerSupply(lateSetup=True)
        self.pConnected = False

        self.sStages = {"S0": False, "S1": False, "S2": False, "S3": False}
        
        cwd = os.getcwd()
        self.controller = stageController(self.ui)
        os.chdir(cwd)

        self.r2_map = {"label": self.ui.r2_l, "start": self.ui.r2_start_edit, "stop": self.ui.r2_stop_edit, "step": self.ui.r2_step_edit, "spin": self.ui.r2_spin, "stat": self.ui.r2_stat_c, "rev": self.ui.r2_reverse_c}
        self.s0_map = {"label": self.ui.s0_l, "start": self.ui.s0_start_edit, "stop": self.ui.s0_stop_edit, "step": self.ui.s0_step_edit, "spin": self.ui.s0_spin, "stat": self.ui.s0_stat_c, "rev": self.ui.s0_reverse_c}
        self.s1_map = {"label": self.ui.s1_l, "start": self.ui.s1_start_edit, "stop": self.ui.s1_stop_edit, "step": self.ui.s1_step_edit, "spin": self.ui.s1_spin, "stat": self.ui.s1_stat_c, "rev": self.ui.s1_reverse_c}
        self.s2_map = {"label": self.ui.s2_l, "start": self.ui.s2_start_edit, "stop": self.ui.s2_stop_edit, "step": self.ui.s2_step_edit, "spin": self.ui.s2_spin, "stat": self.ui.s2_stat_c, "rev": self.ui.s2_reverse_c}
        self.s3_map = {"label": self.ui.s3_l, "start": self.ui.s3_start_edit, "stop": self.ui.s3_stop_edit, "step": self.ui.s3_step_edit, "spin": self.ui.s3_spin, "stat": self.ui.s3_stat_c, "rev": self.ui.s3_reverse_c}
        self.allMap = {"R2": self.r2_map, "S0": self.s0_map, "S1": self.s1_map, "S2": self.s2_map, "S3": self.s3_map}

        self.r2_list = [self.ui.r2_l, self.ui.r2_start_edit, self.ui.r2_stop_edit, self.ui.r2_step_edit, self.ui.r2_spin, self.ui.r2_stat_c, self.ui.r2_reverse_c, self.ui.r2_tp_edit]
        self.s0_list = [self.ui.s0_l, self.ui.s0_start_edit, self.ui.s0_stop_edit, self.ui.s0_step_edit, self.ui.s0_spin, self.ui.s0_stat_c, self.ui.s0_reverse_c, self.ui.s0_tp_edit]
        self.s1_list = [self.ui.s1_l, self.ui.s1_start_edit, self.ui.s1_stop_edit, self.ui.s1_step_edit, self.ui.s1_spin, self.ui.s1_stat_c, self.ui.s1_reverse_c, self.ui.s1_tp_edit]
        self.s2_list = [self.ui.s2_l, self.ui.s2_start_edit, self.ui.s2_stop_edit, self.ui.s2_step_edit, self.ui.s2_spin, self.ui.s2_stat_c, self.ui.s2_reverse_c, self.ui.s2_tp_edit]
        self.s3_list = [self.ui.s3_l, self.ui.s3_start_edit, self.ui.s3_stop_edit, self.ui.s3_step_edit, self.ui.s3_spin, self.ui.s3_stat_c, self.ui.s3_reverse_c, self.ui.s3_tp_edit]
        self.allDict = {"R2": self.r2_list, "S0": self.s0_list, "S1": self.s1_list, "S2": self.s2_list, "S3": self.s3_list}
        self.allList = [self.r2_list, self.s0_list, self.s1_list, self.s2_list, self.s3_list]
        self.allSSS = [[self.ui.r2_start_edit, self.ui.r2_stop_edit, self.ui.r2_step_edit],
                       [self.ui.s0_start_edit, self.ui.s0_stop_edit, self.ui.s0_step_edit],
                       [self.ui.s1_start_edit, self.ui.s1_stop_edit, self.ui.s1_step_edit],
                       [self.ui.s2_start_edit, self.ui.s2_stop_edit, self.ui.s2_step_edit],
                       [self.ui.s3_start_edit, self.ui.s3_stop_edit, self.ui.s3_step_edit]]
        self.checkboxes = [self.ui.r2_c, self.ui.s0_c, self.ui.s1_c, self.ui.s2_c, self.ui.s3_c]
        self.axNames = ["R2", "S0", "S1", "S2", "S3"]
        self.axMap = {-1: "R2", 0: "S0", 1: "S1", 2: "S2", 3: "S3"}
        self.posLabels = [self.ui.s0_volt_l, self.ui.s1_volt_l, self.ui.s2_volt_l, self.ui.s3_volt_l]
        self.plotting = {key: False for key in self.axNames[1:]}
        self.redWidgets = []
        self.plotButtons = [self.ui.s0_plot_btn, self.ui.s1_plot_btn, self.ui.s2_plot_btn, self.ui.s3_plot_btn]
        self.anyPlot = False
        self.reverseCheckboxes = [self.ui.r2_reverse_c, self.ui.s0_reverse_c, self.ui.s1_reverse_c, self.ui.s2_reverse_c, self.ui.s3_reverse_c]
        self.turningPoints = [self.ui.r2_tp_edit, self.ui.s0_tp_edit, self.ui.s1_tp_edit, self.ui.s2_tp_edit, self.ui.s3_tp_edit]
        self.turningPointsValues = []
        for i in self.plotButtons:
            i.setStyleSheet("background-color: red;")

        self.checked = {"R2": False, "S0": False, "S1": False, "S2": False, "S3": False}

        self.spinMap = {"R2": self.ui.r2_spin, "S0": self.ui.s0_spin, "S1": self.ui.s1_spin, "S2": self.ui.s2_spin, "S3": self.ui.s3_spin}

        self.stepTimes = {"R2": [], "S0": [], "S1": [], "S2": [], "S3": []}

        reg_ex = QRegExp("-{0,1}[0-9]+(\.[0-9]+){1}$")
        no_neg_reg_ex = QRegExp("[0-9]+(\.[0-9]+){1}$")
        no_neg_int_reg_ex = QRegExp("[0-9]+$")
        for i in [self.ui.tip_edit, self.ui.mf_edit, self.ui.exposure_from_edit, self.ui.exposure_to_edit, self.ui.gain_from_edit, self.ui.gain_to_edit]:
            inp_val1 = QRegExpValidator(no_neg_reg_ex, i)
            i.setValidator(inp_val1)

        for i in self.allSSS:
            for j in i:
                inp_val = QRegExpValidator(reg_ex, j)
                j.setValidator(inp_val)
        
        reg_ex_reverse_steps = QRegExp("([0-9]|\.|,|-)*")
        for i in self.turningPoints:
            inp_val2 = QRegExpValidator(reg_ex_reverse_steps, i)
            i.setValidator(inp_val2)

        self.axOrder = []
        self.connectSignals()
        for i in self.allList:
            self.checkBoxUpdate(i, False, None)
        
        self.ui.tp_l.setVisible(False)
        for i, j in zip(self.turningPoints, self.reverseCheckboxes):
            i.setVisible(False)
            j.setVisible(False)
        
        self.camChanged(0)
        
        self.totalActions = -1
        self.currentAction = -1

        self.usedChannels = []

        self.currents = None
        self.voltages = None
        self.times = None
        self.df = pd.DataFrame()

        self.outputDir = None
        self.outputFile = None

        self.workerThread = None
        self.mutex = QReadWriteLock()

        self.voltBackgroundThread = None
        self.offThread = None

        self.cam = None

        self.plotUpdateThread = None

        self.snapShotPath = None
        self.camPopup = cameraPopup(self)


    """
    Connects signals from buttons, checkboxes etc. to the correct methods
    """
    def connectSignals(self):
        self.ui.r2_c.stateChanged.connect(lambda: self.checkBoxUpdate(self.r2_list, self.ui.r2_c.isChecked(), "R2"))
        self.ui.s0_c.stateChanged.connect(lambda: self.checkBoxUpdate(self.s0_list, self.ui.s0_c.isChecked(), "S0"))
        self.ui.s1_c.stateChanged.connect(lambda: self.checkBoxUpdate(self.s1_list, self.ui.s1_c.isChecked(), "S1"))
        self.ui.s2_c.stateChanged.connect(lambda: self.checkBoxUpdate(self.s2_list, self.ui.s2_c.isChecked(), "S2"))
        self.ui.s3_c.stateChanged.connect(lambda: self.checkBoxUpdate(self.s3_list, self.ui.s3_c.isChecked(), "S3"))
        self.ui.save_btn.clicked.connect(self.saveOptions)
        self.ui.load_btn.clicked.connect(self.loadOptions)
        self.ui.run_btn.clicked.connect(self.run)
        self.ui.s_connect_btn.clicked.connect(self.voltConnect)
        self.ui.stop_exp_btn.clicked.connect(self.stopExp)
        self.ui.s0_plot_btn.clicked.connect(lambda: self.plotChannel(0))
        self.ui.s1_plot_btn.clicked.connect(lambda: self.plotChannel(1))
        self.ui.s2_plot_btn.clicked.connect(lambda: self.plotChannel(2))
        self.ui.s3_plot_btn.clicked.connect(lambda: self.plotChannel(3))
        self.ui.r2_stat_c.stateChanged.connect(lambda: self.statCheckBoxUpdate("R2"))
        self.ui.s0_stat_c.stateChanged.connect(lambda: self.statCheckBoxUpdate("S0"))
        self.ui.s1_stat_c.stateChanged.connect(lambda: self.statCheckBoxUpdate("S1"))
        self.ui.s2_stat_c.stateChanged.connect(lambda: self.statCheckBoxUpdate("S2"))
        self.ui.s3_stat_c.stateChanged.connect(lambda: self.statCheckBoxUpdate("S3"))
        self.ui.s0_go_btn.clicked.connect(lambda: self.goToV("S0", self.ui.s0_volt_edit.text()))
        self.ui.s1_go_btn.clicked.connect(lambda: self.goToV("S1", self.ui.s1_volt_edit.text()))
        self.ui.s2_go_btn.clicked.connect(lambda: self.goToV("S2", self.ui.s2_volt_edit.text()))
        self.ui.s3_go_btn.clicked.connect(lambda: self.goToV("S3", self.ui.s3_volt_edit.text()))
        self.ui.cam_c.stateChanged.connect(self.camChanged)
        self.ui.cam_snapshot_btn.clicked.connect(self.camSnapShot)
        self.ui.cam_preview_btn.clicked.connect(self.camPreview)
        self.ui.exposure_from_edit.textChanged.connect(lambda: self.changeExposureLimits(True))
        self.ui.exposure_to_edit.textChanged.connect(lambda: self.changeExposureLimits(True))
        self.ui.gain_from_edit.textChanged.connect(lambda: self.changeGainLimits(True))
        self.ui.gain_to_edit.textChanged.connect(lambda: self.changeGainLimits(True))
        self.ui.exposure_slider.valueChanged.connect(lambda: self.changeExposure(self.ui.exposure_slider))
        self.ui.gain_slider.valueChanged.connect(lambda: self.changeGain(self.ui.gain_slider))
        self.ui.exposure_cur_edit.textChanged.connect(lambda: self.setExposure(True))
        self.ui.gain_cur_edit.textChanged.connect(lambda: self.setGain(True))
        self.ui.ref_r2_btn.clicked.connect(lambda: self.controller.move(1))
        
        for i in self.allSSS:
            for j in i:
                j.textChanged.connect(self.calcSteps)
        for key in self.spinMap:
            self.spinMap[key].valueChanged.connect(self.updateSpinOrder)
        
        for i in self.turningPoints:
            i.textChanged.connect(self.calcSteps)
        
        for i in self.reverseCheckboxes:
            i.toggled.connect(self.reverseChange)
            i.toggled.connect(self.calcSteps)

        #self.ui.r2_reverse_c.toggled.connect(self.reverseChange)
        #self.ui.s0_reverse_c.toggled.connect(self.reverseChange)
        #self.ui.s1_reverse_c.toggled.connect(self.reverseChange)
        #self.ui.s2_reverse_c.toggled.connect(self.reverseChange)
        #self.ui.s3_reverse_c.toggled.connect(self.reverseChange)
    
    def reverseChange(self):
        status = []
        for i in range(len(self.reverseCheckboxes)):
            checked = self.reverseCheckboxes[i].isChecked()
            status.append(checked)
            self.turningPoints[i].setVisible(checked)
        self.ui.tp_l.setVisible(any(status))
            
            
        
    
    def makeOutputDir(self):
        if os.path.exists("./outputs-MCP/"):
            return
        else:
            os.mkdir("./outputs-MCP/")


    def run(self):
        if self.controller.connected:
            self.controller.connectDisconnect()
        if self.pConnected:
            self.pSupply.disconnectAll()
        if not self.validityCheck():
            print("Validitycheck FAILED")
            return
        print("Validitycheck PASSED!")
        axes = self.getAxes()
        sss = self.getSSS()
        statAxes = self.getStatAxes()
        self.makeOutputDir()
        self.outputDir = self.makeDir(f"./outputs-MCP/{self.ui.name_inp.text()}")
        self.outputFile = os.path.join(self.outputDir, "voltage_and_currents.csv")
        camDict = {}
        if self.ui.cam_c.isChecked():
            camDict["exposure"] = float(self.ui.exposure_edit.text())
            camDict["gain"] = float(self.ui.gain_edit.text())
            camDict["resX"] = int(self.ui.resolution_x_edit.text())
            camDict["resY"] = int(self.ui.resolution_y_edit.text())

        np_tps = np.flip(np.array(self.turningPointsValues, dtype=object).copy())
        self.workerThread = runExperiment(axes, sss, statAxes, np_tps.tolist(),
                                          float(self.ui.tip_edit.text()), float(self.ui.mf_edit.text()), int(self.ui.totalsteps_l.text()),
                                          self.controller.R2Controller, self.pSupply, self.mutex,
                                          self.ui.cam_c.isChecked(), self.ui.dummy_PS_c.isChecked(), camDict,
                                          self.outputDir)
        
        self.connect(self.workerThread, SIGNAL("finished()"), self.done)
        self.connect(self.workerThread, SIGNAL("totalProgress(int)"), self.totalProgress)
        self.connect(self.workerThread, SIGNAL("progressUpdate(int)"), self.progressUpdate)
        self.connect(self.workerThread, SIGNAL("newData(float)"), self.addData)
        self.connect(self.workerThread, SIGNAL("getStepsTime(float,int)"), self.getStepsTime)
        self.connect(self.workerThread, SIGNAL("haveBeenStopped()"), self.interruptCleanUp)
        self.start_time = time.time()

        self.ui.progress_l.setText(f"Running...")
        self.ui.progress_l.setStyleSheet("color: black;")

        self.workerThread.start()
        #self.workerThread.wait()
    
    def stopExp(self):
        self.workerThread.requestInterruption()
    
    def addData(self, r2pos):
        self.mutex.lockForRead()
        newCurrents = self.workerThread.exp.newCurrents
        newVoltages = self.workerThread.exp.newVoltages
        newTimes = self.workerThread.exp.newTimes
        self.mutex.unlock()
        newDict = {"time": newTimes}
        newDict["position"] = [r2pos for _ in range(len(newTimes))]
        for key in newCurrents:
            newDict[f"current{key}"] = newCurrents[key]
        for key in newVoltages:
            newDict[f"voltage{key}"] = newVoltages[key]
        
        self.df = pd.DataFrame.from_dict(newDict)
        #print(self.df)
        if not os.path.exists(self.outputFile):
            self.df.to_csv(self.outputFile, mode="w", index=False)
        else:
            self.df.to_csv(self.outputFile, mode="a", index=False, header=False)
    
    def done(self):
        print(f"Done")
        if self.offThread == None or self.offThread.isFinished():
            self.offThread = turnOffVoltagesBackgroundThread(self.pSupply, self.usedChannels)
            self.offThread.start()
        currentTime = self.getCurrTime()
        self.end_time = time.time()
        self.actual_time = self.end_time - self.start_time
        formattedTime = self.formatSeconds(self.actual_time)
        self.ui.progress_l.setText(f"Done! ({currentTime})")
        self.ui.progress_l.setStyleSheet("color: green;")
        self.ui.exp_time_l.setText(f"Experiment took: {formattedTime[0]}:{formattedTime[1]}:{formattedTime[2]}")
        self.currents = self.workerThread.exp.currents
        self.voltages = self.workerThread.exp.voltages
        self.times = self.workerThread.exp.times

    def totalProgress(self, c):
        self.totalActions = c
        self.ui.progressBar.setMaximum(c)
        #print(f"Total Progress: {c}")
    
    def progressUpdate(self, c):
        self.currentAction = c
        self.ui.progressBar.setValue(c)
        #print(f"Progress update: {c}")
    
    def interruptCleanUp(self):
        if self.offThread == None or self.offThread.isFinished():
            self.offThread = turnOffVoltagesBackgroundThread(self.pSupply, self.usedChannels)
            self.offThread.start()
        self.end_time = time.time()
        self.actual_time = self.end_time - self.start_time
        currentTime = self.getCurrTime()
        self.ui.progress_l.setText(f"Interrupted! ({currentTime})")
        self.ui.progress_l.setStyleSheet("color: red;")
        formattedTime = self.formatSeconds(self.actual_time)
        self.ui.exp_time_l.setText(f"Experiment took: {formattedTime[0]}:{formattedTime[1]}:{formattedTime[2]}")
        
    
    def getStepsTime(self, stepTime, axis):
        self.stepTimes[self.axMap[axis]].append(stepTime)
        #print(f"{self.axMap[axis]} took {stepTime} to move!")
    
    def setExposure(self, fromMainWindow):
        if fromMainWindow:
            exposure = self.ui.exposure_cur_edit.text()
            self.camPopup.ui.exposure_cur_edit.setText(exposure)
        else:
            exposure = self.camPopup.ui.exposure_cur_edit.text()
            self.ui.exposure_cur_edit.setText(exposure)
        if self.isValid(exposure):
            self.ui.exposure_slider.setValue(int(exposure))
            self.camPopup.ui.exposure_slider.setValue(int(exposure))

    def setGain(self, fromMainWindow):
        if fromMainWindow:
            gain = self.ui.gain_cur_edit.text()
            self.camPopup.ui.gain_cur_edit.setText(gain)
        else:
            gain = self.camPopup.ui.gain_cur_edit.text()
            self.ui.gain_cur_edit.setText(gain)
        if self.isValid(gain):
            self.ui.gain_slider.setValue(int(gain))
            self.camPopup.ui.gain_slider.setValue(int(gain))
            

    def changeExposureLimits(self, fromMainWindow):
        if fromMainWindow:
            lower = self.ui.exposure_from_edit.text()
            upper = self.ui.exposure_to_edit.text()
            self.camPopup.ui.exposure_from_edit.setText(lower)
            self.camPopup.ui.exposure_to_edit.setText(upper)
        else:
            lower = self.camPopup.ui.exposure_from_edit.text()
            upper = self.camPopup.ui.exposure_to_edit.text()
            self.ui.exposure_from_edit.setText(lower)
            self.ui.exposure_to_edit.setText(upper)
        if self.isValid(lower) and self.isValid(upper):
            self.ui.exposure_slider.setMinimum(float(lower))
            self.ui.exposure_slider.setMaximum(float(upper))
            self.camPopup.ui.exposure_slider.setMinimum(float(lower))
            self.camPopup.ui.exposure_slider.setMaximum(float(upper))
            
    
    def changeGainLimits(self, fromMainWindow):
        if fromMainWindow:
            lower = self.ui.gain_from_edit.text()
            upper = self.ui.gain_to_edit.text()
            self.camPopup.ui.gain_from_edit.setText(lower)
            self.camPopup.ui.gain_to_edit.setText(upper)
        else:
            lower = self.camPopup.ui.gain_from_edit.text()
            upper = self.camPopup.ui.gain_to_edit.text()
            self.ui.gain_from_edit.setText(lower)
            self.ui.gain_to_edit.setText(upper)
        if self.isValid(lower) and self.isValid(upper):
            self.ui.gain_slider.setMinimum(float(lower))
            self.ui.gain_slider.setMaximum(float(upper))
            self.camPopup.ui.gain_slider.setMinimum(float(lower))
            self.camPopup.ui.gain_slider.setMaximum(float(upper))
            

    def changeExposure(self, slider):
        self.ui.exposure_cur_edit.setText(str(slider.value()))
        self.camPopup.ui.exposure_cur_edit.setText(str(slider.value()))
        if self.cam == None:
            return
        if self.isValid(str(slider.value())):
            self.cam.setProperty(20, slider.value())
    
    def changeGain(self, slider):
        self.ui.gain_cur_edit.setText(str(slider.value()))
        self.camPopup.ui.gain_cur_edit.setText(str(slider.value()))
        if self.cam == None:
            return
        if self.isValid(str(slider.value())):
            self.cam.setProperty(40, slider.value())

    def getSnapshotPath(self):
        counter = 1
        imgName = f"Exposure-{self.ui.exposure_cur_edit.text()}_Gain-{self.ui.gain_cur_edit.text()}"
        name = os.path.join(self.snapShotPath, f"{imgName}.png")
        while os.path.exists(name):
            name = os.path.join(self.snapShotPath, f"{imgName}-{counter}.png")
            counter += 1
        return name
    
    def camSnapShot(self):
        if self.cam == None:
            self.cam = Camera()
        exposure = self.ui.exposure_cur_edit.text()
        gain = self.ui.gain_cur_edit.text()
        if self.isValid(exposure) and self.isValid(gain):
            if self.snapShotPath == None:
                self.snapShotPath = str(QFileDialog.getExistingDirectory(self, "Select Directory"))
            self.cam.create_snapshot(exposure=float(exposure), gain=float(gain))
            res = self.cam.TakeSnapshot()
            path = self.getSnapshotPath()
            self.cam.SaveImage(res, path)
    
    def camPreview(self):
        
        if self.cam == None:
            self.cam = Camera()
        if self.cam.camera._displaying_window:
            self.cam.camera.StreamVideoControl("stop_streaming")
            self.cam.camera.DestroyDisplayWindow()
        self.cam.setProperty(20, int(self.ui.exposure_cur_edit.text()))
        self.cam.setProperty(40, int(self.ui.gain_cur_edit.text()))
        
        if not self.camPopup.shown:
            self.camPopup.show()
            self.camPopup.shown = True
        
        self.cam.camera.CreateDisplayWindow(title=b"Live Camera Preview")
        self.cam.camera.StreamVideoControl("start_display")
        

        
    def sToggle(self, c, ax):
        funcMap = {True: self.pSupply.enable_channel, False: self.pSupply.disable_channel}
        if self.pConnected:
            self.sStages[ax] = c
            funcMap[c](int(ax[1]))
    
    def getPlotFreq(self):
        return self.ui.freq_spin.value()

    def getPlotDataPoints(self):
        return self.ui.datapoints_spin.value()
    
    def plotChannel(self, channel):
        if not self.pSupply.isSetUp:
            self.pSupply.connectAll()
        if not any([self.plotting[key] for key in self.plotting]):
            self.plot_pipe, self.plotter_pipe = mp.Pipe()
            self.plotter = channelPlotter(self.getPlotDataPoints(), self.getPlotFreq())
            self.plot_process = mp.Process(target=self.plotter, args=(self.plotter_pipe,), daemon=True)
            self.plot_process.start()
            if self.plotUpdateThread == None:
                self.plotUpdateThread = updatePlotter(self.plotting, self.updatePlot, self.pSupply, self.getPlotFreq)
                self.plotUpdateThread.start()
        self.plotting[self.axMap[channel]] = not self.plotting[self.axMap[channel]]
        if not any([self.plotting[key] for key in self.plotting]):
            if self.plotter != None:
                self.plot_pipe.send(None)
        if self.plotting[self.axMap[channel]]:
            self.plotButtons[channel].setStyleSheet("background-color: green;")
        else:
            self.plotButtons[channel].setStyleSheet("background-color: red;")

    
    def updatePlot(self, finished=False):
        send = self.plot_pipe.send
        if finished:
            send(None)
        else:
            if any([self.plotting[key] for key in self.plotting]):
                data = []
                for key in self.plotting:
                    if self.plotting[key]:
                        #data.append(np.random.random())
                        data.append(self.pSupply.get_channel_current(int(key[1])))
                        #data.append(self.pSupply.get_channel_voltage(int(key[1])))
                    else:
                        data.append(None)
                data.append(self.getPlotDataPoints())
                send(data)

    """
    Checks that all relevant widgets contain an appropriate value
    """
    def validityCheck(self):
        self.resetSaveLabel()
        self.resetRedWidgets()
        if len(self.ui.name_inp.text()) == 0:
            self.reportError([self.ui.exp_l], "Please givce the experiment a name")
            return False
        anyChecked = False
        for i in self.checkboxes:
            if i.isChecked():
                anyChecked = True
                break
        if not anyChecked:
            self.reportError(self.checkboxes, "Check at least 1 checkbox")
            return False
        ordValues = []
        chekced_amount = 0
        for key in self.checked:
            if self.allMap[key]["stat"].isChecked():
                if not self.isValid(self.allMap[key]["start"].text()):
                    self.reportError([self.allMap[key]["start"]], "Invalid input!")
                continue
            if self.checked[key]:
                chekced_amount += 1
                ordValues.append(self.spinMap[key].value())
                for i in self.allDict[key][1:-1]:
                    if not self.isValid(i.text()):
                        self.reportError([self.allDict[key][0]], "Invalid input!")
                        return False
        if sorted(ordValues) != list(range(1, chekced_amount + 1)):
            self.reportError(self.column(self.allList, 4), f"Ordering doesn't match 1 to {chekced_amount}")
            return False
        if len(self.ui.tip_edit.text()) == 0:
            self.reportError([self.ui.tip_l], "Invalid input!")
            return False
        if len(self.ui.mf_edit.text()) == 0:
            self.reportError([self.ui.mf_l], "Invalid input!")
            return False
        return True
        

    def reportError(self, widgetList, errormsg):
        for i in range(len(widgetList)):
            widgetList[i].setStyleSheet("color: red;")
            self.redWidgets.append(widgetList[i])
        self.ui.err_l.setText(errormsg)
    
    def goToV(self, ax, v):
        if self.isValid(v) and self.pConnected:
            to = float(v)
            self.pSupply.set_channel_voltage(int(ax[1]), to)


    def voltConnect(self):
        if self.pConnected:
            self.voltBackgroundThread.requestInterruption()
            self.voltBackgroundThread.wait()
            self.pSupply.disconnectAll()
            self.pConnected = False
            self.ui.s_connect_btn.setText("Connect")
        else:
            self.pSupply.connectAll()
            self.pConnected = True
            self.voltBackgroundThread = voltageBackgroundThread(self.pSupply, self.posLabels)
            self.voltBackgroundThread.start()
            self.ui.s_connect_btn.setText("Disconnect")
    
    def camChanged(self, c):
        checked = bool(c)
        self.ui.cam_settings_frame.setVisible(checked)

    def statCheckBoxUpdate(self, ax):
        checked = self.allMap[ax]["stat"].isChecked()
        if checked:
            self.allMap[ax]["spin"].setValue(0)
            self.axOrder.remove(ax)
            self.setSpinOrder()
        else:
            self.axOrder.append(ax)
            self.setSpinOrder()
        
        for key in self.allMap[ax]:
            if key == "stop" or key == "step" or key == "spin":
                self.allMap[ax][key].setDisabled(checked)


    """
    Updates the visible widgets based on the state of the checkboxes
    x: list of widgets, such as self.r2_list
    c: Bool indicating wether or not the 'name'-checkbox is checked
    name: String indicating which checkbox was toggled (R2, S0, S1, S2, S3)
    """
    def checkBoxUpdate(self, x, c, name):
        self.resetSaveLabel()
        for i in x:
            i.setVisible(c)
        if name != None:
            self.checked[name] = c
            if c:
                self.axOrder.append(name)
            else:
                self.axOrder.remove(name)
            self.allDict[name][7].setVisible(self.allDict[name][6].isChecked())
        self.setSpinOrder()
    
    """
    Sets the number in the spinboxes based on self.axOrder
    """
    def setSpinOrder(self):
        for i in range(len(self.axOrder)):
            self.spinMap[self.axOrder[i]].setValue(i + 1)
    

    def updateSpinOrder(self):
        if not self.checkOrder():
            return
        
        axOrderList = [None, None, None, None, None]
        for key in self.spinMap:
            val = self.spinMap[key].value()
            if val > 0:
                axOrderList[val - 1] = key
        self.axOrder = [i for i in axOrderList if i != None]

    
    """
    Takes a string and checks if it is a valid float (Not valid: '-', '4.', '')
    toTest: String of a number (fx '-4.2' or '1.62')
    """
    def isValid(self, toTest):
        if len(toTest) == 0:
            return False
        if len(toTest) == 1 and toTest[0] == '-':
            return False
        if toTest[-1] == ".":
            return False
        if toTest[-1] == ",":
            return False
        return True
    

    """
    Takes a start, stop and step value and checks that the stage doesn't overshoot the stop-value ((stop - start)/step = int)
    start, stop, step: Float
    """
    def checkStepMatch(self, start, stop, step):
        return round((stop - start)/step, 10).is_integer()
    

    """
    Checks that all relevant input-fields have a valid value and calculates the total amount of steps
    """
    def calcSteps(self):
        self.resetSaveLabel()
        startVals = []
        stopVals = []
        stepVals = []
        self.turningPointsValues = []
        statSteps = 0
        for key in self.checked:
            if self.allMap[key]["stat"].isChecked():
                statSteps += 1
                continue
            if self.checked[key]:
                start = self.allDict[key][1].text()
                stop = self.allDict[key][2].text()
                step = self.allDict[key][3].text()
                if self.isValid(start) and self.isValid(stop) and self.isValid(step):
                    startVals.append(float(start))
                    stopVals.append(float(stop))
                    stepVals.append(float(step))
                    if self.allMap[key]["rev"].isChecked() and self.isValid(self.allDict[key][7].text()):
                        tp = self.allDict[key][7].text().split(",")
                        self.turningPointsValues.append([float(x) for x in tp])
                    else:
                        self.turningPointsValues.append(None)
                else: return
                if float(step) != 0.0:
                    if not self.checkStepMatch(float(start), float(stop), float(step)):
                        self.allDict[key][0].setStyleSheet("color: red;")
                        continue
                    else:
                        self.allDict[key][0].setStyleSheet("color: black;")
                    
                    if self.allMap[key]["rev"].isChecked() and self.isValid(self.allDict[key][7].text()):
                        tp = self.allDict[key][7].text().split(",")
                        tps = [float(x) for x in tp if self.isValid(x)]
                        for i in tps:
                            if not self.checkStepMatch(float(start), float(i), float(step)):
                                self.allDict[key][0].setStyleSheet("color: red;")
        steps = []
        
        for i in range(len(startVals)):
            if stepVals[i] != 0.0:
                if self.turningPointsValues[i] == None:
                    steps.append(((stopVals[i] - startVals[i]) / stepVals[i]) + 1)
                else:
                    stepamount = 0
                    for j in self.turningPointsValues[i]:
                        stepamount += (((j - startVals[i]) / stepVals[i]) * 2)
                    stepamount += (((stopVals[i] - startVals[i]) / stepVals[i]) * 2) + 1

                    steps.append(stepamount)
        totalSteps = np.prod(steps)# + statSteps
        self.ui.totalsteps_l.setText(str(int(totalSteps)))
    
    def getNewFilePath(self, filepath:'str') -> 'str': #If filepath exists add "-n" before the file extension (Before: test.json After: test-1.json)
        count = 0
        if os.path.exists(filepath):
            count = 1
            dotindx = filepath[1:].index(".")
            while os.path.exists(f"{filepath[:dotindx + 1]}-{count}{filepath[dotindx + 1:]}"):
                count += 1
        else:
            return filepath
        return f"{filepath[:dotindx + 1]}-{count}{filepath[dotindx + 1:]}"

    """
    Saves the state of all relevant widgets in a json file
    """
    def saveOptions(self):
        self.resetSaveLabel()
        if not os.path.isdir("./savedVoltOptions"):
            os.mkdir("./savedVoltOptions")
        saveDict = {}
        if len(self.ui.name_inp.text()) == 0:
            name = "UNNAMED"
        else:
            name = self.ui.name_inp.text()
        saveDict["name"] = name
        for i in range(len(self.axNames)):
            saveDict[self.axNames[i]] = self.checkboxes[i].isChecked()
        saveDict["R2Home"] = self.ui.home_r2_c.isChecked()
        saveDict["camera"] = self.ui.cam_c.isChecked()
        saveDict["TIP"] = self.ui.tip_edit.text()
        saveDict["MF"] = self.ui.mf_edit.text()
        sss_names = ["start", "stop", "step"]
        for i in range(len(self.allSSS)):
            lst = self.allSSS[i]
            for j in range(len(lst)):
                saveDict[self.axNames[i] + "_" + sss_names[j]] = lst[j].text()
        for key in self.spinMap:
            saveDict[key + "_" + "spin"] = self.spinMap[key].value()
        saveDict["R2Stat"] = self.ui.r2_stat_c.isChecked()
        saveDict["S0Stat"] = self.ui.s0_stat_c.isChecked()
        saveDict["S1Stat"] = self.ui.s1_stat_c.isChecked()
        saveDict["S2Stat"] = self.ui.s2_stat_c.isChecked()
        saveDict["S3Stat"] = self.ui.s3_stat_c.isChecked()
        saveDict["exposure"] = self.ui.exposure_cur_edit.text()
        saveDict["exposureFrom"] = self.ui.exposure_from_edit.text()
        saveDict["exposureTo"] = self.ui.exposure_to_edit.text()
        saveDict["gain"] = self.ui.gain_cur_edit.text()
        saveDict["gainFrom"] = self.ui.gain_from_edit.text()
        saveDict["gainTo"] = self.ui.gain_to_edit.text()
        saveDict["r2_reverse_c"] = self.ui.r2_reverse_c.isChecked()
        saveDict["s0_reverse_c"] = self.ui.s0_reverse_c.isChecked()
        saveDict["s1_reverse_c"] = self.ui.s1_reverse_c.isChecked()
        saveDict["s2_reverse_c"] = self.ui.s2_reverse_c.isChecked()
        saveDict["s3_reverse_c"] = self.ui.s3_reverse_c.isChecked()
        saveDict["r2_turns"] = self.ui.r2_tp_edit.text()
        saveDict["s0_turns"] = self.ui.s0_tp_edit.text()
        saveDict["s1_turns"] = self.ui.s1_tp_edit.text()
        saveDict["s2_turns"] = self.ui.s2_tp_edit.text()
        saveDict["s3_turns"] = self.ui.s3_tp_edit.text()
        
        

        
        
        saveDict["dummyPS"] = self.ui.dummy_PS_c.isChecked()
        path = self.getNewFilePath(f"./savedVoltOptions/{name}.json")
        with open(path, "w") as outfile:
            json.dump(saveDict, outfile)
        date = datetime.datetime.now().strftime("%H:%M:%S")
        self.ui.saved_l.setText(f"Saved {date}")
    
    """
    Lets the user choose a file (json) and loads options from that file
    """
    def loadOptions(self):
        self.resetSaveLabel()
        filename, _ = QFileDialog.getOpenFileName(self, ("Open File"), "./savedVoltOptions")
        file = open(filename, "r")
        saveDict = json.load(file)
        file.close()
        self.ui.name_inp.setText(saveDict["name"])
        for i in range(len(self.axNames)):
            self.checkboxes[i].setChecked(saveDict[self.axNames[i]])
        self.ui.home_r2_c.setChecked(saveDict["R2Home"])
        self.ui.cam_c.setChecked(saveDict["camera"])
        self.ui.tip_edit.setText(saveDict["TIP"])
        self.ui.mf_edit.setText(saveDict["MF"])
        sss_names = ["start", "stop", "step"]
        for i in range(len(self.allSSS)):
            lst = self.allSSS[i]
            for j in range(len(lst)):
                lst[j].setText(saveDict[self.axNames[i] + "_" + sss_names[j]])
        for key in self.spinMap:
            val = saveDict[key + "_" + "spin"]
            self.spinMap[key].setValue(val)
        self.updateSpinOrder()
        self.ui.dummy_PS_c.setChecked(saveDict["dummyPS"])
        self.ui.r2_stat_c.setChecked(saveDict["R2Stat"])
        self.ui.s0_stat_c.setChecked(saveDict["S0Stat"])
        self.ui.s1_stat_c.setChecked(saveDict["S1Stat"])
        self.ui.s2_stat_c.setChecked(saveDict["S2Stat"])
        self.ui.s3_stat_c.setChecked(saveDict["S3Stat"])
        self.ui.exposure_cur_edit.setText(saveDict["exposure"])
        self.ui.exposure_from_edit.setText(saveDict["exposureFrom"])
        self.ui.exposure_to_edit.setText(saveDict["exposureTo"])
        self.ui.gain_cur_edit.setText(saveDict["gain"])
        self.ui.gain_from_edit.setText(saveDict["gainFrom"])
        self.ui.gain_to_edit.setText(saveDict["gainTo"])
        if "r2_reverse_c" in saveDict.keys():
            self.ui.r2_reverse_c.setChecked(saveDict["r2_reverse_c"])
            self.ui.s0_reverse_c.setChecked(saveDict["s0_reverse_c"])
            self.ui.s1_reverse_c.setChecked(saveDict["s1_reverse_c"])
            self.ui.s2_reverse_c.setChecked(saveDict["s2_reverse_c"])
            self.ui.s3_reverse_c.setChecked(saveDict["s3_reverse_c"])

            self.ui.r2_tp_edit.setText(saveDict["r2_turns"])
            self.ui.s0_tp_edit.setText(saveDict["s0_turns"])
            self.ui.s1_tp_edit.setText(saveDict["s1_turns"])
            self.ui.s2_tp_edit.setText(saveDict["s2_turns"])
            self.ui.s3_tp_edit.setText(saveDict["s3_turns"])
            

            
        self.calcSteps()

        
        
    
    """
    Creates and returns the array [axes], ordered where the left-most one moves last
    """
    def getAxes(self):
        axes = [self.axOrder[i] for i in range(len(self.axOrder) - 1, -1, -1)]
        statAxes = self.getStatAxes()
        allAxes = []
        for i in axes:
            allAxes.append(i)
        for key in statAxes:
            allAxes.append(key)
        self.usedChannels = sorted([int(i[1]) for i in allAxes if i[0] == 'S'])
        return axes
        
    
    def getStatAxes(self):
        statAxes = {}
        for i in self.axNames:
            if self.checked[i] and self.allMap[i]["stat"].isChecked():
                statAxes[i] = float(self.allMap[i]["start"].text())
        return statAxes

    """
    Creates and returns the 2-d array [start, stop, step], where start could be [1, 4, 6] if we have 3 axis starting in 1, 4 and 6
    This is ordered, so it matches the order of self.getAxes
    """
    def getSSS(self):
        sss = [[None, None, None, None, None], [None, None, None, None, None], [None, None, None, None, None]]
        for key in self.allDict:
            lst = [self.allMap[key]["start"], self.allMap[key]["stop"], self.allMap[key]["step"]]
            if self.checked[key] and not self.allMap[key]["stat"].isChecked():
                for j in range(len(lst)):
                    sss[j][self.axOrder.index(key)] = float(lst[j].text())

        # Removes all "None"
        for i in range(len(sss)):
            sss[i] = [j for j in sss[i] if j !=None]

        # Flips the arrays, since we want the left-most to turn last, and right now it is the other way around
        for i in range(len(sss)):
            sss[i] = [sss[i][j] for j in range(len(sss[i]) - 1, -1, -1)]

        return sss

    def checkOrder(self):
        ordValues = []
        chekced_amount = 0
        for key in self.checked:
            if self.checked[key]:
                chekced_amount += 1
                ordValues.append(self.spinMap[key].value())
        if sorted(ordValues) != list(range(1, chekced_amount + 1)):
            return False
        return True

    
    def column(self, matrix, i):
        return [row[i] for row in matrix]

    def resetRedWidgets(self):
        for i in self.redWidgets:
            i.setStyleSheet("color: black;")
        self.redWidgets = []

    def resetSaveLabel(self):
        self.ui.saved_l.setText("")
    
    def getCurrTime(self):
        date = datetime.datetime.now()
        actualTime = date.strftime("%H:%M:%S")
        return actualTime

    def zeroPad(self, n):
        res = ""
        if n < 10:
            res = "0" + str(n)
        else:
            res = str(n)
        return res
    
    def formatSeconds(self, s):
        hours = int(s/3600)
        s -= hours * 3600
        minutes = int(s/60)
        s -= minutes * 60
        seconds = round(s, 3)
        return [self.zeroPad(hours), self.zeroPad(minutes), self.zeroPad(seconds)]

    def makeDir(self, path):
        if os.path.exists(path):
            count = 1
            while os.path.exists(path + f"_{count}"):
                count += 1
            os.mkdir(path + f"_{count}")
            return path + f"_{count}"
        else:
            os.mkdir(path)
            return path
        
    def closeEvent(self, event):
        self.hide()
        if self.offThread == None or self.offThread.isFinished():
            self.offThread = turnOffVoltagesBackgroundThread(self.pSupply, [0, 1, 2, 3])
            self.offThread.start()
        if self.controller.connected:
            self.controller.connectDisconnect()
        self.offThread.wait()
        if self.pConnected:
            self.voltConnect()
        if self.plotUpdateThread != None:
            self.plotUpdateThread.requestInterruption()
            self.plotUpdateThread.wait()
        if self.cam != None:
            if self.cam.camera._displaying_window:
                self.cam.camera.StreamVideoControl("stop_streaming")
                self.cam.camera.DestroyDisplayWindow()
        

class updatePlotter(QThread):
    def __init__(self, plotting, updateFunc, pSupply, freq) -> None:
        QThread.__init__(self)
        self.plotting = plotting
        self.updateFunc = updateFunc
        self.pSupply = pSupply
        self.freq = freq
    
    def run(self):
        while True:
            if self.isInterruptionRequested():
                break
            if any([self.plotting[key] for key in self.plotting]):
                self.updateFunc()
            time.sleep(1/self.freq())
        return

class channelPlotter(object):
    def __init__(self, maxDataPoints, updateSpeed):
        self.x = [[], [], [], []]
        self.y = [[], [], [], []]
        self.colorMap = {0: 'r', 1: 'g', 2: 'b', 3: 'y'}
        self.labelMap = {0: "S0", 1: "S1", 2: "S2", 3: "S3"}
        self.maxXValue = -1
        self.maxDataPoints = maxDataPoints
        self.updateSpeed = updateSpeed
        self.plotting = {"S0": False, "S1": False, "S2": False, "S3": False}
        self.legendLoc = ['upper left', 'upper right', 'lower left', 'lower right']
        self.handles = [None, None, None, None]
        self.legends = [None, None, None, None]
        self.newAxes = [False, False, False, False]
        
    def terminate(self):
        pass
        #plt.close('all')
    
    def cleanXY(self):
        for i in range(len(self.x)):
            if len(self.x[i]) > 0 and max(self.x[i]) < self.maxXValue - self.maxDataPoints:
                self.x[i] = []
                self.y[i] = []

    def call_back(self):
        try:
            self.pipe.poll()
        except:
            return False
        
        while self.pipe.poll():
            
            self.maxXValue += 1
            command = self.pipe.recv()
            if command is None:
                self.terminate()
                return False
            else:
                for i in range(len(command) - 1):
                    if command[i] != None:
                        if not self.plotting[self.labelMap[i]]:
                            self.newAxes[i] = True
                            self.plotting[self.labelMap[i]] = True
                        self.x[i].append(self.maxXValue)
                        self.y[i].append(command[i])
                self.maxDataPoints = command[-1]

        self.cleanXY()
        for i in range(len(self.y)):
            if len(self.y[i]) > 1:
                self.handles[i] = self.ax.plot(self.x[i], self.y[i], color=self.colorMap[i], label=self.labelMap[i])
                if self.newAxes[i]:
                    self.legends[i] = self.ax.legend(handles=self.handles[i], loc=self.legendLoc[i])
                    self.ax.add_artist(self.legends[i])
                    self.newAxes[i] = False
        
        if self.maxXValue > self.maxDataPoints:
            self.ax.set_xlim(self.maxXValue - self.maxDataPoints, self.maxXValue)
        else:
            if self.maxXValue == 0:
                self.ax.set_xlim(0, 1)
            else:
                self.ax.set_xlim(0, self.maxXValue)

        self.fig.canvas.draw()
        return True

    def __call__(self, pipe):
        self.pipe = pipe
        self.fig, self.ax = plt.subplots()
        self.ax.legend(["S0", "S1", "S2", "S3"])
        timer = self.fig.canvas.new_timer(interval=10)
        timer.add_callback(self.call_back)
        timer.start()
        plt.show()

if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = MainWindow()
    widget.show()
    sys.exit(app.exec())