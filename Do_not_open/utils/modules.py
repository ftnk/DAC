import numpy as np

class Scanner():
    def __init__(self,readerType,axes,specialAxis,vaccumaxis,startCoords,stopCoords,steps,scanStage,specialInst,vaccumStage,scanReader,stageSettleTime,resolution,stageData,useCam,oscChannel,home_after,timescan,timemeas,amp_par,onlyamp=[False, 1, 1]):
        self._axis_num = len(axes)
        self._saxis_num = len(specialAxis) 
        self._vaxis_num = len(vaccumaxis)  
        self._nameMap = {}
        self._revNameMap = {}
        self.readerType = readerType
        self.stageData = stageData
        for stage in stageData:
            # map axis name to stage name
            self._nameMap[stage[3]] = stage[0]
        for stage in stageData:
            # map stage name to axis name
            self._revNameMap[stage[0]] = stage[3]
        self._numToAxMap = {0: "VX", 1: "VY", 2: "VZ"}
        self._axes = axes
        self._saxes = specialAxis
        self._vaxis = vaccumaxis
        self.startCoords = startCoords
        self.stopCoords = stopCoords
        self.steps = steps
        self.scanStage = scanStage
        self.vaccumStage = vaccumStage
        self.scanReader = scanReader
        self.stageSettleTime =stageSettleTime
        self._resolution = resolution
        self._actions = []
        self._specialInst = specialInst
        self.useCam = useCam
        self.oscChannel = oscChannel
        self.home_after = home_after
        self.timescan = timescan
        self.amp_par = amp_par
        if timescan:
            self._nameMap["TimeStage"] = "TimeStage"
            self._revNameMap["TimeStage"] = "TimeStage"
        if len(timemeas) > 0:
            self.timemeas = int(timemeas)
        else:
            self.timemeas = 0
        self.onlyamp = onlyamp
        self.decimals = self.getDecimals()
        self.digits = 0
        for key in self.decimals:
            if self.decimals[key] > self.digits:
                self.digits = self.decimals[key]
        self.allSteps = self.createStepsArray()
        self.usedSteps = 0
        self.specialWaitTime = 0.5
        #self.printInfo()

    def printInfo(self):
        params = [self.readerType, self._axes, self._saxes, self._vaxis, self.startCoords, self.stopCoords,
                  self.steps, self.scanStage, self._specialInst, self.vaccumStage, self.scanReader, self.stageSettleTime,
                  self._resolution, self.stageData]
        otherVars = [self._axis_num, self._saxis_num, self._vaxis_num, self._nameMap, self._revNameMap, self._actions]
        print("Scanner parameters:")
        for i in params:
            print(i)
        print("\nOther Variables:")
        for i in otherVars:
            print(i)
    
    def isOutOfBounds(self, stop, step, curr):
        if step < 0:
            return curr < stop
        if step > 0:
            return curr > stop
        
    def createStepsArray(self):
        start = np.array(self.startCoords).copy()
        stop = np.array(self.stopCoords).copy()
        step = np.array(self.steps).copy()
        coords = [start.copy()]
        done = False
        while not done:
            curr = coords[-1].copy()
            curr[-1] = round(curr[-1] + step[-1], self.digits)
            for i in range(len(curr) - 1, -1, -1):
                if self.isOutOfBounds(stop[i], step[i], curr[i]):
                    if i == 0:
                        done = True
                        break
                    curr[i] = start[i]
                    curr[i-1] = round(curr[i-1] + step[i-1], self.digits)
            if not done:
                coords.append(curr)
        return coords

    def getDecimals(self):
        decimals = {}
        start = self.startCoords
        stop = self.stopCoords
        steps = self.steps
        for i in range(len(steps)):
            step = float(steps[i])
            c_start = float(start[i])
            c_stop = float(stop[i])
            dotindx_step = str(step).index(".")
            dotindx_start = str(c_start).index(".")
            dotindx_stop = str(c_stop).index(".")
            step_dec = len(str(step)[dotindx_step + 1:])
            start_dec = len(str(c_start)[dotindx_start + 1:])
            stop_dec = len(str(c_stop)[dotindx_stop + 1:])
            decimals[self._axes[i]] = max([step_dec, start_dec, stop_dec])
        return decimals
    
    def getnameIndex(self,stage_name):
        # get the name of axis from stage name
        return self._axes.index(self._revNameMap[stage_name])
    
    def getsIndex(self,saxis):
        # get the name of stage from axis name
        return self._axes.index(saxis)
    
    def getvIndex(self,vaxis):
        return self._vaxis.index(vaxis)

    def scan_prelim(self,axis,actions, first):
        #for i in [axis, actions]: print(i)
        temp_actions =[]
        index = self._axes.index(axis)   ## index of the coordinates to be used for this scan
        startCoords = float(self.startCoords[index]) 
        stopCoords = float(self.stopCoords[index])
        steps = self.steps[index]
        if axis in self._vaxis:
            temp_actions.append(Action(self.vaccumStage,'MoveStage',[axis,[startCoords]]))
            #if not first:
            #    temp_actions.append(Action('SYSTEM','Wait',[self.stageSettleTime]))
        elif axis in self._saxes:
            temp_actions.append(Action(self._specialInst,'MoveAxis',[axis,[startCoords]]))
            #if not first:
            #    temp_actions.append(Action('SYSTEM','Wait',[self.stageSettleTime]))
        elif axis == "TimeStage":
            temp_actions.append(Action("TimeStage",'MoveStage',["TimeStage",[startCoords]]))
        else:
            temp_actions.append(Action(self.scanStage,'MoveStage',[self._nameMap[axis],[startCoords]]))
            #if not first:
            #    temp_actions.append(Action('SYSTEM','Wait',[self.stageSettleTime]))
        
        for step in range(round((startCoords+steps)*self._resolution),round((stopCoords+steps)*self._resolution),round(steps*self._resolution)):
            temp_actions+=actions
            coord = step/self._resolution
            if axis in self._vaxis:
                temp_actions.append(Action(self.vaccumStage,'MoveStage',[axis,[round(step/self._resolution, self.decimals[axis])]]))
                #temp_actions.append(Action('SYSTEM','Wait',[self.stageSettleTime]))
            elif axis in self._saxes:
                temp_actions.append(Action(self._specialInst,'MoveAxis',[axis,[round(step/self._resolution, self.decimals[axis])]]))
                #temp_actions.append(Action('SYSTEM','Wait',[self.stageSettleTime]))
            elif axis == "TimeStage":
                temp_actions.append(Action("TimeStage",'MoveStage',["TimeStage",[round(step/self._resolution, self.decimals[axis])]]))
            else:
                temp_actions.append(Action(self.scanStage,'MoveStage',[self._nameMap[axis],[round(step/self._resolution, self.decimals[axis])]]))
                #if first:
                #    temp_actions.append(Action('SYSTEM','Wait',[self.stageSettleTime]))
        temp_actions+=actions
        return temp_actions
    
    def sscan_prelim(self,axis,actions):
        temp_actions =[]
        index = self._axes.index(axis)   ## index of the coordinates to be used for this scan
        startCoords = float(self.startCoords[index]) 
        stopCoords = float(self.stopCoords[index])
        steps = self.steps[index]
        temp_actions.append(Action(self._specialInst,'MoveAxis',[self._nameMap[axis],[startCoords]]))
        temp_actions.append(Action('SYSTEM','Wait',[self.stageSettleTime]))
        
        for step in range(int((startCoords+steps)*self._resolution),
                            int((stopCoords+steps)*self._resolution),
                            int(steps*self._resolution)):
            temp_actions+=actions
            coord = step/self._resolution
            temp_actions.append(
                                Action(self.scanStage,'MoveStage',
                                [self._nameMap[axis],[step/self._resolution]])
                                )
            temp_actions.append(Action('SYSTEM','Wait',[self.stageSettleTime]))
        temp_actions+=actions
        return temp_actions
        
    def add_read_actions(self):
        actions =[]
        if self.onlyamp[0]:
            actions.append(Action("Ammeter", "READAMP", [self.onlyamp[1], self.onlyamp[2]]))
        else:
            actions.append(Action(self.scanReader,'SET_ACQ_STATE',['STOP']))
            actions.append(Action('SYSTEM','Wait',[self.specialWaitTime]))
            actions.append(Action(self.scanReader,'GET_DATA',[self.oscChannel]))
            actions.append(Action(self.scanReader,'SET_ACQ_STATE',['RUN']))
            #actions.append(Action('SYSTEM','Wait',[5.0])) #ONLY FOR CUSTOM EXPERIMENT
        return actions
    
    def add_read_actions_lockin(self):
        actions =[]
        actions.append(Action('SYSTEM','Wait',[0.5]))
        actions.append(Action(self.scanReader,'GET_DATA',[2]))
        return actions
    
    def insertCameraActions(self, actions):
        actionLen = len(actions)
        i = 0
        while i < actionLen:
            if actions[i]._actionType == "GET_DATA":
                actions.insert(i+1, Action("Camera", "SnapShot", self.allSteps[self.usedSteps]))
                self.usedSteps += 1
                actionLen += 1
            i += 1
        return actions


    def insertWaitAndReset(self, actions, amp_par):
        actionLen = len(actions)
        i = 0
        while i < actionLen - 1:
            action_i_move = actions[i]._actionType == "MoveStage" or actions[i]._actionType == "MoveAxis"
            action_i_1_move = actions[i + 1]._actionType == "MoveStage" or actions[i + 1]._actionType == "MoveAxis"
            if action_i_move and not action_i_1_move:
                if amp_par[0]:
                    actions.insert(i+1, Action("Ammeter", "READAMP", [amp_par[1], amp_par[2]]))
                else:
                    actions.insert(i+1, Action('SYSTEM','Wait',[self.stageSettleTime]))
                if self.readerType == 0:
                    actions.insert(i+1, Action(self.scanReader, "RESET_HIST", [1]))
                    actionLen += 1
                
                actionLen += 1
            i += 1
        return actions
    
    def insertHomeAfterActions(self, actions):
        for i in range(len(self.home_after)):
            if self.home_after[i]:
                stage_name = self._numToAxMap[i]
                actions.append(Action(instName="VaccumStage", actionType="MoveStage", actionData=[stage_name, [0.0]]))
        return actions


    def scanND(self, n_times=1):   
        dimensions = self._axis_num
        if self.readerType == 0:
            actions = []
            for _ in range(n_times):
                res = self.add_read_actions()
                actions = actions + res
        if self.readerType == 1:
            actions = self.add_read_actions_lockin()
        first = True
        while dimensions>0:
            actions = self.scan_prelim(self._axes[dimensions-1],actions, first)
            dimensions-=1 
            first = False
        actions = self.insertWaitAndReset(actions, self.amp_par)
        if self.useCam:
            actions = self.insertCameraActions(actions)
        if len(self.home_after) > 0:
            actions = self.insertHomeAfterActions(actions)
        return actions
  
    def get_actions(self):
        return (self._actions)

    def initiate(self):
        for axis in self._axes:
            index = self._axisMap[axis]
            self._actions.append(Action(self.scanStage,'MoveStage'
                            ,[self._nameMap[axis],self.startCoords[index]]))
    
    def getStartPosActions(self):
        splitIndex = -1
        for i in range(len(self._actions)):
            if self._actions[i]._actionType != "MoveStage" and self._actions[i]._actionType != "Wait":
                splitIndex = i
                break
        startActions = self._actions[:splitIndex]
        self._actions = self._actions[splitIndex:]
        for i in range(len(startActions) -1, -1, -1):
            if startActions[i]._actionType == "Wait":
                del startActions[i]

        return startActions

    def compile(self, n_times=1, sysTest=False):
        if sysTest: self.specialWaitTime = 0.0
        #self.initiate()
        #print("STARTACTIONLIST")
        #for i in self._actions: print(i)
        self._actions += self.scanND(n_times)
        startPosActions = self.getStartPosActions()
        #print(startPosActions)
        #print("###########################")
        #for i in self._actions: print(i)
        #print("STOPACTIONLIST")


        if self._specialInst == "VoltageCont":
            for i in self._saxes:
                if i in self._axes:
                    self._actions.append(
                        Action(self._specialInst,'ShutDown',[i,['']])
                        )
        """
        i = 0
        while i < len(self._actions)-1:
        #for i in range(len(self._actions)-1):
            if self._actions[i]._actionType == "MoveStage":
                self._actions.insert(i+1, Action("TempAmp", "MultipleTypes", []))
                #print(f"This action is a movestage:\n{self._actions[i]}\n")
            i += 1
        """
        if self.onlyamp[0]:
            self._actions.insert(0, Action("Ammeter", "TIMEOUT", [60000]))
            self._actions.insert(0, Action("Ammeter", "POWER", [1]))
            self._actions.append(Action("Ammeter", "POWER", [0]))

        actions_to_print = -1

        for i in self._actions:
            print(i)
            if actions_to_print != -1:
                actions_to_print -= 1
                if actions_to_print == 0: break

        return self._actions, startPosActions


if __name__ == "__main__":
    from action import Action
    axes = ['S1','X','Y','Z']       #definition found in line 88ff  ,'Z', 'R1'    LEFTMOST APRAMETERS TURNS LAST
    startCoords = [1300,3,3,3]      
    stopCoords = [1200,1,1,1]     
    steps =  [100,-1,-1,-1]  
    scanStage = 'ScanStage'  
    specialInst = "VoltageCont"  
    scanReader = 'ScanOsc'       
    stageSettleTime = 0.5      
    resolution = 100   
    stageData = []
    #specialAxis = []
    specialAxis = ['S1','S2','S3','S4']
    specialInst1Name = "VoltageCont"
    specialInst1Type = "VoltageController"
    stageData.append(['X-Stage','linear',27256338,'X',False]) 
    stageData.append(['Y-Stage','linear',27255894,'Y',False])  
    stageData.append(['Z-Stage','linear',27005114,'Z',False])
    stageData.append(['rotating','rotational',2700500,'R1',False])
    stageData.append(['wiregrid','rotational',7255354,'R2',False])
    scan = Scanner(axes,specialAxis,startCoords,stopCoords,steps,scanStage,
                    specialInst,scanReader,stageSettleTime,resolution,
                    stageData)
    scan.compile()
else:
    from .action import Action

        