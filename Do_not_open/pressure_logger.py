import datetime
from dtu.dev.vacuum_controllers import TPG362

gauge = TPG362()

fname = 'pressure_data.dat'

while True:
    print('Logging pressure')
    with open(fname, 'a') as f:
        f.write('%s, %s\n' % (datetime.datetime.now().strftime('%Y%m%d-%H:%M:%S'), gauge.get_pressure(gauge=1)))