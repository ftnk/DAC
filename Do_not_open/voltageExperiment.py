"""
@author: Rune
"""

import os
from ctypes import c_char_p
import numpy as np
import threading
import time
import pandas as pd
import matplotlib.pyplot as plt
from utils.action import Action
from PySide6.QtCore import SIGNAL, QObject, QThread
from hardware.camera import Camera
import math


class Experiment:
    def __init__(self, SSS, axes, statAxes, turningPointsValues, tip, mf, totalSteps, useCam=None, volt=None, stage=None, workerThread=None, mutex=None, dummy=False, camDict=None, outputDir=None) -> None:
        self.data = pd.DataFrame()
        self.axes = axes
        self.statAxes = statAxes
        self.turningPointsValues = turningPointsValues
        allAxes = []
        for i in self.axes:
            allAxes.append(i)
        for key in self.statAxes: allAxes.append(key)
        self.mutex = mutex
        self.axMap = {}
        self.revAxMap = {}
        for i, j in enumerate(self.axes):
            self.axMap[j] = i       # R2: 0, S1: 1
            self.revAxMap[i] = j    # 0: R2, 1: S1
        self.usedChannels = sorted([int(i[1]) for i in allAxes if i[0] == 'S'])
        self.SSS = SSS
        self.tip = tip
        self.mf = mf
        self.totalSteps = totalSteps
        self.volt = volt
        self.stage = stage
        self.useCam = useCam
        self.dummy = dummy
        self.camDict = camDict
        self.outputDir = outputDir
        self.imagesPath = None
        if self.useCam:
            self.cam = Camera()
            self.cam.create_snapshot(exposure=self.camDict["exposure"], gain=self.camDict["gain"])
            self.imagesPath = os.path.join(self.outputDir, "images")
            if not os.path.exists(self.imagesPath):
                os.mkdir(self.imagesPath)
        else:
            self.cam = None
        self.digits = self.findDigits(self.SSS[2])
        self.actionList = []
        self.stageEpsilon = 0.001
        self.currents = {ch:[] for ch in self.usedChannels}
        self.voltages = {ch:[] for ch in self.usedChannels}
        self.times = []
        self.newCurrents = None
        self.newVoltages = None
        self.newTimes = None
        self.latestPos = -1.0
        self.currentCoords = []
        self.workerThread = workerThread
        
    """
    Returns how many decimals are needed based on the amount of decimals in step_size
    """
    def findDigits(self,step_size):
        maxDigits = -1
        for i in step_size:
            dotIndx = str(i).find(".")+1
            if len(str(i)[dotIndx:]) > maxDigits:
                maxDigits = len(str(i)[dotIndx:])
        return maxDigits

    """
    Creates a dictionary of the form: {axis: [start, stop, step]} (fx: {S0: -2000, -1950, 5}) that contains all axes
    """
    def createMovementMap(self):
        resDict = {}
        for i in self.axes: resDict[i] = []
        for key in self.statAxes: resDict[key] = []
        for i in range(len(self.SSS)):
            for j in range(len(self.SSS[i])):
                resDict[self.axes[j]].append(self.SSS[i][j])
        for key in self.statAxes:
            resDict[key].append(self.statAxes[key])
        return resDict

    """
    Makes sure all instruments are ready to start the experiment (they are connected and in the starting positions)
    """
    def initialize_instruments(self):
        self.volt.connectAll()
        movementMap = self.createMovementMap()
        print(movementMap)

        print(f"Initializing instruments...")

        for key in movementMap:
            if key[0] == 'S':
                #pass
                self.volt.disable_channel(int(key[1]))
        for key in movementMap:
            if key[0] == 'S':
                print(f"    Ramping {key} to {movementMap[key][0]}")
                self.volt.set_channel_voltage(int(key[1]), movementMap[key][0])
                self.volt.enable_channel(int(key[1]))
        
        print(f"    Waiting for ramp to complete for channels {self.usedChannels}...")
        time.sleep(2)
        while self.volt.check_if_ramping(self.usedChannels):
            time.sleep(0.5)
        
        print(f"    Ramping complete!\n")

        if "R2" in self.axes:
            if not self.stage.isSetUp:
                print(f"    Configuring R2")
                self.stage.late_setup()
                print(f"    R2 has been configured!\n")
            r2StartPos = movementMap["R2"][0]
            print(f"    Moving R2 to start position ({r2StartPos})...")
            self.stage.move_device(r2StartPos)
            print(f"    R2 has move to {r2StartPos}!\n")
            self.latestPos = r2StartPos
        
        print(f"Instruments have benn initialized!\n")
    
    """
    Prints a list of actions, so they are easily readable from the terminal
    """
    def printActionList(self, actionList):
        lastPrint = ""
        counter = 1
        for i in actionList:
            if i._instName == "PS":
                toPrint = lastPrint
            else:
                toPrint = ""
                for j in self.axes:
                    if j == i._instName:
                        break
                    toPrint += "  "
                lastPrint = toPrint
            print(str(counter) + ":" + toPrint + str(i))
            counter += 1

    def addAction(self, inst_name, action_type, action_data):
        if self.dummy:
            inst_name = "DUMMY-" + inst_name
        self.actionList.append(Action(instName=inst_name, actionType=action_type, actionData=action_data))


    def addStartActions(self, start, axes):
        for i in range(len(axes)):
            self.addAction(axes[i], action_type="Move", action_data=start[i])
        self.addAction("Program", "Update", np.array(start).copy().tolist())
        if self.useCam:
            self.addAction("Camera", "TakeImage", start)
        self.addAction("PS", "Read", "V")

        
    def getSteps1D(self, start, stop, step, turningPoints):
        #print(f"Called with: {start}, {stop}, {step}, {turningPoints}")
        if turningPoints == None:
            res = np.linspace(start + step, stop, int(abs(stop - start) / step)).tolist()
            res = [round(x, self.digits) for x in res]
            res.append(start)
            return res
        else:
            res = []
            first = True
            for i in turningPoints:
                arr1 = np.linspace(start, i, round(abs(i - start) / step) + 1, endpoint=True).tolist()
                arr1 = [round(x, self.digits) for x in arr1]
                if first:
                    arr1 = arr1[1:]
                    first = False

                arr2 = np.linspace(i, start, round(abs(i - start) / step) + 1).tolist()
                arr2 = [round(x, self.digits) for x in arr2]

                arr = arr1 + arr2
                res += arr

            fullMotion = np.linspace(start, stop, int(abs(stop - start) / step) + 1)
            fullMotion_rev = np.flip(fullMotion.copy()).tolist()
            fullMotion = fullMotion.tolist()
            fullMotion = [round(x, self.digits) for x in fullMotion]
            fullMotion_rev = [round(x, self.digits) for x in fullMotion_rev]
            res += fullMotion
            res += fullMotion_rev
            return res

    def getMoveActions1D(self, steps, axis):
        actions = []
        counter = 0
        if self.dummy:
            axName = "DUMMY-" + axis
            psName = "DUMMY-PS"
        else:
            psName = "PS"
            axName = axis
        for i in steps:
            if counter > 0:
                if steps[counter] != steps[counter - 1]:
                    actions.append(Action(instName=axName, actionType="Move", actionData=i))
            else:
                actions.append(Action(instName=axName, actionType="Move", actionData=i))
            if counter == len(steps) - 1:
                if self.turningPointsValues[self.axMap[axis]] != None:
                    actions.append(Action(instName=psName, actionType="Read", actionData="V"))
            else:
                actions.append(Action(instName=psName, actionType="Read", actionData="V"))
            counter += 1

        return actions

    def getCoords(self, actions):
        for i in range(len(actions) - 1, -1, -1):
            if actions[i]._actionType == "Update":
                return actions[i]._actionData
    
    def addDimension(self, actions, newSteps, newAxis, newAxisStart):
        baseActions = np.array(actions).copy().tolist()
        #print(f"Adding dimension for axis: {newAxis}")
        if self.dummy:
            psName = "DUMMY-PS"
            newAxName = "DUMMY-" + newAxis
        else:
            psName = "PS"
            newAxName = newAxis
        for i in range(len(newSteps)):
            if i > 0:
                if newSteps[i] == newSteps[i-1]:
                    #print(f"Similar steps at {i} and {i-1} in {newSteps}")
                    actions += [Action(instName=psName, actionType="Read", actionData="V")]
                elif i == len(newSteps) - 1 and self.turningPointsValues[self.axMap[newAxis]] == None:
                    actions += [Action(instName=newAxName, actionType="Move", actionData=newAxisStart)]
                else:
                    actions += [Action(instName=newAxName, actionType="Move", actionData=newSteps[i]), Action(instName=psName, actionType="Read", actionData="V")]
                    actions += baseActions
            else:
                actions += [Action(instName=newAxName, actionType="Move", actionData=newSteps[i]), Action(instName=psName, actionType="Read", actionData="V")]
                actions += baseActions
        return actions


    def addUpdateActions(self, actionlist):
        coord = np.array(self.SSS[0]).copy()
        toInsert = {}
        for i in range(len(actionlist)):
            if actionlist[i]._actionType == "Move":
                axis = actionlist[i]._instName
                if axis[:5] == "DUMMY":
                    axName = axis[6:]
                else:
                    axName = axis
                coord[self.axMap[axName]] = actionlist[i]._actionData
                toInsert[i + 1] = np.array(coord).copy().tolist()
        
        keylist = list(toInsert.keys())
        for i in range(len(keylist) - 1, -1, -1):
            actionlist.insert(keylist[i], Action(instName="Program", actionType="Update", actionData=toInsert[keylist[i]]))

        return actionlist


    """
    Creates self.actionList, which is used when running the experiment
    """
    def createStepsArray(self):
        start = np.array(self.SSS[0]).copy()
        stop = np.array(self.SSS[1]).copy()
        step = np.array(self.SSS[2]).copy()
        coords = [start.copy()]
        self.addStartActions(start, self.axes)
        done = False
        ax1Steps = self.getSteps1D(start[-1], stop[-1], step[-1], self.turningPointsValues[-1])
        #print(f"ax1Steps: {ax1Steps}")
        actions = self.getMoveActions1D(ax1Steps, self.axes[-1])
        #self.printActionList(actions)
        #print(f"Actions: {len(actions)}")
        for i in range(len(self.axes) - 2, -1, -1):
            currSteps = self.getSteps1D(start[i], stop[i], step[i], self.turningPointsValues[i])

            #print(f"currSteps: {currSteps}")
            actions = self.addDimension(actions, currSteps, self.axes[i], start[i])
        #print(f"Actions: {len(actions)}")
        actions = self.addUpdateActions(actions)
        #print(f"Actions: {len(actions)}")
        self.actionList += actions
        #print(f"Actions: {len(self.actionList)}")
        self.printActionList(self.actionList)
        print(self.turningPointsValues)
        return -1
    
    """
    Reads voltages and currents from the powersupply, puts them in variables and sends a signal to main thread to read those variables
    """
    def readData(self):
        #print("Reading Data...")
        currents = {ch:[] for ch in self.usedChannels}
        voltages = {ch:[] for ch in self.usedChannels}
        mWait = 1 / self.mf
        times = []
        startTime = time.time()
        counter = 1

        while time.time() - startTime < self.tip:

            measTime = time.time()
            for ch in self.usedChannels:
                #currents[ch].append(np.random.randint(1,10))
                #voltages[ch].append(np.random.randint(1000, 2000))
                currents[ch].append(self.volt.get_channel_current(ch))
                voltages[ch].append(self.volt.get_channel_voltage(ch))
            times.append(measTime - startTime)

            sleepTo = startTime + (mWait * counter)
            #print(f"{sleepTo}\n{time.time()}")

            time.sleep(sleepTo - time.time())

            #time.sleep(1 / self.mf - (time.time() - measTime))
            counter += 1

        self.mutex.lockForWrite()
        self.newCurrents = currents
        self.newVoltages = voltages
        self.newTimes = times
        self.workerThread.emit(SIGNAL("newData(float)"), self.latestPos)
        self.mutex.unlock()
        
        for ch in self.usedChannels:
            self.currents[ch].append(currents[ch])
            self.voltages[ch].append(voltages[ch])
        self.times.append(times)
        curTime = time.time() - startTime
        print(f"Meassurements: {len(times)} Done in {curTime}")
    
    def readDummyData(self):
        print(f"Reading dummy data")
        nMeassurements = round(self.tip * self.mf)
        currents = {ch:[] for ch in self.usedChannels}
        voltages = {ch:[] for ch in self.usedChannels}
        
        counter = 1

        for i in range(nMeassurements):
            for ch in self.usedChannels:
                currents[ch].append(np.random.randint(1,10))
                voltages[ch].append(np.random.randint(1000, 2000))
            counter += 1

        self.mutex.lockForWrite()
        self.newCurrents = currents
        self.newVoltages = voltages
        self.newTimes = []
        for i in range(nMeassurements):
            self.newTimes.append(round(i * (1 / self.mf), 3))
        self.workerThread.emit(SIGNAL("newData(float)"), self.latestPos)
        self.mutex.unlock()

        print(f"Meassurements: {nMeassurements} Done")


    """
    Runs the experiment, mainly by running through the self.actionList and completing the actions one by one
    """
    def run(self):
        print(f"Axes:  {self.axes}")
        print(f"Start: {self.SSS[0]}")
        print(f"Stop:  {self.SSS[1]}")
        print(f"Step:  {self.SSS[2]}")
        self.currentCoords = np.array(self.SSS[0]).copy().tolist()
        currentCoords = None
        if not self.dummy:
            self.initialize_instruments()
        coords = self.createStepsArray()
        #return
        self.workerThread.emit(SIGNAL("totalProgress(int)"), len(self.actionList))
        #if coords == -1:
        #    return -1
        actionCounter = 0 
        for action in self.actionList:
            if self.workerThread.isInterruptionRequested():
                self.workerThread.emit(SIGNAL("haveBeenStopped()"))
                break
            actionCounter += 1
            instr = action._instName
            action_type = action._actionType
            action_data = action._actionData
            print(f"Current action ({actionCounter} / {len(self.actionList)}):\n{instr} - {action_type} - {action_data}")
            startTime = time.time()
            if instr[0] == "R" and action_type == "Move":
                radians = math.acos(math.sqrt(action_data))
                pos = radians * (180/math.pi)
                self.latestPos = pos
                #continue
                self.stage.move_device(pos)
                while abs(self.stage.get_position() - pos) > self.stageEpsilon:
                    time.sleep(0.5)
                endTime = time.time()
                
                self.workerThread.emit(SIGNAL("getStepsTime(float,int)"), endTime - startTime, -1)
            elif instr[0] == "S" and action_type == "Move":
                #continue
                self.volt.set_channel_voltage(int(instr[1]), action_data)
                time.sleep(2)
                while self.volt.check_if_ramping([int(instr[1])]):
                    time.sleep(0.5)
                endTime = time.time()
                self.workerThread.emit(SIGNAL("getStepsTime(float,int)"), endTime - startTime, int(instr[1]))
            elif instr == "PS" and action_type == "Read":
                #print(f"Action: {actionCounter} / {len(self.actionList)}")
                self.readData()
            elif instr == "Program" and action_type == "Update":
                currentCoords = action_data
            elif instr[0] == "D" and instr != "DUMMY-Camera":
                print(f"Performing dummy action: {action}")
                if instr[-2:] == "PS":
                    self.readDummyData()
                if instr[-2:] == "R2":
                    radians = math.acos(math.sqrt(action_data))
                    pos = radians * (180/math.pi)
                    self.latestPos = pos
            elif instr[-6:] == "Camera":
                res = self.cam.TakeSnapshot()
                self.cam.SaveImage(res, os.path.join(self.outputDir, f"{currentCoords}_IMG.png"))
            self.workerThread.emit(SIGNAL("progressUpdate(int)"), actionCounter)
            print(f"Action {actionCounter} Done!\n")

        print(f"Exp Done")
        
        