# -*- coding: utf-8 -*-
"""
Created on Fri May  7 09:12:14 2021

@author: willi
edited by: Arwinder
edited by: Rune
"""

# Imports dependecies
from tabnanny import check

#from grpc import Channel
from . import clientCommon
import os
import sys
import time

# c_path = os.getcwd()
# path = os.path.abspath(__file__)[:-25]
# if not path in sys.path:
    # sys.path.append(path)
# print(os.path.abspath(__file__)[:-25])
from . import icsClientPython_3 as icsClient
# os.chdir(c_path)

class PowerSupply:
    '''Class for interacting with power supply'''

    def __init__(self, ip=None, lateSetup = False):
        #object_methods = [method_name for method_name in dir(icsClient)
        #          if callable(getattr(icsClient, method_name))]

        #print(dir(icsClient))


        # Sets necessary parameters for connection        
        self.connectionName = "script_localhost"
        self.connectionLogin = "will"
        self.connectionPassword = "password"

        self.isSetUp = False

        # Checks if ip is inserted
        if ip is not None:
            self.connectionParameters = ip
        else:
            # Default ip
            # Might change when moving the device due to router asignment
            self.connectionParameters = "ws://192.168.1.2:8080"

        if not lateSetup:
            # Make connection        
            self.connection = clientCommon.establishConnection(icsClient, self.connectionName, self.connectionParameters, self.connectionLogin, self.connectionPassword)

            # Checks if connected
            if self.connection == 0:
                self.connected = False
            else:
                self.connected = True

            # If not connected returns
            if not self.connected:
                print("Failed to connect => Exit.")
                return
            self.isSetUp = True
            # Gets modules from connection
            self.modules = self.connection.getModules()
            #print(f"Self.modules are {self.modules}")
            # Extracts channels from modules
            self.channels = self.modules[0].channels
            # print(f"Self.channels are {self.channels}")

    def set_parameter(self, param, val):
        '''Functionf for setting specific parameters'''
        pass

    def get_channel_voltage(self, channel):
        '''Gets voltage of channel'''
        return self.channels[channel].getStatusVoltageMeasure()

    def get_channel_current(self, channel):
        '''Gets current of channel'''
        return self.channels[channel].getStatusCurrentMeasure()

    def set_channel_voltage(self, channel, voltage):
        '''Set voltage of channel'''
        return self.channels[channel].setControlVoltageSet(voltage)

    def enable_channel(self, channel):
        '''Turn channel on'''
        return self.channels[channel].setControlOn(1)

    def disable_channel(self, channel):
        '''Turn channel off'''
        return self.channels[channel].setControlOn(0)

    def isRamping(self, channel):
        '''Check if channel is ramping'''
        #print(self.channels[channel].getStatusVoltageRamping())
        return self.channels[channel].getStatusVoltageRamping()

    def check_if_ramping(self, channels):
        '''Checks if any channel is ramping'''
        try:
            return (sum([self.isRamping(chan) for chan in channels]) > 0)
        except:
            return False

    def waitForRamping(self,channel):
        print(f"ramping status {self.isRamping(channel)}")
        while self.isRamping(channel):
            continue
        return True
    
    def disconnectAll(self):
        clientCommon.disconnectAll(icsClient)
    
    def connectAll(self):
        # Make connection        
        self.connection = clientCommon.establishConnection(icsClient, self.connectionName, self.connectionParameters, self.connectionLogin, self.connectionPassword)

        # Checks if connected
        if self.connection == 0:
            self.connected = False
        else:
            self.connected = True

        # If not connected returns
        if not self.connected:
            print("Failed to connect => Exit.")
            return

        # Gets modules from connection
        self.modules = self.connection.getModules()
        #print(f"Self.modules are {self.modules}")
        # Extracts channels from modules
        self.channels = self.modules[0].channels
        #print(f"Self.channels are {self.channels}")
        self.isSetUp = True


if __name__ == '__main__':
    power = PowerSupply()
    print(f"Testing the  power supply by ramping channel 0 to 1200 ")
    uinput = input("press y if you wish to continue::")
    if uinput.lower == "y":
        power.enable_channel(0)
        power.set_channel_voltage(0,100)
        power.waitForRamping()
    print("We done here brah!!") 