from ast import Pow
from reprlib import aRepr
import sys
#sys.path.append(r'C:\Users\slla\Dropbox\DTU\PhD\code\dtu')
#sys.path.append(r'C:\Users\slla\Dropbox\DTU\PhD\code\dtu\icontrol')
import os
from ctypes import *
from unittest import installHandler
from .Voltage_Supply.power_supply import PowerSupply
import time
from hardware.dummyHardware import dummyMover, dummyReader,dummyLockin
from . import motion_controllers
from .camera import Camera
from .oscilloscopes import Tektronix_DPO4102B
from .lockin import SR830_Control
import datetime
from time import sleep
from . import vaccumstage
from .amperemeter import Amperemeter
import config
parent = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(parent)
import json
import threading

# instrument list is a dictionary , where key is the name of instrument and value is instrument parameters,first parameter is the type of instrument.
class instrumentHandler():
    def __init__(self):
        self._inst = {}
        self._outputData = None
        self._inst['SYSTEM'] = System()      # system is a dummy instrument that allows to do things that any instrument cannot 
        self.needHome = {"Kinesis": [], "Vacuum": []}
        self.home_threads = []
        
    def add_instrument(self,instData):        # adds instrument to the self._inst  which is the intrument dictionary
        
        #instData[0] = inatrumentType
        #instData[1] = instrumentName
        #instData[2] = instrument relevent data
        #for i in instData: print(i)
        if instData[0] == 'KinesisController':
            
            # instData[2] for kinesis controller is a dictionary with
            # createStage: True
            # stageData: stageData is a list of individual stage data which looks like 
            # ['stageName','type:linear/rotational',serial number,axis,homeStage]
            self._inst[instData[1]] = thorlabsKinesisMotionController(instData[2])
            for i in instData[2]["stageData"]:
                if i[4]:
                    self.needHome["Kinesis"].append(self._inst[instData[1]]._stages[i[0]])
            
        if instData[0] == 'Tektronix_DPO4102B':
            
            # instData[2] for techtroniz oscilloscope is dictionary with:
            # ipAddress : ipAdress in string format
            self._inst[instData[1]] = techtronixOsc(instData[2][0], instData[2][1])
        
        if instData[0] == 'dummyReader':
            # same as oscilloscope
            self._inst[instData[1]] = dummyReaderif(instData[2])
            
        if instData[0] == 'dummyMover':

            # same as stage

            self._inst[instData[1]] = dummyMoverif(instData[2])
        
        if instData[0] == 'VoltageController':

            # instdata[2] for voltage controller is the a list of axis that can be 
            # run on this device

            self._inst[instData[1]] = igsecPowerSupplyif([instData[2]])
        
        if instData[0] == 'dummyVoltage':
            self._inst[instData[1]] = dummyPower()

        if instData[0] == "LockInSRS800":
            self._inst[instData[1]] = LockinSRS830(instData[2])
        
        if instData[0] == "dummyLockinamp":
            self._inst[instData[1]] = dummyLockinamp(instData[2])
        
        if instData[0] == "VaccumStage":
            self._inst[instData[1]] = VaccumStage(instData[2][0],instData[2][1])
            if any(instData[2][1]):
                self.needHome["Vacuum"].append([self._inst[instData[1]], instData[2][1]])
        
        if instData[0] == "DummyVaccum":
            self._inst[instData[1]] = DummyVaccumStage(instData[2][0],instData[2][1])
        
        if instData[0] == "Camera":
            self._inst[instData[1]] = CameraInstrument(instData[2])

        if instData[0] == "Ammeter":
            self._inst[instData[1]] = Amperemeter()
        
        if instData[0] == "timeMover":
            self._inst[instData[1]] = timeMover()
        
    def moveAllToStartPos(self, startPosActions):
        #print("#####_v_StartPosActions_v_#####")
        #for i in startPosActions: print(i)
        #print("#####_^_StartPosActions_^_#####")

        #for i in startPosActions: self._inst[i._instName].doAction(i)

        threads = []
        for i in startPosActions:
            threads.append(threading.Thread(target=(self._inst[i._instName].doAction), args=(i,)))
            threads[-1].start()
        
        for i in threads:
            i.join()
        print(f"ALL STAGES AT START POSITION")

    def homeAll_multithread(self):
        self.home_threads = []
        for i in self.needHome["Kinesis"]:
            self.home_threads.append(threading.Thread(target=i.home_device, args=()))
        for i in self.needHome["Vacuum"]:
            counter = 1
            for j in i[1]:
                if j:
                    self.home_threads.append(threading.Thread(target=i[0]._device.home, args=(str(counter),)))
                counter += 1
        for i in self.home_threads:
            i.start()
    
    def joinAll_multithread(self):
        for i in self.home_threads:
            i.join()
            

    def runAction(self,action):
        # runs the action provided as input to the instrument controller
        # first gets the instrument name and then sends the action to 
        # that instrument to run
        instrument = self._inst[action._instName]
        if self.checkValidAction(instrument,action):
            output = instrument.doAction(action)
            return output

    def checkValidAction(self,instrument,action):
        # an cation is only valid if it is contained in the list of valid actions
        actions = instrument.getActions()
        if action._actionType in actions:
            return True
        return False

class CameraInstrument():
    def __init__(self, data):
        self._actions = ['SnapShot']
        self.cam = Camera()
        exposure = int(data[0])
        gain = int(data[1])
        self.path = data[2]
        self.axes = data[3]
        self.setExposureAndGain(exposure, gain)
    
    def setExposureAndGain(self, exposure, gain):
        self.cam.create_snapshot(exposure=exposure, gain=gain)
    
    def doAction(self, action):
        if action._actionType == "SnapShot":
            res = self.cam.TakeSnapshot()
            coord = ""
            for i in range(len(action._actionData)):
                coord += self.axes[i] + "=" + str(action._actionData[i])
            coord += ".png"
            snapShotFolder = os.path.join(self.path, "Snapshots")
            if not os.path.exists(snapShotFolder):
                os.mkdir(snapShotFolder)
            endpath = os.path.join(snapShotFolder, coord)
            print(f"COORD:\n{coord}")
            print(f"PATH:\n{endpath}")
            self.cam.SaveImage(res, endpath)
        return [True, []]

    def getActions(self):
        return self._actions


class System():
    # system class is used to create wait action primarily
    def __init__(self):
        self._actions = ['Wait','Increment']
        self.timer = 0
        if config.useAmperemeter:
            self.useAmp = True
            self.amp = Amperemeter()
            self.intTime = config.integrationTime
            self.n = config.numberOfMeassurements

    def _useAmp(self):
        mean, std = self.amp.get_multiple_currents(self.intTime, self.n)
        print(f"\nMEAN: {mean}\nSTD: {std}\n")
    
    def doAction(self,action):
        if action._actionType=='Wait':
            try:
                sleep(action._actionData[0])
                return [True,[]]
            except:
                print("Couldn't complete WAIT action (instrumentComponent.py, System.doAction())")
                return [False,[]]
        if action._actionType=='Increment':
            self.timer += 1
            return [True,[self.timer]]
    
    def getActions(self):
        return self._actions



class thorlabsKinesisMotionController():
    def __init__(self,autoCreationData): 
        self._stageData = {}      # holds the stage data
        self._stages = {}         # holds the created stage objects with keys as stage names
        self._axis = {}           # maps the name of the stage to the axis name
        self._actions =['MoveStage']  # Kinesis controller has only one action called move stage
        if autoCreationData['createStages']:

            # if the stage has autocreation data on the create mapppings for 
            # self._ stages and self._axis , this allows us to refer the objects name from axis and
            # refer the stage object from the name
            #TODO check if we can directly map the axis name to the object instead of including stage name

            for stage in autoCreationData['stageData']:
                if stage[4]:
                    self._stages[stage[0]] = (motion_controllers.KinesisController(serialNumber=stage[2], stageType=stage[1], stageName=stage[0], HomeStage=True, lateSetup=True))
                    self._axis[stage[3]] = stage[0]
                else:
                    self._stages[stage[0]] = (motion_controllers.KinesisController(serialNumber=stage[2], stageType=stage[1], stageName=stage[0], HomeStage=False, lateSetup=True))
                    self._axis[stage[3]] = stage[0]
        self.initializeStages()
    
    def initializeStages(self):
        for key in self._stages: self._stages[key].late_setup()
        """
        threads = []
        for key in self._stages:
            threads.append(threading.Thread(target=self._stages[key].late_setup, args=()))
        
        for i in range(len(list(self._stages.keys()))):
            #print(f"Starting thread for {list(self._stages.keys())[i]}")
            threads[i].start()
        
        for i in threads:
            i.join()
        """
        print("ALL STAGES INITIALIZED")
        


    def doAction(self,action):
        if action._actionType=='MoveStage':
            #print(f"Kinesis controller working on {action}")
            #print(self._stages[action._actionData[0]].type)
            try:
                
                # check if the stage type is field or power, the simple move ation includes both rotational and 
                # linear
                # TODO make the the whole stage data more clean

                if self._stages[action._actionData[0]].type == 'field':
                    #print("stage type field")
                    self._stages[action._actionData[0]].set_rel_efield(rel_efield = action._actionData[1][0]) 
                    #print(f"Succesfully moved {action._instName} to {action._actionData}")   
                    return [True,[action._actionData[1][0]]]

                if self._stages[action._actionData[0]].type == 'power':
                    self._stages[action._actionData[0]].set_rel_power(rel_power = action._actionData[1][0])
                    #print(f"Succesfully moved {action._instName} to {action._actionData}")
                    return [True,[action._actionData[1][0]]]

                else:
                    self._stages[action._actionData[0]].move_device(position=action._actionData[1][0])
                    #print(f"Succesfully moved {action._instName} to {action._actionData}")
                    return [True,[action._actionData[1][0]]]
                
            except:
                print("breaking down")
                return [False,[]]

    
    def getActions(self):
        return self._actions
    
    def getStages(self):
        return self._stages

class dummyPower():
    def __init__(self):
        pass

    def doAction(self,action):
        return [False,[]]
    
    def getActions(self):
        return []

class timeMover():
    def __init__(self) -> None:
        self._actions =['MoveStage']

    def doAction(self,action):
        if action._actionType=='MoveStage':
            return [True,[action._actionData[1][0]]]
        
    def getActions(self):
        return self._actions
    

class dummyMoverif():
    def __init__(self,autoCreationData): 

        # Most things are same as the kinesis controller

        self._stageData = {}
        self._stages = {}
        self._axis = {}
        self._actions =['MoveStage']
        if autoCreationData['createStages']:
            for stage in autoCreationData['stageData']:
                if stage[4]:
                    self._stages[stage[0]] = (dummyMover(serialNumber=stage[2], stageType=stage[1], stageName=stage[0], HomeStage=True))
                    self._axis[stage[3]] = stage[0]
                else:
                    self._stages[stage[0]] = (dummyMover(serialNumber=stage[2], stageType=stage[1], stageName=stage[0], HomeStage=False))
                    self._axis[stage[3]] = stage[0]

    def doAction(self,action):
        #print(action._actionType)
        if action._actionType=='MoveStage':
            #try:
            if action._actionData[0] in self._stages.keys():
                self._stages[action._actionData[0]].move_device(position=action._actionData[1][0])
            else:
                print(f"Trying to move a stage that doesn't exist in array (see instrumentComponent.py line 228)")
            return [True,[action._actionData[1][0]]]
            #except:
            #    return [False,[]]

    
    def getActions(self):
        return self._actions
    
    def getStages(self):
        return self._stages

class techtronixOsc():
    # Class that acts as interface between the instrument level methods and
    # the instrument component
    def __init__(self,ipAddress, onlyamp=False):
        self.isOSC = True
        self._ipAddress = ipAddress
        self._osc = Tektronix_DPO4102B(self._ipAddress)
        self._maxNumOscPoints = self._osc.get_horizontal_record_length()
        self._oscConnected = True
        self._actions = ['SET_ACQ_MODE','GET_DATA','SET_ACQ_STATE','GET_ACQ_PARAMS','RESET_HIST']
        if not onlyamp:
            self._osc.set_histogram_mode("HORizontal")

    #mode = enum('AVE',..)
    #numOscAverages is a part of optional arguments which can be added depending upon the required parameters for a mode
    def doAction(self,action):
        # simply sends  the actions to the lower layer by calling corresponding method in the hardware 
        # component
        if action._actionType == 'SET_ACQ_MODE':
            actionData = action._actionData
            try:
                output = self.setAcqMode(actionData[0],actionData[1])
                return [True,[]]
            except:
                return[False,[]]

        if action._actionType == 'SET_ACQ_STATE':
            actionData = self.setAcqState(action._actionData[0])
            try:
                return [True,[]]
            except:
                return [False,[]]

        if action._actionType == 'GET_DATA':
            print(f"        Getting Data from scope")
            actionData = self.getData(action._actionData[0])
            try:
                return [True,[actionData]]
            except:
                return [False,[]]
        
        if action._actionType == 'GET_ACQ_PARAMS':
            actionData = self.getAcqParams()
            try:
                return [True,[actionData]]
            except:
                return [False,[]]

        if action._actionType == 'GET_ACQ_MODE':
            actionData = self.getAcqMode()
            try:
                return [True,[actionData]]
            except:
                return [False,[]]


        if action._actionType == 'GET_NUM_AVG':
            actionData = self.getNumAvg()
            try:
                return [True,[actionData]]
            except:
                return [False,[]]
        

        if action._actionType == 'RESET_HIST':
            self._osc.reset_histogram()
            try:
                return [True,[]]
            except:
                return [False,[]]


    def setAcqMode(self,AcqMode,numOscAverages):
        # TODO make acqussiton mode an enumeration class
        if self._oscConnected:
            self._osc.set_acquisition_mode(mode=AcqMode)
            self._osc.set_number_averages(averages=numOscAverages)

    def setAcqState(self,AcqState):
        if self._oscConnected:
            self._osc.set_acquisition_state(state=AcqState)

    def getData(self,channel):
        return(self._osc.get_data(channel))

    def getActions(self):
        return self._actions

    def getAcqParams(self):
        return(self._osc.query_all_acquisition())

    def getAcqMode(self):
        return self._osc.query_acquisition_mode()

    def getNumAvg(self):
        return self._osc.query_number_averages()

class dummyReaderif():
    #same as the techtronix oscilloscope
    def __init__(self,ipAddress):
        self._ipAddress = ipAddress
        self._osc = dummyReader(self._ipAddress)
        self._maxNumOscPoints = 40
        self._oscConnected = True
        self._actions = ['SET_ACQ_MODE','GET_DATA','SET_ACQ_STATE','GET_ACQ_PARAMS']
    #mode = enum('AVE',..)
    #numOscAverages is a part of optional arguments which can be added depending upon the required parameters for a mode
    def doAction(self,action):
        if action._actionType == 'SET_ACQ_MODE':
            actionData = action._actionData
            try:
                output = self.setAcqMode(actionData[0],actionData[1])
                return [True,[]]
            except:
                return[False,[]]

        if action._actionType == 'SET_ACQ_STATE':
            actionData = self.setAcqState(action._actionData[0])
            try:
                return [True,[]]
            except:
                return [False,[]]

        if action._actionType == 'GET_DATA':
            actionData = self.getData(action._actionData[0])
            try:
                return [True,[actionData]]
            except:
                return [False,[]]
        
        if action._actionType == 'GET_ACQ_PARAMS':
            actionData = self.getAcqParams()
            try:
                return [True,[actionData]]
            except:
                return [False,[]]
        

    def setAcqMode(self,AcqMode,numOscAverages):
        if self._oscConnected:
            self._osc.set_acquisition_mode(mode=AcqMode)
            self._osc.set_number_averages(averages=numOscAverages)

    def setAcqState(self,AcqState):
        if self._oscConnected:
            self._osc.set_acquisition_state(state=AcqState)

    def getData(self,channel):
        return(self._osc.get_data(channel))

    def getActions(self):
        return self._actions

    def getAcqParams(self):
        return(self._osc.query_all_acquisition())
    ## def querry busy():
    def get_curve(self):
        return

class igsecPowerSupplyif:
    # Class interface between specail axis intrument componentt
    # and the hardware voltage supply
    def __init__(self,channels):
        self.connectionIP = "ws://192.168.1.2:8080"
        self.powerSupply = PowerSupply(self.connectionIP)
        self._actions = ['MoveAxis','ShutDown']

    def doAction(self,action):
        channel = int(action._actionData[0][1])
        print(f"Channel is {channel}")
        if action._actionType == 'MoveAxis':
            print(f"Moving to {action._actionData[1]}")
            self.powerSupply.enable_channel(channel)
            
            self.powerSupply.set_channel_voltage(channel, action._actionData[1][0])
            time.sleep(2)
            self.powerSupply.waitForRamping(channel)
            return [True,[action._actionData[1]]]


        elif action._actionType == 'ShutDown':
            self.powerSupply.set_channel_voltage(channel, 0)
            self.powerSupply.waitForRamping(channel)
            self.powerSupply.disable_channel(channel)
            return [True,[channel+0]]
        
        else:
            return [False,[]]

    def getActions(self):
        return self._actions

class LockinSRS830:
    def __init__(self,address):
        self.isOsc = False
        self.lockin = SR830_Control(address)
        self._actions = ['SET_ACQ_MODE','GET_DATA','SET_ACQ_STATE','GET_ACQ_PARAMS']

    def doAction(self,action):
        if action._actionType == 'GET_DATA':
            actionData =  self.lockin.get_SLVL()
            return [True,actionData]
    
    def getActions(self):
        return self._actions

class dummyLockinamp:
    def __init__(self,address):
        self.lockin = dummyLockin(address)
        self._actions = ['SET_ACQ_MODE','GET_DATA','SET_ACQ_STATE','GET_ACQ_PARAMS']

    def doAction(self,action):
        if action._actionType == 'GET_DATA':
            actionData =  self.lockin.get_SLVL()
            return [True,actionData]
        else:
            return [True,None]
    
    def getActions(self):
        return self._actions

class VaccumStage:
    def __init__(self,ipaddress,homeStage):
        self._device = vaccumstage.VaccumStageHW(ipaddress)
        #count = 1
        #for i in homeStage:
        #    if i :
        #        self._device.home(str(count))
        #    count+=1
        self._actions =['MoveStage']

    def doAction(self,action):
        if action._actionType == 'MoveStage':
            #print(f"Action taken is MoveStage")
            #print(f"The action data is {action._actionData}")
            #print(f"The instrument name is {action._instName}")
            self._device.move(action._actionData[0],action._actionData[1][0])
            return [True,action._actionData[1]]

    def getActions(self):
        return self._actions

class DummyVaccumStage:
    def __init__(self,ipaddress,home):
        self._device = vaccumstage.DummyVaccumStage(ipaddress)
        self._stageData = {}
        self._stages = {'1','2','3'}
        self._axis = ['VX','VY', 'VZ']
        self._actions =['MoveStage']

    def doAction(self,action):
        #print("in here")
        if action._actionType == 'MoveStage':
            #print(f"Action taken is MoveStage")
            #print(f"The action data is {action._actionData}")
            #print(f"The instrument name is {action._instName}")

            return [True,action._actionData[1]]

    def getActions(self):
        return self._actions

if __name__ == "__main__":
    print(f"Testing Testing voltage supply")
    instruments = instrumentHandler()
    specialAxis = ['S0','S1','S2','S3']
    specialInst1Name = "VoltageCont"
    specialInst = "VoltageCont"
    specialInst1Type = "VoltageController"
    data = [specialInst1Type,specialInst1Name,specialAxis]
    instruments.add_instrument(data)
    from utils.action import Action
    act1 = Action(specialInst,'MoveAxis',['S0',0])
    act2 = Action(specialInst,'MoveAxis',['S0',100])
    act3 = Action(specialInst,'MoveAxis',['S0',150])
    act4 = Action(specialInst,'MoveAxis',['S0',100])
    act5 = Action(specialInst,'MoveAxis',['S0',50])
    act6 = Action(specialInst,'MoveAxis',['S0',0])
    act7 = Action(specialInst,'ShutDown',['S0',''])
    print(instruments.runAction(act1))
    print(instruments.runAction(act2))
    print(instruments.runAction(act3))
    print(instruments.runAction(act4))
    print(instruments.runAction(act5))
    print(instruments.runAction(act6))
    print(instruments.runAction(act7))
