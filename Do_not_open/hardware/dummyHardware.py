import numpy as np
import time
class dummyReader:
    def __init__(self,instrumentIp):
        self._ip = instrumentIp

    def set_acquisition_mode(self,mode):
        return 

    def set_number_averages(self,averages):
        return

    def set_acquisition_state(self,state):
        return

    def get_data(self,channel=2):
        time_base =np.arange(0,10000.00).astype(np.float32)
        ydata_in_units = np.random.uniform(low=0,high=0.5,size = 10000).astype(np.float32)

        timevalues = self.get_time_values()
        histdata = self.get_histogram_data()
        return time_base, ydata_in_units, timevalues, histdata

    def query_all_acquisition(self):
        return 
    
    def get_curve(self):
        return 
    
    def set_histogram_mode(self, mode):
        return
    
    def query_acquisition_mode(self):
        return
    
    def get_histogram_data(self):
        data = []
        for i in range(1000):
            data.append(np.random.randint(0, 10000))
        return data

    def get_time_values(self):
        timeValues = np.random.uniform(low=0,high=0.5,size = 1000).astype(np.float32)
        #print(timeValues)
        return timeValues
    
    def reset_histogram(self):
        return

class dummyLockin:
    def __init__(self,addrr):
        self.addr = addrr
    
    def set_acquisition_mode(self,mode):
        return 

    def set_number_averages(self,averages):
        return

    def set_acquisition_state(self,state):
        return

    def get_SLVL(self):
       
        return "21"

    def query_all_acquisition(self):
        return 

class dummyMover:
    def __init__(self,serialNumber=None, HomeStage=False, stageName=None, stageType='linear', path_kinesis_install=r"C:\Program Files\Thorlabs\Kinesis"):
        
        return None


    def move_device(self,position):
        pass

    def get_position(self):
        return "DummyPosition"


class dummyAmp:
    def __init__(self):
        pass
    
    def get_multiple_currents(self, intTime=0.1, n=10):
        tim = intTime * n
        #print(f"Reading (dummy)Amp... ({tim}s)")
        #time.sleep(tim)
        #print("Done reading (dummy)Amp")
        return None, None