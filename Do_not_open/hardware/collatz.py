import matplotlib.pyplot as plt
cont = 'y'
while cont == 'y':
    m = int(input("enter the starting number"))
    
    data =[]
    data.append(m)
    while not(m==1 or m==2 or m==4):
        if m%2 == 0:
            m =int(m/2)
        else:
            m=3*m+1
        print(f"next number is {m}")
        data.append(m)
    plt.plot(data, color='magenta', marker='o',mfc='pink' ) #plot the data
    plt.xticks(range(0,len(data), 1)) #set the tick frequency on x-axis

    plt.ylabel('data') #set the label for y axis
    plt.xlabel('index') #set the label for x-axis
    plt.title(f"Collatz Conjecture for number {data[0]}") #set the title of the graph
    plt.show() #display the graph
    cont = str(input("continue ? press y?n"))
