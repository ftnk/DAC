"""
Created on Mon April 06, 18:14:05 2020

@author: malte
"""

#from requests import request
import serial
import time
import sys
#from . import connections
from pymeasure.adapters import VISAAdapter
from pymeasure.instruments.srs import SR830
#adapter = VISAAdapter("GPIB8::8::INSTR")
#value = adapter.ask('SLVL?\n').split("\n")[0]
#print(value)
from time import sleep




class SR830_Control:

    def __init__(self,addr): 
        #inputs mean addr1 is GPIB8 and addr2 is 8 as in above example.
        
        adapter = VISAAdapter(f"{addr[0]}::{addr[1]}::INSTR")
        self.Instrument = SR830(adapter)

    # --- Native commands --- #

    def PHAS_question(self):
        command = "PHAS?"
        phase = self.Instrument.ask(command)
        return phase
        # Get the set phase of the Lock-in

    def PHAS(self, phase):
        command = "PHAS" + str(phase)
        self.Instrument.send_command(command)
        # Set the phase of the Lock-In, The phase may be programmed from -360.00 £ x £ 729.99 and will be
        # wrapped around at ±180°

    def FREQ_question(self):
        command = "FREQ?"
        freq = self.Instrument.transaction(command)
        return freq
        # get the used referece frequency

    def ISRC(self, i):
        command = "ISRC" + str(i)
        request = "ISRC?"
        self.Instrument.send_command(command)
        return self.Instrument.transaction(request) # return is the 
        # The ISRC command sets or queries the input configuration. The parameter
        # i selects A (i=0), A-B (i=1), I (1 MW) (i=2) or I (100 MW) (i=3)

    def IGND(self, i):
        command = "IGND" + str(i)
        request = "IGND?"
        self.Instrument.send_command(command)
        return self.Instrument.transaction(request)
        # The IGND command sets or queries the input shield grounding. The
        # parameter i selects Float (i=0) or Ground (i=1).

    def ICPL(self, i):
        command = "ICPL" + str(i)
        request = "ICPL?"
        self.Instrument.send_command(command)
        return self.Instrument.transaction(request)

    def get_SLVL(self):
        return self.Instrument.x


    def testCommands(self,commands):
        sleep(1)
        

if __name__ == "__main__": 
    adapter = VISAAdapter("GPIB0::8::INSTR")
    inst = SR830(adapter)
    #value = adapter.ask('SLVL?\n').split("\n")[0]
    print(inst.x)
#value = adapter.ask('SLVL?\n').split("\n")[0]
#print(value) 