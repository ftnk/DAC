
from pipython.pidevice.gcscommands import GCSCommands
from pipython.pidevice.gcsmessages import GCSMessages
from pipython.pidevice.interfaces.pisocket import PISocket
from pipython.pidevice.interfaces.piusb import PIUSB
from pipython import GCS2Device
import time
#gateway = PISocket(host='192.168.1.3')
#messages = GCSMessages(gateway)
##gcs = GCSCommands(messages)
#print(gcs.qIDN())
#print(gcs.read('*IDN?'))

class DummyVaccumStage:
    def __init__(self,ipaddress):
        self._ip = ipaddress
    
    def getId(self):
        return 1
    
    def MOV(self, ax, pos):
        return 

    def homeAll(self):
        return
    
    def disconnect(self):
        return


class VaccumStageHW:
    def __init__(self,ipaddress):
        self._gcs = GCS2Device()
        self._gcs.ConnectTCPIP(ipaddress=ipaddress)

        #self._gateway = PISocket(host=ipaddress)
        #self._messages = GCSMessages(self._gateway)
        #self._gcs = GCSCommands(self._messages)
        idn = self._gcs.qIDN()
        print(f"{idn}")
        self._map = {'VX':'1','VY':'2','VZ':'3'}
        self._rmap = {'1':'VX','2':'VY','3':'VZ'}
        self.connected_axes = []
        for i in range(1, 4, 1):
            self._gcs.SVO(str(i), '1')
                
    
    def getId(self):
        #print(self._gcs.qSAI())
        #print(self._gcs.qPOS('1'))
        self._gcs.send('2 SVO')
        #self._gcs.send('GOH %s' % '3')
        #self._gcs.send('MOV 3 1')
        #print(self._gcs.qPOS('3'))
        #self._axes_of_sub_module_2 = self._gcs.read('2 SAI?').split(' ')[2].replace('\n' , '')
    
    def homeAll(self):
        for i in range(1, 4, 1):
            try:
                self._gcs.GOH(str(i))
                time.sleep(1)
                self.connected_axes.append(i)
            except:
                print(f"Cannot communicate with axis: {self._rmap[str(i)]}")
    
    def disconnect(self):
        self._gcs.close()

    def home(self,axis):
        #print("HOME:")
        #print(axis)
        #print(f"map: {self._map['VZ']}")
        try:
            print(f"Homing: {self._rmap[axis]}")
            self._gcs.GOH(str(axis))
            time.sleep(1)
            self.connected_axes.append(axis)
            print(f"Homing Complete: {self._rmap[axis]}")
        except:
            print(f"Cannot communicate with axis: {self._rmap[str(axis)]}")


    def move(self,axis,pos):
        self._gcs.MOV(self._map[axis], pos)
        time.sleep(1)

    def getpos(self,axis):
        return self._gcs.qPOS(self._map[axis])

if __name__=="__main__":
    vs = VaccumStageHW('192.168.1.3')
    vs.home('VZ')
    print(vs.getpos('VZ'))
    vs.move('VZ',5)
    print(vs.getpos('VZ'))
    vs.move('VZ',4)
    print(vs.getpos('VZ'))
    vs.home('VZ')
    print(vs.getpos('VZ'))
    #vs.move()


