import time
import math
import os
from ctypes import *
import numpy as np


class KinesisController:
    def __init__(self, serialNumber=None, HomeStage=False, stageName=None, stageType='linear', path_kinesis_install=r"C:\Program Files\Thorlabs\Kinesis", lateSetup=False):

        os.chdir(path_kinesis_install)
        self.name = "C:\Program Files\Thorlabs\Kinesis\Thorlabs.MotionControl.KCube.DCServo.dll"
        self.stageName = stageName
        self.lib = cdll.LoadLibrary(self.name)
        self.type = stageType

        # Build device list
        self.lib.TLI_BuildDeviceList()

        self.serialNumber = serialNumber
        self.stepsPerRev = 512
        self.gearBoxRatio = 67
        if stageType == 'linear':
            self.pitch = 1
        elif stageType == 'rotational' or stageType == 'field' or stageType == 'power':
            self.pitch = 17.87
        else:
            print('Stage type undefined')

        self.moveTimeout = 30.0

        self.HomeStage = HomeStage

        self.isSetUp = False
        #print(f"lateSetup for {self.stageName}: {lateSetup}")
        if not lateSetup:
            # Setting up stage
            print(f"Initializing {self.stageName}")
            self.initialize_device()

            # Adjusting stage parameters
            print(f"Adjusting {self.stageName} settings")
            self.set_up_device()

            # Homing stages
            #if HomeStage:
            #    print(print(f"Homing {self.stageName}"))
            #    self.home_device()
            self.isSetUp = True
    
    def late_setup(self):
        # Setting up stage
        print(f"Initializing {self.stageName}")
        self.initialize_device()

        # Adjusting stage parameters
        print(f"Adjusting {self.stageName} settings")
        self.set_up_device()

        # Homing stages
        #if self.HomeStage:
        #    print(print(f"Homing {self.stageName}"))
        #    self.home_device()
        self.isSetUp = True


    def initialize_device(self):
        # set up device
        self.lib.CC_Open(self.serialNumber)
        self.lib.CC_StartPolling(self.serialNumber, c_int(200))
        # might need to enable the channel:
        # lib.CC_EnableChannel(serialNumber)

        time.sleep(3)
        self.lib.CC_ClearMessageQueue(self.serialNumber)


    def clean_up_device(self):
        # clean up and exit
        self.lib.CC_ClearMessageQueue(self.serialNumber)
        # print(lib.CC_GetPosition())
        self.lib.CC_StopPolling(self.serialNumber)
        self.lib.CC_Close(self.serialNumber)


    def home_device(self):
        homeStartTime = time.time()
        self.lib.CC_Home(self.serialNumber)

        self.messageType = c_ushort()
        self.messageID = c_ushort()
        self.messageData = c_ulong()

        homed = False
        print(f"Homing: {self.stageName}")
        while (homed == False):
            self.lib.CC_GetNextMessage(self.serialNumber, byref(self.messageType), byref(self.messageID), byref(self.messageData))
            if ((self.messageID.value == 0 and self.messageType.value == 2) or (time.time() - homeStartTime) > self.moveTimeout):
                homed = True
        self.lib.CC_ClearMessageQueue(self.serialNumber)
        print(f"Homing Complete: {self.stageName}")


    def set_up_device(self):
        # Set up to convert physical units to units on the device
        self.lib.CC_SetMotorParamsExt(self.serialNumber, c_double(self.stepsPerRev), c_double(self.gearBoxRatio), c_double(self.pitch))

        deviceUnit = c_int()
        self.deviceUnit = deviceUnit


    def move_device(self, position, tries = 0):
        deviceUnit = c_int()

        realUnit = c_double(position)

        self.lib.CC_GetDeviceUnitFromRealValue(self.serialNumber, realUnit, byref(deviceUnit), 0)

        moveStartTime = time.time()
        self.lib.CC_MoveToPosition(self.serialNumber, deviceUnit)

        moved = False
        messageType = c_ushort()
        messageID = c_ushort()
        messageData = c_ulong()
        timedOut = False
        while (moved == False):
            self.lib.CC_GetNextMessage(self.serialNumber, byref(messageType), byref(messageID), byref(messageData))

            if ((messageID.value == 1 and messageType.value == 2) or (time.time() - moveStartTime) > self.moveTimeout):
                if not (messageID.value == 1 and messageType.value == 2):
                    timedOut = True
                    print(f"{self.stageName} TIMED OUT!")
                else:
                    print(f"{self.stageName} Moved successfully")
                moved = True
        if timedOut:
            print(f"{self.stageName} Timed out, try restarting the stage or the software")
            #if tries >= 3:
                #print(f"{self.stageName} Timed out 3 consecutive times, try restarting the stage")
            #else:
            #    self.initialize_device()
            #    self.move_device(position, tries = tries + 1)

    def set_rel_efield(self, rel_efield):
        #print("in rel_efield")
        angle = np.arccos(np.sqrt(rel_efield)) * 180 / math.pi
        self.move_device(position=angle)
    
    def set_rel_power(self, rel_power):
        angle = np.arccos(rel_power) * 180 / math.pi
        self.move_device(position=angle)


    def rotate_stage(self, angle):
        self.move_device(position=angle)

    def get_position(self):
        dev_pos = c_int(self.lib.CC_GetPosition(self.serialNumber))
        real_pos = c_double()
        self.lib.CC_GetRealValueFromDeviceUnit(self.serialNumber, #https://github.com/Thorlabs/Motion_Control_Examples/blob/main/Python/KCube/KDC101/kdc101_example.py
                                          dev_pos,
                                          byref(real_pos),
                                          0)
        return real_pos.value



if __name__=="__main__":
    k = KinesisController(serialNumber=c_char_p(b'27255894'), HomeStage=True, stageName='Xaxis')
    k.move_device(2)
    k.clean_up_device()
