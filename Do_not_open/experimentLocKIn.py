import datetime,os

from config import getInstrumentHandler,getInstrumentHandlerUI,getScanner,getScannerUI,experimentName,axes,startCoords,stopCoords,step_size,moduleType,stageSettleTime,useAmperemeter,resolution,readerType,integrationTime,numberOfMeassurements
from utils.action import Action
import h5py
import numpy as np
import shutil
from hardware.amperemeter import Amperemeter
from PySide6.QtCore import SIGNAL, QObject, QThread
import json


UI = False

def makeDir(filepath:'str',count:'int') -> 'str':
    '''
    Creates a directory and if the directory already exists, it creates a directory with t numbered name to avoid conflict
    #Input args : path to directory, current count
    #output : new path to file 

    '''
    while os.path.isdir(f"{filepath}-{count}"):
        count += 1
    try:                                  # if there is no naming conflict already
        filePath = f"{filepath}-{count}"
        os.mkdir(filePath)
    except:                               # if threre is a naming conflict then increase the count
        filePath = makeDir(filepath,count+1)
    return filePath

class MainBackgroundThreadL(QThread):
    def __init__(self, expName, directory, dacFolder):
        QThread.__init__(self)
        self.expName, self.directory, self.dacFolder = expName, directory, dacFolder

    def run(self):
        exp = Experiment(self.expName,verbose=True, useUI=True, path=self.directory, dacFolder=self.dacFolder, worker_thread=self)
        exp.instHandler.homeAll_multithread()
        exp.loadScanner()
        exp.instHandler.joinAll_multithread()
        exp.instHandler.moveAllToStartPos(exp._startPosActions)
        exp.runExperiment()


class ExperimentMetadata():
    # Keeps track of all the metadata for the experiment, this includes:
    # creating directory for the new experiment, copy the contents of the config file into the new file
    # copy all the relevent data processing files 
    # creating and providing referece to the hdf5 file      
    def __init__(self,experimentName:'str', path, dacFolder) -> 'None':
        # create new director
        global UI
        self.experimentName = experimentName
        self.dacFolder = dacFolder
        if UI:
            data = json.load(open(os.path.join(path, "ui_data.json"), 'r'))
            self.experimentName = data["expname"]
            startCoords1 = data["startCoords"]
            stopCoords1 = data["stopCoords"]
            self.step_size1 = data["step_size"]
            axes1 = data["axes"]
            self.directory = path
        else:
            startCoords1 = startCoords
            stopCoords1 = stopCoords
            self.step_size1 = step_size
            axes1 = axes
            self.directory = os.path.join(os.path.dirname(os.path.realpath(__file__)),os.path.join('outputs-PMT',experimentName+'-'+str(datetime.datetime.now().date())))
            self.directory = makeDir(self.directory,0)

        self._mainModuleFile = os.path.join(self.directory,'MainExp.txt') # create log file
        self._configFile = self.create_config()                           # copy required files
        # set up hdf5 file
        self._fileNameh = os.path.join(self.directory,'LockinData.hdf5')     
        fileh = h5py.File(self._fileNameh, "a")
        config_data = [np.array(startCoords1),np.array(stopCoords1),np.array(self.step_size1)]
        config_data = np.array(config_data)
        fileh.create_dataset(f"config", config_data.shape, dtype='f',data = config_data)
        string_dt = h5py.special_dtype(vlen=str)
        #print(axes1)
        config_data = np.array(axes1,dtype=object)
        fileh.create_dataset(f"axes", config_data.shape, dtype=string_dt,data = config_data)
        fileh.close()
    
    def create_config(self) ->'str':
    ############add files that need to be copied to new folder of experiment#########
        files_to_copy = ["Context.py","DataManager.py"]
        files_from_folder = ["DataHandler.py","DataVisualizer.py","SDP.py","energyHeatMap.py","heatMap.py","D1plot.py"]
        ui_files_to_copy = ["ui_form_DM.py", "GUI.py", "DataManager_GUI.bat"]
        os.mkdir(os.path.join(self.directory, "ExpSpecific"))
        for file in files_to_copy:
            shutil.copyfile(os.path.join("data_processing",file), os.path.join(self.directory,file))
        for file in files_from_folder:
            shutil.copyfile(os.path.join(os.path.join("data_processing", "ExpSpecific"), file), os.path.join(os.path.join(self.directory, "ExpSpecific"), file))
        for file in ui_files_to_copy:
            shutil.copyfile(os.path.join("DatManagerUI",file), os.path.join(self.directory,file))
    ###############copying the config file###########################################
        filename = os.path.join(self.directory,"config.py")
        file = open(filename,"a")
        configfile = open(os.path.join(self.dacFolder, os.path.join("Do_not_open", "config.py")),"r")
        ignore_lines = ["","from","def","scanner"]
        file.writelines("from ctypes import * \n")
        for i in configfile.readlines():
            if i.split(" ")[0] not in ignore_lines:
                file.writelines(i)
        #file.writelines("{"*"*10} \n")
        file.close()
        configfile.close()
        return filename

class ExperimentOutput():
    def __init__(self,experimentName, path, dacFolder):
        self._metadata = ExperimentMetadata(experimentName, path, dacFolder)
  
###   This is where all the output is handled and this is where we change the data logging and storing modules
    def addStepData(self,action,output,coords):
        # Writes the output to hdf5 file
        actionData = f'TimeStamp:{datetime.datetime.now()}, Instrument:{action._instName}, Action Type:{action._actionType}, Action Data:{action._actionData}, coords are {coords}'
        if output[0]:
            actionData= actionData+': Success'
            if action._actionType  == "GET_DATA":   # only action data corresponding to the get-Data is added to the hdf5 file
                
                actionData= actionData+'File:GetDataOsc'+str(datetime.datetime.now().date())+'_'
                outputData = output[1]
                #print(len(outputData[0]))
                #print(len(outputData[1]))
                #h5data = np.array([outputData[0],outputData[1]])
                fileh = h5py.File(self._metadata._fileNameh, "a")
                print(coords)
                fileh.create_dataset(f"{coords}", 1, dtype='f',data = outputData)
                fileh.close()
        else:
            actionData= actionData+': Failure'
        file = open(self._metadata._mainModuleFile,"a")
        #logging data
        file.writelines(actionData+'\n')
        file.close()
    

        



class Experiment():

# Actual experiment method this method calls all other methods and then performs actions one at a time.
    def __init__(self,experimentName,experimentMetaData=None,verbose=False,useUI=False,path=None,dacFolder=None, worker_thread=None):
        global UI
        UI = useUI
        self.useUI = useUI
        self.dacFolder = dacFolder
        self.UIData = {}
        self._startPosActions = []
        workingDir = os.getcwd()
        self.worker_thread = worker_thread
        self._metaData = experimentMetaData                   
        self._output = ExperimentOutput(experimentName, path, dacFolder)        
        self._actions = []    
        self.actionDataDict = {"X-Stage": 0, "Y-Stage": 1, "Z-Stage": 2, "rotating": 3, "wiregrid": 4, "wiregrid2": 5 ,
                               "VX": 6, "VY": 7, "VZ": 8, "S0": 9, "S1": 10, "S2": 11, "S3": 12} 
        if useUI:
            self.UIData = json.load(open(os.path.join(path, "ui_data.json"), 'r'))
            useAmperemeter1 = self.UIData["ampmeter"]
            self.instHandler = getInstrumentHandlerUI(os.path.join(path, "ui_data.json"))
        else:
            useAmperemeter1 = useAmperemeter
            self.instHandler = getInstrumentHandler()
                            
        os.chdir(workingDir)
        if useUI:
            self.scanner = getScannerUI(os.path.join(path, "ui_data.json"))
        else:
            self.scanner = getScanner()
        self._currCoord = self.scanner.startCoords
        self._verbose = verbose                              # make this true if you want to print detailed logs
        self._usedVaccum = False
        if useAmperemeter1:
            self.amp = Amperemeter()

    def loadScanner(self):                                   # use scanner to create all sets of actions
        self._actions, self._startPosActions  = self.scanner.compile()
        if self.worker_thread != None:
            self.worker_thread.emit(SIGNAL("totalProgress(int)"), len(self._actions))
        #print(self._actions)
    
    def printActions(self):                                  # print all the actions 
        for action in self._actions:
            print(action)

    def addAction(self,instName,actionType,actionData):      # method to add custom actions to the actions
        self._actions.append(Action(instName,actionType,actionData))

    def addActionFunction(self,actionfunc):                  # use a different method than the one used scanner to create actions
        self._actions = actionfunc()

    def runExperiment(self):                                 # runs the experiment       
        if self.useUI:
            stageSettleTime = self.UIData["SST"]
            useAmperemeter = self.UIData["ampmeter"]
        totalActions = len(self._actions)
        currActionCount = 0
        #print(totalActions)
        first=True                                            # used for printing time information, start time needs to be captured in the first iteration
        for currAction in self._actions:
            if self.worker_thread != None:
                if self.worker_thread.isInterruptionRequested():
                    self.worker_thread.emit(SIGNAL("haveBeenStopped()"))
                    print("Experiment was stopped from the GUI")
                    break
            #try:## add level of failure as well , by returning strings
            if first:
                timestart = datetime.datetime.now()           # start time
            currOutput  = self.instHandler.runAction(currAction)       # get outputs from lower layer
            print(f"currOutput: {currOutput}")
            if currAction._instName == self.scanner.scanStage:           # stage data is used to get coordinate 
                index = self.scanner.getnameIndex(currAction._actionData[0]) 
                self._currCoord[index]  = currAction._actionData[1][0]
            if currAction._instName == self.scanner._specialInst:
                index = self.scanner.getsIndex(currAction._actionData[0])
                self._currCoord[index]  = currAction._actionData[1][0]
            if currAction._instName == self.scanner.vaccumStage:
                self._usedVaccum = True
                index = self.scanner.getvIndex(currAction._actionData[0])
                self._currCoord[index] = currAction._actionData[1][0]
            if self._verbose==True:
                print(f'Output for action {currAction._actionType} with data {currAction._actionData} is {currOutput},coords are:{self._currCoord}')
            currActionCount+=1
            if currOutput != None:
                self._output.addStepData(currAction,currOutput,self._currCoord)
            if int(currActionCount%(totalActions*0.05)) == 0:     # used for timing information
                timenow = datetime.datetime.now()
                timeDone = timenow - timestart
                print(timeDone,(totalActions-currActionCount)/currActionCount)
                first = False

                print(f'Percent Completed: {int( (currActionCount/totalActions)*100)} %;  Estimated time remaining: {((totalActions-currActionCount)/currActionCount)*timeDone} ')
            #except:
              #  print(f"Action {currAction._actionType} for instrument {currAction._instName} failed to execute" )
            if self.worker_thread != None:
                self.worker_thread.emit(SIGNAL('progressUpdate(int)'), currActionCount) #Tells the UI how may actions have been completed
        if self._usedVaccum:
            self.vaccumCleanUp()

    def vaccumCleanUp(self):
        self.instHandler._inst['VaccumStage']._device.homeAll()

if __name__=="__main__":
    exp = Experiment(experimentName,verbose=True,useUI=False)
    exp.loadScanner()
    exp.runExperiment()