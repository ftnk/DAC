from ctypes import * 
from utils.modules import Scanner
from hardware.instrumentComponent import instrumentHandler
from collections import Counter
import json
'''
This is the configuration file for the experiments.
If you do not want any line to be considered for the purpose of an expriment, you can commentit out by 
putting a hash('#') at the start of the line.

First Thing to define is the name of the experiment. This is used to create a directory for the data files 
of your experiment. This can be any name but in general try to AVOID SPACES in the name. Also, it is very 
helpful to have descriptive and short name for the experiment
'''
experimentName = 'Experiment'

'''
This is the section where the instruments can be added or removed for the scans.
Each instrument needs to be configured by adding the correct configuration data. 
Instruments have different configuration data like address, serial number.
Every instrument has a type and a name apart from along with specific configuration data like
address, serial numbers etc. 
In general, on the left of '=' sign is the variable name used by python and should not be changes. On the 
right side of  '='  is the configurable data and can be changed the as described. 
'''
instruments = []
'''
This section defines the oscilloscope that is to be used in the scan. The configuration of an oscilloscope 
currently supported required three main configuration data. These are name, model and ip_address of the scope
'''
## Its important to have only lockin or osc sections comment everything out fromthe other section, name is okay 
################################################################################ 
model = 'Tektronix_DPO4102B' #uncomment for scope 
readerAddress = '192.168.1.5' #uncomment for scope

useDummyReader = False           #Only relevant for one specific experiment (should be removed later)
#model = 'dummyReader'
name= 'ScanOsc'

useHistogram = True              #Only relevant for one specific experiment (should be removed later)

#model = 'dummyLockinamp'
#model = "LockInSRS800"
#wname = 'ScanLockIn'  #uncomment for lockin
#readerAddress = ['GPIB0','8']  #uncomment for lockin
data = [model,name,readerAddress] 
instruments.append(data)





#################################################################

'''
In this section the stages to be used for the scan are configured, there is an option to collect all satges 
to one n-dimensional stage,for this configuration data for each of the stages need to be configured. This 
can be done adding or removing lines from the following. 
NOTE: Take care not to repeat any stages.
The section below is for stage data section. Add or Remove lines by adding  a hashtag '#' symbol at the start 
of the line(please do not delete lines from below.). All lines with a hashtag will not be considered and treated 
as comment. 
'''
stageData= []
'''
The following stages are of type THorlabs Kinesis.Each of the following lines defines a stage congifuration 
and the values inside the square brackets can be changed to change configuration
The configurations are explained below:
'''

stageData.append(['Y-Stage','linear',c_char_p(b'27256338'),'Y',False])
stageData.append(['X-Stage','linear',c_char_p(b'27255894'),'X',False])
#stageData.append(['Z-Stage','linear',c_char_p(b'27004364'),'Z',False])
#stageData.append(['rotating','rotational',c_char_p(b'27005004'),'R1',True])
#stageData.append(['wiregrid','field',c_char_p(b'27255354'),'R2',False])
#stageData.append(['wiregrid2','rotational',c_char_p(b'27255354'),'R3',True])

'''
Following is the overall configuration of all the all the stages , the kinesis stages are the only onne currently supported
The configuration data means:

'''
useDummyMover = False            #Only relevant for one specific experiment (should be removed later)
stage_name = 'ScanStage'
stage_type = 'KinesisController'
#stage_type = 'dummyMover'
data = [stage_type,stage_name,{'createStages':True,'stageData':stageData}]  

instruments.append(data)
###################################################################
specialAxis = ['S0','S1','S2','S3']


specialInst1Name = "VoltageCont"
specialInst1Type = "VoltageController"
#specialInst1Type = "dummyVoltage"
data = [specialInst1Type,specialInst1Name,specialAxis]
instruments.append(data)
####################################################################
vaccumStageName = "VaccumStage"
useDummyVac = False              #Only relevant for one specific experiment (should be removed later)
vaccumStageType = "VaccumStage"
#vaccumStageType = "DummyVaccum"

 
vaccumStageAxis = ['VX','VY','VZ'] #VX: PerpVacDoor, VY: ParrVacDoor
homeStage = [True,True,True]
vaccumStageIp = '192.168.1.3'
data = [vaccumStageType,vaccumStageName,[vaccumStageIp,homeStage]]
instruments.append(data)
'''
This is the configuration for the scan that needs to be performed. Each of the configuration values are :

'''
moduleType = '2DScan' 
readerType = 0   #0 for oscilloscope 1 for lockin   
axes = ['X','Y']       #definition found in line 88ff  ,'Z', 'R1'    LEFTMOST APRAMETERS TURNS LAST
startCoords = [0.4,0]      
stopCoords = [1.0,0.6]     
step_size =  [0.2,0.2]

startCoords = [float(x) for x in startCoords]
stopCoords = [float(x) for x in stopCoords]
step_size = [float(x) for x in step_size]

#for i in range(len(startCoords)):
#    if stopCoords[i] < startCoords[i] and step_size[i] > 0:
#        step_size[i] = step_size[i] * -1

scanStage = 'ScanStage'  
specialInst = 'VoltageCont'
vaccumStage = 'VaccumStage'
scanReader = 'ScanOsc'       
stageSettleTime = 0.5      
resolution = 100        


useAmperemeter = False       #True if the experiment uses the Amperemeter, False otherwise
useDummyAmp = False          #Only relevant for one specific experiment (should be removed later)
integrationTime = 0.5       
numberOfMeassurements = 3

useCam = False
oscChannel = 2
home_after = []
timescan = False
timemeas = ''
amp_par = [False, 0, 0]

scanner = Scanner(readerType,axes,specialAxis,vaccumStageAxis,startCoords,stopCoords,step_size,scanStage,specialInst,vaccumStage,scanReader,stageSettleTime,resolution,stageData,useCam,oscChannel,home_after,timescan,timemeas,amp_par)
###########################Do not change anything after this point##############################
def getInstrumentHandler():
    '''
        This is an intrument handler
    '''
    instHandler = instrumentHandler()
    check_for_duplicates(instruments,1,'instruments-names')
    check_for_duplicates(stageData,0,'stages-names')
    check_for_duplicates(stageData,3,'sage-axis')
    for inst in instruments:
        instHandler.add_instrument(inst)
        pass
    return instHandler

def getScanner():
    global scanner
    return scanner  


# a list of lists having dupliction at any index
def check_for_duplicates(data,check_index,name):
    new_data = list(map(lambda x:x[check_index],data))
    count = [key for key in Counter(new_data).keys() if Counter(new_data)[key]>1]
    if len(count)>0:
        raise KeyError(f"One or more {name} names have been duplicated {count}")

def getScannerUI(path):
    specialAxis = ['S0','S1','S2','S3']
    vaccumStageAxis = ['VX','VY','VZ'] #VX: PerpVacDoor, VY: ParrVacDoor
    scanStage = 'ScanStage'  
    specialInst = 'VoltageCont'
    vaccumStage = 'VaccumStage'
    resolution = 10000
    f = open(path, "r")
    data = json.load(f)
    f.close()
    if data["radio"] != "lockin":
        scanReader = 'ScanOsc'
    else:
        scanReader = 'ScanLockIn'
    rType = data["reader_type"]
    axes = data["axes"]
    startCoords = data["startCoords"]
    stopCoords = data["stopCoords"]
    step_size = data["step_size"]
    waitTime = float(data["SST"])
    stageData = data["stage_data"]
    oscChannel = 2
    if "oscChannel" in list(data.keys()):
        oscChannel = data["oscChannel"]
    useCam = False
    if "camChecked" in list(data.keys()):
        useCam = data["camChecked"]
    onlyamp = False
    if "onlyamp" in list(data.keys()):
        onlyamp = [data["onlyamp"], data["meas"], data["inttime"]]
    home_after = []
    if "home_after_checkboxes" in list(data.keys()):
        home_after = data["home_after_checkboxes"]
    timeScan = data["timebox"]
    timemeas = data["waittime"]
    amp_par = [data["ampmeter"], data["inttime"], data["meas"]]

    scanner = Scanner(rType, axes, specialAxis, vaccumStageAxis, startCoords, stopCoords, step_size, scanStage, specialInst, vaccumStage, scanReader, waitTime, resolution, stageData, useCam, oscChannel, home_after, timeScan, timemeas, amp_par, onlyamp)
    return scanner



def getInstrumentHandlerUI(path):
    '''
        This is an intrument handler, that uses UI data
    '''
    instHandler = instrumentHandler()
    f = open(path, "r")
    data = json.load(f)
    f.close()
    instruments = data["instruments"]
    for i in instruments:
        if i[0] == 'KinesisController':
            for j in range(len(i[2]['stageData'])):
                a = b''
                for k in i[2]['stageData'][j][2]:
                    a += k.encode('ascii')
                i[2]['stageData'][j][2] = c_char_p(a)
    if data['dummyps']:
        for i in instruments:
            if i[0] == 'VoltageController':
                i[0] = 'dummyVoltage'
    if data['dummyVac']:
        for i in instruments:
            if i[0] == 'VaccumStage':
                i[0] = 'DummyVaccum'
    if "camChecked" in list(data.keys()):
        if data["camChecked"]:
            instruments.append(["Camera", "Camera", [data["exposure"], data["gain"], path[:-13], data["axes"]]])
    check_for_duplicates(instruments,1,'instruments-names')
    check_for_duplicates(stageData,0,'stages-names')
    check_for_duplicates(stageData,3,'sage-axis')
    
    for inst in instruments:
        instHandler.add_instrument(inst)
    return instHandler