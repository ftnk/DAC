import datetime,os
import time
from config import getInstrumentHandler,getInstrumentHandlerUI,getScanner,getScannerUI,axes,startCoords,stopCoords,step_size,useAmperemeter
from utils.action import Action
import h5py
import numpy as np
import shutil
from hardware.amperemeter import Amperemeter
import json
from PySide6.QtCore import SIGNAL, QObject, QThread



UI = False

def makeDir(filepath:'str',count:'int') -> 'str':
    '''
    Creates a directory and if the directory already exists, it creates a directory with t numbered name to avoid conflict
    #Input args : path to directory, current count
    #output : new path to file 

    '''
    while os.path.isdir(f"{filepath}-{count}"):
        count += 1
    try:                                  # if there is no naming conflict already
        filePath = f"{filepath}-{count}"
        os.mkdir(filePath)
    except:                               # if threre is a naming conflict then increase the count
        filePath = makeDir(filepath,count+1)
    return filePath


class MainBackgroundThread(QThread):
    def __init__(self, expName, directory, dacFolder, n_times, noHist):
        QThread.__init__(self)
        self.expName, self.directory, self.dacFolder, self.n_times, self.noHist = expName, directory, dacFolder, n_times, noHist

    def run(self):
        
        exp = Experiment(self.expName,verbose=True, useUI=True, path=self.directory, dacFolder=self.dacFolder, worker_thread=self, noHist=self.noHist)
        exp.instHandler.homeAll_multithread()
        exp.loadScanner(self.n_times)
        exp.instHandler.joinAll_multithread()
        exp.instHandler.moveAllToStartPos(exp._startPosActions)
        exp.runExperiment()

class ExperimentMetadata():
    # Keeps track of all the metadata for the experiment, this includes:
    # creating directory for the new experiment, copy the contents of the config file into the new file
    # copy all the relevent data processing files 
    # creating and providing referece to the hdf5 file      
    def __init__(self,experimentName:'str', path, dacFolder, noHist) -> 'None':
        # create new director
        self.experimentName = experimentName
        self.dacFolder = dacFolder
        self.noHist = noHist
        self.onlyamp = False
        self.sideamp = False
        global UI
        if UI:
            data = json.load(open(os.path.join(path, "ui_data.json"), 'r'))
            self.experimentName = data["expname"]
            startCoords1 = data["startCoords"]
            stopCoords1 = data["stopCoords"]
            self.step_size1 = data["step_size"]
            axes1 = data["axes"]
            if "onlyamp" in list(data.keys()):
                self.onlyamp = data["onlyamp"]
            if "ampmeter" in list(data.keys()):
                self.sideamp = data["ampmeter"]
            self.directory = path
        else:
            startCoords1 = startCoords
            stopCoords1 = stopCoords
            self.step_size1 = step_size
            axes1 = axes
            self.directory = os.path.join(os.path.dirname(os.path.realpath(__file__)),os.path.join('outputs-PMT',experimentName+'-'+str(datetime.datetime.now().date())))
            self.directory = makeDir(self.directory,0)

        self._mainModuleFile = os.path.join(self.directory,'MainExp.txt') # create log file
        self._configFile = self.create_config()                           # copy required files
        if not self.onlyamp:
            # set up hdf5 file
            self._fileNameh = os.path.join(self.directory,'OscData.hdf5')     
            fileh = h5py.File(self._fileNameh, "a")
            config_data = [np.array(startCoords1),np.array(stopCoords1),np.array(self.step_size1)]
            config_data = np.array(config_data)
            fileh.create_dataset(f"config", config_data.shape, dtype='f',data = config_data)
            string_dt = h5py.special_dtype(vlen=str)
            #print(axes1)
            config_data = np.array(axes1,dtype=object)
            fileh.create_dataset(f"axes", config_data.shape, dtype=string_dt,data = config_data)
            fileh.close()

            if not self.noHist:
                self._fileNameh_hist = os.path.join(self.directory,'HistData.hdf5')
                fileh_hist = h5py.File(self._fileNameh_hist, "a")
                fileh_hist.create_dataset(f"axes", data=axes1)
                fileh_hist.create_dataset(f"config", data=[startCoords1, stopCoords1, self.step_size1])
                fileh_hist.close()

        if self.onlyamp or self.sideamp:
            self._fileNameh_amp = os.path.join(self.directory, 'AmpData.hdf5')
            fileh_amp = h5py.File(self._fileNameh_amp, "a")
            fileh_amp.create_dataset(f"axes", data=axes1)
            fileh_amp.create_dataset(f"config", data=[startCoords1, stopCoords1, self.step_size1])
            fileh_amp.close()
        
    
    def create_config(self) ->'str':
    ############add files that need to be copied to new folder of experiment#########
        files_to_copy = ["replayNew.py","replay.bat","Context.py","DataManager.py"]
        files_from_folder = ["DataHandler.py","DataVisualizer.py","SDP.py","energyHeatMap.py","heatMap.py","D1plot.py"]
        ui_files_to_copy = ["ui_form_DM.py", "GUI.py", "DataManager_GUI.bat"]
        os.mkdir(os.path.join(self.directory, "ExpSpecific"))
        for file in files_to_copy:
            shutil.copyfile(os.path.join("data_processing",file), os.path.join(self.directory,file))
        for file in files_from_folder:
            shutil.copyfile(os.path.join(os.path.join("data_processing", "ExpSpecific"), file), os.path.join(os.path.join(self.directory, "ExpSpecific"), file))
        for file in ui_files_to_copy:
            shutil.copyfile(os.path.join("DatManagerUI",file), os.path.join(self.directory,file))
    ###############copying the config file###########################################
        filename = os.path.join(self.directory,"config.py")
        file = open(filename,"a")
        configfile = open(os.path.join(self.dacFolder, os.path.join("Do_not_open", "config.py")),"r")
        ignore_lines = ["","from","def","scanner"]
        file.writelines("from ctypes import * \n")
        for i in configfile.readlines():
            if i.split(" ")[0] not in ignore_lines:
                file.writelines(i)
        #file.writelines("{"*"*10} \n")
        file.close()
        configfile.close()
        return filename

class ExperimentOutput():
    def __init__(self,experimentName, path, dacFolder, noHist):
        self._metadata = ExperimentMetadata(experimentName, path, dacFolder, noHist)
        self.noHist = noHist

###   This is where all the output is handled and this is where we change the data logging and storing modules
    def addStepData(self,action,output,coords):
        # Writes the output to hdf5 file
        actionData = f'TimeStamp:{datetime.datetime.now()}, Instrument:{action._instName}, Action Type:{action._actionType}, Action Data:{action._actionData}, coords are {coords}'
        if output[0]:
            actionData= actionData+': Success'
            if action._actionType  == "GET_DATA":   # only action data corresponding to the get-Data is added to the hdf5 file
                actionData= actionData+'File:GetDataOsc'+str(datetime.datetime.now().date())+'_'
                outputData = output[1][0]
                #print(len(outputData[0]))
                #print(len(outputData[1]))
                h5data = np.array([outputData[0],outputData[1]])
                fileh = h5py.File(self._metadata._fileNameh, "a")
                coords = [float(x) for x in coords]
                print(f"Coords: {coords}")
                #print(f"OSC:  Creating dataset with key {str(coords)}")
                fileh.create_dataset(f"{coords}", h5data.shape, dtype='f',data = h5data)
                fileh.close()
        else:
            actionData= actionData+': Failure'
        file = open(self._metadata._mainModuleFile,"a")
        #logging data
        file.writelines(actionData+'\n')
        file.close()
    
    def addNewStepData(self,output,coords):
        # Writes the output to hdf5 file
        outputData = output[1][0]
        h5data = np.array([outputData[0],outputData[1]])
        fileh = h5py.File(self._metadata._fileNameh, "a")
        coords = [float(x) for x in coords]
        
        #print(f"OSC:  Creating dataset with key {str(coords)}")
        fileh.create_dataset(f"{coords}", h5data.shape, dtype='f',data = h5data)
        fileh.close()
        
        if not self.noHist:
            timevalues = [float(x) for x in outputData[2]]
            histData = [float(x) for x in outputData[3]]
            h5dataHist = np.array([timevalues, histData]) 
            fileh_hist = h5py.File(self._metadata._fileNameh_hist, "a") 
            fileh_hist.create_dataset(f"{coords}", h5dataHist.shape, dtype='f', data=h5dataHist) 
            fileh_hist.close()
    
    def addNewAmpData(self, output, coords):
        h5data = np.array(output)
        fileh_amp = h5py.File(self._metadata._fileNameh_amp, "a")
        fileh_amp.create_dataset(f"{coords}", h5data.shape, dtype='f', data=h5data) 
        fileh_amp.close()
        print(f"Adding Amp Data: {output} at {coords} to data file")
        

    """
    def addHistSetpData(self, coords, osc, action, output):
        if output[0] and action._actionType == "Wait":
            fileh_hist = h5py.File(self._metadata._fileNameh_hist, "a")
            coords = [float(x) for x in coords]
            if str(coords) not in fileh_hist.keys():
                timevalues = osc.get_time_values()
                histdata = osc.get_histogram_data()
                #osc.reset_histogram()
                #print(f"HIST: Creating dataset with key {str(coords)}")
                fileh_hist.create_dataset(str(coords), dtype="f", data=[timevalues, histdata])
                fileh_hist.close()
    """
    

class Experiment(QObject):

# Actual experiment method this method calls all other methods and then performs actions one at a time.
    def __init__(self,experimentName,experimentMetaData=None,verbose=False,useUI=False,path=None,dacFolder=None,worker_thread=None,noHist=False):
        global UI
        UI = useUI
        workingDir = os.getcwd()
        self.worker_thread = worker_thread
        self.noHist = noHist
        self._actions = [] 
        self._startPosActions = []
        self._metaData = experimentMetaData                   
        self._output = ExperimentOutput(experimentName, path, dacFolder, noHist)  
        self.useUI = useUI
        self.UIData = {}
        self.actionDataDict = {"X-Stage": 0, "Y-Stage": 1, "Z-Stage": 2, "rotating": 3, "wiregrid": 4, "wiregrid2": 5 ,
                               "VX": 6, "VY": 7, "VZ": 8, "S0": 9, "S1": 10, "S2": 11, "S3": 12, "TimeStage": 13}
        self.onlyAmpmeter = False
        if useUI:
            self.UIData = json.load(open(os.path.join(path, "ui_data.json"), 'r'))
            useAmperemeter1 = self.UIData["ampmeter"]
            if "onlyamp" in list(self.UIData.keys()):
                self.onlyAmpmeter = self.UIData["onlyamp"]
            self.instHandler = getInstrumentHandlerUI(os.path.join(path, "ui_data.json"))
        else:
            useAmperemeter1 = useAmperemeter
            self.instHandler = getInstrumentHandler()
        os.chdir(workingDir)
        if useUI:
            self.scanner = getScannerUI(os.path.join(path, "ui_data.json"))
        else:
            self.scanner = getScanner()
        self._currCoord = self.scanner.startCoords
        self._verbose = verbose                              # make this true if you want to print detailed logs
        self._usedVaccum = False
        if useAmperemeter1 or self.onlyAmpmeter:
            self.amp = Amperemeter()
        super().__init__()

    def loadScanner(self, n_times=1, sysTest=False):                                   # use scanner to create all sets of actions
        self._actions, self._startPosActions = self.scanner.compile(n_times, sysTest=sysTest)
        if self.worker_thread != None:
            self.worker_thread.emit(SIGNAL("totalProgress(int)"), len(self._actions))
        #print(self._actions)
    
    def printActions(self):                                  # print all the actions 
        for action in self._actions:
            print(action)

    def addAction(self,instName,actionType,actionData):      # method to add custom actions to the actions
        self._actions.append(Action(instName,actionType,actionData))

    def addActionFunction(self,actionfunc):                  # use a different method than the one used scanner to create actions
        self._actions = actionfunc()

    def runExperiment(self):                                 # runs the experiment           
        totalActions = len(self._actions)
        currActionCount = 0
        #print(totalActions)
        counter = 0
        first=True                                            # used for printing time information, start time needs to be captured in the first iteration
        for currAction in self._actions:
            if self.worker_thread != None:
                if self.worker_thread.isInterruptionRequested():
                    if self._usedVaccum:
                        self.vaccumCleanUp()
                    self.worker_thread.emit(SIGNAL("haveBeenStopped()"))
                    print("Experiment was stopped from the GUI")
                    break
            if first:
                timestart = datetime.datetime.now()           # start time
            if currAction._actionType == "MoveStage" or currAction._actionType == "MoveAxis":
                timeA = time.time()
            #print(f"About to run action:\n    {currAction}")
            currOutput  = self.instHandler.runAction(currAction)       # get outputs from lower layer
            #print(f"Action completed:\n    {currAction}")

            if currAction._actionType == "MoveStage" or currAction._actionType == "MoveAxis":
                timeB = time.time()
                timetaken = timeB - timeA
                if self.worker_thread != None:
                    self.worker_thread.emit(SIGNAL("getStepsTime(float,int)"), timetaken, self.actionDataDict[currAction._actionData[0]])

            #print(f"actionData: {currAction._actionData}")
            #print(f"currOutput: {currOutput}")
            justMoved = False
            if currAction._actionData[0] == 'wiregrid':
                lastPos = currAction._actionData[1][0]
            if currAction._instName == self.scanner.scanStage:           # stage data is used to get coordinate 
                index = self.scanner.getnameIndex(currAction._actionData[0]) 
                self._currCoord[index]  = currAction._actionData[1][0]
                justMoved = True
            if currAction._instName == self.scanner._specialInst:
                if currAction._actionType != "ShutDown":
                    index = self.scanner.getsIndex(currAction._actionData[0])
                    self._currCoord[index]  = currAction._actionData[1][0]
                    justMoved = True
            if currAction._instName == self.scanner.vaccumStage:
                self._usedVaccum = True
                index = self.scanner.getsIndex(currAction._actionData[0])
                self._currCoord[index]  = currAction._actionData[1][0]
                justMoved = True
            if currAction._instName == "TimeStage":
                index = self.scanner.getnameIndex(currAction._actionData[0])
                self._currCoord[index]  = currAction._actionData[1][0]
                justMoved = True
            if self._verbose==True:
                pass
                #print(f'Output for action {currAction._actionType} with data {currAction._actionData} is {currOutput},coords are:{self._currCoord}')
            if justMoved:
                print(f"Coords: {self._currCoord}")
            currActionCount+=1
            
            

            if currAction._actionType == "GET_DATA" and currOutput[0]:
                self._output.addNewStepData(currOutput,self._currCoord)
            
            if currAction._actionType == "READAMP":
                self._output.addNewAmpData(currOutput, self._currCoord)

            if int(currActionCount%(totalActions*0.05)) == 0:     # used for timing information
                timenow = datetime.datetime.now()
                timeDone = timenow - timestart
                #print(timeDone,(totalActions-currActionCount)/currActionCount)
                first = False
                timeStamp = f"[{timenow.strftime('%H')}:{timenow.strftime('%M')}:{timenow.strftime('%S')}]"

                print(f'{timeStamp} Percent Completed: {int( (currActionCount/totalActions)*100)} %;  Estimated time remaining: {((totalActions-currActionCount)/currActionCount)*timeDone} ')
           
            counter += 1
            if self.worker_thread != None:
                self.worker_thread.emit(SIGNAL('progressUpdate(int)'), counter) #Tells the UI how may actions have been completed
        if self._usedVaccum:
            self.vaccumCleanUp()
    
    def vaccumCleanUp(self):
        #self.instHandler._inst['VaccumStage']._device.homeAll()
        self.instHandler._inst['VaccumStage']._device.disconnect()
