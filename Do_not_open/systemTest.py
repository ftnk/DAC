from experiment import Experiment
import os
import datetime
import json
import shutil

def makeDir(path):
    if os.path.exists(path):
        count = 1
        while os.path.exists(path + f"_{count}"):
            count += 1
        os.mkdir(path + f"_{count}")
        return path + f"_{count}"
    else:
        os.mkdir(path)
        return path

def getNewFilePath(filepath:'str') -> 'str': #If filepath exists add "-n" before the file extension (Before: test.json After: test-1.json)
    count = 0
    if os.path.exists(filepath):
        count = 1
        dotindx = filepath[1:].index(".")
        while os.path.exists(f"{filepath[:dotindx + 1]}-{count}{filepath[dotindx + 1:]}"):
            count += 1
    else:
        return filepath
    return f"{filepath[:dotindx + 1]}-{count}{filepath[dotindx + 1:]}"

def runExperiment_TEST():
    expName = "SYSTEST"
    instruments = []
    model = "dummyReader"
    readerAddress = "192.168.1.5"
    name = "ScanOsc"

    instruments.append([model,name,readerAddress])
    stageData= []
    stageData.append(['X-Stage','linear','27255894','X',True])
    stageData.append(['Y-Stage','linear','27256338','Y',True])
    stageData.append(['Z-Stage','linear','27004364','Z',True])
    stageData.append(['rotating','rotational','27005004','R1',True])
    stageData.append(['wiregrid','field','27255354','R2',True])
    instruments.append(["dummyMover", "ScanStage",{'createStages':True,'stageData':stageData}])


    instruments.append(["DummyVaccum", "VaccumStage", ['192.168.1.3',[True, True, True]]])

    dataDict = {}
    dataDict["expname"] = expName
    dataDict["radio"] = "osc"
    dataDict["checkboxes"] = [True, True, True, True, True, False, True, True, True, False, False, False, False]
    dataDict["home_checkboxes"] = [True, True, True, True, True, False, True, True, True, False, False, False, False]
    dataDict["timebox"] = False
    dataDict["SST"] = "0.0"
    dataDict["ampmeter"] = False
    dataDict["inttime"] = "10"
    dataDict["meas"] = "5"
    dataDict["start"] = ["0", "0", "0", "0", "0", "", "0", "0", "0", "", "", "", ""]
    dataDict["stop"] = ["10", "10", "10", "180", "1", "", "3", "3", "3", "", "", "", ""]
    dataDict["step"] = ["10", "10", "10", "180", "1", "", "3", "3", "3", "", "", "", ""]
    dataDict["ord"] = ["1", "2", "3", "4", "5", "", "6", "7", "8", "", "", "", ""]
    dataDict["waittime"] = ""
    dataDict["dummyps"] = True
    dataDict["exposure"] = "10"
    dataDict["gain"] = "1"
    dataDict["camChecked"] = False
    dataDict["exposureMin"] = "0"
    dataDict["exposureMax"] = "100"
    dataDict["gainMin"] = "0"
    dataDict["gainMax"] = "10"
    dataDict["axes"] = ["X", "Y", "Z", "R1", "R2", "VX", "VY", "VZ"]
    dataDict["oscChannel"] = "2"

    dataDict["instruments"] = instruments
    dataDict["startCoords"] = ["0", "0", "0", "0", "0", "0", "0", "0"]
    dataDict["stopCoords"] = ["3", "3", "3", "1", "180", "10", "10", "10"]
    dataDict["step_size"] = ["3", "3", "3", "1", "180", "10", "10", "10"]
    dataDict["axes"] = ["VZ", "VY", "VX", "R2", "R1", "Z", "Y", "X"]

    dataDict["dummyVac"] = True
    dataDict["reader_type"] = 0
    dataDict["stage_data"] = stageData
    dataDict["resolution"] = 10000
    dataDict["interrupted"] = False
    dataDict["new_stop"] = []

    path = os.getcwd()
    dacFolder = os.path.abspath(path)

    outputs = os.path.join(dacFolder, 'outputs-PMT')
    if not os.path.isdir(outputs):
        os.mkdir(outputs)
    
    directory = os.path.join(dacFolder,os.path.join('outputs-PMT',expName+'-'+str(datetime.datetime.now().date())))
    directory = makeDir(directory)

    timeDict = {}
    timeDict["timescan"] = False
    timeDict["waittime"] = ""
    timeDict["dummymover"] = True
    timeDict["dummyVac"] = True
    timeDict["steps"] = 256
    timeDict["SST"] = "0.0"
    timeDict["mintime"] = 128
    timeDict["axes"] = ["X", "Y", "Z", "R1", "R2", "VX", "VY", "VZ"]
    timeDict["stepTimes"] = {"X": [], "Y": [], "Z": [], "R1": [], "R2": [], "R3": [],
                             "VX": [], "VY": [], "VZ": [], "S0": [], "S1": [], "S2": [], "S3": []}


    dataDict["startCoords"] = [float(x) for x in dataDict["startCoords"]]
    dataDict["stopCoords"] = [float(x) for x in dataDict["stopCoords"]]
    dataDict["step_size"] = [float(x) for x in dataDict["step_size"]]
    
    jsonpath = getNewFilePath(os.path.join(directory , "ui_data.json"))
    outfile = open(jsonpath, "w")
    json.dump(dataDict, outfile)
    outfile.close()

    exp = Experiment(expName, verbose=True, useUI=True, path=directory, dacFolder=dacFolder, worker_thread=None)
    exp.instHandler.homeAll_multithread()
    exp.loadScanner(n_times=1, sysTest=True)
    exp.instHandler.joinAll_multithread()
    exp.instHandler.moveAllToStartPos(exp._startPosActions)
    exp.runExperiment()

    return directory

def copyDataFiles():
    files_to_copy = ["Context.py","DataManager.py"]
    files_from_folder = ["DataHandler.py","DataVisualizer.py","SDP.py","energyHeatMap.py","heatMap.py","D1plot.py"]
    for file in files_to_copy:
        #print(f"Trying to copy from: {os.path.join(os.getcwd(), os.path.join('data_processing',file))} to {os.path.join(os.path.join(os.getcwd(), 'Do_not_open'), file)}")
        shutil.copyfile(os.path.join(os.getcwd(), os.path.join('data_processing',file)), os.path.join(os.path.join(os.getcwd(), 'Do_not_open'), file))
    for file in files_from_folder:
        shutil.copyfile(os.path.join(os.getcwd(), os.path.join(os.path.join('data_processing', "ExpSpecific"),file)), os.path.join(os.path.join(os.getcwd(), 'Do_not_open'), file))


def deleteDataFiles():
    if not os.path.exists(os.path.join(os.path.join(os.getcwd(), 'Do_not_open'), "DataHandler.py")): return
    files_to_delete = ["Context.py","DataManager.py","DataHandler.py","DataVisualizer.py","SDP.py","energyHeatMap.py","heatMap.py","D1plot.py"]
    for file in files_to_delete:
        os.remove(os.path.join(os.path.join(os.getcwd(), 'Do_not_open'), file))

def p_point(coord, hist):
    print(f"PlotPoint: {coord} (hist: {hist})")

def runAllPlots_TEST(directory):
    if not os.path.exists(os.path.join(os.path.join(os.getcwd(), 'Do_not_open'), "DataHandler.py")):
        copyDataFiles()

    from DataHandler import DataHandler
    from DataVisualizer import DataVisualizer
    dh = DataHandler(directory)
    dv = DataVisualizer(dh)
    dv.OneDPlot(["Y", "Z", "R1", "R2", "VX", "VY", "VZ"], [0, 0, 0, 0, 0, 0, 0], False)
    dv.OneDPlot(["Y", "Z", "R1", "R2", "VX", "VY", "VZ"], [0, 0, 0, 0, 0, 0, 0], True)
    dv.heatMap(["Y", "Z", "R1", "VX", "VY", "VZ"], [0, 0, 0, 0, 0, 0], False, axisMap={"X": "X", "Y": "R2"})
    dv.heatMap(["Y", "Z", "R1", "VX", "VY", "VZ"], [0, 0, 0, 0, 0, 0], True, axisMap={"X": "R2", "Y": "X"})
    dv.energyHeatMap("R2", ["VZ", "VY", "VX", "R1", "Z", "Y", "X"],
                     {"VZ": 0, "VY": 1, "VX": 2, "R2": 3, "R1": 4, "Z": 5, "Y": 6, "X": 7},
                     [3, 3, 3, 180, 10, 10, 10],
                     [[0, 0, 0, 0, 0, 0, 0, 0],
                      [3, 3, 3, 1, 180, 10, 10, 10], 
                      [3, 3, 3, 1, 180, 10, 10, 10]], 0, 1, {"start": 0, "stop": 1, "index": 3})
    dv.plotPoint([0, 3, 3, 0, 180, 0, 10, 10], True)
    dv.plotPoint([3, 3, 3, 0, 180, 0, 10, 0], False)
    
    deleteDataFiles()

def runWholeTest():
    directory = runExperiment_TEST()
    print(f"Experiment completed")
    print(f"Output folder: {directory}")
    runAllPlots_TEST(directory)

def runPlots(directory):
    deleteDataFiles()
    runAllPlots_TEST(directory)

#runPlots("C:/Users/rune2/Documents/DTU/DAC_MAIN/DAC/outputs-PMT/SYSTEST-2023-08-23")
#runAllPlots_TEST("C:/Users/rune2/Documents/DTU/DAC_MAIN/DAC/outputs-PMT/SYSTEST-2023-08-02_14")
#deleteDataFiles()
runWholeTest()

