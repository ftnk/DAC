"""
@author: Rune
"""

import datetime,os

from config import getInstrumentHandler,getScanner,experimentName,axes,startCoords,stopCoords,step_size,stageData,stageSettleTime,useAmperemeter,useDummyMover,useDummyReader,useDummyAmp,integrationTime,numberOfMeassurements,useDummyVac,useHistogram
from utils.action import Action
import h5py
import numpy as np
import shutil
from hardware.amperemeter import Amperemeter
from hardware.oscilloscopes import Tektronix_DPO4102B
from hardware.motion_controllers import KinesisController
from hardware.dummyHardware import dummyMover
from hardware.dummyHardware import dummyReader
from hardware.dummyHardware import dummyAmp
from hardware.vaccumstage import DummyVaccumStage
import time
from ctypes import * 
from pipython.pidevice.gcscommands import GCSCommands
from pipython.pidevice.gcsmessages import GCSMessages
from pipython.pidevice.interfaces.pisocket import PISocket
import csv
import pandas as pd




def makeDir(filepath:'str',count:'int') -> 'str':
    '''
    Creates a directory and if the directory already exists, it creates a directory with t numbered name to avoid conflict
    #Input args : path to directory, current count
    #output : new path to file 

    '''
    try:                                  # if there is no naming conflict already
        filePath = f"{filepath}-{count}"
        os.mkdir(filePath)
    except:                               # if threre is a naming conflict then increase the count
        filePath = makeDir(filepath,count+1)
    return filePath




class ExperimentMetadata():
    # Keeps track of all the metadata for the experiment, this includes:
    # creating directory for the new experiment, copy the contents of the config file into the new file
    # copy all the relevent data processing files 
    # creating and providing referece to the hdf5 file      
    def __init__(self,experimentName:'str') -> 'None':
        # create new director
        self.experimentName = experimentName
        self.directory = os.path.join(os.path.dirname(os.path.realpath(__file__)),os.path.join('outputs-PMT',experimentName+'-'+str(datetime.datetime.now().date())))
        self.directory = makeDir(self.directory,0)

        self._mainModuleFile = os.path.join(self.directory,'MainExp.txt') # create log file
        self._configFile = self.create_config()                           # copy required files
        # set up hdf5 file
        self._fileNameh = os.path.join(self.directory,'OscData.hdf5')
        self._fileNameHist = os.path.join(self.directory, 'HistData.hdf5')     
        fileh = h5py.File(self._fileNameh, "a")
        fileHist = h5py.File(self._fileNameHist, "a")
        config_data = [np.array(startCoords),np.array(stopCoords),np.array(step_size)]
        config_data = np.array(config_data)
        fileh.create_dataset(f"config", config_data.shape, dtype='f',data = config_data)
        string_dt = h5py.special_dtype(vlen=str)
        #print(axes)
        config_data = np.array(axes,dtype=object)
        fileh.create_dataset(f"axes", config_data.shape, dtype=string_dt,data = config_data)
        fileh.close()
    
    def create_config(self) ->'str':
    ############add files that need to be copied to new folder of experiment#########
        files_to_copy = ["replayNew.py","replay.bat","Context.py","DataManager.py","DataHandler.py","DataVisualizer.py"]
        for file in files_to_copy:
            shutil.copyfile(os.path.join("data_processing",file), os.path.join(self.directory,file))
    ###############copying the config file###########################################
        filename = os.path.join(self.directory,"config.py")
        file = open(filename,"a")
        configfile = open("config.py","r")
        ignore_lines = ["","from","def","scanner"]
        file.writelines("from ctypes import * \n")
        for i in configfile.readlines():
            if i.split(" ")[0] not in ignore_lines:
                file.writelines(i)
        #file.writelines("{"*"*10} \n")
        file.close()
        configfile.close()
        return filename

class ExperimentOutput():
    def __init__(self,experimentName):
        self._metadata = ExperimentMetadata(experimentName)
  
###   This is where all the output is handled and this is where we change the data logging and storing modules
    def addStepData(self,action,output,coords):
        # Writes the output to hdf5 file
        actionData = f'TimeStamp:{datetime.datetime.now()}, Instrument:{action._instName}, Action Type:{action._actionType}, Action Data:{action._actionData}, coords are {coords}'
        if output[0]:
            actionData= actionData+': Success'
            if action._actionType  == "GET_DATA":   # only action data corresponding to the get-Data is added to the hdf5 file
                actionData= actionData+'File:GetDataOsc'+str(datetime.datetime.now().date())+'_'
                outputData = output[1][0]
                #print(len(outputData[0]))
                #print(len(outputData[1]))
                h5data = np.array([outputData[0],outputData[1]])
                fileh = h5py.File(self._metadata._fileNameh, "a")
                print(f"Coords: {coords}")
                fileh.create_dataset(f"{coords}", h5data.shape, dtype='f',data = h5data)
                fileh.close()
        else:
            actionData= actionData+': Failure'
        file = open(self._metadata._mainModuleFile,"a")
        #logging data
        file.writelines(actionData+'\n')
        file.close()
    

        



class Experiment():

# Actual experiment method this method calls all other methods and then performs actions one at a time.
    def __init__(self,experimentName,experimentMetaData=None,verbose=False):
        self._metaData = experimentMetaData
        #self.outPath = os.path.join(os.path.dirname(os.path.realpath(__file__)),'PosMeanSTD.csv')
        #self.outPath = os.path.join(os.path.dirname(os.path.realpath(__file__)),os.path.join('outputs-PMT',experimentName+'-'+str(datetime.datetime.now().date())))                   
        self._output = ExperimentOutput(experimentName)
        self.outPath = os.path.join(self._output._metadata.directory,'PosMeanSTD.csv')        
        self._actions = []                         
        self._verbose = verbose                              # make this true if you want to print detailed logs
        self._usedVaccum = False
        
        self.Rmap = {"R1": [c_char_p(b'27005004'), 'rotational', 'rotating', False],
                     "R2": [c_char_p(b'27255354'), 'field', 'wiregrid', False],
                     "R3": [c_char_p(b'27255354'), 'rotational', 'wiregrid2', False]}
        self.XYZmap = {"X": [c_char_p(b'27255894'), 'linear', 'X-Stage', False],
                       "Y": [c_char_p(b'27256338'), 'linear', 'Y-Stage', False]}
        for i in stageData:
            if str(i[3][0]) == "R":
                self.Rmap[i[3]][-1] = i[-1]
            else:
                self.XYZmap[i[3]][-1] = i[-1]
        if not useDummyVac:
            self.gateway = PISocket(host='192.168.1.3')
            self.messeges = GCSMessages(self.gateway)
            self.gcs = GCSCommands(self.messeges)
        else:
            self.gcs = DummyVaccumStage('192.168.1.3')
        self.map = {'VX':'1','VY':'2','VZ':'3'}
        if useAmperemeter and not useDummyAmp:
            self.amp = Amperemeter()
        else:
            self.amp = dummyAmp()
        self.R_motion_map = {}
        self.XYZ_motion_map = {}
        if useDummyMover:
            self.R_motion_map = {"R1": dummyMover(), "R2": dummyMover(), "R3": dummyMover()}
            self.XYZ_motion_map = {"X": dummyMover(), "Y": dummyMover(), "Z": dummyMover()}
        else:
             for i in axes:
                if i[0] == 'R':
                    self.R_motion_map[i] = KinesisController(serialNumber=self.Rmap[i][0], stageType=self.Rmap[i][1], stageName=self.Rmap[i][2], HomeStage=self.Rmap[i][3])
                elif i[0] != 'V':
                    self.XYZ_motion_map[i] = KinesisController(serialNumber=self.XYZmap[i][0], stageType=self.XYZmap[i][1], stageName=self.XYZmap[i][2], HomeStage=self.XYZmap[i][3])
        if useDummyReader:
            self.scope = dummyReader("192.168.1.5")
        else:
            self.scope = Tektronix_DPO4102B("192.168.1.5")
        if useHistogram:
            self.scope.set_histogram_mode("HORizontal")
        self.dataToWrite = []
        self.started = False
        self.notDone = True
        self.meanDict = {}
        self.stdDict = {}
        self.values = {}
        self.timeExported = False
        self.dictLength = int(round((stopCoords[0] - startCoords[0]) / step_size[0], 0))
        for i in axes:
            self.values[i] = []

        

    def runExperiment(self):                                 # runs the experiment           
        steps = []
        for i in range(len(startCoords)):
            steps.append(int(abs(stopCoords[i] - startCoords[i]) / step_size[i]))
        curPos = []
        for i in startCoords:
            curPos.append(i)
        counter = len(curPos)-1
        preMove = []
        preTo = []
        preFr = []
        for i in range(len(curPos)):
            preMove.append(axes[i])
            preTo.append(startCoords[i])
            preFr.append(startCoords[i])
        self.moveMotionController(preMove, preTo, preFr, curPos)

        while self.notDone:
            toMove = []
            toarr = []
            frarr = []
            curPos[counter] += step_size[counter]
            curPos = [round(i, 2) for i in curPos]
            if curPos[counter] <= stopCoords[counter]:
                self.moveMotionController([axes[counter]], [curPos[counter]], [curPos[counter] - step_size[counter]], curPos)
            if curPos[counter] > stopCoords[counter]:
                for i in range(len(curPos)-1, -1, -1):
                    if curPos[i] > stopCoords[i]:
                        toMove.append(axes[i])
                        toarr.append(startCoords[i])
                        frarr.append(curPos[i] - step_size[i])
                        curPos[i] = startCoords[i]

                        if i > 0:
                            curPos[i-1] += step_size[i-1]
                            curPos = [round(i, 2) for i in curPos]
                            if curPos[i-1] <= stopCoords[i-1]:
                                toMove.append(axes[i-1])
                                toarr.append(curPos[i-1])
                                frarr.append(curPos[i-1] - step_size[i-1])
                        else: 
                            break

            if curPos == stopCoords:
                break

            if len(toMove) > 0:
                self.moveMotionController(toMove, toarr, frarr, curPos)
                
            counter = len(curPos)-1

            
        
        
            
            

    
    def moveMotionController(self, axis, toold, frold, curposold):
        if not self.notDone:
            return
        to = [round(i, 2) for i in toold]
        fr = [round(i, 2) for i in frold]
        curpos = [round(i, 2) for i in curposold]
        if curpos == startCoords and self.started:
            self.notDone = False
            return
        self.started = True
        for i in range(len(axis)):
            #print(f"Move {axis[i]} from {fr[i]} to {to[i]}")
            if axis[i][0] == "V":
                self.gcs.MOV(self.map[axis[i]], to[i])
                if to[i] not in self.values[axis[i]]:
                    self.values[axis[i]].append(to[i])
            elif axis[i][0] == "R":
                if self.Rmap[axis[i]][1] == "field":
                    temp = np.arccos(np.sqrt(to[i]))*180/np.pi
                    self.R_motion_map[axis[i]].move_device(temp)
                else:
                    self.R_motion_map[axis[i]].move_device(to[i])
            else:
                self.XYZ_motion_map[axis[i]].move_device(to[i])
        curpos = [round(i, 2) for i in curpos]
        print(f"Pos: {curpos}")
        #print("Resetting scope")
        #print("Scope aq mode:")
        #print(self.scope.query_acquisition_mode())
        #print(type(self.scope.query_acquisition_mode()))
        if self.scope.query_acquisition_mode() == "ENVELOPE":
            self.scope.set_acquisition_mode("SAM")
            self.scope.set_acquisition_mode("ENV")
        
        mean, std = self.amp.get_multiple_currents(intTime=integrationTime, n=numberOfMeassurements)
        #print(f"Amp results: {mean}, {std}")
        value = str(curpos) + "," + str(mean) + "," + str(std) + "\n"
        value = value.replace("[", '').replace("]", '').replace(" ", '').replace("\n", '')
        toWrite = value.split(",")
        #print(f"Writing: {toWrite}")
        if useAmperemeter and not useDummyAmp:
            self.saveExperiment(toWrite)
        #self.dataToWrite.append(toWrite)
        
        #print(f"Waiting stageSetteTime ({stageSettleTime}s)")
        time.sleep(stageSettleTime)
        outputData = self.scope.get_data()
        if useHistogram:
            self.saveHistogramData(curpos)
        #print("Reading scope:")
        #print(outputData)
        h5data = np.array([outputData[0], outputData[1]])
        fileh = h5py.File(self._output._metadata._fileNameh, "a")
        coords = [float(x) for x in curpos]
        fileh.create_dataset(f"{coords}", h5data.shape, dtype='f',data = h5data)
        fileh.close()
        #print()

    def preSaveSetup(self):
        if len(axes) == 2:
            self.meanDict[axes[0] + "/" + axes[1]] = ['None' for _ in range(self.dictLength)]
            self.stdDict[axes[0] + "/" + axes[1]] = ['None' for _ in range(self.dictLength)]
        elif len(axes) == 1:
            self.meanDict[axes[0]] = []
            self.meanDict["Values"] = []
            self.stdDict[axes[0]] = []
            self.stdDict["Values"] = []


    def saveExperiment(self, data):
        if len(axes) == 2:
            if str(data[0]) not in self.meanDict.keys():
                self.meanDict[str(data[0])] = ['None' for _ in range(self.dictLength)]
                self.stdDict[str(data[0])] = ['None' for _ in range(self.dictLength)]
            if str(data[1]) not in self.meanDict[axes[0] + "/" + axes[1]]:
                indx = -1
                for i in range(len(self.meanDict[axes[0] + "/" + axes[1]])):
                    if self.meanDict[axes[0] + "/" + axes[1]][i] == 'None':
                        indx = i
                        break
                self.meanDict[axes[0] + "/" + axes[1]][indx] = str(data[1])
                self.stdDict[axes[0] + "/" + axes[1]][indx] = str(data[1])
            indx = -1
            for i in range(len(self.meanDict[str(data[0])])):
                if self.meanDict[str(data[0])][i] == 'None':
                    indx = i
                    break
            self.meanDict[str(data[0])][indx] = data[2]
            self.stdDict[str(data[0])][indx] = data[3]


        elif len(axes) == 1:
            self.meanDict[axes[0]].append(data[0])
            self.stdDict[axes[0]].append(data[0])
            self.meanDict['Values'].append(data[1])
            self.stdDict['Values'].append(data[2])
        meanDF = pd.DataFrame(self.meanDict)
        stdDF = pd.DataFrame(self.stdDict)
        meanWriter = pd.ExcelWriter(os.path.join(self._output._metadata.directory, "mean.xlsx"), engine='xlsxwriter')
        stdWriter = pd.ExcelWriter(os.path.join(self._output._metadata.directory, "std.xlsx"), engine='xlsxwriter')
        
        meanDF.to_excel(meanWriter, sheet_name='Sheet1', startrow=1, header=False, index=False)
        stdDF.to_excel(stdWriter, sheet_name='Sheet1', startrow=1, header=False, index=False)
        meanWorksheet = meanWriter.sheets['Sheet1']
        stdWorksheet = stdWriter.sheets['Sheet1']
        (max_row, max_col) = meanDF.shape
        meanColumn_settings = [{'header': column} for column in meanDF.columns]
        stdColumn_settings = [{'header': column} for column in stdDF.columns]
        meanWorksheet.add_table(0, 0, max_row, max_col - 1, {'columns': meanColumn_settings})
        stdWorksheet.add_table(0, 0, max_row, max_col - 1, {'columns': stdColumn_settings})
        meanWorksheet.set_column(0, max_col - 1, 12)
        stdWorksheet.set_column(0, max_col - 1, 12)
        meanWriter.save()
        stdWriter.save()
    
    def saveHistogramData(self, pos):
        pos = [float(x) for x in pos]
        fileHist = h5py.File(self._output._metadata._fileNameHist, "a")
        data = self.scope.get_histogram_data()
        data = [float(x) for x in data]
        timevalues = self.scope.get_time_values()
        if not self.timeExported:
            #fileHist.create_dataset("timeValues", dtype='f',data = timevalues)
            fileHist.create_dataset("axes", data = axes)
            fileHist.create_dataset("config", dtype='f', data=[startCoords, stopCoords, step_size])
            self.timeExported = True
        fileHist.create_dataset(str(pos), dtype='f',data = [timevalues, data])
        self.scope.reset_histogram()

    def vaccumCleanUp(self):
        self.instHandler._inst['VaccumStage']._device.homeAll()

if __name__=="__main__":
    exp = Experiment(experimentName,verbose=True)
    exp.preSaveSetup()
    exp.runExperiment()