# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'stagesForm.ui'
##
## Created by: Qt User Interface Compiler version 6.4.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QGridLayout, QHBoxLayout,
    QLabel, QLineEdit, QPushButton, QSizePolicy,
    QWidget)

class Ui_StageForm(object):
    def setupUi(self, StageForm):
        if not StageForm.objectName():
            StageForm.setObjectName(u"StageForm")
        StageForm.resize(950, 440)
        self.horizontalLayoutWidget = QWidget(StageForm)
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(10, 10, 931, 201))
        self.horizontalLayout = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.frame = QFrame(self.horizontalLayoutWidget)
        self.frame.setObjectName(u"frame")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy)
        self.frame.setMaximumSize(QSize(281, 177))
        self.frame.setFrameShape(QFrame.Box)
        self.frame.setFrameShadow(QFrame.Plain)
        self.layoutWidget = QWidget(self.frame)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(10, 10, 262, 163))
        self.gridLayout = QGridLayout(self.layoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.x_con_l = QLabel(self.layoutWidget)
        self.x_con_l.setObjectName(u"x_con_l")
        font = QFont()
        font.setPointSize(8)
        self.x_con_l.setFont(font)

        self.gridLayout.addWidget(self.x_con_l, 0, 2, 1, 1)

        self.step_x_edit = QLineEdit(self.layoutWidget)
        self.step_x_edit.setObjectName(u"step_x_edit")

        self.gridLayout.addWidget(self.step_x_edit, 2, 1, 1, 1)

        self.right_x_btn = QPushButton(self.layoutWidget)
        self.right_x_btn.setObjectName(u"right_x_btn")
        self.right_x_btn.setMaximumSize(QSize(16777215, 24))
        font1 = QFont()
        font1.setPointSize(11)
        font1.setBold(True)
        self.right_x_btn.setFont(font1)

        self.gridLayout.addWidget(self.right_x_btn, 3, 2, 1, 1)

        self.label_2 = QLabel(self.layoutWidget)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)

        self.label = QLabel(self.layoutWidget)
        self.label.setObjectName(u"label")
        self.label.setMinimumSize(QSize(0, 49))
        font2 = QFont()
        font2.setPointSize(24)
        self.label.setFont(font2)
        self.label.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.label, 0, 1, 1, 1)

        self.left_x_btn = QPushButton(self.layoutWidget)
        self.left_x_btn.setObjectName(u"left_x_btn")
        self.left_x_btn.setMaximumSize(QSize(16777215, 24))
        font3 = QFont()
        font3.setPointSize(11)
        font3.setBold(True)
        font3.setUnderline(False)
        font3.setStrikeOut(False)
        self.left_x_btn.setFont(font3)

        self.gridLayout.addWidget(self.left_x_btn, 3, 0, 1, 1)

        self.curPos_x_edit = QLineEdit(self.layoutWidget)
        self.curPos_x_edit.setObjectName(u"curPos_x_edit")

        self.gridLayout.addWidget(self.curPos_x_edit, 3, 1, 1, 1)

        self.go_x_btn = QPushButton(self.layoutWidget)
        self.go_x_btn.setObjectName(u"go_x_btn")
        self.go_x_btn.setMinimumSize(QSize(0, 24))
        font4 = QFont()
        font4.setPointSize(9)
        font4.setBold(True)
        self.go_x_btn.setFont(font4)

        self.gridLayout.addWidget(self.go_x_btn, 4, 1, 1, 1)

        self.ref_x_btn = QPushButton(self.layoutWidget)
        self.ref_x_btn.setObjectName(u"ref_x_btn")

        self.gridLayout.addWidget(self.ref_x_btn, 0, 0, 1, 1)

        self.label_3 = QLabel(self.layoutWidget)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)

        self.pos_x_l = QLabel(self.layoutWidget)
        self.pos_x_l.setObjectName(u"pos_x_l")

        self.gridLayout.addWidget(self.pos_x_l, 1, 1, 1, 1)


        self.horizontalLayout.addWidget(self.frame)

        self.frame_2 = QFrame(self.horizontalLayoutWidget)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setMaximumSize(QSize(281, 177))
        self.frame_2.setFrameShape(QFrame.Box)
        self.frame_2.setFrameShadow(QFrame.Plain)
        self.layoutWidget1 = QWidget(self.frame_2)
        self.layoutWidget1.setObjectName(u"layoutWidget1")
        self.layoutWidget1.setGeometry(QRect(10, 10, 262, 157))
        self.gridLayout_2 = QGridLayout(self.layoutWidget1)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setContentsMargins(1, 1, 0, 0)
        self.label_4 = QLabel(self.layoutWidget1)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout_2.addWidget(self.label_4, 2, 0, 1, 1)

        self.right_y_btn = QPushButton(self.layoutWidget1)
        self.right_y_btn.setObjectName(u"right_y_btn")
        self.right_y_btn.setMaximumSize(QSize(16777215, 24))
        self.right_y_btn.setFont(font1)

        self.gridLayout_2.addWidget(self.right_y_btn, 3, 2, 1, 1)

        self.label_7 = QLabel(self.layoutWidget1)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setFont(font2)
        self.label_7.setAlignment(Qt.AlignCenter)

        self.gridLayout_2.addWidget(self.label_7, 0, 1, 1, 1)

        self.ref_y_btn = QPushButton(self.layoutWidget1)
        self.ref_y_btn.setObjectName(u"ref_y_btn")

        self.gridLayout_2.addWidget(self.ref_y_btn, 0, 0, 1, 1)

        self.step_y_edit = QLineEdit(self.layoutWidget1)
        self.step_y_edit.setObjectName(u"step_y_edit")

        self.gridLayout_2.addWidget(self.step_y_edit, 2, 1, 1, 1)

        self.go_y_btn = QPushButton(self.layoutWidget1)
        self.go_y_btn.setObjectName(u"go_y_btn")
        self.go_y_btn.setMinimumSize(QSize(88, 24))
        self.go_y_btn.setFont(font4)

        self.gridLayout_2.addWidget(self.go_y_btn, 4, 1, 1, 1)

        self.left_y_btn = QPushButton(self.layoutWidget1)
        self.left_y_btn.setObjectName(u"left_y_btn")
        self.left_y_btn.setMaximumSize(QSize(16777215, 24))
        self.left_y_btn.setFont(font3)

        self.gridLayout_2.addWidget(self.left_y_btn, 3, 0, 1, 1)

        self.y_con_l = QLabel(self.layoutWidget1)
        self.y_con_l.setObjectName(u"y_con_l")
        self.y_con_l.setFont(font)

        self.gridLayout_2.addWidget(self.y_con_l, 0, 2, 1, 1)

        self.curPos_y_edit = QLineEdit(self.layoutWidget1)
        self.curPos_y_edit.setObjectName(u"curPos_y_edit")

        self.gridLayout_2.addWidget(self.curPos_y_edit, 3, 1, 1, 1)

        self.label_8 = QLabel(self.layoutWidget1)
        self.label_8.setObjectName(u"label_8")

        self.gridLayout_2.addWidget(self.label_8, 1, 0, 1, 1)

        self.pos_y_l = QLabel(self.layoutWidget1)
        self.pos_y_l.setObjectName(u"pos_y_l")

        self.gridLayout_2.addWidget(self.pos_y_l, 1, 1, 1, 1)


        self.horizontalLayout.addWidget(self.frame_2)

        self.frame_3 = QFrame(self.horizontalLayoutWidget)
        self.frame_3.setObjectName(u"frame_3")
        self.frame_3.setMaximumSize(QSize(281, 177))
        self.frame_3.setFrameShape(QFrame.Box)
        self.frame_3.setFrameShadow(QFrame.Plain)
        self.layoutWidget2 = QWidget(self.frame_3)
        self.layoutWidget2.setObjectName(u"layoutWidget2")
        self.layoutWidget2.setGeometry(QRect(10, 10, 262, 157))
        self.gridLayout_3 = QGridLayout(self.layoutWidget2)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.gridLayout_3.setContentsMargins(1, 1, 0, 0)
        self.z_con_l = QLabel(self.layoutWidget2)
        self.z_con_l.setObjectName(u"z_con_l")
        self.z_con_l.setFont(font)

        self.gridLayout_3.addWidget(self.z_con_l, 0, 2, 1, 1)

        self.label_5 = QLabel(self.layoutWidget2)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setFont(font2)
        self.label_5.setAlignment(Qt.AlignCenter)

        self.gridLayout_3.addWidget(self.label_5, 0, 1, 1, 1)

        self.step_z_edit = QLineEdit(self.layoutWidget2)
        self.step_z_edit.setObjectName(u"step_z_edit")

        self.gridLayout_3.addWidget(self.step_z_edit, 2, 1, 1, 1)

        self.go_z_btn = QPushButton(self.layoutWidget2)
        self.go_z_btn.setObjectName(u"go_z_btn")
        self.go_z_btn.setMinimumSize(QSize(83, 0))
        self.go_z_btn.setFont(font4)

        self.gridLayout_3.addWidget(self.go_z_btn, 4, 1, 1, 1)

        self.left_z_btn = QPushButton(self.layoutWidget2)
        self.left_z_btn.setObjectName(u"left_z_btn")
        self.left_z_btn.setMaximumSize(QSize(16777215, 24))
        self.left_z_btn.setFont(font3)

        self.gridLayout_3.addWidget(self.left_z_btn, 3, 0, 1, 1)

        self.label_6 = QLabel(self.layoutWidget2)
        self.label_6.setObjectName(u"label_6")

        self.gridLayout_3.addWidget(self.label_6, 2, 0, 1, 1)

        self.ref_z_btn = QPushButton(self.layoutWidget2)
        self.ref_z_btn.setObjectName(u"ref_z_btn")

        self.gridLayout_3.addWidget(self.ref_z_btn, 0, 0, 1, 1)

        self.curPos_z_edit = QLineEdit(self.layoutWidget2)
        self.curPos_z_edit.setObjectName(u"curPos_z_edit")

        self.gridLayout_3.addWidget(self.curPos_z_edit, 3, 1, 1, 1)

        self.right_z_btn = QPushButton(self.layoutWidget2)
        self.right_z_btn.setObjectName(u"right_z_btn")
        self.right_z_btn.setMaximumSize(QSize(16777215, 24))
        self.right_z_btn.setFont(font1)

        self.gridLayout_3.addWidget(self.right_z_btn, 3, 2, 1, 1)

        self.label_9 = QLabel(self.layoutWidget2)
        self.label_9.setObjectName(u"label_9")

        self.gridLayout_3.addWidget(self.label_9, 1, 0, 1, 1)

        self.pos_z_l = QLabel(self.layoutWidget2)
        self.pos_z_l.setObjectName(u"pos_z_l")

        self.gridLayout_3.addWidget(self.pos_z_l, 1, 1, 1, 1)


        self.horizontalLayout.addWidget(self.frame_3)

        self.horizontalLayoutWidget_2 = QWidget(StageForm)
        self.horizontalLayoutWidget_2.setObjectName(u"horizontalLayoutWidget_2")
        self.horizontalLayoutWidget_2.setGeometry(QRect(10, 220, 931, 201))
        self.horizontalLayout_3 = QHBoxLayout(self.horizontalLayoutWidget_2)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.frame_7 = QFrame(self.horizontalLayoutWidget_2)
        self.frame_7.setObjectName(u"frame_7")
        sizePolicy.setHeightForWidth(self.frame_7.sizePolicy().hasHeightForWidth())
        self.frame_7.setSizePolicy(sizePolicy)
        self.frame_7.setMaximumSize(QSize(281, 177))
        self.frame_7.setFrameShape(QFrame.Box)
        self.frame_7.setFrameShadow(QFrame.Plain)
        self.layoutWidget_5 = QWidget(self.frame_7)
        self.layoutWidget_5.setObjectName(u"layoutWidget_5")
        self.layoutWidget_5.setGeometry(QRect(10, 10, 262, 163))
        self.gridLayout_7 = QGridLayout(self.layoutWidget_5)
        self.gridLayout_7.setObjectName(u"gridLayout_7")
        self.gridLayout_7.setContentsMargins(0, 0, 0, 0)
        self.r1_con_l = QLabel(self.layoutWidget_5)
        self.r1_con_l.setObjectName(u"r1_con_l")
        self.r1_con_l.setFont(font)

        self.gridLayout_7.addWidget(self.r1_con_l, 0, 2, 1, 1)

        self.step_r1_edit = QLineEdit(self.layoutWidget_5)
        self.step_r1_edit.setObjectName(u"step_r1_edit")

        self.gridLayout_7.addWidget(self.step_r1_edit, 2, 1, 1, 1)

        self.right_r1_btn = QPushButton(self.layoutWidget_5)
        self.right_r1_btn.setObjectName(u"right_r1_btn")
        self.right_r1_btn.setMaximumSize(QSize(16777215, 24))
        self.right_r1_btn.setFont(font1)

        self.gridLayout_7.addWidget(self.right_r1_btn, 3, 2, 1, 1)

        self.label_19 = QLabel(self.layoutWidget_5)
        self.label_19.setObjectName(u"label_19")

        self.gridLayout_7.addWidget(self.label_19, 2, 0, 1, 1)

        self.label_20 = QLabel(self.layoutWidget_5)
        self.label_20.setObjectName(u"label_20")
        self.label_20.setMinimumSize(QSize(0, 49))
        self.label_20.setFont(font2)
        self.label_20.setAlignment(Qt.AlignCenter)

        self.gridLayout_7.addWidget(self.label_20, 0, 1, 1, 1)

        self.left_r1_btn = QPushButton(self.layoutWidget_5)
        self.left_r1_btn.setObjectName(u"left_r1_btn")
        self.left_r1_btn.setMaximumSize(QSize(16777215, 24))
        self.left_r1_btn.setFont(font3)

        self.gridLayout_7.addWidget(self.left_r1_btn, 3, 0, 1, 1)

        self.curPos_r1_edit = QLineEdit(self.layoutWidget_5)
        self.curPos_r1_edit.setObjectName(u"curPos_r1_edit")

        self.gridLayout_7.addWidget(self.curPos_r1_edit, 3, 1, 1, 1)

        self.go_r1_btn = QPushButton(self.layoutWidget_5)
        self.go_r1_btn.setObjectName(u"go_r1_btn")
        self.go_r1_btn.setMinimumSize(QSize(0, 24))
        self.go_r1_btn.setFont(font4)

        self.gridLayout_7.addWidget(self.go_r1_btn, 4, 1, 1, 1)

        self.ref_r1_btn = QPushButton(self.layoutWidget_5)
        self.ref_r1_btn.setObjectName(u"ref_r1_btn")

        self.gridLayout_7.addWidget(self.ref_r1_btn, 0, 0, 1, 1)

        self.label_21 = QLabel(self.layoutWidget_5)
        self.label_21.setObjectName(u"label_21")

        self.gridLayout_7.addWidget(self.label_21, 1, 0, 1, 1)

        self.pos_r1_l = QLabel(self.layoutWidget_5)
        self.pos_r1_l.setObjectName(u"pos_r1_l")

        self.gridLayout_7.addWidget(self.pos_r1_l, 1, 1, 1, 1)


        self.horizontalLayout_3.addWidget(self.frame_7)

        self.frame_9 = QFrame(self.horizontalLayoutWidget_2)
        self.frame_9.setObjectName(u"frame_9")
        self.frame_9.setMaximumSize(QSize(281, 177))
        self.frame_9.setFrameShape(QFrame.Box)
        self.frame_9.setFrameShadow(QFrame.Plain)
        self.layoutWidget_7 = QWidget(self.frame_9)
        self.layoutWidget_7.setObjectName(u"layoutWidget_7")
        self.layoutWidget_7.setGeometry(QRect(10, 10, 262, 157))
        self.gridLayout_9 = QGridLayout(self.layoutWidget_7)
        self.gridLayout_9.setObjectName(u"gridLayout_9")
        self.gridLayout_9.setContentsMargins(1, 1, 0, 0)
        self.r2_con_l = QLabel(self.layoutWidget_7)
        self.r2_con_l.setObjectName(u"r2_con_l")
        self.r2_con_l.setFont(font)

        self.gridLayout_9.addWidget(self.r2_con_l, 0, 2, 1, 1)

        self.label_25 = QLabel(self.layoutWidget_7)
        self.label_25.setObjectName(u"label_25")
        self.label_25.setFont(font2)
        self.label_25.setAlignment(Qt.AlignCenter)

        self.gridLayout_9.addWidget(self.label_25, 0, 1, 1, 1)

        self.step_r2_edit = QLineEdit(self.layoutWidget_7)
        self.step_r2_edit.setObjectName(u"step_r2_edit")

        self.gridLayout_9.addWidget(self.step_r2_edit, 2, 1, 1, 1)

        self.go_r2_btn = QPushButton(self.layoutWidget_7)
        self.go_r2_btn.setObjectName(u"go_r2_btn")
        self.go_r2_btn.setMinimumSize(QSize(83, 0))
        self.go_r2_btn.setFont(font4)

        self.gridLayout_9.addWidget(self.go_r2_btn, 4, 1, 1, 1)

        self.left_r2_btn = QPushButton(self.layoutWidget_7)
        self.left_r2_btn.setObjectName(u"left_r2_btn")
        self.left_r2_btn.setMaximumSize(QSize(16777215, 24))
        self.left_r2_btn.setFont(font3)

        self.gridLayout_9.addWidget(self.left_r2_btn, 3, 0, 1, 1)

        self.label_26 = QLabel(self.layoutWidget_7)
        self.label_26.setObjectName(u"label_26")

        self.gridLayout_9.addWidget(self.label_26, 2, 0, 1, 1)

        self.ref_r2_btn = QPushButton(self.layoutWidget_7)
        self.ref_r2_btn.setObjectName(u"ref_r2_btn")

        self.gridLayout_9.addWidget(self.ref_r2_btn, 0, 0, 1, 1)

        self.curPos_r2_edit = QLineEdit(self.layoutWidget_7)
        self.curPos_r2_edit.setObjectName(u"curPos_r2_edit")

        self.gridLayout_9.addWidget(self.curPos_r2_edit, 3, 1, 1, 1)

        self.right_r2_btn = QPushButton(self.layoutWidget_7)
        self.right_r2_btn.setObjectName(u"right_r2_btn")
        self.right_r2_btn.setMaximumSize(QSize(16777215, 24))
        self.right_r2_btn.setFont(font1)

        self.gridLayout_9.addWidget(self.right_r2_btn, 3, 2, 1, 1)

        self.label_27 = QLabel(self.layoutWidget_7)
        self.label_27.setObjectName(u"label_27")

        self.gridLayout_9.addWidget(self.label_27, 1, 0, 1, 1)

        self.pos_r2_l = QLabel(self.layoutWidget_7)
        self.pos_r2_l.setObjectName(u"pos_r2_l")

        self.gridLayout_9.addWidget(self.pos_r2_l, 1, 1, 1, 1)


        self.horizontalLayout_3.addWidget(self.frame_9)


        self.retranslateUi(StageForm)

        QMetaObject.connectSlotsByName(StageForm)
    # setupUi

    def retranslateUi(self, StageForm):
        StageForm.setWindowTitle(QCoreApplication.translate("StageForm", u"Form", None))
        self.x_con_l.setText(QCoreApplication.translate("StageForm", u"Disconnected", None))
        self.right_x_btn.setText(QCoreApplication.translate("StageForm", u"--->", None))
        self.label_2.setText(QCoreApplication.translate("StageForm", u"Step size:", None))
        self.label.setText(QCoreApplication.translate("StageForm", u"X", None))
        self.left_x_btn.setText(QCoreApplication.translate("StageForm", u"<---", None))
        self.go_x_btn.setText(QCoreApplication.translate("StageForm", u"Go", None))
        self.ref_x_btn.setText(QCoreApplication.translate("StageForm", u"Reference", None))
        self.label_3.setText(QCoreApplication.translate("StageForm", u"Actual Pos:", None))
        self.pos_x_l.setText(QCoreApplication.translate("StageForm", u"TextLabel", None))
        self.label_4.setText(QCoreApplication.translate("StageForm", u"Step size:", None))
        self.right_y_btn.setText(QCoreApplication.translate("StageForm", u"--->", None))
        self.label_7.setText(QCoreApplication.translate("StageForm", u"Y", None))
        self.ref_y_btn.setText(QCoreApplication.translate("StageForm", u"Reference", None))
        self.go_y_btn.setText(QCoreApplication.translate("StageForm", u"Go", None))
        self.left_y_btn.setText(QCoreApplication.translate("StageForm", u"<---", None))
        self.y_con_l.setText(QCoreApplication.translate("StageForm", u"Disconnected", None))
        self.label_8.setText(QCoreApplication.translate("StageForm", u"Actual Pos:", None))
        self.pos_y_l.setText(QCoreApplication.translate("StageForm", u"TextLabel", None))
        self.z_con_l.setText(QCoreApplication.translate("StageForm", u"Disconnected", None))
        self.label_5.setText(QCoreApplication.translate("StageForm", u"Z", None))
        self.go_z_btn.setText(QCoreApplication.translate("StageForm", u"Go", None))
        self.left_z_btn.setText(QCoreApplication.translate("StageForm", u"<---", None))
        self.label_6.setText(QCoreApplication.translate("StageForm", u"Step size:", None))
        self.ref_z_btn.setText(QCoreApplication.translate("StageForm", u"Reference", None))
        self.right_z_btn.setText(QCoreApplication.translate("StageForm", u"--->", None))
        self.label_9.setText(QCoreApplication.translate("StageForm", u"Actual Pos:", None))
        self.pos_z_l.setText(QCoreApplication.translate("StageForm", u"TextLabel", None))
        self.r1_con_l.setText(QCoreApplication.translate("StageForm", u"Disconnected", None))
        self.right_r1_btn.setText(QCoreApplication.translate("StageForm", u"--->", None))
        self.label_19.setText(QCoreApplication.translate("StageForm", u"Step size:", None))
        self.label_20.setText(QCoreApplication.translate("StageForm", u"R1", None))
        self.left_r1_btn.setText(QCoreApplication.translate("StageForm", u"<---", None))
        self.go_r1_btn.setText(QCoreApplication.translate("StageForm", u"Go", None))
        self.ref_r1_btn.setText(QCoreApplication.translate("StageForm", u"Reference", None))
        self.label_21.setText(QCoreApplication.translate("StageForm", u"Actual Pos:", None))
        self.pos_r1_l.setText(QCoreApplication.translate("StageForm", u"TextLabel", None))
        self.r2_con_l.setText(QCoreApplication.translate("StageForm", u"Disconnected", None))
        self.label_25.setText(QCoreApplication.translate("StageForm", u"R2", None))
        self.go_r2_btn.setText(QCoreApplication.translate("StageForm", u"Go", None))
        self.left_r2_btn.setText(QCoreApplication.translate("StageForm", u"<---", None))
        self.label_26.setText(QCoreApplication.translate("StageForm", u"Step size:", None))
        self.ref_r2_btn.setText(QCoreApplication.translate("StageForm", u"Reference", None))
        self.right_r2_btn.setText(QCoreApplication.translate("StageForm", u"--->", None))
        self.label_27.setText(QCoreApplication.translate("StageForm", u"Actual Pos:", None))
        self.pos_r2_l.setText(QCoreApplication.translate("StageForm", u"TextLabel", None))
    # retranslateUi

