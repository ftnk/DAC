# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'camPopup.ui'
##
## Created by: Qt User Interface Compiler version 6.4.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QGridLayout, QLabel,
    QLineEdit, QMainWindow, QSizePolicy, QSlider,
    QSpacerItem, QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(270, 101)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy1)
        self.cam_settings_frame = QFrame(self.centralwidget)
        self.cam_settings_frame.setObjectName(u"cam_settings_frame")
        self.cam_settings_frame.setGeometry(QRect(0, 0, 270, 101))
        self.cam_settings_frame.setMinimumSize(QSize(270, 0))
        self.cam_settings_frame.setMaximumSize(QSize(246, 16777215))
        self.cam_settings_frame.setFrameShape(QFrame.Box)
        self.cam_settings_frame.setFrameShadow(QFrame.Plain)
        self.gridLayoutWidget_2 = QWidget(self.cam_settings_frame)
        self.gridLayoutWidget_2.setObjectName(u"gridLayoutWidget_2")
        self.gridLayoutWidget_2.setGeometry(QRect(5, 5, 261, 91))
        self.gridLayout_7 = QGridLayout(self.gridLayoutWidget_2)
        self.gridLayout_7.setObjectName(u"gridLayout_7")
        self.gridLayout_7.setContentsMargins(0, 0, 0, 0)
        self.exposure_cur_edit = QLineEdit(self.gridLayoutWidget_2)
        self.exposure_cur_edit.setObjectName(u"exposure_cur_edit")
        self.exposure_cur_edit.setMaximumSize(QSize(25, 16777215))

        self.gridLayout_7.addWidget(self.exposure_cur_edit, 0, 1, 1, 1)

        self.gain_slider = QSlider(self.gridLayoutWidget_2)
        self.gain_slider.setObjectName(u"gain_slider")
        self.gain_slider.setMaximum(10)
        self.gain_slider.setValue(1)
        self.gain_slider.setOrientation(Qt.Horizontal)

        self.gridLayout_7.addWidget(self.gain_slider, 3, 0, 1, 6)

        self.gain_from_edit = QLineEdit(self.gridLayoutWidget_2)
        self.gain_from_edit.setObjectName(u"gain_from_edit")
        self.gain_from_edit.setMaximumSize(QSize(30, 16777215))

        self.gridLayout_7.addWidget(self.gain_from_edit, 2, 3, 1, 1)

        self.label_9 = QLabel(self.gridLayoutWidget_2)
        self.label_9.setObjectName(u"label_9")

        self.gridLayout_7.addWidget(self.label_9, 2, 4, 1, 1)

        self.exposure_slider = QSlider(self.gridLayoutWidget_2)
        self.exposure_slider.setObjectName(u"exposure_slider")
        self.exposure_slider.setMaximum(100)
        self.exposure_slider.setValue(10)
        self.exposure_slider.setOrientation(Qt.Horizontal)

        self.gridLayout_7.addWidget(self.exposure_slider, 1, 0, 1, 6)

        self.exposure_from_edit = QLineEdit(self.gridLayoutWidget_2)
        self.exposure_from_edit.setObjectName(u"exposure_from_edit")
        self.exposure_from_edit.setMaximumSize(QSize(30, 16777215))

        self.gridLayout_7.addWidget(self.exposure_from_edit, 0, 3, 1, 1)

        self.gain_to_edit = QLineEdit(self.gridLayoutWidget_2)
        self.gain_to_edit.setObjectName(u"gain_to_edit")
        self.gain_to_edit.setMaximumSize(QSize(30, 16777215))

        self.gridLayout_7.addWidget(self.gain_to_edit, 2, 5, 1, 1)

        self.exposure_l = QLabel(self.gridLayoutWidget_2)
        self.exposure_l.setObjectName(u"exposure_l")

        self.gridLayout_7.addWidget(self.exposure_l, 0, 0, 1, 1)

        self.label_7 = QLabel(self.gridLayoutWidget_2)
        self.label_7.setObjectName(u"label_7")

        self.gridLayout_7.addWidget(self.label_7, 0, 4, 1, 1)

        self.gain_l = QLabel(self.gridLayoutWidget_2)
        self.gain_l.setObjectName(u"gain_l")

        self.gridLayout_7.addWidget(self.gain_l, 2, 0, 1, 1)

        self.gain_cur_edit = QLineEdit(self.gridLayoutWidget_2)
        self.gain_cur_edit.setObjectName(u"gain_cur_edit")
        self.gain_cur_edit.setMaximumSize(QSize(25, 16777215))

        self.gridLayout_7.addWidget(self.gain_cur_edit, 2, 1, 1, 1)

        self.exposure_to_edit = QLineEdit(self.gridLayoutWidget_2)
        self.exposure_to_edit.setObjectName(u"exposure_to_edit")
        self.exposure_to_edit.setMaximumSize(QSize(30, 16777215))

        self.gridLayout_7.addWidget(self.exposure_to_edit, 0, 5, 1, 1)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_7.addItem(self.horizontalSpacer, 0, 2, 1, 1)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_7.addItem(self.horizontalSpacer_2, 2, 2, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.exposure_cur_edit.setText(QCoreApplication.translate("MainWindow", u"10", None))
        self.gain_from_edit.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.label_9.setText(QCoreApplication.translate("MainWindow", u"to", None))
        self.exposure_from_edit.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.gain_to_edit.setText(QCoreApplication.translate("MainWindow", u"10", None))
        self.exposure_l.setText(QCoreApplication.translate("MainWindow", u"Exposure (ms):", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"to", None))
        self.gain_l.setText(QCoreApplication.translate("MainWindow", u"Gain:", None))
        self.gain_cur_edit.setText(QCoreApplication.translate("MainWindow", u"1", None))
        self.exposure_to_edit.setText(QCoreApplication.translate("MainWindow", u"100", None))
    # retranslateUi

