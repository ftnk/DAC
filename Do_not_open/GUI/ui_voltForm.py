# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'voltForm.ui'
##
## Created by: Qt User Interface Compiler version 6.4.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QCheckBox, QFrame, QGridLayout,
    QHBoxLayout, QLabel, QLayout, QLineEdit,
    QMainWindow, QProgressBar, QPushButton, QSizePolicy,
    QSlider, QSpacerItem, QSpinBox, QVBoxLayout,
    QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(962, 650)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy1)
        self.saved_l = QLabel(self.centralwidget)
        self.saved_l.setObjectName(u"saved_l")
        self.saved_l.setGeometry(QRect(80, 580, 116, 21))
        font = QFont()
        font.setPointSize(8)
        self.saved_l.setFont(font)
        self.saved_l.setStyleSheet(u"color: green;")
        self.saved_l.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.progress_l = QLabel(self.centralwidget)
        self.progress_l.setObjectName(u"progress_l")
        self.progress_l.setGeometry(QRect(625, 570, 330, 16))
        self.progress_l.setAlignment(Qt.AlignCenter)
        self.dummy_PS_c = QCheckBox(self.centralwidget)
        self.dummy_PS_c.setObjectName(u"dummy_PS_c")
        self.dummy_PS_c.setGeometry(QRect(195, 620, 141, 20))
        self.progressBar = QProgressBar(self.centralwidget)
        self.progressBar.setObjectName(u"progressBar")
        self.progressBar.setGeometry(QRect(625, 590, 330, 20))
        self.progressBar.setValue(0)
        self.stop_exp_btn = QPushButton(self.centralwidget)
        self.stop_exp_btn.setObjectName(u"stop_exp_btn")
        self.stop_exp_btn.setGeometry(QRect(345, 620, 120, 24))
        self.err_l = QLabel(self.centralwidget)
        self.err_l.setObjectName(u"err_l")
        self.err_l.setGeometry(QRect(415, 85, 316, 61))
        font1 = QFont()
        font1.setPointSize(12)
        self.err_l.setFont(font1)
        self.err_l.setStyleSheet(u"color : red;")
        self.err_l.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)
        self.err_l.setWordWrap(True)
        self.load_btn = QPushButton(self.centralwidget)
        self.load_btn.setObjectName(u"load_btn")
        self.load_btn.setGeometry(QRect(520, 574, 90, 32))
        self.run_btn = QPushButton(self.centralwidget)
        self.run_btn.setObjectName(u"run_btn")
        self.run_btn.setGeometry(QRect(305, 570, 200, 40))
        font2 = QFont()
        font2.setPointSize(16)
        font2.setBold(True)
        self.run_btn.setFont(font2)
        self.run_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.save_btn = QPushButton(self.centralwidget)
        self.save_btn.setObjectName(u"save_btn")
        self.save_btn.setGeometry(QRect(200, 574, 90, 32))
        self.frame_2 = QFrame(self.centralwidget)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setGeometry(QRect(5, 185, 651, 381))
        self.frame_2.setFrameShape(QFrame.Box)
        self.frame_2.setFrameShadow(QFrame.Plain)
        self.verticalLayoutWidget_3 = QWidget(self.frame_2)
        self.verticalLayoutWidget_3.setObjectName(u"verticalLayoutWidget_3")
        self.verticalLayoutWidget_3.setEnabled(True)
        self.verticalLayoutWidget_3.setGeometry(QRect(5, 5, 639, 370))
        self.verticalLayout_7 = QVBoxLayout(self.verticalLayoutWidget_3)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.s3_start_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.s3_start_edit.setObjectName(u"s3_start_edit")

        self.gridLayout_2.addWidget(self.s3_start_edit, 1, 5, 1, 1)

        self.s1_stop_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.s1_stop_edit.setObjectName(u"s1_stop_edit")

        self.gridLayout_2.addWidget(self.s1_stop_edit, 2, 3, 1, 1)

        self.s1_spin = QSpinBox(self.verticalLayoutWidget_3)
        self.s1_spin.setObjectName(u"s1_spin")

        self.gridLayout_2.addWidget(self.s1_spin, 5, 3, 1, 1)

        self.s2_start_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.s2_start_edit.setObjectName(u"s2_start_edit")

        self.gridLayout_2.addWidget(self.s2_start_edit, 1, 4, 1, 1)

        self.s3_stop_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.s3_stop_edit.setObjectName(u"s3_stop_edit")

        self.gridLayout_2.addWidget(self.s3_stop_edit, 2, 5, 1, 1)

        self.label_15 = QLabel(self.verticalLayoutWidget_3)
        self.label_15.setObjectName(u"label_15")

        self.gridLayout_2.addWidget(self.label_15, 1, 0, 1, 1)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_2.addItem(self.horizontalSpacer, 1, 6, 1, 1)

        self.s3_spin = QSpinBox(self.verticalLayoutWidget_3)
        self.s3_spin.setObjectName(u"s3_spin")

        self.gridLayout_2.addWidget(self.s3_spin, 5, 5, 1, 1)

        self.s1_start_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.s1_start_edit.setObjectName(u"s1_start_edit")

        self.gridLayout_2.addWidget(self.s1_start_edit, 1, 3, 1, 1)

        self.s0_spin = QSpinBox(self.verticalLayoutWidget_3)
        self.s0_spin.setObjectName(u"s0_spin")

        self.gridLayout_2.addWidget(self.s0_spin, 5, 2, 1, 1)

        self.verticalLayout_3 = QVBoxLayout()
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.r2_l = QLabel(self.verticalLayoutWidget_3)
        self.r2_l.setObjectName(u"r2_l")
        self.r2_l.setMaximumSize(QSize(16777215, 45))
        font3 = QFont()
        font3.setPointSize(24)
        font3.setBold(True)
        self.r2_l.setFont(font3)
        self.r2_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_3.addWidget(self.r2_l)

        self.r2_stat_c = QCheckBox(self.verticalLayoutWidget_3)
        self.r2_stat_c.setObjectName(u"r2_stat_c")

        self.verticalLayout_3.addWidget(self.r2_stat_c)

        self.r2_reverse_c = QCheckBox(self.verticalLayoutWidget_3)
        self.r2_reverse_c.setObjectName(u"r2_reverse_c")

        self.verticalLayout_3.addWidget(self.r2_reverse_c)


        self.gridLayout_2.addLayout(self.verticalLayout_3, 0, 1, 1, 1)

        self.verticalLayout_9 = QVBoxLayout()
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.s3_l = QLabel(self.verticalLayoutWidget_3)
        self.s3_l.setObjectName(u"s3_l")
        self.s3_l.setFont(font3)
        self.s3_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_9.addWidget(self.s3_l)

        self.s3_stat_c = QCheckBox(self.verticalLayoutWidget_3)
        self.s3_stat_c.setObjectName(u"s3_stat_c")

        self.verticalLayout_9.addWidget(self.s3_stat_c)

        self.s3_reverse_c = QCheckBox(self.verticalLayoutWidget_3)
        self.s3_reverse_c.setObjectName(u"s3_reverse_c")

        self.verticalLayout_9.addWidget(self.s3_reverse_c)


        self.gridLayout_2.addLayout(self.verticalLayout_9, 0, 5, 1, 1)

        self.verticalLayout_6 = QVBoxLayout()
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.s1_l = QLabel(self.verticalLayoutWidget_3)
        self.s1_l.setObjectName(u"s1_l")
        self.s1_l.setFont(font3)
        self.s1_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_6.addWidget(self.s1_l)

        self.s1_stat_c = QCheckBox(self.verticalLayoutWidget_3)
        self.s1_stat_c.setObjectName(u"s1_stat_c")

        self.verticalLayout_6.addWidget(self.s1_stat_c)

        self.s1_reverse_c = QCheckBox(self.verticalLayoutWidget_3)
        self.s1_reverse_c.setObjectName(u"s1_reverse_c")

        self.verticalLayout_6.addWidget(self.s1_reverse_c)


        self.gridLayout_2.addLayout(self.verticalLayout_6, 0, 3, 1, 1)

        self.label_23 = QLabel(self.verticalLayoutWidget_3)
        self.label_23.setObjectName(u"label_23")

        self.gridLayout_2.addWidget(self.label_23, 5, 0, 1, 1)

        self.s0_step_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.s0_step_edit.setObjectName(u"s0_step_edit")

        self.gridLayout_2.addWidget(self.s0_step_edit, 3, 2, 1, 1)

        self.r2_start_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.r2_start_edit.setObjectName(u"r2_start_edit")

        self.gridLayout_2.addWidget(self.r2_start_edit, 1, 1, 1, 1)

        self.s2_spin = QSpinBox(self.verticalLayoutWidget_3)
        self.s2_spin.setObjectName(u"s2_spin")

        self.gridLayout_2.addWidget(self.s2_spin, 5, 4, 1, 1)

        self.label_16 = QLabel(self.verticalLayoutWidget_3)
        self.label_16.setObjectName(u"label_16")

        self.gridLayout_2.addWidget(self.label_16, 2, 0, 1, 1)

        self.s0_stop_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.s0_stop_edit.setObjectName(u"s0_stop_edit")

        self.gridLayout_2.addWidget(self.s0_stop_edit, 2, 2, 1, 1)

        self.verticalLayout_5 = QVBoxLayout()
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.s0_l = QLabel(self.verticalLayoutWidget_3)
        self.s0_l.setObjectName(u"s0_l")
        self.s0_l.setFont(font3)
        self.s0_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_5.addWidget(self.s0_l)

        self.s0_stat_c = QCheckBox(self.verticalLayoutWidget_3)
        self.s0_stat_c.setObjectName(u"s0_stat_c")

        self.verticalLayout_5.addWidget(self.s0_stat_c)

        self.s0_reverse_c = QCheckBox(self.verticalLayoutWidget_3)
        self.s0_reverse_c.setObjectName(u"s0_reverse_c")

        self.verticalLayout_5.addWidget(self.s0_reverse_c)


        self.gridLayout_2.addLayout(self.verticalLayout_5, 0, 2, 1, 1)

        self.s0_start_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.s0_start_edit.setObjectName(u"s0_start_edit")

        self.gridLayout_2.addWidget(self.s0_start_edit, 1, 2, 1, 1)

        self.r2_spin = QSpinBox(self.verticalLayoutWidget_3)
        self.r2_spin.setObjectName(u"r2_spin")
        self.r2_spin.setValue(0)

        self.gridLayout_2.addWidget(self.r2_spin, 5, 1, 1, 1)

        self.verticalLayout_8 = QVBoxLayout()
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.s2_l = QLabel(self.verticalLayoutWidget_3)
        self.s2_l.setObjectName(u"s2_l")
        self.s2_l.setFont(font3)
        self.s2_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_8.addWidget(self.s2_l)

        self.s2_stat_c = QCheckBox(self.verticalLayoutWidget_3)
        self.s2_stat_c.setObjectName(u"s2_stat_c")

        self.verticalLayout_8.addWidget(self.s2_stat_c)

        self.s2_reverse_c = QCheckBox(self.verticalLayoutWidget_3)
        self.s2_reverse_c.setObjectName(u"s2_reverse_c")

        self.verticalLayout_8.addWidget(self.s2_reverse_c)


        self.gridLayout_2.addLayout(self.verticalLayout_8, 0, 4, 1, 1)

        self.s1_step_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.s1_step_edit.setObjectName(u"s1_step_edit")

        self.gridLayout_2.addWidget(self.s1_step_edit, 3, 3, 1, 1)

        self.s2_stop_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.s2_stop_edit.setObjectName(u"s2_stop_edit")

        self.gridLayout_2.addWidget(self.s2_stop_edit, 2, 4, 1, 1)

        self.s3_step_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.s3_step_edit.setObjectName(u"s3_step_edit")

        self.gridLayout_2.addWidget(self.s3_step_edit, 3, 5, 1, 1)

        self.r2_step_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.r2_step_edit.setObjectName(u"r2_step_edit")

        self.gridLayout_2.addWidget(self.r2_step_edit, 3, 1, 1, 1)

        self.label_17 = QLabel(self.verticalLayoutWidget_3)
        self.label_17.setObjectName(u"label_17")

        self.gridLayout_2.addWidget(self.label_17, 3, 0, 1, 1)

        self.r2_stop_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.r2_stop_edit.setObjectName(u"r2_stop_edit")

        self.gridLayout_2.addWidget(self.r2_stop_edit, 2, 1, 1, 1)

        self.s2_step_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.s2_step_edit.setObjectName(u"s2_step_edit")

        self.gridLayout_2.addWidget(self.s2_step_edit, 3, 4, 1, 1)

        self.tp_l = QLabel(self.verticalLayoutWidget_3)
        self.tp_l.setObjectName(u"tp_l")

        self.gridLayout_2.addWidget(self.tp_l, 4, 0, 1, 1)

        self.r2_tp_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.r2_tp_edit.setObjectName(u"r2_tp_edit")

        self.gridLayout_2.addWidget(self.r2_tp_edit, 4, 1, 1, 1)

        self.s0_tp_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.s0_tp_edit.setObjectName(u"s0_tp_edit")

        self.gridLayout_2.addWidget(self.s0_tp_edit, 4, 2, 1, 1)

        self.s1_tp_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.s1_tp_edit.setObjectName(u"s1_tp_edit")

        self.gridLayout_2.addWidget(self.s1_tp_edit, 4, 3, 1, 1)

        self.s2_tp_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.s2_tp_edit.setObjectName(u"s2_tp_edit")

        self.gridLayout_2.addWidget(self.s2_tp_edit, 4, 4, 1, 1)

        self.s3_tp_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.s3_tp_edit.setObjectName(u"s3_tp_edit")

        self.gridLayout_2.addWidget(self.s3_tp_edit, 4, 5, 1, 1)


        self.verticalLayout_7.addLayout(self.gridLayout_2)

        self.label_51 = QLabel(self.verticalLayoutWidget_3)
        self.label_51.setObjectName(u"label_51")
        sizePolicy2 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.label_51.sizePolicy().hasHeightForWidth())
        self.label_51.setSizePolicy(sizePolicy2)

        self.verticalLayout_7.addWidget(self.label_51)

        self.gridLayout_3 = QGridLayout()
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.mf_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.mf_edit.setObjectName(u"mf_edit")
        self.mf_edit.setMaximumSize(QSize(100, 16777215))

        self.gridLayout_3.addWidget(self.mf_edit, 1, 1, 1, 1)

        self.tip_edit = QLineEdit(self.verticalLayoutWidget_3)
        self.tip_edit.setObjectName(u"tip_edit")
        self.tip_edit.setMaximumSize(QSize(100, 16777215))

        self.gridLayout_3.addWidget(self.tip_edit, 0, 1, 1, 1)

        self.mf_l = QLabel(self.verticalLayoutWidget_3)
        self.mf_l.setObjectName(u"mf_l")

        self.gridLayout_3.addWidget(self.mf_l, 1, 0, 1, 1)

        self.tip_l = QLabel(self.verticalLayoutWidget_3)
        self.tip_l.setObjectName(u"tip_l")

        self.gridLayout_3.addWidget(self.tip_l, 0, 0, 1, 1)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_3.addItem(self.horizontalSpacer_2, 0, 2, 1, 1)


        self.verticalLayout_7.addLayout(self.gridLayout_3)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setSizeConstraint(QLayout.SetMinimumSize)
        self.totalsteps_text_l = QLabel(self.verticalLayoutWidget_3)
        self.totalsteps_text_l.setObjectName(u"totalsteps_text_l")
        self.totalsteps_text_l.setMinimumSize(QSize(100, 0))
        self.totalsteps_text_l.setMaximumSize(QSize(300, 22))
        self.totalsteps_text_l.setFont(font1)

        self.horizontalLayout_3.addWidget(self.totalsteps_text_l)

        self.totalsteps_l = QLabel(self.verticalLayoutWidget_3)
        self.totalsteps_l.setObjectName(u"totalsteps_l")
        self.totalsteps_l.setMaximumSize(QSize(16777215, 22))
        self.totalsteps_l.setFont(font1)

        self.horizontalLayout_3.addWidget(self.totalsteps_l)

        self.horizontalSpacer_5 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_5)


        self.verticalLayout_7.addLayout(self.horizontalLayout_3)

        self.exp_time_l = QLabel(self.centralwidget)
        self.exp_time_l.setObjectName(u"exp_time_l")
        self.exp_time_l.setGeometry(QRect(625, 620, 330, 20))
        self.exp_time_l.setAlignment(Qt.AlignCenter)
        self.stage_r2_frame = QFrame(self.centralwidget)
        self.stage_r2_frame.setObjectName(u"stage_r2_frame")
        self.stage_r2_frame.setGeometry(QRect(660, 185, 291, 191))
        self.stage_r2_frame.setMinimumSize(QSize(0, 0))
        self.stage_r2_frame.setMaximumSize(QSize(16777215, 16777215))
        self.stage_r2_frame.setFrameShape(QFrame.Box)
        self.stage_r2_frame.setFrameShadow(QFrame.Plain)
        self.verticalLayoutWidget_4 = QWidget(self.stage_r2_frame)
        self.verticalLayoutWidget_4.setObjectName(u"verticalLayoutWidget_4")
        self.verticalLayoutWidget_4.setGeometry(QRect(5, 5, 281, 181))
        self.verticalLayout_4 = QVBoxLayout(self.verticalLayoutWidget_4)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.r_connect_btn = QPushButton(self.verticalLayoutWidget_4)
        self.r_connect_btn.setObjectName(u"r_connect_btn")
        self.r_connect_btn.setMaximumSize(QSize(564, 16777215))

        self.verticalLayout_4.addWidget(self.r_connect_btn)

        self.gridLayout_13 = QGridLayout()
        self.gridLayout_13.setObjectName(u"gridLayout_13")
        self.gridLayout_13.setContentsMargins(1, 1, -1, -1)
        self.r2_con_l = QLabel(self.verticalLayoutWidget_4)
        self.r2_con_l.setObjectName(u"r2_con_l")
        self.r2_con_l.setFont(font)

        self.gridLayout_13.addWidget(self.r2_con_l, 0, 2, 1, 1)

        self.label_92 = QLabel(self.verticalLayoutWidget_4)
        self.label_92.setObjectName(u"label_92")
        font4 = QFont()
        font4.setPointSize(12)
        font4.setBold(True)
        self.label_92.setFont(font4)
        self.label_92.setAlignment(Qt.AlignCenter)

        self.gridLayout_13.addWidget(self.label_92, 0, 1, 1, 1)

        self.step_r2_edit = QLineEdit(self.verticalLayoutWidget_4)
        self.step_r2_edit.setObjectName(u"step_r2_edit")

        self.gridLayout_13.addWidget(self.step_r2_edit, 2, 1, 1, 1)

        self.go_r2_btn = QPushButton(self.verticalLayoutWidget_4)
        self.go_r2_btn.setObjectName(u"go_r2_btn")
        self.go_r2_btn.setMinimumSize(QSize(0, 0))
        font5 = QFont()
        font5.setPointSize(9)
        font5.setBold(True)
        self.go_r2_btn.setFont(font5)

        self.gridLayout_13.addWidget(self.go_r2_btn, 4, 1, 1, 1)

        self.left_r2_btn = QPushButton(self.verticalLayoutWidget_4)
        self.left_r2_btn.setObjectName(u"left_r2_btn")
        self.left_r2_btn.setMaximumSize(QSize(16777215, 16777215))
        font6 = QFont()
        font6.setPointSize(11)
        font6.setBold(True)
        font6.setUnderline(False)
        font6.setStrikeOut(False)
        self.left_r2_btn.setFont(font6)

        self.gridLayout_13.addWidget(self.left_r2_btn, 3, 0, 1, 1)

        self.label_93 = QLabel(self.verticalLayoutWidget_4)
        self.label_93.setObjectName(u"label_93")

        self.gridLayout_13.addWidget(self.label_93, 2, 0, 1, 1)

        self.ref_r2_btn = QPushButton(self.verticalLayoutWidget_4)
        self.ref_r2_btn.setObjectName(u"ref_r2_btn")

        self.gridLayout_13.addWidget(self.ref_r2_btn, 0, 0, 1, 1)

        self.curPos_r2_edit = QLineEdit(self.verticalLayoutWidget_4)
        self.curPos_r2_edit.setObjectName(u"curPos_r2_edit")

        self.gridLayout_13.addWidget(self.curPos_r2_edit, 3, 1, 1, 1)

        self.right_r2_btn = QPushButton(self.verticalLayoutWidget_4)
        self.right_r2_btn.setObjectName(u"right_r2_btn")
        self.right_r2_btn.setMaximumSize(QSize(16777215, 16777215))
        font7 = QFont()
        font7.setPointSize(11)
        font7.setBold(True)
        self.right_r2_btn.setFont(font7)

        self.gridLayout_13.addWidget(self.right_r2_btn, 3, 2, 1, 1)

        self.label_94 = QLabel(self.verticalLayoutWidget_4)
        self.label_94.setObjectName(u"label_94")

        self.gridLayout_13.addWidget(self.label_94, 1, 0, 1, 1)

        self.pos_r2_l = QLabel(self.verticalLayoutWidget_4)
        self.pos_r2_l.setObjectName(u"pos_r2_l")
        self.pos_r2_l.setAlignment(Qt.AlignCenter)

        self.gridLayout_13.addWidget(self.pos_r2_l, 1, 1, 1, 1)


        self.verticalLayout_4.addLayout(self.gridLayout_13)

        self.frame_4 = QFrame(self.centralwidget)
        self.frame_4.setObjectName(u"frame_4")
        self.frame_4.setGeometry(QRect(660, 380, 291, 186))
        self.frame_4.setFrameShape(QFrame.Box)
        self.frame_4.setFrameShadow(QFrame.Plain)
        self.gridLayoutWidget = QWidget(self.frame_4)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(5, 5, 281, 176))
        self.gridLayout_4 = QGridLayout(self.gridLayoutWidget)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.gridLayout_4.setContentsMargins(0, 0, 0, 0)
        self.s3_volt_edit = QLineEdit(self.gridLayoutWidget)
        self.s3_volt_edit.setObjectName(u"s3_volt_edit")

        self.gridLayout_4.addWidget(self.s3_volt_edit, 5, 2, 1, 1)

        self.label_8 = QLabel(self.gridLayoutWidget)
        self.label_8.setObjectName(u"label_8")

        self.gridLayout_4.addWidget(self.label_8, 5, 0, 1, 1)

        self.s2_volt_edit = QLineEdit(self.gridLayoutWidget)
        self.s2_volt_edit.setObjectName(u"s2_volt_edit")

        self.gridLayout_4.addWidget(self.s2_volt_edit, 4, 2, 1, 1)

        self.s2_go_btn = QPushButton(self.gridLayoutWidget)
        self.s2_go_btn.setObjectName(u"s2_go_btn")
        self.s2_go_btn.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_4.addWidget(self.s2_go_btn, 4, 3, 1, 1)

        self.s0_volt_l = QLabel(self.gridLayoutWidget)
        self.s0_volt_l.setObjectName(u"s0_volt_l")
        self.s0_volt_l.setAlignment(Qt.AlignCenter)

        self.gridLayout_4.addWidget(self.s0_volt_l, 2, 1, 1, 1)

        self.s1_volt_edit = QLineEdit(self.gridLayoutWidget)
        self.s1_volt_edit.setObjectName(u"s1_volt_edit")

        self.gridLayout_4.addWidget(self.s1_volt_edit, 3, 2, 1, 1)

        self.label_3 = QLabel(self.gridLayoutWidget)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout_4.addWidget(self.label_3, 1, 1, 1, 1)

        self.s0_go_btn = QPushButton(self.gridLayoutWidget)
        self.s0_go_btn.setObjectName(u"s0_go_btn")
        self.s0_go_btn.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_4.addWidget(self.s0_go_btn, 2, 3, 1, 1)

        self.s1_volt_l = QLabel(self.gridLayoutWidget)
        self.s1_volt_l.setObjectName(u"s1_volt_l")
        self.s1_volt_l.setAlignment(Qt.AlignCenter)

        self.gridLayout_4.addWidget(self.s1_volt_l, 3, 1, 1, 1)

        self.label_2 = QLabel(self.gridLayoutWidget)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setMinimumSize(QSize(70, 0))
        self.label_2.setAlignment(Qt.AlignCenter)

        self.gridLayout_4.addWidget(self.label_2, 1, 4, 1, 1)

        self.s1_go_btn = QPushButton(self.gridLayoutWidget)
        self.s1_go_btn.setObjectName(u"s1_go_btn")
        self.s1_go_btn.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_4.addWidget(self.s1_go_btn, 3, 3, 1, 1)

        self.label_6 = QLabel(self.gridLayoutWidget)
        self.label_6.setObjectName(u"label_6")

        self.gridLayout_4.addWidget(self.label_6, 4, 0, 1, 1)

        self.s2_volt_l = QLabel(self.gridLayoutWidget)
        self.s2_volt_l.setObjectName(u"s2_volt_l")
        self.s2_volt_l.setAlignment(Qt.AlignCenter)

        self.gridLayout_4.addWidget(self.s2_volt_l, 4, 1, 1, 1)

        self.label = QLabel(self.gridLayoutWidget)
        self.label.setObjectName(u"label")

        self.gridLayout_4.addWidget(self.label, 2, 0, 1, 1)

        self.s3_volt_l = QLabel(self.gridLayoutWidget)
        self.s3_volt_l.setObjectName(u"s3_volt_l")
        self.s3_volt_l.setAlignment(Qt.AlignCenter)

        self.gridLayout_4.addWidget(self.s3_volt_l, 5, 1, 1, 1)

        self.s3_go_btn = QPushButton(self.gridLayoutWidget)
        self.s3_go_btn.setObjectName(u"s3_go_btn")
        self.s3_go_btn.setMaximumSize(QSize(16777215, 16777215))

        self.gridLayout_4.addWidget(self.s3_go_btn, 5, 3, 1, 1)

        self.label_4 = QLabel(self.gridLayoutWidget)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout_4.addWidget(self.label_4, 3, 0, 1, 1)

        self.s0_volt_edit = QLineEdit(self.gridLayoutWidget)
        self.s0_volt_edit.setObjectName(u"s0_volt_edit")

        self.gridLayout_4.addWidget(self.s0_volt_edit, 2, 2, 1, 1)

        self.s_connect_btn = QPushButton(self.gridLayoutWidget)
        self.s_connect_btn.setObjectName(u"s_connect_btn")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy3.setHorizontalStretch(3)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.s_connect_btn.sizePolicy().hasHeightForWidth())
        self.s_connect_btn.setSizePolicy(sizePolicy3)
        self.s_connect_btn.setMinimumSize(QSize(0, 24))

        self.gridLayout_4.addWidget(self.s_connect_btn, 0, 0, 1, 5)

        self.horizontalLayoutWidget = QWidget(self.centralwidget)
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(5, 10, 946, 171))
        self.horizontalLayout = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.frame_5 = QFrame(self.horizontalLayoutWidget)
        self.frame_5.setObjectName(u"frame_5")
        self.frame_5.setMinimumSize(QSize(136, 0))
        self.frame_5.setMaximumSize(QSize(136, 999999))
        self.frame_5.setFrameShape(QFrame.Box)
        self.frame_5.setFrameShadow(QFrame.Plain)
        self.layoutWidget = QWidget(self.frame_5)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(5, 5, 132, 161))
        self.gridLayout = QGridLayout(self.layoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.s3_c = QCheckBox(self.layoutWidget)
        self.s3_c.setObjectName(u"s3_c")
        self.s3_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.s3_c, 4, 2, 1, 1)

        self.r2_c = QCheckBox(self.layoutWidget)
        self.r2_c.setObjectName(u"r2_c")
        self.r2_c.setStyleSheet(u"border-width: 0;")
        self.r2_c.setChecked(False)

        self.gridLayout.addWidget(self.r2_c, 0, 2, 1, 1)

        self.s1_c = QCheckBox(self.layoutWidget)
        self.s1_c.setObjectName(u"s1_c")
        self.s1_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.s1_c, 2, 2, 1, 1)

        self.s2_c = QCheckBox(self.layoutWidget)
        self.s2_c.setObjectName(u"s2_c")
        self.s2_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.s2_c, 3, 2, 1, 1)

        self.home_r2_c = QCheckBox(self.layoutWidget)
        self.home_r2_c.setObjectName(u"home_r2_c")

        self.gridLayout.addWidget(self.home_r2_c, 0, 3, 1, 1)

        self.s0_c = QCheckBox(self.layoutWidget)
        self.s0_c.setObjectName(u"s0_c")
        self.s0_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.s0_c, 1, 2, 1, 1)

        self.cam_c = QCheckBox(self.layoutWidget)
        self.cam_c.setObjectName(u"cam_c")

        self.gridLayout.addWidget(self.cam_c, 5, 2, 1, 1)


        self.horizontalLayout.addWidget(self.frame_5)

        self.cam_settings_frame = QFrame(self.horizontalLayoutWidget)
        self.cam_settings_frame.setObjectName(u"cam_settings_frame")
        self.cam_settings_frame.setMinimumSize(QSize(270, 0))
        self.cam_settings_frame.setMaximumSize(QSize(246, 16777215))
        self.cam_settings_frame.setFrameShape(QFrame.Box)
        self.cam_settings_frame.setFrameShadow(QFrame.Plain)
        self.gridLayoutWidget_2 = QWidget(self.cam_settings_frame)
        self.gridLayoutWidget_2.setObjectName(u"gridLayoutWidget_2")
        self.gridLayoutWidget_2.setGeometry(QRect(5, 5, 261, 166))
        self.gridLayout_7 = QGridLayout(self.gridLayoutWidget_2)
        self.gridLayout_7.setObjectName(u"gridLayout_7")
        self.gridLayout_7.setContentsMargins(0, 0, 0, 0)
        self.exposure_to_edit = QLineEdit(self.gridLayoutWidget_2)
        self.exposure_to_edit.setObjectName(u"exposure_to_edit")
        self.exposure_to_edit.setMaximumSize(QSize(30, 16777215))

        self.gridLayout_7.addWidget(self.exposure_to_edit, 1, 5, 1, 1)

        self.gain_from_edit = QLineEdit(self.gridLayoutWidget_2)
        self.gain_from_edit.setObjectName(u"gain_from_edit")
        self.gain_from_edit.setMaximumSize(QSize(30, 16777215))

        self.gridLayout_7.addWidget(self.gain_from_edit, 4, 3, 1, 1)

        self.label_9 = QLabel(self.gridLayoutWidget_2)
        self.label_9.setObjectName(u"label_9")

        self.gridLayout_7.addWidget(self.label_9, 4, 4, 1, 1)

        self.label_7 = QLabel(self.gridLayoutWidget_2)
        self.label_7.setObjectName(u"label_7")

        self.gridLayout_7.addWidget(self.label_7, 1, 4, 1, 1)

        self.gain_l = QLabel(self.gridLayoutWidget_2)
        self.gain_l.setObjectName(u"gain_l")

        self.gridLayout_7.addWidget(self.gain_l, 4, 0, 1, 1)

        self.verticalLayout_10 = QVBoxLayout()
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.cam_preview_btn = QPushButton(self.gridLayoutWidget_2)
        self.cam_preview_btn.setObjectName(u"cam_preview_btn")

        self.verticalLayout_10.addWidget(self.cam_preview_btn)

        self.cam_snapshot_btn = QPushButton(self.gridLayoutWidget_2)
        self.cam_snapshot_btn.setObjectName(u"cam_snapshot_btn")

        self.verticalLayout_10.addWidget(self.cam_snapshot_btn)


        self.gridLayout_7.addLayout(self.verticalLayout_10, 0, 3, 1, 3)

        self.gain_cur_edit = QLineEdit(self.gridLayoutWidget_2)
        self.gain_cur_edit.setObjectName(u"gain_cur_edit")
        self.gain_cur_edit.setMaximumSize(QSize(25, 16777215))

        self.gridLayout_7.addWidget(self.gain_cur_edit, 4, 1, 1, 1)

        self.exposure_from_edit = QLineEdit(self.gridLayoutWidget_2)
        self.exposure_from_edit.setObjectName(u"exposure_from_edit")
        self.exposure_from_edit.setMaximumSize(QSize(30, 16777215))

        self.gridLayout_7.addWidget(self.exposure_from_edit, 1, 3, 1, 1)

        self.gain_slider = QSlider(self.gridLayoutWidget_2)
        self.gain_slider.setObjectName(u"gain_slider")
        self.gain_slider.setMaximum(10)
        self.gain_slider.setValue(1)
        self.gain_slider.setOrientation(Qt.Horizontal)

        self.gridLayout_7.addWidget(self.gain_slider, 5, 0, 1, 6)

        self.label_5 = QLabel(self.gridLayoutWidget_2)
        self.label_5.setObjectName(u"label_5")
        font8 = QFont()
        font8.setPointSize(13)
        self.label_5.setFont(font8)
        self.label_5.setAlignment(Qt.AlignCenter)

        self.gridLayout_7.addWidget(self.label_5, 0, 0, 1, 3)

        self.exposure_slider = QSlider(self.gridLayoutWidget_2)
        self.exposure_slider.setObjectName(u"exposure_slider")
        self.exposure_slider.setMaximum(100)
        self.exposure_slider.setValue(10)
        self.exposure_slider.setOrientation(Qt.Horizontal)

        self.gridLayout_7.addWidget(self.exposure_slider, 2, 0, 1, 6)

        self.gain_to_edit = QLineEdit(self.gridLayoutWidget_2)
        self.gain_to_edit.setObjectName(u"gain_to_edit")
        self.gain_to_edit.setMaximumSize(QSize(30, 16777215))

        self.gridLayout_7.addWidget(self.gain_to_edit, 4, 5, 1, 1)

        self.exposure_cur_edit = QLineEdit(self.gridLayoutWidget_2)
        self.exposure_cur_edit.setObjectName(u"exposure_cur_edit")
        self.exposure_cur_edit.setMaximumSize(QSize(25, 16777215))

        self.gridLayout_7.addWidget(self.exposure_cur_edit, 1, 1, 1, 1)

        self.exposure_l = QLabel(self.gridLayoutWidget_2)
        self.exposure_l.setObjectName(u"exposure_l")

        self.gridLayout_7.addWidget(self.exposure_l, 1, 0, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_7.addItem(self.verticalSpacer, 3, 0, 1, 1)


        self.horizontalLayout.addWidget(self.cam_settings_frame)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_3)

        self.frame = QFrame(self.horizontalLayoutWidget)
        self.frame.setObjectName(u"frame")
        self.frame.setMinimumSize(QSize(281, 0))
        self.frame.setMaximumSize(QSize(281, 71))
        self.frame.setAutoFillBackground(False)
        self.frame.setStyleSheet(u"border-width: 1;\n"
"border-radius: 3;\n"
"border-style: solid;\n"
"border-color: rgb(10, 10, 10)")
        self.frame.setFrameShape(QFrame.NoFrame)
        self.frame.setFrameShadow(QFrame.Plain)
        self.frame.setLineWidth(0)
        self.verticalLayoutWidget = QWidget(self.frame)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(5, 5, 271, 61))
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setSpacing(5)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(5, 0, 5, 0)
        self.exp_l = QLabel(self.verticalLayoutWidget)
        self.exp_l.setObjectName(u"exp_l")
        sizePolicy2.setHeightForWidth(self.exp_l.sizePolicy().hasHeightForWidth())
        self.exp_l.setSizePolicy(sizePolicy2)
        font9 = QFont()
        font9.setPointSize(16)
        self.exp_l.setFont(font9)
        self.exp_l.setStyleSheet(u"border-width: 0;\n"
"border-radius: 0;\n"
"border-style: solid;\n"
"border-color: rgb(10, 10, 10)")
        self.exp_l.setFrameShape(QFrame.NoFrame)
        self.exp_l.setLineWidth(0)
        self.exp_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.exp_l)

        self.name_inp = QLineEdit(self.verticalLayoutWidget)
        self.name_inp.setObjectName(u"name_inp")

        self.verticalLayout.addWidget(self.name_inp)


        self.horizontalLayout.addWidget(self.frame)

        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_4)

        self.plots_frame = QFrame(self.horizontalLayoutWidget)
        self.plots_frame.setObjectName(u"plots_frame")
        sizePolicy1.setHeightForWidth(self.plots_frame.sizePolicy().hasHeightForWidth())
        self.plots_frame.setSizePolicy(sizePolicy1)
        self.plots_frame.setMinimumSize(QSize(182, 0))
        self.plots_frame.setMaximumSize(QSize(182, 16777215))
        self.plots_frame.setFrameShape(QFrame.Box)
        self.plots_frame.setFrameShadow(QFrame.Plain)
        self.verticalLayoutWidget_2 = QWidget(self.plots_frame)
        self.verticalLayoutWidget_2.setObjectName(u"verticalLayoutWidget_2")
        self.verticalLayoutWidget_2.setGeometry(QRect(5, 5, 171, 161))
        self.verticalLayout_2 = QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.plots_l = QLabel(self.verticalLayoutWidget_2)
        self.plots_l.setObjectName(u"plots_l")
        self.plots_l.setFont(font4)
        self.plots_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_2.addWidget(self.plots_l)

        self.gridLayout_5 = QGridLayout()
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.datapoints_l = QLabel(self.verticalLayoutWidget_2)
        self.datapoints_l.setObjectName(u"datapoints_l")

        self.gridLayout_5.addWidget(self.datapoints_l, 0, 0, 1, 1)

        self.datapoints_spin = QSpinBox(self.verticalLayoutWidget_2)
        self.datapoints_spin.setObjectName(u"datapoints_spin")
        self.datapoints_spin.setMinimum(2)
        self.datapoints_spin.setMaximum(999999999)
        self.datapoints_spin.setValue(50)

        self.gridLayout_5.addWidget(self.datapoints_spin, 0, 1, 1, 1)

        self.freq_l = QLabel(self.verticalLayoutWidget_2)
        self.freq_l.setObjectName(u"freq_l")

        self.gridLayout_5.addWidget(self.freq_l, 1, 0, 1, 1)

        self.freq_spin = QSpinBox(self.verticalLayoutWidget_2)
        self.freq_spin.setObjectName(u"freq_spin")
        self.freq_spin.setMinimum(1)
        self.freq_spin.setMaximum(100)
        self.freq_spin.setValue(1)

        self.gridLayout_5.addWidget(self.freq_spin, 1, 1, 1, 1)


        self.verticalLayout_2.addLayout(self.gridLayout_5)

        self.gridLayout_6 = QGridLayout()
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.s0_plot_btn = QPushButton(self.verticalLayoutWidget_2)
        self.s0_plot_btn.setObjectName(u"s0_plot_btn")

        self.gridLayout_6.addWidget(self.s0_plot_btn, 0, 0, 1, 1)

        self.s2_plot_btn = QPushButton(self.verticalLayoutWidget_2)
        self.s2_plot_btn.setObjectName(u"s2_plot_btn")

        self.gridLayout_6.addWidget(self.s2_plot_btn, 1, 0, 1, 1)

        self.s1_plot_btn = QPushButton(self.verticalLayoutWidget_2)
        self.s1_plot_btn.setObjectName(u"s1_plot_btn")

        self.gridLayout_6.addWidget(self.s1_plot_btn, 0, 1, 1, 1)

        self.s3_plot_btn = QPushButton(self.verticalLayoutWidget_2)
        self.s3_plot_btn.setObjectName(u"s3_plot_btn")

        self.gridLayout_6.addWidget(self.s3_plot_btn, 1, 1, 1, 1)


        self.verticalLayout_2.addLayout(self.gridLayout_6)


        self.horizontalLayout.addWidget(self.plots_frame)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.saved_l.setText("")
        self.progress_l.setText(QCoreApplication.translate("MainWindow", u"Waiting for start", None))
        self.dummy_PS_c.setText(QCoreApplication.translate("MainWindow", u"Dummy Powersupply", None))
        self.stop_exp_btn.setText(QCoreApplication.translate("MainWindow", u"Stop experiment", None))
        self.err_l.setText("")
        self.load_btn.setText(QCoreApplication.translate("MainWindow", u"Load options", None))
        self.run_btn.setText(QCoreApplication.translate("MainWindow", u"Run experiment", None))
        self.save_btn.setText(QCoreApplication.translate("MainWindow", u"Save options", None))
        self.label_15.setText(QCoreApplication.translate("MainWindow", u"Start: ", None))
        self.r2_l.setText(QCoreApplication.translate("MainWindow", u"R2", None))
        self.r2_stat_c.setText(QCoreApplication.translate("MainWindow", u"Stationary", None))
        self.r2_reverse_c.setText(QCoreApplication.translate("MainWindow", u"Reverse Steps", None))
        self.s3_l.setText(QCoreApplication.translate("MainWindow", u"S3", None))
        self.s3_stat_c.setText(QCoreApplication.translate("MainWindow", u"Stationary", None))
        self.s3_reverse_c.setText(QCoreApplication.translate("MainWindow", u"Reverse Steps", None))
        self.s1_l.setText(QCoreApplication.translate("MainWindow", u"S1", None))
        self.s1_stat_c.setText(QCoreApplication.translate("MainWindow", u"Stationary", None))
        self.s1_reverse_c.setText(QCoreApplication.translate("MainWindow", u"Reverse Steps", None))
        self.label_23.setText(QCoreApplication.translate("MainWindow", u"Order: ", None))
        self.label_16.setText(QCoreApplication.translate("MainWindow", u"Stop: ", None))
        self.s0_l.setText(QCoreApplication.translate("MainWindow", u"S0", None))
        self.s0_stat_c.setText(QCoreApplication.translate("MainWindow", u"Stationary", None))
        self.s0_reverse_c.setText(QCoreApplication.translate("MainWindow", u"Reverse Steps", None))
        self.s2_l.setText(QCoreApplication.translate("MainWindow", u"S2", None))
        self.s2_stat_c.setText(QCoreApplication.translate("MainWindow", u"Stationary", None))
        self.s2_reverse_c.setText(QCoreApplication.translate("MainWindow", u"Reverse Steps", None))
        self.label_17.setText(QCoreApplication.translate("MainWindow", u"Step: ", None))
        self.tp_l.setText(QCoreApplication.translate("MainWindow", u"Turning points:", None))
        self.label_51.setText(QCoreApplication.translate("MainWindow", u"Select which stage moves first by selecting '1' on that stage, the stage with '2' selected will turn second etc.", None))
        self.mf_edit.setText(QCoreApplication.translate("MainWindow", u"5", None))
        self.tip_edit.setText(QCoreApplication.translate("MainWindow", u"10", None))
        self.mf_l.setText(QCoreApplication.translate("MainWindow", u"Meassurements per second: ", None))
        self.tip_l.setText(QCoreApplication.translate("MainWindow", u"Time in position (s): ", None))
        self.totalsteps_text_l.setText(QCoreApplication.translate("MainWindow", u"Total Steps: ", None))
        self.totalsteps_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.exp_time_l.setText(QCoreApplication.translate("MainWindow", u"Experiment took: ???", None))
        self.r_connect_btn.setText(QCoreApplication.translate("MainWindow", u"Connect", None))
        self.r2_con_l.setText(QCoreApplication.translate("MainWindow", u"Disconnected", None))
        self.label_92.setText(QCoreApplication.translate("MainWindow", u"R2", None))
        self.step_r2_edit.setText(QCoreApplication.translate("MainWindow", u"0.05", None))
        self.go_r2_btn.setText(QCoreApplication.translate("MainWindow", u"Go", None))
        self.left_r2_btn.setText(QCoreApplication.translate("MainWindow", u"<---", None))
        self.label_93.setText(QCoreApplication.translate("MainWindow", u"Step size:", None))
        self.ref_r2_btn.setText(QCoreApplication.translate("MainWindow", u"Reference", None))
        self.right_r2_btn.setText(QCoreApplication.translate("MainWindow", u"--->", None))
        self.label_94.setText(QCoreApplication.translate("MainWindow", u"Actual Pos:", None))
        self.pos_r2_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.label_8.setText(QCoreApplication.translate("MainWindow", u"S3: ", None))
        self.s2_go_btn.setText(QCoreApplication.translate("MainWindow", u"Go", None))
        self.s0_volt_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Voltage", None))
        self.s0_go_btn.setText(QCoreApplication.translate("MainWindow", u"Go", None))
        self.s1_volt_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Status", None))
        self.s1_go_btn.setText(QCoreApplication.translate("MainWindow", u"Go", None))
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"S2: ", None))
        self.s2_volt_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"S0: ", None))
        self.s3_volt_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.s3_go_btn.setText(QCoreApplication.translate("MainWindow", u"Go", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"S1: ", None))
        self.s_connect_btn.setText(QCoreApplication.translate("MainWindow", u"Connect", None))
        self.s3_c.setText(QCoreApplication.translate("MainWindow", u"S3 (v)", None))
        self.r2_c.setText(QCoreApplication.translate("MainWindow", u"R2 (n.u)", None))
        self.s1_c.setText(QCoreApplication.translate("MainWindow", u"S1 (v)", None))
        self.s2_c.setText(QCoreApplication.translate("MainWindow", u"S2 (v)", None))
        self.home_r2_c.setText(QCoreApplication.translate("MainWindow", u"Home", None))
        self.s0_c.setText(QCoreApplication.translate("MainWindow", u"S0 (v)", None))
        self.cam_c.setText(QCoreApplication.translate("MainWindow", u"Camera", None))
        self.exposure_to_edit.setText(QCoreApplication.translate("MainWindow", u"100", None))
        self.gain_from_edit.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.label_9.setText(QCoreApplication.translate("MainWindow", u"to", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"to", None))
        self.gain_l.setText(QCoreApplication.translate("MainWindow", u"Gain:", None))
        self.cam_preview_btn.setText(QCoreApplication.translate("MainWindow", u"Preview", None))
        self.cam_snapshot_btn.setText(QCoreApplication.translate("MainWindow", u"Snapshot", None))
        self.gain_cur_edit.setText(QCoreApplication.translate("MainWindow", u"1", None))
        self.exposure_from_edit.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Camera Settings", None))
        self.gain_to_edit.setText(QCoreApplication.translate("MainWindow", u"10", None))
        self.exposure_cur_edit.setText(QCoreApplication.translate("MainWindow", u"10", None))
        self.exposure_l.setText(QCoreApplication.translate("MainWindow", u"Exposure (ms):", None))
        self.exp_l.setText(QCoreApplication.translate("MainWindow", u"Experiment name:", None))
        self.plots_l.setText(QCoreApplication.translate("MainWindow", u"Plots", None))
        self.datapoints_l.setText(QCoreApplication.translate("MainWindow", u"Datapoints:", None))
        self.freq_l.setText(QCoreApplication.translate("MainWindow", u"frequency:", None))
        self.s0_plot_btn.setText(QCoreApplication.translate("MainWindow", u"S0", None))
        self.s2_plot_btn.setText(QCoreApplication.translate("MainWindow", u"S2", None))
        self.s1_plot_btn.setText(QCoreApplication.translate("MainWindow", u"S1", None))
        self.s3_plot_btn.setText(QCoreApplication.translate("MainWindow", u"S3", None))
    # retranslateUi

