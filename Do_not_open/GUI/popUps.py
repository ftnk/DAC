from PySide6.QtWidgets import QApplication, QMainWindow, QFileDialog
from PySide6.QtCore import QRect, QThread, Qt, SIGNAL, QReadWriteLock
from PySide6.QtCore import QRegularExpression as QRegExp
from PySide6.QtGui import QRegularExpressionValidator as QRegExpValidator
from PySide6 import QtCore, QtGui, QtWidgets

from GUI.ui_camPopup import Ui_MainWindow as camMainWindow


class cameraPopup(QMainWindow):
    def __init__(self, mainWindow, parent=None):
        super().__init__(parent)
        self.ui = camMainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle("Camera Controller")
        self.shown = False
        self.mainWindow = mainWindow
        self.connectSignals()
    
    def setValues(self, exposure, gain):
        self.ui.exposure_cur_edit.setText(exposure)
        self.ui.exposure_slider.setValue(int(exposure))
        self.ui.gain_cur_edit.setText(gain)
        self.ui.gain_slider.setValue(int(gain))
    
    def setMinMax(self, exposureMinMax=None, gainMinMax=None):
        if exposureMinMax != None:
            self.ui.exposure_from_edit.setText(exposureMinMax[0])
            self.ui.exposure_to_edit.setText(exposureMinMax[1])
            self.ui.exposure_slider.setMinimum(int(exposureMinMax[0]))
            self.ui.exposure_slider.setMaximum(int(exposureMinMax[1]))
        if gainMinMax != None:
            self.ui.gain_from_edit.setText(gainMinMax[0])
            self.ui.gain_to_edit.setText(gainMinMax[1])
            self.ui.gain_slider.setMinimum(int(gainMinMax[0]))
            self.ui.gain_slider.setMaximum(int(gainMinMax[1]))

    def connectSignals(self):
        self.ui.exposure_from_edit.textChanged.connect(lambda: self.mainWindow.changeExposureLimits(False))
        self.ui.exposure_to_edit.textChanged.connect(lambda: self.mainWindow.changeExposureLimits(False))
        self.ui.gain_from_edit.textChanged.connect(lambda: self.mainWindow.changeGainLimits(False))
        self.ui.gain_to_edit.textChanged.connect(lambda: self.mainWindow.changeGainLimits(False))
        self.ui.exposure_slider.valueChanged.connect(lambda: self.mainWindow.changeExposure(self.ui.exposure_slider))
        self.ui.gain_slider.valueChanged.connect(lambda: self.mainWindow.changeGain(self.ui.gain_slider))
        self.ui.exposure_cur_edit.textChanged.connect(lambda: self.mainWindow.setExposure(False))
        self.ui.gain_cur_edit.textChanged.connect(lambda: self.mainWindow.setGain(False))
    
    def closeEvent(self, event):
        self.shown = False

