# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'vacForm.ui'
##
## Created by: Qt User Interface Compiler version 6.4.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QGridLayout, QHBoxLayout,
    QLabel, QLineEdit, QPushButton, QSizePolicy,
    QWidget)

class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(950, 220)
        self.horizontalLayoutWidget = QWidget(Form)
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(10, 10, 931, 201))
        self.horizontalLayout = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.frame = QFrame(self.horizontalLayoutWidget)
        self.frame.setObjectName(u"frame")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.frame.sizePolicy().hasHeightForWidth())
        self.frame.setSizePolicy(sizePolicy)
        self.frame.setMaximumSize(QSize(281, 177))
        self.frame.setFrameShape(QFrame.Box)
        self.frame.setFrameShadow(QFrame.Plain)
        self.layoutWidget = QWidget(self.frame)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(10, 10, 262, 163))
        self.gridLayout = QGridLayout(self.layoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.vx_con_l = QLabel(self.layoutWidget)
        self.vx_con_l.setObjectName(u"vx_con_l")
        font = QFont()
        font.setPointSize(8)
        self.vx_con_l.setFont(font)

        self.gridLayout.addWidget(self.vx_con_l, 0, 2, 1, 1)

        self.step_vx_edit = QLineEdit(self.layoutWidget)
        self.step_vx_edit.setObjectName(u"step_vx_edit")

        self.gridLayout.addWidget(self.step_vx_edit, 2, 1, 1, 1)

        self.right_vx_btn = QPushButton(self.layoutWidget)
        self.right_vx_btn.setObjectName(u"right_vx_btn")
        self.right_vx_btn.setMaximumSize(QSize(16777215, 24))
        font1 = QFont()
        font1.setPointSize(11)
        font1.setBold(True)
        self.right_vx_btn.setFont(font1)

        self.gridLayout.addWidget(self.right_vx_btn, 3, 2, 1, 1)

        self.label_2 = QLabel(self.layoutWidget)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)

        self.label = QLabel(self.layoutWidget)
        self.label.setObjectName(u"label")
        self.label.setMinimumSize(QSize(0, 49))
        font2 = QFont()
        font2.setPointSize(24)
        self.label.setFont(font2)
        self.label.setAlignment(Qt.AlignCenter)

        self.gridLayout.addWidget(self.label, 0, 1, 1, 1)

        self.left_vx_btn = QPushButton(self.layoutWidget)
        self.left_vx_btn.setObjectName(u"left_vx_btn")
        self.left_vx_btn.setMaximumSize(QSize(16777215, 24))
        font3 = QFont()
        font3.setPointSize(11)
        font3.setBold(True)
        font3.setUnderline(False)
        font3.setStrikeOut(False)
        self.left_vx_btn.setFont(font3)

        self.gridLayout.addWidget(self.left_vx_btn, 3, 0, 1, 1)

        self.curPos_vx_edit = QLineEdit(self.layoutWidget)
        self.curPos_vx_edit.setObjectName(u"curPos_vx_edit")

        self.gridLayout.addWidget(self.curPos_vx_edit, 3, 1, 1, 1)

        self.go_vx_btn = QPushButton(self.layoutWidget)
        self.go_vx_btn.setObjectName(u"go_vx_btn")
        self.go_vx_btn.setMinimumSize(QSize(0, 24))
        font4 = QFont()
        font4.setPointSize(9)
        font4.setBold(True)
        self.go_vx_btn.setFont(font4)

        self.gridLayout.addWidget(self.go_vx_btn, 4, 1, 1, 1)

        self.ref_vx_btn = QPushButton(self.layoutWidget)
        self.ref_vx_btn.setObjectName(u"ref_vx_btn")

        self.gridLayout.addWidget(self.ref_vx_btn, 0, 0, 1, 1)

        self.label_3 = QLabel(self.layoutWidget)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)

        self.pos_vx_l = QLabel(self.layoutWidget)
        self.pos_vx_l.setObjectName(u"pos_vx_l")

        self.gridLayout.addWidget(self.pos_vx_l, 1, 1, 1, 1)


        self.horizontalLayout.addWidget(self.frame)

        self.frame_2 = QFrame(self.horizontalLayoutWidget)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setMaximumSize(QSize(281, 177))
        self.frame_2.setFrameShape(QFrame.Box)
        self.frame_2.setFrameShadow(QFrame.Plain)
        self.layoutWidget1 = QWidget(self.frame_2)
        self.layoutWidget1.setObjectName(u"layoutWidget1")
        self.layoutWidget1.setGeometry(QRect(10, 10, 262, 157))
        self.gridLayout_2 = QGridLayout(self.layoutWidget1)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setContentsMargins(1, 1, 0, 0)
        self.label_4 = QLabel(self.layoutWidget1)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout_2.addWidget(self.label_4, 2, 0, 1, 1)

        self.right_vy_btn = QPushButton(self.layoutWidget1)
        self.right_vy_btn.setObjectName(u"right_vy_btn")
        self.right_vy_btn.setMaximumSize(QSize(16777215, 24))
        self.right_vy_btn.setFont(font1)

        self.gridLayout_2.addWidget(self.right_vy_btn, 3, 2, 1, 1)

        self.label_7 = QLabel(self.layoutWidget1)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setFont(font2)
        self.label_7.setAlignment(Qt.AlignCenter)

        self.gridLayout_2.addWidget(self.label_7, 0, 1, 1, 1)

        self.ref_vy_btn = QPushButton(self.layoutWidget1)
        self.ref_vy_btn.setObjectName(u"ref_vy_btn")

        self.gridLayout_2.addWidget(self.ref_vy_btn, 0, 0, 1, 1)

        self.step_vy_edit = QLineEdit(self.layoutWidget1)
        self.step_vy_edit.setObjectName(u"step_vy_edit")

        self.gridLayout_2.addWidget(self.step_vy_edit, 2, 1, 1, 1)

        self.go_vy_btn = QPushButton(self.layoutWidget1)
        self.go_vy_btn.setObjectName(u"go_vy_btn")
        self.go_vy_btn.setMinimumSize(QSize(88, 24))
        self.go_vy_btn.setFont(font4)

        self.gridLayout_2.addWidget(self.go_vy_btn, 4, 1, 1, 1)

        self.left_vy_btn = QPushButton(self.layoutWidget1)
        self.left_vy_btn.setObjectName(u"left_vy_btn")
        self.left_vy_btn.setMaximumSize(QSize(16777215, 24))
        self.left_vy_btn.setFont(font3)

        self.gridLayout_2.addWidget(self.left_vy_btn, 3, 0, 1, 1)

        self.vy_con_l = QLabel(self.layoutWidget1)
        self.vy_con_l.setObjectName(u"vy_con_l")
        self.vy_con_l.setFont(font)

        self.gridLayout_2.addWidget(self.vy_con_l, 0, 2, 1, 1)

        self.curPos_vy_edit = QLineEdit(self.layoutWidget1)
        self.curPos_vy_edit.setObjectName(u"curPos_vy_edit")

        self.gridLayout_2.addWidget(self.curPos_vy_edit, 3, 1, 1, 1)

        self.label_8 = QLabel(self.layoutWidget1)
        self.label_8.setObjectName(u"label_8")

        self.gridLayout_2.addWidget(self.label_8, 1, 0, 1, 1)

        self.pos_vy_l = QLabel(self.layoutWidget1)
        self.pos_vy_l.setObjectName(u"pos_vy_l")

        self.gridLayout_2.addWidget(self.pos_vy_l, 1, 1, 1, 1)


        self.horizontalLayout.addWidget(self.frame_2)

        self.frame_3 = QFrame(self.horizontalLayoutWidget)
        self.frame_3.setObjectName(u"frame_3")
        self.frame_3.setMaximumSize(QSize(281, 177))
        self.frame_3.setFrameShape(QFrame.Box)
        self.frame_3.setFrameShadow(QFrame.Plain)
        self.layoutWidget2 = QWidget(self.frame_3)
        self.layoutWidget2.setObjectName(u"layoutWidget2")
        self.layoutWidget2.setGeometry(QRect(10, 10, 262, 157))
        self.gridLayout_3 = QGridLayout(self.layoutWidget2)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.gridLayout_3.setContentsMargins(1, 1, 0, 0)
        self.vz_con_l = QLabel(self.layoutWidget2)
        self.vz_con_l.setObjectName(u"vz_con_l")
        self.vz_con_l.setFont(font)

        self.gridLayout_3.addWidget(self.vz_con_l, 0, 2, 1, 1)

        self.label_5 = QLabel(self.layoutWidget2)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setFont(font2)
        self.label_5.setAlignment(Qt.AlignCenter)

        self.gridLayout_3.addWidget(self.label_5, 0, 1, 1, 1)

        self.step_vz_edit = QLineEdit(self.layoutWidget2)
        self.step_vz_edit.setObjectName(u"step_vz_edit")

        self.gridLayout_3.addWidget(self.step_vz_edit, 2, 1, 1, 1)

        self.go_vz_btn = QPushButton(self.layoutWidget2)
        self.go_vz_btn.setObjectName(u"go_vz_btn")
        self.go_vz_btn.setMinimumSize(QSize(83, 0))
        self.go_vz_btn.setFont(font4)

        self.gridLayout_3.addWidget(self.go_vz_btn, 4, 1, 1, 1)

        self.left_vz_btn = QPushButton(self.layoutWidget2)
        self.left_vz_btn.setObjectName(u"left_vz_btn")
        self.left_vz_btn.setMaximumSize(QSize(16777215, 24))
        self.left_vz_btn.setFont(font3)

        self.gridLayout_3.addWidget(self.left_vz_btn, 3, 0, 1, 1)

        self.label_6 = QLabel(self.layoutWidget2)
        self.label_6.setObjectName(u"label_6")

        self.gridLayout_3.addWidget(self.label_6, 2, 0, 1, 1)

        self.ref_vz_btn = QPushButton(self.layoutWidget2)
        self.ref_vz_btn.setObjectName(u"ref_vz_btn")

        self.gridLayout_3.addWidget(self.ref_vz_btn, 0, 0, 1, 1)

        self.curPos_vz_edit = QLineEdit(self.layoutWidget2)
        self.curPos_vz_edit.setObjectName(u"curPos_vz_edit")

        self.gridLayout_3.addWidget(self.curPos_vz_edit, 3, 1, 1, 1)

        self.right_vz_btn = QPushButton(self.layoutWidget2)
        self.right_vz_btn.setObjectName(u"right_vz_btn")
        self.right_vz_btn.setMaximumSize(QSize(16777215, 24))
        self.right_vz_btn.setFont(font1)

        self.gridLayout_3.addWidget(self.right_vz_btn, 3, 2, 1, 1)

        self.label_9 = QLabel(self.layoutWidget2)
        self.label_9.setObjectName(u"label_9")

        self.gridLayout_3.addWidget(self.label_9, 1, 0, 1, 1)

        self.pos_vz_l = QLabel(self.layoutWidget2)
        self.pos_vz_l.setObjectName(u"pos_vz_l")

        self.gridLayout_3.addWidget(self.pos_vz_l, 1, 1, 1, 1)


        self.horizontalLayout.addWidget(self.frame_3)


        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
        self.vx_con_l.setText(QCoreApplication.translate("Form", u"Disconnected", None))
        self.right_vx_btn.setText(QCoreApplication.translate("Form", u"--->", None))
        self.label_2.setText(QCoreApplication.translate("Form", u"Step size:", None))
        self.label.setText(QCoreApplication.translate("Form", u"VX", None))
        self.left_vx_btn.setText(QCoreApplication.translate("Form", u"<---", None))
        self.go_vx_btn.setText(QCoreApplication.translate("Form", u"Go", None))
        self.ref_vx_btn.setText(QCoreApplication.translate("Form", u"Reference", None))
        self.label_3.setText(QCoreApplication.translate("Form", u"Actual Pos:", None))
        self.pos_vx_l.setText(QCoreApplication.translate("Form", u"TextLabel", None))
        self.label_4.setText(QCoreApplication.translate("Form", u"Step size:", None))
        self.right_vy_btn.setText(QCoreApplication.translate("Form", u"--->", None))
        self.label_7.setText(QCoreApplication.translate("Form", u"VY", None))
        self.ref_vy_btn.setText(QCoreApplication.translate("Form", u"Reference", None))
        self.go_vy_btn.setText(QCoreApplication.translate("Form", u"Go", None))
        self.left_vy_btn.setText(QCoreApplication.translate("Form", u"<---", None))
        self.vy_con_l.setText(QCoreApplication.translate("Form", u"Disconnected", None))
        self.label_8.setText(QCoreApplication.translate("Form", u"Actual Pos:", None))
        self.pos_vy_l.setText(QCoreApplication.translate("Form", u"TextLabel", None))
        self.vz_con_l.setText(QCoreApplication.translate("Form", u"Disconnected", None))
        self.label_5.setText(QCoreApplication.translate("Form", u"VZ", None))
        self.go_vz_btn.setText(QCoreApplication.translate("Form", u"Go", None))
        self.left_vz_btn.setText(QCoreApplication.translate("Form", u"<---", None))
        self.label_6.setText(QCoreApplication.translate("Form", u"Step size:", None))
        self.ref_vz_btn.setText(QCoreApplication.translate("Form", u"Reference", None))
        self.right_vz_btn.setText(QCoreApplication.translate("Form", u"--->", None))
        self.label_9.setText(QCoreApplication.translate("Form", u"Actual Pos:", None))
        self.pos_vz_l.setText(QCoreApplication.translate("Form", u"TextLabel", None))
    # retranslateUi

