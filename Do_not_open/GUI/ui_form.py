# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form_old.ui'
##
## Created by: Qt User Interface Compiler version 6.5.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractScrollArea, QApplication, QCheckBox, QFrame,
    QGridLayout, QHBoxLayout, QLabel, QLayout,
    QLineEdit, QMainWindow, QProgressBar, QPushButton,
    QRadioButton, QScrollArea, QSizePolicy, QSpacerItem,
    QSpinBox, QVBoxLayout, QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1202, 802)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy1)
        self.gridLayout_3 = QGridLayout(self.centralwidget)
        self.gridLayout_3.setSpacing(0)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.gridLayout_3.setSizeConstraint(QLayout.SetNoConstraint)
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.scrollArea = QScrollArea(self.centralwidget)
        self.scrollArea.setObjectName(u"scrollArea")
        sizePolicy1.setHeightForWidth(self.scrollArea.sizePolicy().hasHeightForWidth())
        self.scrollArea.setSizePolicy(sizePolicy1)
        self.scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.scrollArea.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.scrollArea.setWidgetResizable(True)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 1200, 800))
        self.scrollAreaWidgetContents.setMinimumSize(QSize(1200, 800))
        self.gridLayout_2 = QGridLayout(self.scrollAreaWidgetContents)
        self.gridLayout_2.setSpacing(0)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setContentsMargins(0, 0, 0, 0)
        self.frame_3 = QFrame(self.scrollAreaWidgetContents)
        self.frame_3.setObjectName(u"frame_3")
        self.frame_3.setFrameShape(QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QFrame.Raised)
        self.frame = QFrame(self.frame_3)
        self.frame.setObjectName(u"frame")
        self.frame.setGeometry(QRect(470, 10, 270, 60))
        self.frame.setAutoFillBackground(False)
        self.frame.setStyleSheet(u"border-width: 1;\n"
"border-radius: 3;\n"
"border-style: solid;\n"
"border-color: rgb(10, 10, 10)")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.verticalLayoutWidget = QWidget(self.frame)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(-1, -1, 271, 61))
        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setSpacing(5)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalLayout.setContentsMargins(5, 0, 5, 0)
        self.label = QLabel(self.verticalLayoutWidget)
        self.label.setObjectName(u"label")
        sizePolicy2 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy2)
        font = QFont()
        font.setPointSize(16)
        self.label.setFont(font)
        self.label.setStyleSheet(u"border-width: 0;\n"
"border-radius: 0;\n"
"border-style: solid;\n"
"border-color: rgb(10, 10, 10)")
        self.label.setAlignment(Qt.AlignCenter)

        self.verticalLayout.addWidget(self.label)

        self.name_inp = QLineEdit(self.verticalLayoutWidget)
        self.name_inp.setObjectName(u"name_inp")

        self.verticalLayout.addWidget(self.name_inp)

        self.verticalLayoutWidget_2 = QWidget(self.frame_3)
        self.verticalLayoutWidget_2.setObjectName(u"verticalLayoutWidget_2")
        self.verticalLayoutWidget_2.setGeometry(QRect(20, 70, 1146, 176))
        self.verticalLayout_2 = QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setSpacing(0)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.frame_2 = QFrame(self.verticalLayoutWidget_2)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setAutoFillBackground(False)
        self.frame_2.setStyleSheet(u"")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.horizontalLayoutWidget = QWidget(self.frame_2)
        self.horizontalLayoutWidget.setObjectName(u"horizontalLayoutWidget")
        self.horizontalLayoutWidget.setGeometry(QRect(0, 10, 1141, 166))
        self.horizontalLayout = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.frame_4 = QFrame(self.horizontalLayoutWidget)
        self.frame_4.setObjectName(u"frame_4")
        self.frame_4.setMaximumSize(QSize(16777215, 149))
        self.frame_4.setFrameShape(QFrame.Box)
        self.frame_4.setFrameShadow(QFrame.Plain)
        self.gridLayoutWidget_2 = QWidget(self.frame_4)
        self.gridLayoutWidget_2.setObjectName(u"gridLayoutWidget_2")
        self.gridLayoutWidget_2.setGeometry(QRect(9, 9, 151, 133))
        self.gridLayout_4 = QGridLayout(self.gridLayoutWidget_2)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.gridLayout_4.setContentsMargins(0, 0, 0, 0)
        self.lockin_radio = QRadioButton(self.gridLayoutWidget_2)
        self.lockin_radio.setObjectName(u"lockin_radio")

        self.gridLayout_4.addWidget(self.lockin_radio, 2, 0, 1, 1)

        self.dummy_radio = QRadioButton(self.gridLayoutWidget_2)
        self.dummy_radio.setObjectName(u"dummy_radio")

        self.gridLayout_4.addWidget(self.dummy_radio, 3, 0, 1, 1)

        self.osc_radio = QRadioButton(self.gridLayoutWidget_2)
        self.osc_radio.setObjectName(u"osc_radio")

        self.gridLayout_4.addWidget(self.osc_radio, 1, 0, 1, 1)

        self.onlyamp_radio = QRadioButton(self.gridLayoutWidget_2)
        self.onlyamp_radio.setObjectName(u"onlyamp_radio")

        self.gridLayout_4.addWidget(self.onlyamp_radio, 4, 0, 1, 1)

        self.label_98 = QLabel(self.gridLayoutWidget_2)
        self.label_98.setObjectName(u"label_98")

        self.gridLayout_4.addWidget(self.label_98, 0, 1, 1, 1)

        self.channel_spin = QSpinBox(self.gridLayoutWidget_2)
        self.channel_spin.setObjectName(u"channel_spin")
        self.channel_spin.setMaximumSize(QSize(100, 16777215))
        self.channel_spin.setValue(2)

        self.gridLayout_4.addWidget(self.channel_spin, 1, 1, 1, 1)


        self.horizontalLayout.addWidget(self.frame_4)

        self.frame_7 = QFrame(self.horizontalLayoutWidget)
        self.frame_7.setObjectName(u"frame_7")
        self.frame_7.setMaximumSize(QSize(16777215, 149))
        self.frame_7.setFrameShape(QFrame.Box)
        self.frame_7.setFrameShadow(QFrame.Plain)
        self.verticalLayoutWidget_6 = QWidget(self.frame_7)
        self.verticalLayoutWidget_6.setObjectName(u"verticalLayoutWidget_6")
        self.verticalLayoutWidget_6.setGeometry(QRect(0, 0, 161, 141))
        self.verticalLayout_6 = QVBoxLayout(self.verticalLayoutWidget_6)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.verticalLayout_6.setContentsMargins(5, 0, 0, 0)
        self.label_97 = QLabel(self.verticalLayoutWidget_6)
        self.label_97.setObjectName(u"label_97")
        font1 = QFont()
        font1.setPointSize(14)
        self.label_97.setFont(font1)
        self.label_97.setAlignment(Qt.AlignCenter)

        self.verticalLayout_6.addWidget(self.label_97)

        self.gridLayout_15 = QGridLayout()
        self.gridLayout_15.setObjectName(u"gridLayout_15")
        self.exposure_l = QLabel(self.verticalLayoutWidget_6)
        self.exposure_l.setObjectName(u"exposure_l")

        self.gridLayout_15.addWidget(self.exposure_l, 1, 1, 1, 1)

        self.gain_l = QLabel(self.verticalLayoutWidget_6)
        self.gain_l.setObjectName(u"gain_l")

        self.gridLayout_15.addWidget(self.gain_l, 2, 1, 1, 1)

        self.cam_conf_btn = QPushButton(self.verticalLayoutWidget_6)
        self.cam_conf_btn.setObjectName(u"cam_conf_btn")

        self.gridLayout_15.addWidget(self.cam_conf_btn, 0, 0, 1, 1)

        self.label_99 = QLabel(self.verticalLayoutWidget_6)
        self.label_99.setObjectName(u"label_99")

        self.gridLayout_15.addWidget(self.label_99, 1, 0, 1, 1)

        self.cam_snap_btn = QPushButton(self.verticalLayoutWidget_6)
        self.cam_snap_btn.setObjectName(u"cam_snap_btn")

        self.gridLayout_15.addWidget(self.cam_snap_btn, 0, 1, 1, 1)

        self.label_101 = QLabel(self.verticalLayoutWidget_6)
        self.label_101.setObjectName(u"label_101")

        self.gridLayout_15.addWidget(self.label_101, 2, 0, 1, 1)

        self.cam_c = QCheckBox(self.verticalLayoutWidget_6)
        self.cam_c.setObjectName(u"cam_c")

        self.gridLayout_15.addWidget(self.cam_c, 3, 0, 1, 1)


        self.verticalLayout_6.addLayout(self.gridLayout_15)


        self.horizontalLayout.addWidget(self.frame_7)

        self.frame_5 = QFrame(self.horizontalLayoutWidget)
        self.frame_5.setObjectName(u"frame_5")
        self.frame_5.setMinimumSize(QSize(542, 0))
        self.frame_5.setMaximumSize(QSize(16777215, 149))
        self.frame_5.setFrameShape(QFrame.Box)
        self.frame_5.setFrameShadow(QFrame.Plain)
        self.layoutWidget = QWidget(self.frame_5)
        self.layoutWidget.setObjectName(u"layoutWidget")
        self.layoutWidget.setGeometry(QRect(10, 10, 521, 147))
        self.verticalLayout_5 = QVBoxLayout(self.layoutWidget)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.label_4 = QLabel(self.layoutWidget)
        self.label_4.setObjectName(u"label_4")
        font2 = QFont()
        font2.setPointSize(10)
        self.label_4.setFont(font2)
        self.label_4.setStyleSheet(u"border-width: 0;")

        self.horizontalLayout_2.addWidget(self.label_4)

        self.time_c = QCheckBox(self.layoutWidget)
        self.time_c.setObjectName(u"time_c")
        self.time_c.setMaximumSize(QSize(125, 16777215))
        self.time_c.setStyleSheet(u"border-width: 0;")

        self.horizontalLayout_2.addWidget(self.time_c)


        self.verticalLayout_5.addLayout(self.horizontalLayout_2)

        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.horizontalSpacer_7 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_7, 0, 6, 1, 1)

        self.y_c = QCheckBox(self.layoutWidget)
        self.y_c.setObjectName(u"y_c")
        self.y_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.y_c, 2, 0, 1, 1)

        self.z_c = QCheckBox(self.layoutWidget)
        self.z_c.setObjectName(u"z_c")
        self.z_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.z_c, 3, 0, 1, 1)

        self.s1_c = QCheckBox(self.layoutWidget)
        self.s1_c.setObjectName(u"s1_c")
        self.s1_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.s1_c, 2, 10, 1, 1)

        self.s2_c = QCheckBox(self.layoutWidget)
        self.s2_c.setObjectName(u"s2_c")
        self.s2_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.s2_c, 3, 10, 1, 1)

        self.s0_c = QCheckBox(self.layoutWidget)
        self.s0_c.setObjectName(u"s0_c")
        self.s0_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.s0_c, 1, 10, 1, 1)

        self.label_57 = QLabel(self.layoutWidget)
        self.label_57.setObjectName(u"label_57")

        self.gridLayout.addWidget(self.label_57, 0, 1, 1, 1)

        self.s3_c = QCheckBox(self.layoutWidget)
        self.s3_c.setObjectName(u"s3_c")
        self.s3_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.s3_c, 4, 10, 1, 1)

        self.label_56 = QLabel(self.layoutWidget)
        self.label_56.setObjectName(u"label_56")

        self.gridLayout.addWidget(self.label_56, 0, 7, 1, 1)

        self.r2_c = QCheckBox(self.layoutWidget)
        self.r2_c.setObjectName(u"r2_c")
        self.r2_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.r2_c, 2, 3, 1, 1)

        self.label_59 = QLabel(self.layoutWidget)
        self.label_59.setObjectName(u"label_59")

        self.gridLayout.addWidget(self.label_59, 0, 8, 1, 1)

        self.label_53 = QLabel(self.layoutWidget)
        self.label_53.setObjectName(u"label_53")

        self.gridLayout.addWidget(self.label_53, 0, 3, 1, 1)

        self.horizontalSpacer_8 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_8, 0, 9, 1, 1)

        self.r3_c = QCheckBox(self.layoutWidget)
        self.r3_c.setObjectName(u"r3_c")
        self.r3_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.r3_c, 3, 3, 1, 1)

        self.horizontalSpacer_6 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_6, 0, 2, 1, 1)

        self.label_10 = QLabel(self.layoutWidget)
        self.label_10.setObjectName(u"label_10")

        self.gridLayout.addWidget(self.label_10, 0, 0, 1, 1)

        self.r1_c = QCheckBox(self.layoutWidget)
        self.r1_c.setObjectName(u"r1_c")
        self.r1_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.r1_c, 1, 3, 1, 1)

        self.label_5 = QLabel(self.layoutWidget)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.label_5, 0, 10, 1, 1)

        self.vz_c = QCheckBox(self.layoutWidget)
        self.vz_c.setObjectName(u"vz_c")
        self.vz_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.vz_c, 3, 7, 1, 1)

        self.vx_c = QCheckBox(self.layoutWidget)
        self.vx_c.setObjectName(u"vx_c")
        self.vx_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.vx_c, 1, 7, 1, 1)

        self.x_c = QCheckBox(self.layoutWidget)
        self.x_c.setObjectName(u"x_c")
        self.x_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.x_c, 1, 0, 1, 1)

        self.label_58 = QLabel(self.layoutWidget)
        self.label_58.setObjectName(u"label_58")

        self.gridLayout.addWidget(self.label_58, 0, 5, 1, 1)

        self.vy_c = QCheckBox(self.layoutWidget)
        self.vy_c.setObjectName(u"vy_c")
        self.vy_c.setStyleSheet(u"border-width: 0;")

        self.gridLayout.addWidget(self.vy_c, 2, 7, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.verticalSpacer, 5, 10, 1, 1)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.home_x_c = QCheckBox(self.layoutWidget)
        self.home_x_c.setObjectName(u"home_x_c")

        self.horizontalLayout_4.addWidget(self.home_x_c)


        self.gridLayout.addLayout(self.horizontalLayout_4, 1, 1, 1, 1)

        self.horizontalLayout_5 = QHBoxLayout()
        self.horizontalLayout_5.setObjectName(u"horizontalLayout_5")
        self.home_y_c = QCheckBox(self.layoutWidget)
        self.home_y_c.setObjectName(u"home_y_c")

        self.horizontalLayout_5.addWidget(self.home_y_c)


        self.gridLayout.addLayout(self.horizontalLayout_5, 2, 1, 1, 1)

        self.horizontalLayout_6 = QHBoxLayout()
        self.horizontalLayout_6.setObjectName(u"horizontalLayout_6")
        self.home_z_c = QCheckBox(self.layoutWidget)
        self.home_z_c.setObjectName(u"home_z_c")

        self.horizontalLayout_6.addWidget(self.home_z_c)


        self.gridLayout.addLayout(self.horizontalLayout_6, 3, 1, 1, 1)

        self.horizontalLayout_11 = QHBoxLayout()
        self.horizontalLayout_11.setObjectName(u"horizontalLayout_11")
        self.home_r1_c = QCheckBox(self.layoutWidget)
        self.home_r1_c.setObjectName(u"home_r1_c")

        self.horizontalLayout_11.addWidget(self.home_r1_c)


        self.gridLayout.addLayout(self.horizontalLayout_11, 1, 5, 1, 1)

        self.horizontalLayout_12 = QHBoxLayout()
        self.horizontalLayout_12.setObjectName(u"horizontalLayout_12")
        self.home_r2_c = QCheckBox(self.layoutWidget)
        self.home_r2_c.setObjectName(u"home_r2_c")

        self.horizontalLayout_12.addWidget(self.home_r2_c)


        self.gridLayout.addLayout(self.horizontalLayout_12, 2, 5, 1, 1)

        self.horizontalLayout_13 = QHBoxLayout()
        self.horizontalLayout_13.setObjectName(u"horizontalLayout_13")
        self.home_r3_c = QCheckBox(self.layoutWidget)
        self.home_r3_c.setObjectName(u"home_r3_c")

        self.horizontalLayout_13.addWidget(self.home_r3_c)


        self.gridLayout.addLayout(self.horizontalLayout_13, 3, 5, 1, 1)

        self.horizontalLayout_14 = QHBoxLayout()
        self.horizontalLayout_14.setObjectName(u"horizontalLayout_14")
        self.home_vx_c = QCheckBox(self.layoutWidget)
        self.home_vx_c.setObjectName(u"home_vx_c")

        self.horizontalLayout_14.addWidget(self.home_vx_c)

        self.home_vx_a_c = QCheckBox(self.layoutWidget)
        self.home_vx_a_c.setObjectName(u"home_vx_a_c")

        self.horizontalLayout_14.addWidget(self.home_vx_a_c)


        self.gridLayout.addLayout(self.horizontalLayout_14, 1, 8, 1, 1)

        self.horizontalLayout_15 = QHBoxLayout()
        self.horizontalLayout_15.setObjectName(u"horizontalLayout_15")
        self.home_vy_c = QCheckBox(self.layoutWidget)
        self.home_vy_c.setObjectName(u"home_vy_c")

        self.horizontalLayout_15.addWidget(self.home_vy_c)

        self.home_vy_a_c = QCheckBox(self.layoutWidget)
        self.home_vy_a_c.setObjectName(u"home_vy_a_c")

        self.horizontalLayout_15.addWidget(self.home_vy_a_c)


        self.gridLayout.addLayout(self.horizontalLayout_15, 2, 8, 1, 1)

        self.horizontalLayout_16 = QHBoxLayout()
        self.horizontalLayout_16.setObjectName(u"horizontalLayout_16")
        self.home_vz_c = QCheckBox(self.layoutWidget)
        self.home_vz_c.setObjectName(u"home_vz_c")

        self.horizontalLayout_16.addWidget(self.home_vz_c)

        self.home_vz_a_c = QCheckBox(self.layoutWidget)
        self.home_vz_a_c.setObjectName(u"home_vz_a_c")

        self.horizontalLayout_16.addWidget(self.home_vz_a_c)


        self.gridLayout.addLayout(self.horizontalLayout_16, 3, 8, 1, 1)


        self.verticalLayout_5.addLayout(self.gridLayout)


        self.horizontalLayout.addWidget(self.frame_5)

        self.frame_6 = QFrame(self.horizontalLayoutWidget)
        self.frame_6.setObjectName(u"frame_6")
        self.frame_6.setMinimumSize(QSize(242, 0))
        self.frame_6.setMaximumSize(QSize(432, 149))
        self.frame_6.setFrameShape(QFrame.Box)
        self.frame_6.setFrameShadow(QFrame.Plain)
        self.gridLayoutWidget = QWidget(self.frame_6)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(5, 5, 251, 141))
        self.gridLayout_6 = QGridLayout(self.gridLayoutWidget)
        self.gridLayout_6.setSpacing(0)
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.gridLayout_6.setContentsMargins(0, 0, 0, 0)
        self.intTime_edit = QLineEdit(self.gridLayoutWidget)
        self.intTime_edit.setObjectName(u"intTime_edit")
        self.intTime_edit.setMaximumSize(QSize(40, 16777215))

        self.gridLayout_6.addWidget(self.intTime_edit, 4, 1, 1, 1)

        self.int_time_s_l = QLabel(self.gridLayoutWidget)
        self.int_time_s_l.setObjectName(u"int_time_s_l")

        self.gridLayout_6.addWidget(self.int_time_s_l, 4, 2, 1, 1)

        self.intTime_l = QLabel(self.gridLayoutWidget)
        self.intTime_l.setObjectName(u"intTime_l")

        self.gridLayout_6.addWidget(self.intTime_l, 4, 0, 1, 1)

        self.nofm_spin = QSpinBox(self.gridLayoutWidget)
        self.nofm_spin.setObjectName(u"nofm_spin")
        self.nofm_spin.setMaximumSize(QSize(55, 16777215))
        self.nofm_spin.setMaximum(999)

        self.gridLayout_6.addWidget(self.nofm_spin, 5, 1, 1, 1)

        self.label_54 = QLabel(self.gridLayoutWidget)
        self.label_54.setObjectName(u"label_54")
        sizePolicy3 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.label_54.sizePolicy().hasHeightForWidth())
        self.label_54.setSizePolicy(sizePolicy3)
        self.label_54.setMinimumSize(QSize(0, 0))
        self.label_54.setFont(font2)

        self.gridLayout_6.addWidget(self.label_54, 0, 0, 1, 1)

        self.label_95 = QLabel(self.gridLayoutWidget)
        self.label_95.setObjectName(u"label_95")
        self.label_95.setFont(font2)

        self.gridLayout_6.addWidget(self.label_95, 2, 0, 1, 1)

        self.early_time_l = QLabel(self.gridLayoutWidget)
        self.early_time_l.setObjectName(u"early_time_l")
        font3 = QFont()
        font3.setPointSize(12)
        self.early_time_l.setFont(font3)

        self.gridLayout_6.addWidget(self.early_time_l, 2, 1, 1, 1)

        self.SST_edit = QLineEdit(self.gridLayoutWidget)
        self.SST_edit.setObjectName(u"SST_edit")
        sizePolicy2.setHeightForWidth(self.SST_edit.sizePolicy().hasHeightForWidth())
        self.SST_edit.setSizePolicy(sizePolicy2)
        self.SST_edit.setMinimumSize(QSize(10, 0))
        self.SST_edit.setMaximumSize(QSize(40, 16777215))

        self.gridLayout_6.addWidget(self.SST_edit, 0, 1, 1, 1)

        self.label_3 = QLabel(self.gridLayoutWidget)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setMinimumSize(QSize(0, 0))
        self.label_3.setMaximumSize(QSize(16777215, 22))
        self.label_3.setFont(font2)

        self.gridLayout_6.addWidget(self.label_3, 1, 0, 1, 1)

        self.nofm_l = QLabel(self.gridLayoutWidget)
        self.nofm_l.setObjectName(u"nofm_l")

        self.gridLayout_6.addWidget(self.nofm_l, 5, 0, 1, 1)

        self.min_time_l = QLabel(self.gridLayoutWidget)
        self.min_time_l.setObjectName(u"min_time_l")
        self.min_time_l.setMinimumSize(QSize(57, 0))
        self.min_time_l.setFont(font3)

        self.gridLayout_6.addWidget(self.min_time_l, 1, 1, 1, 1)

        self.label_55 = QLabel(self.gridLayoutWidget)
        self.label_55.setObjectName(u"label_55")

        self.gridLayout_6.addWidget(self.label_55, 0, 2, 1, 1)

        self.amp_c = QCheckBox(self.gridLayoutWidget)
        self.amp_c.setObjectName(u"amp_c")

        self.gridLayout_6.addWidget(self.amp_c, 3, 0, 1, 1)


        self.horizontalLayout.addWidget(self.frame_6)


        self.verticalLayout_2.addWidget(self.frame_2)

        self.err_l = QLabel(self.frame_3)
        self.err_l.setObjectName(u"err_l")
        self.err_l.setGeometry(QRect(785, 20, 391, 61))
        self.err_l.setFont(font3)
        self.err_l.setStyleSheet(u"color : red;")
        self.err_l.setWordWrap(True)
        self.run_btn = QPushButton(self.frame_3)
        self.run_btn.setObjectName(u"run_btn")
        self.run_btn.setGeometry(QRect(505, 720, 200, 40))
        font4 = QFont()
        font4.setPointSize(16)
        font4.setBold(True)
        self.run_btn.setFont(font4)
        self.run_btn.setCursor(QCursor(Qt.PointingHandCursor))
        self.wait_l = QLabel(self.frame_3)
        self.wait_l.setObjectName(u"wait_l")
        self.wait_l.setEnabled(True)
        self.wait_l.setGeometry(QRect(20, 310, 201, 26))
        self.wait_l.setFont(font1)
        self.wait_inp = QLineEdit(self.frame_3)
        self.wait_inp.setObjectName(u"wait_inp")
        self.wait_inp.setGeometry(QRect(22, 340, 81, 22))
        self.dummy_PS_c = QCheckBox(self.frame_3)
        self.dummy_PS_c.setObjectName(u"dummy_PS_c")
        self.dummy_PS_c.setGeometry(QRect(30, 750, 141, 20))
        self.scrollArea_2 = QScrollArea(self.frame_3)
        self.scrollArea_2.setObjectName(u"scrollArea_2")
        self.scrollArea_2.setGeometry(QRect(20, 290, 271, 421))
        self.scrollArea_2.setLayoutDirection(Qt.LeftToRight)
        self.scrollArea_2.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scrollArea_2.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollArea_2.setWidgetResizable(False)
        self.scrollAreaWidgetContents_2 = QWidget()
        self.scrollAreaWidgetContents_2.setObjectName(u"scrollAreaWidgetContents_2")
        self.scrollAreaWidgetContents_2.setGeometry(QRect(0, 0, 1179, 405))
        self.verticalLayoutWidget_3 = QWidget(self.scrollAreaWidgetContents_2)
        self.verticalLayoutWidget_3.setObjectName(u"verticalLayoutWidget_3")
        self.verticalLayoutWidget_3.setEnabled(True)
        self.verticalLayoutWidget_3.setGeometry(QRect(6, 0, 1357, 401))
        self.verticalLayout_7 = QVBoxLayout(self.verticalLayoutWidget_3)
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.verticalLayout_7.setContentsMargins(0, 0, 0, 0)
        self.label_6 = QLabel(self.verticalLayoutWidget_3)
        self.label_6.setObjectName(u"label_6")
        sizePolicy2.setHeightForWidth(self.label_6.sizePolicy().hasHeightForWidth())
        self.label_6.setSizePolicy(sizePolicy2)
        self.label_6.setFont(font1)

        self.verticalLayout_7.addWidget(self.label_6)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.totalsteps_text_l = QLabel(self.verticalLayoutWidget_3)
        self.totalsteps_text_l.setObjectName(u"totalsteps_text_l")
        self.totalsteps_text_l.setMinimumSize(QSize(100, 0))
        self.totalsteps_text_l.setMaximumSize(QSize(300, 22))
        self.totalsteps_text_l.setFont(font3)

        self.horizontalLayout_3.addWidget(self.totalsteps_text_l)

        self.totalsteps_l = QLabel(self.verticalLayoutWidget_3)
        self.totalsteps_l.setObjectName(u"totalsteps_l")
        self.totalsteps_l.setFont(font3)

        self.horizontalLayout_3.addWidget(self.totalsteps_l)

        self.horizontalSpacer_5 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_5)


        self.verticalLayout_7.addLayout(self.horizontalLayout_3)

        self.label_7 = QLabel(self.verticalLayoutWidget_3)
        self.label_7.setObjectName(u"label_7")
        sizePolicy2.setHeightForWidth(self.label_7.sizePolicy().hasHeightForWidth())
        self.label_7.setSizePolicy(sizePolicy2)
        self.label_7.setFont(font3)

        self.verticalLayout_7.addWidget(self.label_7)

        self.horizontalLayout_7 = QHBoxLayout()
        self.horizontalLayout_7.setObjectName(u"horizontalLayout_7")
        self.nostart_l = QLabel(self.verticalLayoutWidget_3)
        self.nostart_l.setObjectName(u"nostart_l")

        self.horizontalLayout_7.addWidget(self.nostart_l)

        self.x_start_frame = QFrame(self.verticalLayoutWidget_3)
        self.x_start_frame.setObjectName(u"x_start_frame")
        self.x_start_frame.setEnabled(True)
        sizePolicy.setHeightForWidth(self.x_start_frame.sizePolicy().hasHeightForWidth())
        self.x_start_frame.setSizePolicy(sizePolicy)
        self.x_start_frame.setMinimumSize(QSize(83, 44))
        self.x_start_frame.setFrameShape(QFrame.StyledPanel)
        self.x_start_frame.setFrameShadow(QFrame.Raised)
        self.label_8 = QLabel(self.x_start_frame)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setGeometry(QRect(0, 0, 83, 16))
        self.label_8.setAlignment(Qt.AlignHCenter|Qt.AlignTop)
        self.x_start_inp = QLineEdit(self.x_start_frame)
        self.x_start_inp.setObjectName(u"x_start_inp")
        self.x_start_inp.setGeometry(QRect(0, 22, 83, 22))
        self.x_start_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.x_start_frame)

        self.y_start_frame = QFrame(self.verticalLayoutWidget_3)
        self.y_start_frame.setObjectName(u"y_start_frame")
        sizePolicy.setHeightForWidth(self.y_start_frame.sizePolicy().hasHeightForWidth())
        self.y_start_frame.setSizePolicy(sizePolicy)
        self.y_start_frame.setMinimumSize(QSize(83, 44))
        self.y_start_frame.setFrameShape(QFrame.StyledPanel)
        self.y_start_frame.setFrameShadow(QFrame.Raised)
        self.label_9 = QLabel(self.y_start_frame)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setGeometry(QRect(0, 0, 83, 16))
        self.label_9.setAlignment(Qt.AlignCenter)
        self.y_start_inp = QLineEdit(self.y_start_frame)
        self.y_start_inp.setObjectName(u"y_start_inp")
        self.y_start_inp.setGeometry(QRect(0, 22, 83, 22))
        self.y_start_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.y_start_frame)

        self.z_start_frame = QFrame(self.verticalLayoutWidget_3)
        self.z_start_frame.setObjectName(u"z_start_frame")
        sizePolicy.setHeightForWidth(self.z_start_frame.sizePolicy().hasHeightForWidth())
        self.z_start_frame.setSizePolicy(sizePolicy)
        self.z_start_frame.setMinimumSize(QSize(83, 44))
        self.z_start_frame.setFrameShape(QFrame.StyledPanel)
        self.z_start_frame.setFrameShadow(QFrame.Raised)
        self.z_start_inp = QLineEdit(self.z_start_frame)
        self.z_start_inp.setObjectName(u"z_start_inp")
        self.z_start_inp.setGeometry(QRect(0, 22, 83, 22))
        self.z_start_inp.setAlignment(Qt.AlignCenter)
        self.label_11 = QLabel(self.z_start_frame)
        self.label_11.setObjectName(u"label_11")
        self.label_11.setGeometry(QRect(0, 0, 83, 16))
        self.label_11.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.z_start_frame)

        self.r1_start_frame = QFrame(self.verticalLayoutWidget_3)
        self.r1_start_frame.setObjectName(u"r1_start_frame")
        sizePolicy.setHeightForWidth(self.r1_start_frame.sizePolicy().hasHeightForWidth())
        self.r1_start_frame.setSizePolicy(sizePolicy)
        self.r1_start_frame.setMinimumSize(QSize(83, 44))
        self.r1_start_frame.setFrameShape(QFrame.StyledPanel)
        self.r1_start_frame.setFrameShadow(QFrame.Raised)
        self.r1_start_inp = QLineEdit(self.r1_start_frame)
        self.r1_start_inp.setObjectName(u"r1_start_inp")
        self.r1_start_inp.setGeometry(QRect(0, 22, 83, 22))
        self.r1_start_inp.setAlignment(Qt.AlignCenter)
        self.label_12 = QLabel(self.r1_start_frame)
        self.label_12.setObjectName(u"label_12")
        self.label_12.setGeometry(QRect(0, 0, 83, 16))
        self.label_12.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.r1_start_frame)

        self.r2_start_frame = QFrame(self.verticalLayoutWidget_3)
        self.r2_start_frame.setObjectName(u"r2_start_frame")
        sizePolicy.setHeightForWidth(self.r2_start_frame.sizePolicy().hasHeightForWidth())
        self.r2_start_frame.setSizePolicy(sizePolicy)
        self.r2_start_frame.setMinimumSize(QSize(83, 44))
        self.r2_start_frame.setFrameShape(QFrame.StyledPanel)
        self.r2_start_frame.setFrameShadow(QFrame.Raised)
        self.label_13 = QLabel(self.r2_start_frame)
        self.label_13.setObjectName(u"label_13")
        self.label_13.setGeometry(QRect(0, 0, 83, 16))
        self.label_13.setAlignment(Qt.AlignCenter)
        self.r2_start_inp = QLineEdit(self.r2_start_frame)
        self.r2_start_inp.setObjectName(u"r2_start_inp")
        self.r2_start_inp.setGeometry(QRect(0, 22, 83, 22))
        self.r2_start_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.r2_start_frame)

        self.r3_start_frame = QFrame(self.verticalLayoutWidget_3)
        self.r3_start_frame.setObjectName(u"r3_start_frame")
        sizePolicy.setHeightForWidth(self.r3_start_frame.sizePolicy().hasHeightForWidth())
        self.r3_start_frame.setSizePolicy(sizePolicy)
        self.r3_start_frame.setMinimumSize(QSize(83, 44))
        self.r3_start_frame.setFrameShape(QFrame.StyledPanel)
        self.r3_start_frame.setFrameShadow(QFrame.Raised)
        self.r3_start_inp = QLineEdit(self.r3_start_frame)
        self.r3_start_inp.setObjectName(u"r3_start_inp")
        self.r3_start_inp.setGeometry(QRect(0, 22, 83, 22))
        self.r3_start_inp.setAlignment(Qt.AlignCenter)
        self.label_14 = QLabel(self.r3_start_frame)
        self.label_14.setObjectName(u"label_14")
        self.label_14.setGeometry(QRect(0, 0, 83, 16))
        self.label_14.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.r3_start_frame)

        self.vx_start_frame = QFrame(self.verticalLayoutWidget_3)
        self.vx_start_frame.setObjectName(u"vx_start_frame")
        sizePolicy.setHeightForWidth(self.vx_start_frame.sizePolicy().hasHeightForWidth())
        self.vx_start_frame.setSizePolicy(sizePolicy)
        self.vx_start_frame.setMinimumSize(QSize(83, 44))
        self.vx_start_frame.setFrameShape(QFrame.StyledPanel)
        self.vx_start_frame.setFrameShadow(QFrame.Raised)
        self.vx_start_inp = QLineEdit(self.vx_start_frame)
        self.vx_start_inp.setObjectName(u"vx_start_inp")
        self.vx_start_inp.setGeometry(QRect(0, 22, 83, 22))
        self.vx_start_inp.setAlignment(Qt.AlignCenter)
        self.label_15 = QLabel(self.vx_start_frame)
        self.label_15.setObjectName(u"label_15")
        self.label_15.setGeometry(QRect(0, 0, 83, 16))
        self.label_15.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.vx_start_frame)

        self.vy_start_frame = QFrame(self.verticalLayoutWidget_3)
        self.vy_start_frame.setObjectName(u"vy_start_frame")
        sizePolicy.setHeightForWidth(self.vy_start_frame.sizePolicy().hasHeightForWidth())
        self.vy_start_frame.setSizePolicy(sizePolicy)
        self.vy_start_frame.setMinimumSize(QSize(83, 44))
        self.vy_start_frame.setFrameShape(QFrame.StyledPanel)
        self.vy_start_frame.setFrameShadow(QFrame.Raised)
        self.vy_start_inp = QLineEdit(self.vy_start_frame)
        self.vy_start_inp.setObjectName(u"vy_start_inp")
        self.vy_start_inp.setGeometry(QRect(0, 22, 83, 22))
        self.vy_start_inp.setAlignment(Qt.AlignCenter)
        self.label_16 = QLabel(self.vy_start_frame)
        self.label_16.setObjectName(u"label_16")
        self.label_16.setGeometry(QRect(0, 0, 83, 16))
        self.label_16.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.vy_start_frame)

        self.vz_start_frame = QFrame(self.verticalLayoutWidget_3)
        self.vz_start_frame.setObjectName(u"vz_start_frame")
        sizePolicy.setHeightForWidth(self.vz_start_frame.sizePolicy().hasHeightForWidth())
        self.vz_start_frame.setSizePolicy(sizePolicy)
        self.vz_start_frame.setMinimumSize(QSize(83, 44))
        self.vz_start_frame.setFrameShape(QFrame.StyledPanel)
        self.vz_start_frame.setFrameShadow(QFrame.Raised)
        self.vz_start_inp = QLineEdit(self.vz_start_frame)
        self.vz_start_inp.setObjectName(u"vz_start_inp")
        self.vz_start_inp.setGeometry(QRect(0, 22, 83, 22))
        self.vz_start_inp.setAlignment(Qt.AlignCenter)
        self.label_17 = QLabel(self.vz_start_frame)
        self.label_17.setObjectName(u"label_17")
        self.label_17.setGeometry(QRect(0, 0, 83, 16))
        self.label_17.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.vz_start_frame)

        self.s0_start_frame = QFrame(self.verticalLayoutWidget_3)
        self.s0_start_frame.setObjectName(u"s0_start_frame")
        sizePolicy.setHeightForWidth(self.s0_start_frame.sizePolicy().hasHeightForWidth())
        self.s0_start_frame.setSizePolicy(sizePolicy)
        self.s0_start_frame.setMinimumSize(QSize(83, 44))
        self.s0_start_frame.setFrameShape(QFrame.StyledPanel)
        self.s0_start_frame.setFrameShadow(QFrame.Raised)
        self.s0_start_inp = QLineEdit(self.s0_start_frame)
        self.s0_start_inp.setObjectName(u"s0_start_inp")
        self.s0_start_inp.setGeometry(QRect(0, 22, 83, 22))
        self.s0_start_inp.setAlignment(Qt.AlignCenter)
        self.label_18 = QLabel(self.s0_start_frame)
        self.label_18.setObjectName(u"label_18")
        self.label_18.setGeometry(QRect(0, 0, 83, 16))
        self.label_18.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.s0_start_frame)

        self.s1_start_frame = QFrame(self.verticalLayoutWidget_3)
        self.s1_start_frame.setObjectName(u"s1_start_frame")
        sizePolicy.setHeightForWidth(self.s1_start_frame.sizePolicy().hasHeightForWidth())
        self.s1_start_frame.setSizePolicy(sizePolicy)
        self.s1_start_frame.setMinimumSize(QSize(83, 44))
        self.s1_start_frame.setFrameShape(QFrame.StyledPanel)
        self.s1_start_frame.setFrameShadow(QFrame.Raised)
        self.s1_start_inp = QLineEdit(self.s1_start_frame)
        self.s1_start_inp.setObjectName(u"s1_start_inp")
        self.s1_start_inp.setGeometry(QRect(0, 22, 83, 22))
        self.s1_start_inp.setAlignment(Qt.AlignCenter)
        self.label_19 = QLabel(self.s1_start_frame)
        self.label_19.setObjectName(u"label_19")
        self.label_19.setGeometry(QRect(0, 0, 83, 16))
        self.label_19.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.s1_start_frame)

        self.s2_start_frame = QFrame(self.verticalLayoutWidget_3)
        self.s2_start_frame.setObjectName(u"s2_start_frame")
        sizePolicy.setHeightForWidth(self.s2_start_frame.sizePolicy().hasHeightForWidth())
        self.s2_start_frame.setSizePolicy(sizePolicy)
        self.s2_start_frame.setMinimumSize(QSize(83, 44))
        self.s2_start_frame.setFrameShape(QFrame.StyledPanel)
        self.s2_start_frame.setFrameShadow(QFrame.Raised)
        self.s2_start_inp = QLineEdit(self.s2_start_frame)
        self.s2_start_inp.setObjectName(u"s2_start_inp")
        self.s2_start_inp.setGeometry(QRect(0, 22, 83, 22))
        self.s2_start_inp.setAlignment(Qt.AlignCenter)
        self.label_20 = QLabel(self.s2_start_frame)
        self.label_20.setObjectName(u"label_20")
        self.label_20.setGeometry(QRect(0, 0, 83, 16))
        self.label_20.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.s2_start_frame)

        self.s3_start_frame = QFrame(self.verticalLayoutWidget_3)
        self.s3_start_frame.setObjectName(u"s3_start_frame")
        sizePolicy.setHeightForWidth(self.s3_start_frame.sizePolicy().hasHeightForWidth())
        self.s3_start_frame.setSizePolicy(sizePolicy)
        self.s3_start_frame.setMinimumSize(QSize(83, 44))
        self.s3_start_frame.setFrameShape(QFrame.StyledPanel)
        self.s3_start_frame.setFrameShadow(QFrame.Raised)
        self.s3_start_inp = QLineEdit(self.s3_start_frame)
        self.s3_start_inp.setObjectName(u"s3_start_inp")
        self.s3_start_inp.setGeometry(QRect(0, 22, 83, 22))
        self.s3_start_inp.setAlignment(Qt.AlignCenter)
        self.label_21 = QLabel(self.s3_start_frame)
        self.label_21.setObjectName(u"label_21")
        self.label_21.setGeometry(QRect(0, 0, 83, 16))
        self.label_21.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_7.addWidget(self.s3_start_frame)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_7.addItem(self.horizontalSpacer_2)


        self.verticalLayout_7.addLayout(self.horizontalLayout_7)

        self.label_22 = QLabel(self.verticalLayoutWidget_3)
        self.label_22.setObjectName(u"label_22")
        self.label_22.setFont(font3)

        self.verticalLayout_7.addWidget(self.label_22)

        self.horizontalLayout_8 = QHBoxLayout()
        self.horizontalLayout_8.setObjectName(u"horizontalLayout_8")
        self.nostop_l = QLabel(self.verticalLayoutWidget_3)
        self.nostop_l.setObjectName(u"nostop_l")

        self.horizontalLayout_8.addWidget(self.nostop_l)

        self.x_stop_frame = QFrame(self.verticalLayoutWidget_3)
        self.x_stop_frame.setObjectName(u"x_stop_frame")
        self.x_stop_frame.setEnabled(True)
        sizePolicy.setHeightForWidth(self.x_stop_frame.sizePolicy().hasHeightForWidth())
        self.x_stop_frame.setSizePolicy(sizePolicy)
        self.x_stop_frame.setMinimumSize(QSize(83, 44))
        self.x_stop_frame.setFrameShape(QFrame.StyledPanel)
        self.x_stop_frame.setFrameShadow(QFrame.Raised)
        self.label_23 = QLabel(self.x_stop_frame)
        self.label_23.setObjectName(u"label_23")
        self.label_23.setGeometry(QRect(0, 0, 83, 16))
        self.label_23.setAlignment(Qt.AlignCenter)
        self.x_stop_inp = QLineEdit(self.x_stop_frame)
        self.x_stop_inp.setObjectName(u"x_stop_inp")
        self.x_stop_inp.setGeometry(QRect(0, 22, 83, 22))
        self.x_stop_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_8.addWidget(self.x_stop_frame)

        self.y_stop_frame = QFrame(self.verticalLayoutWidget_3)
        self.y_stop_frame.setObjectName(u"y_stop_frame")
        sizePolicy.setHeightForWidth(self.y_stop_frame.sizePolicy().hasHeightForWidth())
        self.y_stop_frame.setSizePolicy(sizePolicy)
        self.y_stop_frame.setMinimumSize(QSize(83, 44))
        self.y_stop_frame.setFrameShape(QFrame.StyledPanel)
        self.y_stop_frame.setFrameShadow(QFrame.Raised)
        self.label_24 = QLabel(self.y_stop_frame)
        self.label_24.setObjectName(u"label_24")
        self.label_24.setGeometry(QRect(0, 0, 83, 16))
        self.label_24.setAlignment(Qt.AlignCenter)
        self.y_stop_inp = QLineEdit(self.y_stop_frame)
        self.y_stop_inp.setObjectName(u"y_stop_inp")
        self.y_stop_inp.setGeometry(QRect(0, 22, 83, 22))
        self.y_stop_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_8.addWidget(self.y_stop_frame)

        self.z_stop_frame = QFrame(self.verticalLayoutWidget_3)
        self.z_stop_frame.setObjectName(u"z_stop_frame")
        sizePolicy.setHeightForWidth(self.z_stop_frame.sizePolicy().hasHeightForWidth())
        self.z_stop_frame.setSizePolicy(sizePolicy)
        self.z_stop_frame.setMinimumSize(QSize(83, 44))
        self.z_stop_frame.setFrameShape(QFrame.StyledPanel)
        self.z_stop_frame.setFrameShadow(QFrame.Raised)
        self.label_25 = QLabel(self.z_stop_frame)
        self.label_25.setObjectName(u"label_25")
        self.label_25.setGeometry(QRect(0, 0, 83, 16))
        self.label_25.setAlignment(Qt.AlignCenter)
        self.z_stop_inp = QLineEdit(self.z_stop_frame)
        self.z_stop_inp.setObjectName(u"z_stop_inp")
        self.z_stop_inp.setGeometry(QRect(0, 22, 83, 22))
        self.z_stop_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_8.addWidget(self.z_stop_frame)

        self.r1_stop_frame = QFrame(self.verticalLayoutWidget_3)
        self.r1_stop_frame.setObjectName(u"r1_stop_frame")
        sizePolicy.setHeightForWidth(self.r1_stop_frame.sizePolicy().hasHeightForWidth())
        self.r1_stop_frame.setSizePolicy(sizePolicy)
        self.r1_stop_frame.setMinimumSize(QSize(83, 44))
        self.r1_stop_frame.setFrameShape(QFrame.StyledPanel)
        self.r1_stop_frame.setFrameShadow(QFrame.Raised)
        self.label_26 = QLabel(self.r1_stop_frame)
        self.label_26.setObjectName(u"label_26")
        self.label_26.setGeometry(QRect(0, 0, 83, 16))
        self.label_26.setAlignment(Qt.AlignCenter)
        self.r1_stop_inp = QLineEdit(self.r1_stop_frame)
        self.r1_stop_inp.setObjectName(u"r1_stop_inp")
        self.r1_stop_inp.setGeometry(QRect(0, 22, 83, 22))
        self.r1_stop_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_8.addWidget(self.r1_stop_frame)

        self.r2_stop_frame = QFrame(self.verticalLayoutWidget_3)
        self.r2_stop_frame.setObjectName(u"r2_stop_frame")
        sizePolicy.setHeightForWidth(self.r2_stop_frame.sizePolicy().hasHeightForWidth())
        self.r2_stop_frame.setSizePolicy(sizePolicy)
        self.r2_stop_frame.setMinimumSize(QSize(83, 44))
        self.r2_stop_frame.setFrameShape(QFrame.StyledPanel)
        self.r2_stop_frame.setFrameShadow(QFrame.Raised)
        self.label_27 = QLabel(self.r2_stop_frame)
        self.label_27.setObjectName(u"label_27")
        self.label_27.setGeometry(QRect(0, 0, 83, 16))
        self.label_27.setAlignment(Qt.AlignCenter)
        self.r2_stop_inp = QLineEdit(self.r2_stop_frame)
        self.r2_stop_inp.setObjectName(u"r2_stop_inp")
        self.r2_stop_inp.setGeometry(QRect(0, 22, 83, 22))
        self.r2_stop_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_8.addWidget(self.r2_stop_frame)

        self.r3_stop_frame = QFrame(self.verticalLayoutWidget_3)
        self.r3_stop_frame.setObjectName(u"r3_stop_frame")
        sizePolicy.setHeightForWidth(self.r3_stop_frame.sizePolicy().hasHeightForWidth())
        self.r3_stop_frame.setSizePolicy(sizePolicy)
        self.r3_stop_frame.setMinimumSize(QSize(83, 44))
        self.r3_stop_frame.setFrameShape(QFrame.StyledPanel)
        self.r3_stop_frame.setFrameShadow(QFrame.Raised)
        self.label_28 = QLabel(self.r3_stop_frame)
        self.label_28.setObjectName(u"label_28")
        self.label_28.setGeometry(QRect(0, 0, 83, 16))
        self.label_28.setAlignment(Qt.AlignCenter)
        self.r3_stop_inp = QLineEdit(self.r3_stop_frame)
        self.r3_stop_inp.setObjectName(u"r3_stop_inp")
        self.r3_stop_inp.setGeometry(QRect(0, 22, 83, 22))
        self.r3_stop_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_8.addWidget(self.r3_stop_frame)

        self.vx_stop_frame = QFrame(self.verticalLayoutWidget_3)
        self.vx_stop_frame.setObjectName(u"vx_stop_frame")
        sizePolicy.setHeightForWidth(self.vx_stop_frame.sizePolicy().hasHeightForWidth())
        self.vx_stop_frame.setSizePolicy(sizePolicy)
        self.vx_stop_frame.setMinimumSize(QSize(83, 44))
        self.vx_stop_frame.setFrameShape(QFrame.StyledPanel)
        self.vx_stop_frame.setFrameShadow(QFrame.Raised)
        self.label_29 = QLabel(self.vx_stop_frame)
        self.label_29.setObjectName(u"label_29")
        self.label_29.setGeometry(QRect(0, 0, 83, 16))
        self.label_29.setAlignment(Qt.AlignCenter)
        self.vx_stop_inp = QLineEdit(self.vx_stop_frame)
        self.vx_stop_inp.setObjectName(u"vx_stop_inp")
        self.vx_stop_inp.setGeometry(QRect(0, 22, 83, 22))
        self.vx_stop_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_8.addWidget(self.vx_stop_frame)

        self.vy_stop_frame = QFrame(self.verticalLayoutWidget_3)
        self.vy_stop_frame.setObjectName(u"vy_stop_frame")
        sizePolicy.setHeightForWidth(self.vy_stop_frame.sizePolicy().hasHeightForWidth())
        self.vy_stop_frame.setSizePolicy(sizePolicy)
        self.vy_stop_frame.setMinimumSize(QSize(83, 44))
        self.vy_stop_frame.setFrameShape(QFrame.StyledPanel)
        self.vy_stop_frame.setFrameShadow(QFrame.Raised)
        self.label_30 = QLabel(self.vy_stop_frame)
        self.label_30.setObjectName(u"label_30")
        self.label_30.setGeometry(QRect(0, 0, 83, 16))
        self.label_30.setAlignment(Qt.AlignCenter)
        self.vy_stop_inp = QLineEdit(self.vy_stop_frame)
        self.vy_stop_inp.setObjectName(u"vy_stop_inp")
        self.vy_stop_inp.setGeometry(QRect(0, 22, 83, 22))
        self.vy_stop_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_8.addWidget(self.vy_stop_frame)

        self.vz_stop_frame = QFrame(self.verticalLayoutWidget_3)
        self.vz_stop_frame.setObjectName(u"vz_stop_frame")
        sizePolicy.setHeightForWidth(self.vz_stop_frame.sizePolicy().hasHeightForWidth())
        self.vz_stop_frame.setSizePolicy(sizePolicy)
        self.vz_stop_frame.setMinimumSize(QSize(83, 44))
        self.vz_stop_frame.setFrameShape(QFrame.StyledPanel)
        self.vz_stop_frame.setFrameShadow(QFrame.Raised)
        self.label_31 = QLabel(self.vz_stop_frame)
        self.label_31.setObjectName(u"label_31")
        self.label_31.setGeometry(QRect(0, 0, 83, 16))
        self.label_31.setAlignment(Qt.AlignCenter)
        self.vz_stop_inp = QLineEdit(self.vz_stop_frame)
        self.vz_stop_inp.setObjectName(u"vz_stop_inp")
        self.vz_stop_inp.setGeometry(QRect(0, 22, 83, 22))
        self.vz_stop_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_8.addWidget(self.vz_stop_frame)

        self.s0_stop_frame = QFrame(self.verticalLayoutWidget_3)
        self.s0_stop_frame.setObjectName(u"s0_stop_frame")
        sizePolicy.setHeightForWidth(self.s0_stop_frame.sizePolicy().hasHeightForWidth())
        self.s0_stop_frame.setSizePolicy(sizePolicy)
        self.s0_stop_frame.setMinimumSize(QSize(83, 44))
        self.s0_stop_frame.setFrameShape(QFrame.StyledPanel)
        self.s0_stop_frame.setFrameShadow(QFrame.Raised)
        self.label_32 = QLabel(self.s0_stop_frame)
        self.label_32.setObjectName(u"label_32")
        self.label_32.setGeometry(QRect(0, 0, 83, 16))
        self.label_32.setAlignment(Qt.AlignCenter)
        self.s0_stop_inp = QLineEdit(self.s0_stop_frame)
        self.s0_stop_inp.setObjectName(u"s0_stop_inp")
        self.s0_stop_inp.setGeometry(QRect(0, 22, 83, 22))
        self.s0_stop_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_8.addWidget(self.s0_stop_frame)

        self.s1_stop_frame = QFrame(self.verticalLayoutWidget_3)
        self.s1_stop_frame.setObjectName(u"s1_stop_frame")
        sizePolicy.setHeightForWidth(self.s1_stop_frame.sizePolicy().hasHeightForWidth())
        self.s1_stop_frame.setSizePolicy(sizePolicy)
        self.s1_stop_frame.setMinimumSize(QSize(83, 44))
        self.s1_stop_frame.setFrameShape(QFrame.StyledPanel)
        self.s1_stop_frame.setFrameShadow(QFrame.Raised)
        self.label_33 = QLabel(self.s1_stop_frame)
        self.label_33.setObjectName(u"label_33")
        self.label_33.setGeometry(QRect(0, 0, 83, 16))
        self.label_33.setAlignment(Qt.AlignCenter)
        self.s1_stop_inp = QLineEdit(self.s1_stop_frame)
        self.s1_stop_inp.setObjectName(u"s1_stop_inp")
        self.s1_stop_inp.setGeometry(QRect(0, 22, 83, 22))
        self.s1_stop_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_8.addWidget(self.s1_stop_frame)

        self.s2_stop_frame = QFrame(self.verticalLayoutWidget_3)
        self.s2_stop_frame.setObjectName(u"s2_stop_frame")
        sizePolicy.setHeightForWidth(self.s2_stop_frame.sizePolicy().hasHeightForWidth())
        self.s2_stop_frame.setSizePolicy(sizePolicy)
        self.s2_stop_frame.setMinimumSize(QSize(83, 44))
        self.s2_stop_frame.setFrameShape(QFrame.StyledPanel)
        self.s2_stop_frame.setFrameShadow(QFrame.Raised)
        self.label_34 = QLabel(self.s2_stop_frame)
        self.label_34.setObjectName(u"label_34")
        self.label_34.setGeometry(QRect(0, 0, 83, 16))
        self.label_34.setAlignment(Qt.AlignCenter)
        self.s2_stop_inp = QLineEdit(self.s2_stop_frame)
        self.s2_stop_inp.setObjectName(u"s2_stop_inp")
        self.s2_stop_inp.setGeometry(QRect(0, 22, 83, 22))
        self.s2_stop_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_8.addWidget(self.s2_stop_frame)

        self.s3_stop_frame = QFrame(self.verticalLayoutWidget_3)
        self.s3_stop_frame.setObjectName(u"s3_stop_frame")
        sizePolicy.setHeightForWidth(self.s3_stop_frame.sizePolicy().hasHeightForWidth())
        self.s3_stop_frame.setSizePolicy(sizePolicy)
        self.s3_stop_frame.setMinimumSize(QSize(83, 44))
        self.s3_stop_frame.setFrameShape(QFrame.StyledPanel)
        self.s3_stop_frame.setFrameShadow(QFrame.Raised)
        self.label_35 = QLabel(self.s3_stop_frame)
        self.label_35.setObjectName(u"label_35")
        self.label_35.setGeometry(QRect(0, 0, 83, 16))
        self.label_35.setAlignment(Qt.AlignCenter)
        self.s3_stop_inp = QLineEdit(self.s3_stop_frame)
        self.s3_stop_inp.setObjectName(u"s3_stop_inp")
        self.s3_stop_inp.setGeometry(QRect(0, 22, 83, 22))
        self.s3_stop_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_8.addWidget(self.s3_stop_frame)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_8.addItem(self.horizontalSpacer_3)


        self.verticalLayout_7.addLayout(self.horizontalLayout_8)

        self.label_36 = QLabel(self.verticalLayoutWidget_3)
        self.label_36.setObjectName(u"label_36")
        self.label_36.setFont(font3)

        self.verticalLayout_7.addWidget(self.label_36)

        self.horizontalLayout_9 = QHBoxLayout()
        self.horizontalLayout_9.setObjectName(u"horizontalLayout_9")
        self.nostep_l = QLabel(self.verticalLayoutWidget_3)
        self.nostep_l.setObjectName(u"nostep_l")

        self.horizontalLayout_9.addWidget(self.nostep_l)

        self.x_step_frame = QFrame(self.verticalLayoutWidget_3)
        self.x_step_frame.setObjectName(u"x_step_frame")
        self.x_step_frame.setEnabled(True)
        sizePolicy.setHeightForWidth(self.x_step_frame.sizePolicy().hasHeightForWidth())
        self.x_step_frame.setSizePolicy(sizePolicy)
        self.x_step_frame.setMinimumSize(QSize(83, 44))
        self.x_step_frame.setFrameShape(QFrame.StyledPanel)
        self.x_step_frame.setFrameShadow(QFrame.Raised)
        self.label_37 = QLabel(self.x_step_frame)
        self.label_37.setObjectName(u"label_37")
        self.label_37.setGeometry(QRect(0, 0, 83, 16))
        self.label_37.setAlignment(Qt.AlignCenter)
        self.x_step_inp = QLineEdit(self.x_step_frame)
        self.x_step_inp.setObjectName(u"x_step_inp")
        self.x_step_inp.setGeometry(QRect(0, 22, 83, 22))
        self.x_step_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_9.addWidget(self.x_step_frame)

        self.y_step_frame = QFrame(self.verticalLayoutWidget_3)
        self.y_step_frame.setObjectName(u"y_step_frame")
        sizePolicy.setHeightForWidth(self.y_step_frame.sizePolicy().hasHeightForWidth())
        self.y_step_frame.setSizePolicy(sizePolicy)
        self.y_step_frame.setMinimumSize(QSize(83, 44))
        self.y_step_frame.setFrameShape(QFrame.StyledPanel)
        self.y_step_frame.setFrameShadow(QFrame.Raised)
        self.label_38 = QLabel(self.y_step_frame)
        self.label_38.setObjectName(u"label_38")
        self.label_38.setGeometry(QRect(0, 0, 83, 16))
        self.label_38.setAlignment(Qt.AlignCenter)
        self.y_step_inp = QLineEdit(self.y_step_frame)
        self.y_step_inp.setObjectName(u"y_step_inp")
        self.y_step_inp.setGeometry(QRect(0, 22, 83, 22))
        self.y_step_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_9.addWidget(self.y_step_frame)

        self.z_step_frame = QFrame(self.verticalLayoutWidget_3)
        self.z_step_frame.setObjectName(u"z_step_frame")
        sizePolicy.setHeightForWidth(self.z_step_frame.sizePolicy().hasHeightForWidth())
        self.z_step_frame.setSizePolicy(sizePolicy)
        self.z_step_frame.setMinimumSize(QSize(83, 44))
        self.z_step_frame.setFrameShape(QFrame.StyledPanel)
        self.z_step_frame.setFrameShadow(QFrame.Raised)
        self.z_step_inp = QLineEdit(self.z_step_frame)
        self.z_step_inp.setObjectName(u"z_step_inp")
        self.z_step_inp.setGeometry(QRect(0, 22, 83, 22))
        self.z_step_inp.setAlignment(Qt.AlignCenter)
        self.label_39 = QLabel(self.z_step_frame)
        self.label_39.setObjectName(u"label_39")
        self.label_39.setGeometry(QRect(0, 0, 83, 16))
        self.label_39.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_9.addWidget(self.z_step_frame)

        self.r1_step_frame = QFrame(self.verticalLayoutWidget_3)
        self.r1_step_frame.setObjectName(u"r1_step_frame")
        sizePolicy.setHeightForWidth(self.r1_step_frame.sizePolicy().hasHeightForWidth())
        self.r1_step_frame.setSizePolicy(sizePolicy)
        self.r1_step_frame.setMinimumSize(QSize(83, 44))
        self.r1_step_frame.setFrameShape(QFrame.StyledPanel)
        self.r1_step_frame.setFrameShadow(QFrame.Raised)
        self.label_40 = QLabel(self.r1_step_frame)
        self.label_40.setObjectName(u"label_40")
        self.label_40.setGeometry(QRect(0, 0, 83, 16))
        self.label_40.setAlignment(Qt.AlignCenter)
        self.r1_step_inp = QLineEdit(self.r1_step_frame)
        self.r1_step_inp.setObjectName(u"r1_step_inp")
        self.r1_step_inp.setGeometry(QRect(0, 22, 83, 22))
        self.r1_step_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_9.addWidget(self.r1_step_frame)

        self.r2_step_frame = QFrame(self.verticalLayoutWidget_3)
        self.r2_step_frame.setObjectName(u"r2_step_frame")
        sizePolicy.setHeightForWidth(self.r2_step_frame.sizePolicy().hasHeightForWidth())
        self.r2_step_frame.setSizePolicy(sizePolicy)
        self.r2_step_frame.setMinimumSize(QSize(83, 44))
        self.r2_step_frame.setFrameShape(QFrame.StyledPanel)
        self.r2_step_frame.setFrameShadow(QFrame.Raised)
        self.label_41 = QLabel(self.r2_step_frame)
        self.label_41.setObjectName(u"label_41")
        self.label_41.setGeometry(QRect(0, 0, 83, 16))
        self.label_41.setAlignment(Qt.AlignCenter)
        self.r2_step_inp = QLineEdit(self.r2_step_frame)
        self.r2_step_inp.setObjectName(u"r2_step_inp")
        self.r2_step_inp.setGeometry(QRect(0, 22, 83, 22))
        self.r2_step_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_9.addWidget(self.r2_step_frame)

        self.r3_step_frame = QFrame(self.verticalLayoutWidget_3)
        self.r3_step_frame.setObjectName(u"r3_step_frame")
        sizePolicy.setHeightForWidth(self.r3_step_frame.sizePolicy().hasHeightForWidth())
        self.r3_step_frame.setSizePolicy(sizePolicy)
        self.r3_step_frame.setMinimumSize(QSize(83, 44))
        self.r3_step_frame.setFrameShape(QFrame.StyledPanel)
        self.r3_step_frame.setFrameShadow(QFrame.Raised)
        self.label_42 = QLabel(self.r3_step_frame)
        self.label_42.setObjectName(u"label_42")
        self.label_42.setGeometry(QRect(0, 0, 83, 16))
        self.label_42.setAlignment(Qt.AlignCenter)
        self.r3_step_inp = QLineEdit(self.r3_step_frame)
        self.r3_step_inp.setObjectName(u"r3_step_inp")
        self.r3_step_inp.setGeometry(QRect(0, 22, 83, 22))
        self.r3_step_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_9.addWidget(self.r3_step_frame)

        self.vx_step_frame = QFrame(self.verticalLayoutWidget_3)
        self.vx_step_frame.setObjectName(u"vx_step_frame")
        sizePolicy.setHeightForWidth(self.vx_step_frame.sizePolicy().hasHeightForWidth())
        self.vx_step_frame.setSizePolicy(sizePolicy)
        self.vx_step_frame.setMinimumSize(QSize(83, 44))
        self.vx_step_frame.setFrameShape(QFrame.StyledPanel)
        self.vx_step_frame.setFrameShadow(QFrame.Raised)
        self.label_43 = QLabel(self.vx_step_frame)
        self.label_43.setObjectName(u"label_43")
        self.label_43.setGeometry(QRect(0, 0, 83, 16))
        self.label_43.setAlignment(Qt.AlignCenter)
        self.vx_step_inp = QLineEdit(self.vx_step_frame)
        self.vx_step_inp.setObjectName(u"vx_step_inp")
        self.vx_step_inp.setGeometry(QRect(0, 22, 83, 22))
        self.vx_step_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_9.addWidget(self.vx_step_frame)

        self.vy_step_frame = QFrame(self.verticalLayoutWidget_3)
        self.vy_step_frame.setObjectName(u"vy_step_frame")
        sizePolicy.setHeightForWidth(self.vy_step_frame.sizePolicy().hasHeightForWidth())
        self.vy_step_frame.setSizePolicy(sizePolicy)
        self.vy_step_frame.setMinimumSize(QSize(83, 44))
        self.vy_step_frame.setFrameShape(QFrame.StyledPanel)
        self.vy_step_frame.setFrameShadow(QFrame.Raised)
        self.label_44 = QLabel(self.vy_step_frame)
        self.label_44.setObjectName(u"label_44")
        self.label_44.setGeometry(QRect(0, 0, 83, 16))
        self.label_44.setAlignment(Qt.AlignCenter)
        self.vy_step_inp = QLineEdit(self.vy_step_frame)
        self.vy_step_inp.setObjectName(u"vy_step_inp")
        self.vy_step_inp.setGeometry(QRect(0, 22, 83, 22))
        self.vy_step_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_9.addWidget(self.vy_step_frame)

        self.vz_step_frame = QFrame(self.verticalLayoutWidget_3)
        self.vz_step_frame.setObjectName(u"vz_step_frame")
        sizePolicy.setHeightForWidth(self.vz_step_frame.sizePolicy().hasHeightForWidth())
        self.vz_step_frame.setSizePolicy(sizePolicy)
        self.vz_step_frame.setMinimumSize(QSize(83, 44))
        self.vz_step_frame.setFrameShape(QFrame.StyledPanel)
        self.vz_step_frame.setFrameShadow(QFrame.Raised)
        self.label_45 = QLabel(self.vz_step_frame)
        self.label_45.setObjectName(u"label_45")
        self.label_45.setGeometry(QRect(0, 0, 83, 16))
        self.label_45.setAlignment(Qt.AlignCenter)
        self.vz_step_inp = QLineEdit(self.vz_step_frame)
        self.vz_step_inp.setObjectName(u"vz_step_inp")
        self.vz_step_inp.setGeometry(QRect(0, 22, 83, 22))
        self.vz_step_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_9.addWidget(self.vz_step_frame)

        self.s0_step_frame = QFrame(self.verticalLayoutWidget_3)
        self.s0_step_frame.setObjectName(u"s0_step_frame")
        sizePolicy.setHeightForWidth(self.s0_step_frame.sizePolicy().hasHeightForWidth())
        self.s0_step_frame.setSizePolicy(sizePolicy)
        self.s0_step_frame.setMinimumSize(QSize(83, 44))
        self.s0_step_frame.setFrameShape(QFrame.StyledPanel)
        self.s0_step_frame.setFrameShadow(QFrame.Raised)
        self.label_46 = QLabel(self.s0_step_frame)
        self.label_46.setObjectName(u"label_46")
        self.label_46.setGeometry(QRect(0, 0, 83, 16))
        self.label_46.setAlignment(Qt.AlignCenter)
        self.s0_step_inp = QLineEdit(self.s0_step_frame)
        self.s0_step_inp.setObjectName(u"s0_step_inp")
        self.s0_step_inp.setGeometry(QRect(0, 22, 83, 22))
        self.s0_step_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_9.addWidget(self.s0_step_frame)

        self.s1_step_frame = QFrame(self.verticalLayoutWidget_3)
        self.s1_step_frame.setObjectName(u"s1_step_frame")
        sizePolicy.setHeightForWidth(self.s1_step_frame.sizePolicy().hasHeightForWidth())
        self.s1_step_frame.setSizePolicy(sizePolicy)
        self.s1_step_frame.setMinimumSize(QSize(83, 44))
        self.s1_step_frame.setFrameShape(QFrame.StyledPanel)
        self.s1_step_frame.setFrameShadow(QFrame.Raised)
        self.label_47 = QLabel(self.s1_step_frame)
        self.label_47.setObjectName(u"label_47")
        self.label_47.setGeometry(QRect(0, 0, 83, 16))
        self.label_47.setAlignment(Qt.AlignCenter)
        self.s1_step_inp = QLineEdit(self.s1_step_frame)
        self.s1_step_inp.setObjectName(u"s1_step_inp")
        self.s1_step_inp.setGeometry(QRect(0, 22, 83, 22))
        self.s1_step_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_9.addWidget(self.s1_step_frame)

        self.s2_step_frame = QFrame(self.verticalLayoutWidget_3)
        self.s2_step_frame.setObjectName(u"s2_step_frame")
        sizePolicy.setHeightForWidth(self.s2_step_frame.sizePolicy().hasHeightForWidth())
        self.s2_step_frame.setSizePolicy(sizePolicy)
        self.s2_step_frame.setMinimumSize(QSize(83, 44))
        self.s2_step_frame.setFrameShape(QFrame.StyledPanel)
        self.s2_step_frame.setFrameShadow(QFrame.Raised)
        self.label_48 = QLabel(self.s2_step_frame)
        self.label_48.setObjectName(u"label_48")
        self.label_48.setGeometry(QRect(0, 0, 83, 16))
        self.label_48.setAlignment(Qt.AlignCenter)
        self.s2_step_inp = QLineEdit(self.s2_step_frame)
        self.s2_step_inp.setObjectName(u"s2_step_inp")
        self.s2_step_inp.setGeometry(QRect(0, 22, 83, 22))
        self.s2_step_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_9.addWidget(self.s2_step_frame)

        self.s3_step_frame = QFrame(self.verticalLayoutWidget_3)
        self.s3_step_frame.setObjectName(u"s3_step_frame")
        sizePolicy.setHeightForWidth(self.s3_step_frame.sizePolicy().hasHeightForWidth())
        self.s3_step_frame.setSizePolicy(sizePolicy)
        self.s3_step_frame.setMinimumSize(QSize(83, 44))
        self.s3_step_frame.setFrameShape(QFrame.StyledPanel)
        self.s3_step_frame.setFrameShadow(QFrame.Raised)
        self.label_49 = QLabel(self.s3_step_frame)
        self.label_49.setObjectName(u"label_49")
        self.label_49.setGeometry(QRect(0, 0, 83, 16))
        self.label_49.setAlignment(Qt.AlignCenter)
        self.s3_step_inp = QLineEdit(self.s3_step_frame)
        self.s3_step_inp.setObjectName(u"s3_step_inp")
        self.s3_step_inp.setGeometry(QRect(0, 22, 83, 22))
        self.s3_step_inp.setAlignment(Qt.AlignCenter)

        self.horizontalLayout_9.addWidget(self.s3_step_frame)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_9.addItem(self.horizontalSpacer)


        self.verticalLayout_7.addLayout(self.horizontalLayout_9)

        self.label_50 = QLabel(self.verticalLayoutWidget_3)
        self.label_50.setObjectName(u"label_50")
        sizePolicy2.setHeightForWidth(self.label_50.sizePolicy().hasHeightForWidth())
        self.label_50.setSizePolicy(sizePolicy2)
        self.label_50.setFont(font3)

        self.verticalLayout_7.addWidget(self.label_50)

        self.label_51 = QLabel(self.verticalLayoutWidget_3)
        self.label_51.setObjectName(u"label_51")
        sizePolicy2.setHeightForWidth(self.label_51.sizePolicy().hasHeightForWidth())
        self.label_51.setSizePolicy(sizePolicy2)

        self.verticalLayout_7.addWidget(self.label_51)

        self.horizontalLayout_10 = QHBoxLayout()
        self.horizontalLayout_10.setObjectName(u"horizontalLayout_10")
        self.noord_l = QLabel(self.verticalLayoutWidget_3)
        self.noord_l.setObjectName(u"noord_l")

        self.horizontalLayout_10.addWidget(self.noord_l)

        self.x_ord_frame = QFrame(self.verticalLayoutWidget_3)
        self.x_ord_frame.setObjectName(u"x_ord_frame")
        self.x_ord_frame.setEnabled(True)
        sizePolicy.setHeightForWidth(self.x_ord_frame.sizePolicy().hasHeightForWidth())
        self.x_ord_frame.setSizePolicy(sizePolicy)
        self.x_ord_frame.setMinimumSize(QSize(83, 44))
        self.x_ord_frame.setFrameShape(QFrame.StyledPanel)
        self.x_ord_frame.setFrameShadow(QFrame.Raised)
        self.label_79 = QLabel(self.x_ord_frame)
        self.label_79.setObjectName(u"label_79")
        self.label_79.setGeometry(QRect(0, 0, 83, 16))
        self.label_79.setAlignment(Qt.AlignCenter)
        self.x_spin = QSpinBox(self.x_ord_frame)
        self.x_spin.setObjectName(u"x_spin")
        self.x_spin.setGeometry(QRect(0, 22, 83, 22))

        self.horizontalLayout_10.addWidget(self.x_ord_frame)

        self.y_ord_frame = QFrame(self.verticalLayoutWidget_3)
        self.y_ord_frame.setObjectName(u"y_ord_frame")
        sizePolicy.setHeightForWidth(self.y_ord_frame.sizePolicy().hasHeightForWidth())
        self.y_ord_frame.setSizePolicy(sizePolicy)
        self.y_ord_frame.setMinimumSize(QSize(83, 44))
        self.y_ord_frame.setFrameShape(QFrame.StyledPanel)
        self.y_ord_frame.setFrameShadow(QFrame.Raised)
        self.label_82 = QLabel(self.y_ord_frame)
        self.label_82.setObjectName(u"label_82")
        self.label_82.setGeometry(QRect(0, 0, 83, 16))
        self.label_82.setAlignment(Qt.AlignCenter)
        self.y_spin = QSpinBox(self.y_ord_frame)
        self.y_spin.setObjectName(u"y_spin")
        self.y_spin.setGeometry(QRect(0, 22, 83, 22))

        self.horizontalLayout_10.addWidget(self.y_ord_frame)

        self.z_ord_frame = QFrame(self.verticalLayoutWidget_3)
        self.z_ord_frame.setObjectName(u"z_ord_frame")
        sizePolicy.setHeightForWidth(self.z_ord_frame.sizePolicy().hasHeightForWidth())
        self.z_ord_frame.setSizePolicy(sizePolicy)
        self.z_ord_frame.setMinimumSize(QSize(83, 44))
        self.z_ord_frame.setFrameShape(QFrame.StyledPanel)
        self.z_ord_frame.setFrameShadow(QFrame.Raised)
        self.label_90 = QLabel(self.z_ord_frame)
        self.label_90.setObjectName(u"label_90")
        self.label_90.setGeometry(QRect(0, 0, 83, 16))
        self.label_90.setAlignment(Qt.AlignCenter)
        self.z_spin = QSpinBox(self.z_ord_frame)
        self.z_spin.setObjectName(u"z_spin")
        self.z_spin.setGeometry(QRect(0, 22, 83, 22))

        self.horizontalLayout_10.addWidget(self.z_ord_frame)

        self.r1_ord_frame = QFrame(self.verticalLayoutWidget_3)
        self.r1_ord_frame.setObjectName(u"r1_ord_frame")
        sizePolicy.setHeightForWidth(self.r1_ord_frame.sizePolicy().hasHeightForWidth())
        self.r1_ord_frame.setSizePolicy(sizePolicy)
        self.r1_ord_frame.setMinimumSize(QSize(83, 44))
        self.r1_ord_frame.setFrameShape(QFrame.StyledPanel)
        self.r1_ord_frame.setFrameShadow(QFrame.Raised)
        self.label_86 = QLabel(self.r1_ord_frame)
        self.label_86.setObjectName(u"label_86")
        self.label_86.setGeometry(QRect(0, 0, 83, 16))
        self.label_86.setAlignment(Qt.AlignCenter)
        self.r1_spin = QSpinBox(self.r1_ord_frame)
        self.r1_spin.setObjectName(u"r1_spin")
        self.r1_spin.setGeometry(QRect(0, 22, 83, 22))

        self.horizontalLayout_10.addWidget(self.r1_ord_frame)

        self.r2_ord_frame = QFrame(self.verticalLayoutWidget_3)
        self.r2_ord_frame.setObjectName(u"r2_ord_frame")
        sizePolicy.setHeightForWidth(self.r2_ord_frame.sizePolicy().hasHeightForWidth())
        self.r2_ord_frame.setSizePolicy(sizePolicy)
        self.r2_ord_frame.setMinimumSize(QSize(83, 44))
        self.r2_ord_frame.setFrameShape(QFrame.StyledPanel)
        self.r2_ord_frame.setFrameShadow(QFrame.Raised)
        self.label_80 = QLabel(self.r2_ord_frame)
        self.label_80.setObjectName(u"label_80")
        self.label_80.setGeometry(QRect(0, 0, 83, 16))
        self.label_80.setAlignment(Qt.AlignCenter)
        self.r2_spin = QSpinBox(self.r2_ord_frame)
        self.r2_spin.setObjectName(u"r2_spin")
        self.r2_spin.setGeometry(QRect(0, 22, 83, 22))

        self.horizontalLayout_10.addWidget(self.r2_ord_frame)

        self.r3_ord_frame = QFrame(self.verticalLayoutWidget_3)
        self.r3_ord_frame.setObjectName(u"r3_ord_frame")
        sizePolicy.setHeightForWidth(self.r3_ord_frame.sizePolicy().hasHeightForWidth())
        self.r3_ord_frame.setSizePolicy(sizePolicy)
        self.r3_ord_frame.setMinimumSize(QSize(83, 44))
        self.r3_ord_frame.setFrameShape(QFrame.StyledPanel)
        self.r3_ord_frame.setFrameShadow(QFrame.Raised)
        self.label_81 = QLabel(self.r3_ord_frame)
        self.label_81.setObjectName(u"label_81")
        self.label_81.setGeometry(QRect(0, 0, 83, 16))
        self.label_81.setAlignment(Qt.AlignCenter)
        self.r3_spin = QSpinBox(self.r3_ord_frame)
        self.r3_spin.setObjectName(u"r3_spin")
        self.r3_spin.setGeometry(QRect(0, 22, 83, 22))

        self.horizontalLayout_10.addWidget(self.r3_ord_frame)

        self.vx_ord_frame = QFrame(self.verticalLayoutWidget_3)
        self.vx_ord_frame.setObjectName(u"vx_ord_frame")
        sizePolicy.setHeightForWidth(self.vx_ord_frame.sizePolicy().hasHeightForWidth())
        self.vx_ord_frame.setSizePolicy(sizePolicy)
        self.vx_ord_frame.setMinimumSize(QSize(83, 44))
        self.vx_ord_frame.setFrameShape(QFrame.StyledPanel)
        self.vx_ord_frame.setFrameShadow(QFrame.Raised)
        self.label_84 = QLabel(self.vx_ord_frame)
        self.label_84.setObjectName(u"label_84")
        self.label_84.setGeometry(QRect(0, 0, 83, 16))
        self.label_84.setAlignment(Qt.AlignCenter)
        self.vx_spin = QSpinBox(self.vx_ord_frame)
        self.vx_spin.setObjectName(u"vx_spin")
        self.vx_spin.setGeometry(QRect(0, 22, 83, 22))

        self.horizontalLayout_10.addWidget(self.vx_ord_frame)

        self.vy_ord_frame = QFrame(self.verticalLayoutWidget_3)
        self.vy_ord_frame.setObjectName(u"vy_ord_frame")
        sizePolicy.setHeightForWidth(self.vy_ord_frame.sizePolicy().hasHeightForWidth())
        self.vy_ord_frame.setSizePolicy(sizePolicy)
        self.vy_ord_frame.setMinimumSize(QSize(83, 44))
        self.vy_ord_frame.setFrameShape(QFrame.StyledPanel)
        self.vy_ord_frame.setFrameShadow(QFrame.Raised)
        self.label_88 = QLabel(self.vy_ord_frame)
        self.label_88.setObjectName(u"label_88")
        self.label_88.setGeometry(QRect(0, 0, 83, 16))
        self.label_88.setAlignment(Qt.AlignCenter)
        self.vy_spin = QSpinBox(self.vy_ord_frame)
        self.vy_spin.setObjectName(u"vy_spin")
        self.vy_spin.setGeometry(QRect(0, 22, 83, 22))

        self.horizontalLayout_10.addWidget(self.vy_ord_frame)

        self.vz_ord_frame = QFrame(self.verticalLayoutWidget_3)
        self.vz_ord_frame.setObjectName(u"vz_ord_frame")
        sizePolicy.setHeightForWidth(self.vz_ord_frame.sizePolicy().hasHeightForWidth())
        self.vz_ord_frame.setSizePolicy(sizePolicy)
        self.vz_ord_frame.setMinimumSize(QSize(83, 44))
        self.vz_ord_frame.setFrameShape(QFrame.StyledPanel)
        self.vz_ord_frame.setFrameShadow(QFrame.Raised)
        self.label_85 = QLabel(self.vz_ord_frame)
        self.label_85.setObjectName(u"label_85")
        self.label_85.setGeometry(QRect(0, 0, 83, 16))
        self.label_85.setAlignment(Qt.AlignCenter)
        self.vz_spin = QSpinBox(self.vz_ord_frame)
        self.vz_spin.setObjectName(u"vz_spin")
        self.vz_spin.setGeometry(QRect(0, 22, 83, 22))

        self.horizontalLayout_10.addWidget(self.vz_ord_frame)

        self.s0_ord_frame = QFrame(self.verticalLayoutWidget_3)
        self.s0_ord_frame.setObjectName(u"s0_ord_frame")
        sizePolicy.setHeightForWidth(self.s0_ord_frame.sizePolicy().hasHeightForWidth())
        self.s0_ord_frame.setSizePolicy(sizePolicy)
        self.s0_ord_frame.setMinimumSize(QSize(83, 44))
        self.s0_ord_frame.setFrameShape(QFrame.StyledPanel)
        self.s0_ord_frame.setFrameShadow(QFrame.Raised)
        self.label_83 = QLabel(self.s0_ord_frame)
        self.label_83.setObjectName(u"label_83")
        self.label_83.setGeometry(QRect(0, 0, 83, 16))
        self.label_83.setAlignment(Qt.AlignCenter)
        self.s0_spin = QSpinBox(self.s0_ord_frame)
        self.s0_spin.setObjectName(u"s0_spin")
        self.s0_spin.setGeometry(QRect(0, 22, 83, 22))

        self.horizontalLayout_10.addWidget(self.s0_ord_frame)

        self.s1_ord_frame = QFrame(self.verticalLayoutWidget_3)
        self.s1_ord_frame.setObjectName(u"s1_ord_frame")
        sizePolicy.setHeightForWidth(self.s1_ord_frame.sizePolicy().hasHeightForWidth())
        self.s1_ord_frame.setSizePolicy(sizePolicy)
        self.s1_ord_frame.setMinimumSize(QSize(83, 44))
        self.s1_ord_frame.setFrameShape(QFrame.StyledPanel)
        self.s1_ord_frame.setFrameShadow(QFrame.Raised)
        self.label_87 = QLabel(self.s1_ord_frame)
        self.label_87.setObjectName(u"label_87")
        self.label_87.setGeometry(QRect(0, 0, 83, 16))
        self.label_87.setAlignment(Qt.AlignCenter)
        self.s1_spin = QSpinBox(self.s1_ord_frame)
        self.s1_spin.setObjectName(u"s1_spin")
        self.s1_spin.setGeometry(QRect(0, 22, 83, 22))

        self.horizontalLayout_10.addWidget(self.s1_ord_frame)

        self.s2_ord_frame = QFrame(self.verticalLayoutWidget_3)
        self.s2_ord_frame.setObjectName(u"s2_ord_frame")
        sizePolicy.setHeightForWidth(self.s2_ord_frame.sizePolicy().hasHeightForWidth())
        self.s2_ord_frame.setSizePolicy(sizePolicy)
        self.s2_ord_frame.setMinimumSize(QSize(83, 44))
        self.s2_ord_frame.setFrameShape(QFrame.StyledPanel)
        self.s2_ord_frame.setFrameShadow(QFrame.Raised)
        self.label_89 = QLabel(self.s2_ord_frame)
        self.label_89.setObjectName(u"label_89")
        self.label_89.setGeometry(QRect(0, 0, 83, 16))
        self.label_89.setAlignment(Qt.AlignCenter)
        self.s2_spin = QSpinBox(self.s2_ord_frame)
        self.s2_spin.setObjectName(u"s2_spin")
        self.s2_spin.setGeometry(QRect(0, 22, 83, 22))

        self.horizontalLayout_10.addWidget(self.s2_ord_frame)

        self.s3_ord_frame = QFrame(self.verticalLayoutWidget_3)
        self.s3_ord_frame.setObjectName(u"s3_ord_frame")
        sizePolicy.setHeightForWidth(self.s3_ord_frame.sizePolicy().hasHeightForWidth())
        self.s3_ord_frame.setSizePolicy(sizePolicy)
        self.s3_ord_frame.setMinimumSize(QSize(83, 44))
        self.s3_ord_frame.setFrameShape(QFrame.StyledPanel)
        self.s3_ord_frame.setFrameShadow(QFrame.Raised)
        self.label_91 = QLabel(self.s3_ord_frame)
        self.label_91.setObjectName(u"label_91")
        self.label_91.setGeometry(QRect(0, 0, 83, 16))
        self.label_91.setAlignment(Qt.AlignCenter)
        self.s3_spin = QSpinBox(self.s3_ord_frame)
        self.s3_spin.setObjectName(u"s3_spin")
        self.s3_spin.setGeometry(QRect(0, 22, 83, 22))

        self.horizontalLayout_10.addWidget(self.s3_ord_frame)

        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_10.addItem(self.horizontalSpacer_4)


        self.verticalLayout_7.addLayout(self.horizontalLayout_10)

        self.scrollArea_2.setWidget(self.scrollAreaWidgetContents_2)
        self.save_btn = QPushButton(self.frame_3)
        self.save_btn.setObjectName(u"save_btn")
        self.save_btn.setGeometry(QRect(400, 724, 90, 32))
        self.load_btn = QPushButton(self.frame_3)
        self.load_btn.setObjectName(u"load_btn")
        self.load_btn.setGeometry(QRect(720, 724, 90, 32))
        self.saved_l = QLabel(self.frame_3)
        self.saved_l.setObjectName(u"saved_l")
        self.saved_l.setGeometry(QRect(290, 730, 101, 21))
        font5 = QFont()
        font5.setPointSize(8)
        self.saved_l.setFont(font5)
        self.saved_l.setStyleSheet(u"color: green;")
        self.progressBar = QProgressBar(self.frame_3)
        self.progressBar.setObjectName(u"progressBar")
        self.progressBar.setGeometry(QRect(860, 740, 330, 20))
        self.progressBar.setValue(0)
        self.progress_l = QLabel(self.frame_3)
        self.progress_l.setObjectName(u"progress_l")
        self.progress_l.setGeometry(QRect(860, 720, 330, 16))
        self.progress_l.setAlignment(Qt.AlignCenter)
        self.stop_exp_btn = QPushButton(self.frame_3)
        self.stop_exp_btn.setObjectName(u"stop_exp_btn")
        self.stop_exp_btn.setGeometry(QRect(545, 770, 120, 24))
        self.exp_time_l = QLabel(self.frame_3)
        self.exp_time_l.setObjectName(u"exp_time_l")
        self.exp_time_l.setGeometry(QRect(860, 770, 330, 20))
        self.exp_time_l.setAlignment(Qt.AlignCenter)
        self.datManager_btn = QPushButton(self.frame_3)
        self.datManager_btn.setObjectName(u"datManager_btn")
        self.datManager_btn.setGeometry(QRect(720, 760, 90, 32))
        self.verticalLayoutWidget_5 = QWidget(self.frame_3)
        self.verticalLayoutWidget_5.setObjectName(u"verticalLayoutWidget_5")
        self.verticalLayoutWidget_5.setGeometry(QRect(880, 250, 281, 464))
        self.verticalLayout_3 = QVBoxLayout(self.verticalLayoutWidget_5)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.v_connect_btn = QPushButton(self.verticalLayoutWidget_5)
        self.v_connect_btn.setObjectName(u"v_connect_btn")
        self.v_connect_btn.setMinimumSize(QSize(0, 0))

        self.verticalLayout_3.addWidget(self.v_connect_btn)

        self.vx_frame = QFrame(self.verticalLayoutWidget_5)
        self.vx_frame.setObjectName(u"vx_frame")
        sizePolicy1.setHeightForWidth(self.vx_frame.sizePolicy().hasHeightForWidth())
        self.vx_frame.setSizePolicy(sizePolicy1)
        self.vx_frame.setMinimumSize(QSize(0, 138))
        self.vx_frame.setMaximumSize(QSize(281, 138))
        self.vx_frame.setFrameShape(QFrame.Box)
        self.vx_frame.setFrameShadow(QFrame.Plain)
        self.layoutWidget_2 = QWidget(self.vx_frame)
        self.layoutWidget_2.setObjectName(u"layoutWidget_2")
        self.layoutWidget_2.setGeometry(QRect(10, 10, 261, 121))
        self.gridLayout_5 = QGridLayout(self.layoutWidget_2)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.gridLayout_5.setContentsMargins(0, 0, 0, 0)
        self.vx_con_l = QLabel(self.layoutWidget_2)
        self.vx_con_l.setObjectName(u"vx_con_l")
        self.vx_con_l.setFont(font5)

        self.gridLayout_5.addWidget(self.vx_con_l, 0, 2, 1, 1)

        self.step_vx_edit = QLineEdit(self.layoutWidget_2)
        self.step_vx_edit.setObjectName(u"step_vx_edit")

        self.gridLayout_5.addWidget(self.step_vx_edit, 2, 1, 1, 1)

        self.right_vx_btn = QPushButton(self.layoutWidget_2)
        self.right_vx_btn.setObjectName(u"right_vx_btn")
        self.right_vx_btn.setMaximumSize(QSize(16777215, 16777215))
        font6 = QFont()
        font6.setPointSize(11)
        font6.setBold(True)
        self.right_vx_btn.setFont(font6)

        self.gridLayout_5.addWidget(self.right_vx_btn, 3, 2, 1, 1)

        self.label_2 = QLabel(self.layoutWidget_2)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout_5.addWidget(self.label_2, 2, 0, 1, 1)

        self.label_52 = QLabel(self.layoutWidget_2)
        self.label_52.setObjectName(u"label_52")
        self.label_52.setMinimumSize(QSize(0, 0))
        font7 = QFont()
        font7.setPointSize(12)
        font7.setBold(True)
        self.label_52.setFont(font7)
        self.label_52.setAlignment(Qt.AlignCenter)

        self.gridLayout_5.addWidget(self.label_52, 0, 1, 1, 1)

        self.left_vx_btn = QPushButton(self.layoutWidget_2)
        self.left_vx_btn.setObjectName(u"left_vx_btn")
        self.left_vx_btn.setMaximumSize(QSize(16777215, 16777215))
        font8 = QFont()
        font8.setPointSize(11)
        font8.setBold(True)
        font8.setUnderline(False)
        font8.setStrikeOut(False)
        self.left_vx_btn.setFont(font8)

        self.gridLayout_5.addWidget(self.left_vx_btn, 3, 0, 1, 1)

        self.curPos_vx_edit = QLineEdit(self.layoutWidget_2)
        self.curPos_vx_edit.setObjectName(u"curPos_vx_edit")

        self.gridLayout_5.addWidget(self.curPos_vx_edit, 3, 1, 1, 1)

        self.go_vx_btn = QPushButton(self.layoutWidget_2)
        self.go_vx_btn.setObjectName(u"go_vx_btn")
        self.go_vx_btn.setMinimumSize(QSize(0, 0))
        font9 = QFont()
        font9.setPointSize(9)
        font9.setBold(True)
        self.go_vx_btn.setFont(font9)

        self.gridLayout_5.addWidget(self.go_vx_btn, 4, 1, 1, 1)

        self.ref_vx_btn = QPushButton(self.layoutWidget_2)
        self.ref_vx_btn.setObjectName(u"ref_vx_btn")

        self.gridLayout_5.addWidget(self.ref_vx_btn, 0, 0, 1, 1)

        self.label_60 = QLabel(self.layoutWidget_2)
        self.label_60.setObjectName(u"label_60")

        self.gridLayout_5.addWidget(self.label_60, 1, 0, 1, 1)

        self.pos_vx_l = QLabel(self.layoutWidget_2)
        self.pos_vx_l.setObjectName(u"pos_vx_l")
        self.pos_vx_l.setAlignment(Qt.AlignCenter)

        self.gridLayout_5.addWidget(self.pos_vx_l, 1, 1, 1, 1)


        self.verticalLayout_3.addWidget(self.vx_frame)

        self.vy_frame = QFrame(self.verticalLayoutWidget_5)
        self.vy_frame.setObjectName(u"vy_frame")
        self.vy_frame.setMinimumSize(QSize(0, 138))
        self.vy_frame.setMaximumSize(QSize(281, 138))
        self.vy_frame.setFrameShape(QFrame.Box)
        self.vy_frame.setFrameShadow(QFrame.Plain)
        self.layoutWidget_3 = QWidget(self.vy_frame)
        self.layoutWidget_3.setObjectName(u"layoutWidget_3")
        self.layoutWidget_3.setGeometry(QRect(10, 10, 261, 121))
        self.gridLayout_7 = QGridLayout(self.layoutWidget_3)
        self.gridLayout_7.setObjectName(u"gridLayout_7")
        self.gridLayout_7.setContentsMargins(1, 1, 0, 0)
        self.label_61 = QLabel(self.layoutWidget_3)
        self.label_61.setObjectName(u"label_61")

        self.gridLayout_7.addWidget(self.label_61, 2, 0, 1, 1)

        self.right_vy_btn = QPushButton(self.layoutWidget_3)
        self.right_vy_btn.setObjectName(u"right_vy_btn")
        self.right_vy_btn.setMaximumSize(QSize(16777215, 16777215))
        self.right_vy_btn.setFont(font6)

        self.gridLayout_7.addWidget(self.right_vy_btn, 3, 2, 1, 1)

        self.label_62 = QLabel(self.layoutWidget_3)
        self.label_62.setObjectName(u"label_62")
        self.label_62.setFont(font7)
        self.label_62.setAlignment(Qt.AlignCenter)

        self.gridLayout_7.addWidget(self.label_62, 0, 1, 1, 1)

        self.ref_vy_btn = QPushButton(self.layoutWidget_3)
        self.ref_vy_btn.setObjectName(u"ref_vy_btn")

        self.gridLayout_7.addWidget(self.ref_vy_btn, 0, 0, 1, 1)

        self.step_vy_edit = QLineEdit(self.layoutWidget_3)
        self.step_vy_edit.setObjectName(u"step_vy_edit")

        self.gridLayout_7.addWidget(self.step_vy_edit, 2, 1, 1, 1)

        self.go_vy_btn = QPushButton(self.layoutWidget_3)
        self.go_vy_btn.setObjectName(u"go_vy_btn")
        self.go_vy_btn.setMinimumSize(QSize(0, 0))
        self.go_vy_btn.setFont(font9)

        self.gridLayout_7.addWidget(self.go_vy_btn, 4, 1, 1, 1)

        self.left_vy_btn = QPushButton(self.layoutWidget_3)
        self.left_vy_btn.setObjectName(u"left_vy_btn")
        self.left_vy_btn.setMaximumSize(QSize(16777215, 16777215))
        self.left_vy_btn.setFont(font8)

        self.gridLayout_7.addWidget(self.left_vy_btn, 3, 0, 1, 1)

        self.vy_con_l = QLabel(self.layoutWidget_3)
        self.vy_con_l.setObjectName(u"vy_con_l")
        self.vy_con_l.setFont(font5)

        self.gridLayout_7.addWidget(self.vy_con_l, 0, 2, 1, 1)

        self.curPos_vy_edit = QLineEdit(self.layoutWidget_3)
        self.curPos_vy_edit.setObjectName(u"curPos_vy_edit")

        self.gridLayout_7.addWidget(self.curPos_vy_edit, 3, 1, 1, 1)

        self.label_63 = QLabel(self.layoutWidget_3)
        self.label_63.setObjectName(u"label_63")

        self.gridLayout_7.addWidget(self.label_63, 1, 0, 1, 1)

        self.pos_vy_l = QLabel(self.layoutWidget_3)
        self.pos_vy_l.setObjectName(u"pos_vy_l")
        self.pos_vy_l.setAlignment(Qt.AlignCenter)

        self.gridLayout_7.addWidget(self.pos_vy_l, 1, 1, 1, 1)


        self.verticalLayout_3.addWidget(self.vy_frame)

        self.vz_frame = QFrame(self.verticalLayoutWidget_5)
        self.vz_frame.setObjectName(u"vz_frame")
        self.vz_frame.setMinimumSize(QSize(0, 138))
        self.vz_frame.setMaximumSize(QSize(281, 138))
        self.vz_frame.setFrameShape(QFrame.Box)
        self.vz_frame.setFrameShadow(QFrame.Plain)
        self.layoutWidget_4 = QWidget(self.vz_frame)
        self.layoutWidget_4.setObjectName(u"layoutWidget_4")
        self.layoutWidget_4.setGeometry(QRect(10, 10, 261, 121))
        self.gridLayout_8 = QGridLayout(self.layoutWidget_4)
        self.gridLayout_8.setObjectName(u"gridLayout_8")
        self.gridLayout_8.setContentsMargins(1, 1, 0, 0)
        self.vz_con_l = QLabel(self.layoutWidget_4)
        self.vz_con_l.setObjectName(u"vz_con_l")
        self.vz_con_l.setFont(font5)

        self.gridLayout_8.addWidget(self.vz_con_l, 0, 2, 1, 1)

        self.label_64 = QLabel(self.layoutWidget_4)
        self.label_64.setObjectName(u"label_64")
        self.label_64.setFont(font7)
        self.label_64.setAlignment(Qt.AlignCenter)

        self.gridLayout_8.addWidget(self.label_64, 0, 1, 1, 1)

        self.step_vz_edit = QLineEdit(self.layoutWidget_4)
        self.step_vz_edit.setObjectName(u"step_vz_edit")

        self.gridLayout_8.addWidget(self.step_vz_edit, 2, 1, 1, 1)

        self.go_vz_btn = QPushButton(self.layoutWidget_4)
        self.go_vz_btn.setObjectName(u"go_vz_btn")
        self.go_vz_btn.setMinimumSize(QSize(0, 0))
        self.go_vz_btn.setFont(font9)

        self.gridLayout_8.addWidget(self.go_vz_btn, 4, 1, 1, 1)

        self.left_vz_btn = QPushButton(self.layoutWidget_4)
        self.left_vz_btn.setObjectName(u"left_vz_btn")
        self.left_vz_btn.setMaximumSize(QSize(16777215, 16777215))
        self.left_vz_btn.setFont(font8)

        self.gridLayout_8.addWidget(self.left_vz_btn, 3, 0, 1, 1)

        self.label_65 = QLabel(self.layoutWidget_4)
        self.label_65.setObjectName(u"label_65")

        self.gridLayout_8.addWidget(self.label_65, 2, 0, 1, 1)

        self.ref_vz_btn = QPushButton(self.layoutWidget_4)
        self.ref_vz_btn.setObjectName(u"ref_vz_btn")

        self.gridLayout_8.addWidget(self.ref_vz_btn, 0, 0, 1, 1)

        self.curPos_vz_edit = QLineEdit(self.layoutWidget_4)
        self.curPos_vz_edit.setObjectName(u"curPos_vz_edit")

        self.gridLayout_8.addWidget(self.curPos_vz_edit, 3, 1, 1, 1)

        self.right_vz_btn = QPushButton(self.layoutWidget_4)
        self.right_vz_btn.setObjectName(u"right_vz_btn")
        self.right_vz_btn.setMaximumSize(QSize(16777215, 16777215))
        self.right_vz_btn.setFont(font6)

        self.gridLayout_8.addWidget(self.right_vz_btn, 3, 2, 1, 1)

        self.label_66 = QLabel(self.layoutWidget_4)
        self.label_66.setObjectName(u"label_66")

        self.gridLayout_8.addWidget(self.label_66, 1, 0, 1, 1)

        self.pos_vz_l = QLabel(self.layoutWidget_4)
        self.pos_vz_l.setObjectName(u"pos_vz_l")
        self.pos_vz_l.setAlignment(Qt.AlignCenter)

        self.gridLayout_8.addWidget(self.pos_vz_l, 1, 1, 1, 1)


        self.verticalLayout_3.addWidget(self.vz_frame)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer_2)

        self.verticalLayoutWidget_4 = QWidget(self.frame_3)
        self.verticalLayoutWidget_4.setObjectName(u"verticalLayoutWidget_4")
        self.verticalLayoutWidget_4.setGeometry(QRect(300, 250, 574, 471))
        self.verticalLayout_4 = QVBoxLayout(self.verticalLayoutWidget_4)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.s_connect_btn = QPushButton(self.verticalLayoutWidget_4)
        self.s_connect_btn.setObjectName(u"s_connect_btn")
        self.s_connect_btn.setMaximumSize(QSize(564, 16777215))

        self.verticalLayout_4.addWidget(self.s_connect_btn)

        self.gridLayout_14 = QGridLayout()
        self.gridLayout_14.setObjectName(u"gridLayout_14")
        self.stage_z_frame = QFrame(self.verticalLayoutWidget_4)
        self.stage_z_frame.setObjectName(u"stage_z_frame")
        self.stage_z_frame.setMinimumSize(QSize(279, 138))
        self.stage_z_frame.setMaximumSize(QSize(281, 177))
        self.stage_z_frame.setFrameShape(QFrame.Box)
        self.stage_z_frame.setFrameShadow(QFrame.Plain)
        self.layoutWidget_7 = QWidget(self.stage_z_frame)
        self.layoutWidget_7.setObjectName(u"layoutWidget_7")
        self.layoutWidget_7.setGeometry(QRect(10, 10, 262, 121))
        self.gridLayout_11 = QGridLayout(self.layoutWidget_7)
        self.gridLayout_11.setObjectName(u"gridLayout_11")
        self.gridLayout_11.setContentsMargins(1, 1, 0, 0)
        self.z_con_l = QLabel(self.layoutWidget_7)
        self.z_con_l.setObjectName(u"z_con_l")
        self.z_con_l.setFont(font5)

        self.gridLayout_11.addWidget(self.z_con_l, 0, 2, 1, 1)

        self.label_73 = QLabel(self.layoutWidget_7)
        self.label_73.setObjectName(u"label_73")
        self.label_73.setFont(font7)
        self.label_73.setAlignment(Qt.AlignCenter)

        self.gridLayout_11.addWidget(self.label_73, 0, 1, 1, 1)

        self.step_z_edit = QLineEdit(self.layoutWidget_7)
        self.step_z_edit.setObjectName(u"step_z_edit")

        self.gridLayout_11.addWidget(self.step_z_edit, 2, 1, 1, 1)

        self.go_z_btn = QPushButton(self.layoutWidget_7)
        self.go_z_btn.setObjectName(u"go_z_btn")
        self.go_z_btn.setMinimumSize(QSize(0, 0))
        self.go_z_btn.setFont(font9)

        self.gridLayout_11.addWidget(self.go_z_btn, 4, 1, 1, 1)

        self.left_z_btn = QPushButton(self.layoutWidget_7)
        self.left_z_btn.setObjectName(u"left_z_btn")
        self.left_z_btn.setMaximumSize(QSize(16777215, 16777215))
        self.left_z_btn.setFont(font8)

        self.gridLayout_11.addWidget(self.left_z_btn, 3, 0, 1, 1)

        self.label_74 = QLabel(self.layoutWidget_7)
        self.label_74.setObjectName(u"label_74")

        self.gridLayout_11.addWidget(self.label_74, 2, 0, 1, 1)

        self.ref_z_btn = QPushButton(self.layoutWidget_7)
        self.ref_z_btn.setObjectName(u"ref_z_btn")

        self.gridLayout_11.addWidget(self.ref_z_btn, 0, 0, 1, 1)

        self.curPos_z_edit = QLineEdit(self.layoutWidget_7)
        self.curPos_z_edit.setObjectName(u"curPos_z_edit")

        self.gridLayout_11.addWidget(self.curPos_z_edit, 3, 1, 1, 1)

        self.right_z_btn = QPushButton(self.layoutWidget_7)
        self.right_z_btn.setObjectName(u"right_z_btn")
        self.right_z_btn.setMaximumSize(QSize(16777215, 16777215))
        self.right_z_btn.setFont(font6)

        self.gridLayout_11.addWidget(self.right_z_btn, 3, 2, 1, 1)

        self.label_75 = QLabel(self.layoutWidget_7)
        self.label_75.setObjectName(u"label_75")

        self.gridLayout_11.addWidget(self.label_75, 1, 0, 1, 1)

        self.pos_z_l = QLabel(self.layoutWidget_7)
        self.pos_z_l.setObjectName(u"pos_z_l")
        self.pos_z_l.setAlignment(Qt.AlignCenter)

        self.gridLayout_11.addWidget(self.pos_z_l, 1, 1, 1, 1)


        self.gridLayout_14.addWidget(self.stage_z_frame, 3, 0, 1, 1)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_14.addItem(self.verticalSpacer_3, 4, 0, 1, 1)

        self.stage_y_frame = QFrame(self.verticalLayoutWidget_4)
        self.stage_y_frame.setObjectName(u"stage_y_frame")
        self.stage_y_frame.setMinimumSize(QSize(279, 138))
        self.stage_y_frame.setMaximumSize(QSize(281, 177))
        self.stage_y_frame.setFrameShape(QFrame.Box)
        self.stage_y_frame.setFrameShadow(QFrame.Plain)
        self.layoutWidget_6 = QWidget(self.stage_y_frame)
        self.layoutWidget_6.setObjectName(u"layoutWidget_6")
        self.layoutWidget_6.setGeometry(QRect(10, 10, 262, 121))
        self.gridLayout_10 = QGridLayout(self.layoutWidget_6)
        self.gridLayout_10.setObjectName(u"gridLayout_10")
        self.gridLayout_10.setContentsMargins(1, 1, 0, 0)
        self.label_70 = QLabel(self.layoutWidget_6)
        self.label_70.setObjectName(u"label_70")

        self.gridLayout_10.addWidget(self.label_70, 2, 0, 1, 1)

        self.right_y_btn = QPushButton(self.layoutWidget_6)
        self.right_y_btn.setObjectName(u"right_y_btn")
        self.right_y_btn.setMaximumSize(QSize(16777215, 16777215))
        self.right_y_btn.setFont(font6)

        self.gridLayout_10.addWidget(self.right_y_btn, 3, 2, 1, 1)

        self.label_71 = QLabel(self.layoutWidget_6)
        self.label_71.setObjectName(u"label_71")
        self.label_71.setFont(font7)
        self.label_71.setAlignment(Qt.AlignCenter)

        self.gridLayout_10.addWidget(self.label_71, 0, 1, 1, 1)

        self.ref_y_btn = QPushButton(self.layoutWidget_6)
        self.ref_y_btn.setObjectName(u"ref_y_btn")

        self.gridLayout_10.addWidget(self.ref_y_btn, 0, 0, 1, 1)

        self.step_y_edit = QLineEdit(self.layoutWidget_6)
        self.step_y_edit.setObjectName(u"step_y_edit")

        self.gridLayout_10.addWidget(self.step_y_edit, 2, 1, 1, 1)

        self.go_y_btn = QPushButton(self.layoutWidget_6)
        self.go_y_btn.setObjectName(u"go_y_btn")
        self.go_y_btn.setMinimumSize(QSize(0, 0))
        self.go_y_btn.setFont(font9)

        self.gridLayout_10.addWidget(self.go_y_btn, 4, 1, 1, 1)

        self.left_y_btn = QPushButton(self.layoutWidget_6)
        self.left_y_btn.setObjectName(u"left_y_btn")
        self.left_y_btn.setMaximumSize(QSize(16777215, 16777215))
        self.left_y_btn.setFont(font8)

        self.gridLayout_10.addWidget(self.left_y_btn, 3, 0, 1, 1)

        self.y_con_l = QLabel(self.layoutWidget_6)
        self.y_con_l.setObjectName(u"y_con_l")
        self.y_con_l.setFont(font5)

        self.gridLayout_10.addWidget(self.y_con_l, 0, 2, 1, 1)

        self.curPos_y_edit = QLineEdit(self.layoutWidget_6)
        self.curPos_y_edit.setObjectName(u"curPos_y_edit")

        self.gridLayout_10.addWidget(self.curPos_y_edit, 3, 1, 1, 1)

        self.label_72 = QLabel(self.layoutWidget_6)
        self.label_72.setObjectName(u"label_72")

        self.gridLayout_10.addWidget(self.label_72, 1, 0, 1, 1)

        self.pos_y_l = QLabel(self.layoutWidget_6)
        self.pos_y_l.setObjectName(u"pos_y_l")
        self.pos_y_l.setAlignment(Qt.AlignCenter)

        self.gridLayout_10.addWidget(self.pos_y_l, 1, 1, 1, 1)


        self.gridLayout_14.addWidget(self.stage_y_frame, 2, 0, 1, 1)

        self.stage_r1_frame = QFrame(self.verticalLayoutWidget_4)
        self.stage_r1_frame.setObjectName(u"stage_r1_frame")
        sizePolicy3.setHeightForWidth(self.stage_r1_frame.sizePolicy().hasHeightForWidth())
        self.stage_r1_frame.setSizePolicy(sizePolicy3)
        self.stage_r1_frame.setMinimumSize(QSize(279, 138))
        self.stage_r1_frame.setMaximumSize(QSize(281, 177))
        self.stage_r1_frame.setFrameShape(QFrame.Box)
        self.stage_r1_frame.setFrameShadow(QFrame.Plain)
        self.layoutWidget_8 = QWidget(self.stage_r1_frame)
        self.layoutWidget_8.setObjectName(u"layoutWidget_8")
        self.layoutWidget_8.setGeometry(QRect(10, 10, 262, 121))
        self.gridLayout_12 = QGridLayout(self.layoutWidget_8)
        self.gridLayout_12.setObjectName(u"gridLayout_12")
        self.gridLayout_12.setContentsMargins(0, 0, 0, 0)
        self.r1_con_l = QLabel(self.layoutWidget_8)
        self.r1_con_l.setObjectName(u"r1_con_l")
        self.r1_con_l.setFont(font5)

        self.gridLayout_12.addWidget(self.r1_con_l, 0, 2, 1, 1)

        self.step_r1_edit = QLineEdit(self.layoutWidget_8)
        self.step_r1_edit.setObjectName(u"step_r1_edit")

        self.gridLayout_12.addWidget(self.step_r1_edit, 2, 1, 1, 1)

        self.right_r1_btn = QPushButton(self.layoutWidget_8)
        self.right_r1_btn.setObjectName(u"right_r1_btn")
        self.right_r1_btn.setMaximumSize(QSize(16777215, 16777215))
        self.right_r1_btn.setFont(font6)

        self.gridLayout_12.addWidget(self.right_r1_btn, 3, 2, 1, 1)

        self.label_76 = QLabel(self.layoutWidget_8)
        self.label_76.setObjectName(u"label_76")

        self.gridLayout_12.addWidget(self.label_76, 2, 0, 1, 1)

        self.label_77 = QLabel(self.layoutWidget_8)
        self.label_77.setObjectName(u"label_77")
        self.label_77.setMinimumSize(QSize(0, 0))
        self.label_77.setFont(font7)
        self.label_77.setAlignment(Qt.AlignCenter)

        self.gridLayout_12.addWidget(self.label_77, 0, 1, 1, 1)

        self.left_r1_btn = QPushButton(self.layoutWidget_8)
        self.left_r1_btn.setObjectName(u"left_r1_btn")
        self.left_r1_btn.setMaximumSize(QSize(16777215, 16777215))
        self.left_r1_btn.setFont(font8)

        self.gridLayout_12.addWidget(self.left_r1_btn, 3, 0, 1, 1)

        self.curPos_r1_edit = QLineEdit(self.layoutWidget_8)
        self.curPos_r1_edit.setObjectName(u"curPos_r1_edit")

        self.gridLayout_12.addWidget(self.curPos_r1_edit, 3, 1, 1, 1)

        self.go_r1_btn = QPushButton(self.layoutWidget_8)
        self.go_r1_btn.setObjectName(u"go_r1_btn")
        self.go_r1_btn.setMinimumSize(QSize(0, 0))
        self.go_r1_btn.setFont(font9)

        self.gridLayout_12.addWidget(self.go_r1_btn, 4, 1, 1, 1)

        self.ref_r1_btn = QPushButton(self.layoutWidget_8)
        self.ref_r1_btn.setObjectName(u"ref_r1_btn")

        self.gridLayout_12.addWidget(self.ref_r1_btn, 0, 0, 1, 1)

        self.label_78 = QLabel(self.layoutWidget_8)
        self.label_78.setObjectName(u"label_78")

        self.gridLayout_12.addWidget(self.label_78, 1, 0, 1, 1)

        self.pos_r1_l = QLabel(self.layoutWidget_8)
        self.pos_r1_l.setObjectName(u"pos_r1_l")
        self.pos_r1_l.setAlignment(Qt.AlignCenter)

        self.gridLayout_12.addWidget(self.pos_r1_l, 1, 1, 1, 1)


        self.gridLayout_14.addWidget(self.stage_r1_frame, 0, 1, 1, 1)

        self.stage_r2_frame = QFrame(self.verticalLayoutWidget_4)
        self.stage_r2_frame.setObjectName(u"stage_r2_frame")
        self.stage_r2_frame.setMinimumSize(QSize(279, 138))
        self.stage_r2_frame.setMaximumSize(QSize(281, 177))
        self.stage_r2_frame.setFrameShape(QFrame.Box)
        self.stage_r2_frame.setFrameShadow(QFrame.Plain)
        self.layoutWidget_9 = QWidget(self.stage_r2_frame)
        self.layoutWidget_9.setObjectName(u"layoutWidget_9")
        self.layoutWidget_9.setGeometry(QRect(10, 10, 262, 121))
        self.gridLayout_13 = QGridLayout(self.layoutWidget_9)
        self.gridLayout_13.setObjectName(u"gridLayout_13")
        self.gridLayout_13.setContentsMargins(1, 1, 0, 0)
        self.r2_con_l = QLabel(self.layoutWidget_9)
        self.r2_con_l.setObjectName(u"r2_con_l")
        self.r2_con_l.setFont(font5)

        self.gridLayout_13.addWidget(self.r2_con_l, 0, 2, 1, 1)

        self.label_92 = QLabel(self.layoutWidget_9)
        self.label_92.setObjectName(u"label_92")
        self.label_92.setFont(font7)
        self.label_92.setAlignment(Qt.AlignCenter)

        self.gridLayout_13.addWidget(self.label_92, 0, 1, 1, 1)

        self.step_r2_edit = QLineEdit(self.layoutWidget_9)
        self.step_r2_edit.setObjectName(u"step_r2_edit")

        self.gridLayout_13.addWidget(self.step_r2_edit, 2, 1, 1, 1)

        self.go_r2_btn = QPushButton(self.layoutWidget_9)
        self.go_r2_btn.setObjectName(u"go_r2_btn")
        self.go_r2_btn.setMinimumSize(QSize(0, 0))
        self.go_r2_btn.setFont(font9)

        self.gridLayout_13.addWidget(self.go_r2_btn, 4, 1, 1, 1)

        self.left_r2_btn = QPushButton(self.layoutWidget_9)
        self.left_r2_btn.setObjectName(u"left_r2_btn")
        self.left_r2_btn.setMaximumSize(QSize(16777215, 16777215))
        self.left_r2_btn.setFont(font8)

        self.gridLayout_13.addWidget(self.left_r2_btn, 3, 0, 1, 1)

        self.label_93 = QLabel(self.layoutWidget_9)
        self.label_93.setObjectName(u"label_93")

        self.gridLayout_13.addWidget(self.label_93, 2, 0, 1, 1)

        self.ref_r2_btn = QPushButton(self.layoutWidget_9)
        self.ref_r2_btn.setObjectName(u"ref_r2_btn")

        self.gridLayout_13.addWidget(self.ref_r2_btn, 0, 0, 1, 1)

        self.curPos_r2_edit = QLineEdit(self.layoutWidget_9)
        self.curPos_r2_edit.setObjectName(u"curPos_r2_edit")

        self.gridLayout_13.addWidget(self.curPos_r2_edit, 3, 1, 1, 1)

        self.right_r2_btn = QPushButton(self.layoutWidget_9)
        self.right_r2_btn.setObjectName(u"right_r2_btn")
        self.right_r2_btn.setMaximumSize(QSize(16777215, 16777215))
        self.right_r2_btn.setFont(font6)

        self.gridLayout_13.addWidget(self.right_r2_btn, 3, 2, 1, 1)

        self.label_94 = QLabel(self.layoutWidget_9)
        self.label_94.setObjectName(u"label_94")

        self.gridLayout_13.addWidget(self.label_94, 1, 0, 1, 1)

        self.pos_r2_l = QLabel(self.layoutWidget_9)
        self.pos_r2_l.setObjectName(u"pos_r2_l")
        self.pos_r2_l.setAlignment(Qt.AlignCenter)

        self.gridLayout_13.addWidget(self.pos_r2_l, 1, 1, 1, 1)


        self.gridLayout_14.addWidget(self.stage_r2_frame, 2, 1, 1, 1)

        self.stage_x_frame = QFrame(self.verticalLayoutWidget_4)
        self.stage_x_frame.setObjectName(u"stage_x_frame")
        sizePolicy3.setHeightForWidth(self.stage_x_frame.sizePolicy().hasHeightForWidth())
        self.stage_x_frame.setSizePolicy(sizePolicy3)
        self.stage_x_frame.setMinimumSize(QSize(279, 138))
        self.stage_x_frame.setMaximumSize(QSize(281, 177))
        self.stage_x_frame.setFrameShape(QFrame.Box)
        self.stage_x_frame.setFrameShadow(QFrame.Plain)
        self.layoutWidget_5 = QWidget(self.stage_x_frame)
        self.layoutWidget_5.setObjectName(u"layoutWidget_5")
        self.layoutWidget_5.setGeometry(QRect(10, 10, 262, 121))
        self.gridLayout_9 = QGridLayout(self.layoutWidget_5)
        self.gridLayout_9.setObjectName(u"gridLayout_9")
        self.gridLayout_9.setContentsMargins(0, 0, 0, 0)
        self.x_con_l = QLabel(self.layoutWidget_5)
        self.x_con_l.setObjectName(u"x_con_l")
        self.x_con_l.setFont(font5)

        self.gridLayout_9.addWidget(self.x_con_l, 0, 2, 1, 1)

        self.step_x_edit = QLineEdit(self.layoutWidget_5)
        self.step_x_edit.setObjectName(u"step_x_edit")

        self.gridLayout_9.addWidget(self.step_x_edit, 2, 1, 1, 1)

        self.right_x_btn = QPushButton(self.layoutWidget_5)
        self.right_x_btn.setObjectName(u"right_x_btn")
        self.right_x_btn.setMaximumSize(QSize(16777215, 16777215))
        self.right_x_btn.setFont(font6)

        self.gridLayout_9.addWidget(self.right_x_btn, 3, 2, 1, 1)

        self.label_67 = QLabel(self.layoutWidget_5)
        self.label_67.setObjectName(u"label_67")

        self.gridLayout_9.addWidget(self.label_67, 2, 0, 1, 1)

        self.label_68 = QLabel(self.layoutWidget_5)
        self.label_68.setObjectName(u"label_68")
        self.label_68.setMinimumSize(QSize(0, 0))
        self.label_68.setFont(font7)
        self.label_68.setAlignment(Qt.AlignCenter)

        self.gridLayout_9.addWidget(self.label_68, 0, 1, 1, 1)

        self.left_x_btn = QPushButton(self.layoutWidget_5)
        self.left_x_btn.setObjectName(u"left_x_btn")
        self.left_x_btn.setMaximumSize(QSize(16777215, 16777215))
        self.left_x_btn.setFont(font8)

        self.gridLayout_9.addWidget(self.left_x_btn, 3, 0, 1, 1)

        self.curPos_x_edit = QLineEdit(self.layoutWidget_5)
        self.curPos_x_edit.setObjectName(u"curPos_x_edit")

        self.gridLayout_9.addWidget(self.curPos_x_edit, 3, 1, 1, 1)

        self.go_x_btn = QPushButton(self.layoutWidget_5)
        self.go_x_btn.setObjectName(u"go_x_btn")
        self.go_x_btn.setMinimumSize(QSize(0, 0))
        self.go_x_btn.setFont(font9)

        self.gridLayout_9.addWidget(self.go_x_btn, 4, 1, 1, 1)

        self.ref_x_btn = QPushButton(self.layoutWidget_5)
        self.ref_x_btn.setObjectName(u"ref_x_btn")

        self.gridLayout_9.addWidget(self.ref_x_btn, 0, 0, 1, 1)

        self.label_69 = QLabel(self.layoutWidget_5)
        self.label_69.setObjectName(u"label_69")

        self.gridLayout_9.addWidget(self.label_69, 1, 0, 1, 1)

        self.pos_x_l = QLabel(self.layoutWidget_5)
        self.pos_x_l.setObjectName(u"pos_x_l")
        self.pos_x_l.setAlignment(Qt.AlignCenter)

        self.gridLayout_9.addWidget(self.pos_x_l, 1, 1, 1, 1)


        self.gridLayout_14.addWidget(self.stage_x_frame, 0, 0, 1, 1)

        self.horizontalSpacer_9 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_14.addItem(self.horizontalSpacer_9, 2, 2, 1, 1)


        self.verticalLayout_4.addLayout(self.gridLayout_14)

        self.n_edit = QLineEdit(self.frame_3)
        self.n_edit.setObjectName(u"n_edit")
        self.n_edit.setGeometry(QRect(785, 30, 113, 24))
        self.n_label = QLabel(self.frame_3)
        self.n_label.setObjectName(u"n_label")
        self.n_label.setGeometry(QRect(770, 35, 16, 16))
        self.no_histogram_c = QCheckBox(self.frame_3)
        self.no_histogram_c.setObjectName(u"no_histogram_c")
        self.no_histogram_c.setGeometry(QRect(30, 725, 151, 22))

        self.gridLayout_2.addWidget(self.frame_3, 0, 0, 1, 1)

        self.scrollArea.setWidget(self.scrollAreaWidgetContents)

        self.gridLayout_3.addWidget(self.scrollArea, 0, 0, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"Experiment name:", None))
        self.lockin_radio.setText(QCoreApplication.translate("MainWindow", u"LockIn", None))
        self.dummy_radio.setText(QCoreApplication.translate("MainWindow", u"Dummy", None))
        self.osc_radio.setText(QCoreApplication.translate("MainWindow", u"Oscilloscope", None))
        self.onlyamp_radio.setText(QCoreApplication.translate("MainWindow", u"Ammeter", None))
        self.label_98.setText(QCoreApplication.translate("MainWindow", u"Channel", None))
        self.label_97.setText(QCoreApplication.translate("MainWindow", u"Camera Settings", None))
        self.exposure_l.setText(QCoreApplication.translate("MainWindow", u"10", None))
        self.gain_l.setText(QCoreApplication.translate("MainWindow", u"1", None))
        self.cam_conf_btn.setText(QCoreApplication.translate("MainWindow", u"Configure", None))
        self.label_99.setText(QCoreApplication.translate("MainWindow", u"Exposure:", None))
        self.cam_snap_btn.setText(QCoreApplication.translate("MainWindow", u"Snapshot", None))
        self.label_101.setText(QCoreApplication.translate("MainWindow", u"Gain:", None))
        self.cam_c.setText(QCoreApplication.translate("MainWindow", u"Enabled", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"Stages", None))
        self.time_c.setText(QCoreApplication.translate("MainWindow", u"Time", None))
        self.y_c.setText(QCoreApplication.translate("MainWindow", u"Y (mm)", None))
        self.z_c.setText(QCoreApplication.translate("MainWindow", u"Z (mm)", None))
        self.s1_c.setText(QCoreApplication.translate("MainWindow", u"S1 (v)", None))
        self.s2_c.setText(QCoreApplication.translate("MainWindow", u"S2 (v)", None))
        self.s0_c.setText(QCoreApplication.translate("MainWindow", u"S0 (v)", None))
        self.label_57.setText(QCoreApplication.translate("MainWindow", u"Home", None))
        self.s3_c.setText(QCoreApplication.translate("MainWindow", u"S3 (v)", None))
        self.label_56.setText(QCoreApplication.translate("MainWindow", u"Vacuum stages", None))
        self.r2_c.setText(QCoreApplication.translate("MainWindow", u"R2 (n.u)", None))
        self.label_59.setText(QCoreApplication.translate("MainWindow", u"Home", None))
        self.label_53.setText(QCoreApplication.translate("MainWindow", u"Rotation stages", None))
        self.r3_c.setText(QCoreApplication.translate("MainWindow", u"R3 (\u00b0)", None))
        self.label_10.setText(QCoreApplication.translate("MainWindow", u"Linear stages", None))
        self.r1_c.setText(QCoreApplication.translate("MainWindow", u"R1 (\u00b0)", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Voltage supply", None))
        self.vz_c.setText(QCoreApplication.translate("MainWindow", u"VZ (mm)", None))
        self.vx_c.setText(QCoreApplication.translate("MainWindow", u"VX (mm)", None))
        self.x_c.setText(QCoreApplication.translate("MainWindow", u"X (mm)", None))
        self.label_58.setText(QCoreApplication.translate("MainWindow", u"Home", None))
        self.vy_c.setText(QCoreApplication.translate("MainWindow", u"VY (mm)", None))
        self.home_x_c.setText("")
        self.home_y_c.setText("")
        self.home_z_c.setText("")
        self.home_r1_c.setText("")
        self.home_r2_c.setText("")
        self.home_r3_c.setText("")
        self.home_vx_c.setText("")
        self.home_vx_a_c.setText("")
        self.home_vy_c.setText("")
        self.home_vy_a_c.setText("")
        self.home_vz_c.setText("")
        self.home_vz_a_c.setText("")
        self.intTime_edit.setText(QCoreApplication.translate("MainWindow", u"1", None))
        self.int_time_s_l.setText(QCoreApplication.translate("MainWindow", u"(s)", None))
        self.intTime_l.setText(QCoreApplication.translate("MainWindow", u"Integration Time:", None))
        self.label_54.setText(QCoreApplication.translate("MainWindow", u"Wait time:", None))
        self.label_95.setText(QCoreApplication.translate("MainWindow", u"Earliest completion time:   ", None))
        self.early_time_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.SST_edit.setText(QCoreApplication.translate("MainWindow", u"0.5", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Minimum exp time:", None))
        self.nofm_l.setText(QCoreApplication.translate("MainWindow", u"Measurements:", None))
        self.min_time_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.label_55.setText(QCoreApplication.translate("MainWindow", u"(s)", None))
        self.amp_c.setText(QCoreApplication.translate("MainWindow", u"Ammeter", None))
        self.err_l.setText("")
        self.run_btn.setText(QCoreApplication.translate("MainWindow", u"Run experiment", None))
        self.wait_l.setText(QCoreApplication.translate("MainWindow", u"Number of scans", None))
        self.dummy_PS_c.setText(QCoreApplication.translate("MainWindow", u"Dummy Powersupply", None))
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"Coordinates & step size", None))
        self.totalsteps_text_l.setText(QCoreApplication.translate("MainWindow", u"Total Steps: ", None))
        self.totalsteps_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"Starting coordinates", None))
        self.nostart_l.setText(QCoreApplication.translate("MainWindow", u"Select stages/special axes to begin...", None))
        self.x_start_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"X", None))
        self.label_8.setText(QCoreApplication.translate("MainWindow", u"X", None))
        self.y_start_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"Y", None))
        self.label_9.setText(QCoreApplication.translate("MainWindow", u"Y", None))
        self.z_start_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"Z", None))
        self.label_11.setText(QCoreApplication.translate("MainWindow", u"Z", None))
        self.r1_start_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"R1", None))
        self.label_12.setText(QCoreApplication.translate("MainWindow", u"R1", None))
        self.r2_start_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"R2", None))
        self.label_13.setText(QCoreApplication.translate("MainWindow", u"R2", None))
        self.r3_start_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"R3", None))
        self.label_14.setText(QCoreApplication.translate("MainWindow", u"R3", None))
        self.vx_start_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"VX", None))
        self.label_15.setText(QCoreApplication.translate("MainWindow", u"VX", None))
        self.vy_start_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"VY", None))
        self.label_16.setText(QCoreApplication.translate("MainWindow", u"VY", None))
        self.vz_start_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"VZ", None))
        self.label_17.setText(QCoreApplication.translate("MainWindow", u"VZ", None))
        self.s0_start_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"S0", None))
        self.label_18.setText(QCoreApplication.translate("MainWindow", u"S0", None))
        self.s1_start_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"S1", None))
        self.label_19.setText(QCoreApplication.translate("MainWindow", u"S1", None))
        self.s2_start_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"S2", None))
        self.label_20.setText(QCoreApplication.translate("MainWindow", u"S2", None))
        self.s3_start_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"S3", None))
        self.label_21.setText(QCoreApplication.translate("MainWindow", u"S3", None))
        self.label_22.setText(QCoreApplication.translate("MainWindow", u"Stopping coordinates", None))
        self.nostop_l.setText(QCoreApplication.translate("MainWindow", u"Select stages/special axes to begin...", None))
        self.x_stop_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"X", None))
        self.label_23.setText(QCoreApplication.translate("MainWindow", u"X", None))
        self.y_stop_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"Y", None))
        self.label_24.setText(QCoreApplication.translate("MainWindow", u"Y", None))
        self.z_stop_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"Z", None))
        self.label_25.setText(QCoreApplication.translate("MainWindow", u"Z", None))
        self.r1_stop_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"R1", None))
        self.label_26.setText(QCoreApplication.translate("MainWindow", u"R1", None))
        self.r2_stop_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"R2", None))
        self.label_27.setText(QCoreApplication.translate("MainWindow", u"R2", None))
        self.r3_stop_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"R3", None))
        self.label_28.setText(QCoreApplication.translate("MainWindow", u"R3", None))
        self.vx_stop_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"VX", None))
        self.label_29.setText(QCoreApplication.translate("MainWindow", u"VX", None))
        self.vy_stop_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"VY", None))
        self.label_30.setText(QCoreApplication.translate("MainWindow", u"VY", None))
        self.vz_stop_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"VZ", None))
        self.label_31.setText(QCoreApplication.translate("MainWindow", u"VZ", None))
        self.s0_stop_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"S0", None))
        self.label_32.setText(QCoreApplication.translate("MainWindow", u"S0", None))
        self.s1_stop_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"S1", None))
        self.label_33.setText(QCoreApplication.translate("MainWindow", u"S1", None))
        self.s2_stop_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"S2", None))
        self.label_34.setText(QCoreApplication.translate("MainWindow", u"S2", None))
        self.s3_stop_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"S3", None))
        self.label_35.setText(QCoreApplication.translate("MainWindow", u"S3", None))
        self.label_36.setText(QCoreApplication.translate("MainWindow", u"Step size", None))
        self.nostep_l.setText(QCoreApplication.translate("MainWindow", u"Select stages/special axes to begin...", None))
        self.x_step_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"X", None))
        self.label_37.setText(QCoreApplication.translate("MainWindow", u"X", None))
        self.y_step_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"Y", None))
        self.label_38.setText(QCoreApplication.translate("MainWindow", u"Y", None))
        self.z_step_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"Z", None))
        self.label_39.setText(QCoreApplication.translate("MainWindow", u"Z", None))
        self.r1_step_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"R1", None))
        self.label_40.setText(QCoreApplication.translate("MainWindow", u"R1", None))
        self.r2_step_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"R2", None))
        self.label_41.setText(QCoreApplication.translate("MainWindow", u"R2", None))
        self.r3_step_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"R3", None))
        self.label_42.setText(QCoreApplication.translate("MainWindow", u"R3", None))
        self.vx_step_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"VX", None))
        self.label_43.setText(QCoreApplication.translate("MainWindow", u"VX", None))
        self.vy_step_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"VY", None))
        self.label_44.setText(QCoreApplication.translate("MainWindow", u"VY", None))
        self.vz_step_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"VZ", None))
        self.label_45.setText(QCoreApplication.translate("MainWindow", u"VZ", None))
        self.s0_step_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"S0", None))
        self.label_46.setText(QCoreApplication.translate("MainWindow", u"S0", None))
        self.s1_step_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"S1", None))
        self.label_47.setText(QCoreApplication.translate("MainWindow", u"S1", None))
        self.s2_step_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"S2", None))
        self.label_48.setText(QCoreApplication.translate("MainWindow", u"S2", None))
        self.s3_step_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"S3", None))
        self.label_49.setText(QCoreApplication.translate("MainWindow", u"S3", None))
        self.label_50.setText(QCoreApplication.translate("MainWindow", u"Axes ordering", None))
        self.label_51.setText(QCoreApplication.translate("MainWindow", u"Select which stage turns first by selecting '1' on that stage, the stage with '2' selected will turn second etc.", None))
        self.noord_l.setText(QCoreApplication.translate("MainWindow", u"Select stages/special axes to begin...", None))
        self.x_ord_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"X", None))
        self.label_79.setText(QCoreApplication.translate("MainWindow", u"X", None))
        self.y_ord_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"Y", None))
        self.label_82.setText(QCoreApplication.translate("MainWindow", u"Y", None))
        self.z_ord_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"Z", None))
        self.label_90.setText(QCoreApplication.translate("MainWindow", u"Z", None))
        self.r1_ord_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"R1", None))
        self.label_86.setText(QCoreApplication.translate("MainWindow", u"R1", None))
        self.r2_ord_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"R2", None))
        self.label_80.setText(QCoreApplication.translate("MainWindow", u"R2", None))
        self.r3_ord_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"R3", None))
        self.label_81.setText(QCoreApplication.translate("MainWindow", u"R3", None))
        self.vx_ord_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"VX", None))
        self.label_84.setText(QCoreApplication.translate("MainWindow", u"VX", None))
        self.vy_ord_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"VY", None))
        self.label_88.setText(QCoreApplication.translate("MainWindow", u"VY", None))
        self.vz_ord_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"VZ", None))
        self.label_85.setText(QCoreApplication.translate("MainWindow", u"VZ", None))
        self.s0_ord_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"S0", None))
        self.label_83.setText(QCoreApplication.translate("MainWindow", u"S0", None))
        self.s1_ord_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"S1", None))
        self.label_87.setText(QCoreApplication.translate("MainWindow", u"S1", None))
        self.s2_ord_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"S2", None))
        self.label_89.setText(QCoreApplication.translate("MainWindow", u"S2", None))
        self.s3_ord_frame.setProperty("axName", QCoreApplication.translate("MainWindow", u"S3", None))
        self.label_91.setText(QCoreApplication.translate("MainWindow", u"S3", None))
        self.save_btn.setText(QCoreApplication.translate("MainWindow", u"Save options", None))
        self.load_btn.setText(QCoreApplication.translate("MainWindow", u"Load options", None))
        self.saved_l.setText("")
        self.progress_l.setText(QCoreApplication.translate("MainWindow", u"Waiting for start", None))
        self.stop_exp_btn.setText(QCoreApplication.translate("MainWindow", u"Stop experiment", None))
        self.exp_time_l.setText(QCoreApplication.translate("MainWindow", u"Experiment took: ???", None))
        self.datManager_btn.setText(QCoreApplication.translate("MainWindow", u"DataManager", None))
        self.v_connect_btn.setText(QCoreApplication.translate("MainWindow", u"Connect", None))
        self.vx_con_l.setText(QCoreApplication.translate("MainWindow", u"Disconnected", None))
        self.right_vx_btn.setText(QCoreApplication.translate("MainWindow", u"--->", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Step size:", None))
        self.label_52.setText(QCoreApplication.translate("MainWindow", u"VX", None))
        self.left_vx_btn.setText(QCoreApplication.translate("MainWindow", u"<---", None))
        self.go_vx_btn.setText(QCoreApplication.translate("MainWindow", u"Go", None))
        self.ref_vx_btn.setText(QCoreApplication.translate("MainWindow", u"Reference", None))
        self.label_60.setText(QCoreApplication.translate("MainWindow", u"Actual Pos:", None))
        self.pos_vx_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.label_61.setText(QCoreApplication.translate("MainWindow", u"Step size:", None))
        self.right_vy_btn.setText(QCoreApplication.translate("MainWindow", u"--->", None))
        self.label_62.setText(QCoreApplication.translate("MainWindow", u"VY", None))
        self.ref_vy_btn.setText(QCoreApplication.translate("MainWindow", u"Reference", None))
        self.go_vy_btn.setText(QCoreApplication.translate("MainWindow", u"Go", None))
        self.left_vy_btn.setText(QCoreApplication.translate("MainWindow", u"<---", None))
        self.vy_con_l.setText(QCoreApplication.translate("MainWindow", u"Disconnected", None))
        self.label_63.setText(QCoreApplication.translate("MainWindow", u"Actual Pos:", None))
        self.pos_vy_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.vz_con_l.setText(QCoreApplication.translate("MainWindow", u"Disconnected", None))
        self.label_64.setText(QCoreApplication.translate("MainWindow", u"VZ", None))
        self.go_vz_btn.setText(QCoreApplication.translate("MainWindow", u"Go", None))
        self.left_vz_btn.setText(QCoreApplication.translate("MainWindow", u"<---", None))
        self.label_65.setText(QCoreApplication.translate("MainWindow", u"Step size:", None))
        self.ref_vz_btn.setText(QCoreApplication.translate("MainWindow", u"Reference", None))
        self.right_vz_btn.setText(QCoreApplication.translate("MainWindow", u"--->", None))
        self.label_66.setText(QCoreApplication.translate("MainWindow", u"Actual Pos:", None))
        self.pos_vz_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.s_connect_btn.setText(QCoreApplication.translate("MainWindow", u"Connect", None))
        self.z_con_l.setText(QCoreApplication.translate("MainWindow", u"Disconnected", None))
        self.label_73.setText(QCoreApplication.translate("MainWindow", u"Z", None))
        self.step_z_edit.setText(QCoreApplication.translate("MainWindow", u"0.25", None))
        self.go_z_btn.setText(QCoreApplication.translate("MainWindow", u"Go", None))
        self.left_z_btn.setText(QCoreApplication.translate("MainWindow", u"<---", None))
        self.label_74.setText(QCoreApplication.translate("MainWindow", u"Step size:", None))
        self.ref_z_btn.setText(QCoreApplication.translate("MainWindow", u"Reference", None))
        self.right_z_btn.setText(QCoreApplication.translate("MainWindow", u"--->", None))
        self.label_75.setText(QCoreApplication.translate("MainWindow", u"Actual Pos:", None))
        self.pos_z_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.label_70.setText(QCoreApplication.translate("MainWindow", u"Step size:", None))
        self.right_y_btn.setText(QCoreApplication.translate("MainWindow", u"--->", None))
        self.label_71.setText(QCoreApplication.translate("MainWindow", u"Y", None))
        self.ref_y_btn.setText(QCoreApplication.translate("MainWindow", u"Reference", None))
        self.step_y_edit.setText(QCoreApplication.translate("MainWindow", u"0.25", None))
        self.go_y_btn.setText(QCoreApplication.translate("MainWindow", u"Go", None))
        self.left_y_btn.setText(QCoreApplication.translate("MainWindow", u"<---", None))
        self.y_con_l.setText(QCoreApplication.translate("MainWindow", u"Disconnected", None))
        self.label_72.setText(QCoreApplication.translate("MainWindow", u"Actual Pos:", None))
        self.pos_y_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.r1_con_l.setText(QCoreApplication.translate("MainWindow", u"Disconnected", None))
        self.step_r1_edit.setText(QCoreApplication.translate("MainWindow", u"10", None))
        self.right_r1_btn.setText(QCoreApplication.translate("MainWindow", u"--->", None))
        self.label_76.setText(QCoreApplication.translate("MainWindow", u"Step size:", None))
        self.label_77.setText(QCoreApplication.translate("MainWindow", u"R1", None))
        self.left_r1_btn.setText(QCoreApplication.translate("MainWindow", u"<---", None))
        self.go_r1_btn.setText(QCoreApplication.translate("MainWindow", u"Go", None))
        self.ref_r1_btn.setText(QCoreApplication.translate("MainWindow", u"Reference", None))
        self.label_78.setText(QCoreApplication.translate("MainWindow", u"Actual Pos:", None))
        self.pos_r1_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.r2_con_l.setText(QCoreApplication.translate("MainWindow", u"Disconnected", None))
        self.label_92.setText(QCoreApplication.translate("MainWindow", u"R2", None))
        self.step_r2_edit.setText(QCoreApplication.translate("MainWindow", u"0.05", None))
        self.go_r2_btn.setText(QCoreApplication.translate("MainWindow", u"Go", None))
        self.left_r2_btn.setText(QCoreApplication.translate("MainWindow", u"<---", None))
        self.label_93.setText(QCoreApplication.translate("MainWindow", u"Step size:", None))
        self.ref_r2_btn.setText(QCoreApplication.translate("MainWindow", u"Reference", None))
        self.right_r2_btn.setText(QCoreApplication.translate("MainWindow", u"--->", None))
        self.label_94.setText(QCoreApplication.translate("MainWindow", u"Actual Pos:", None))
        self.pos_r2_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.x_con_l.setText(QCoreApplication.translate("MainWindow", u"Disconnected", None))
        self.step_x_edit.setText(QCoreApplication.translate("MainWindow", u"0.25", None))
        self.right_x_btn.setText(QCoreApplication.translate("MainWindow", u"--->", None))
        self.label_67.setText(QCoreApplication.translate("MainWindow", u"Step size:", None))
        self.label_68.setText(QCoreApplication.translate("MainWindow", u"X", None))
        self.left_x_btn.setText(QCoreApplication.translate("MainWindow", u"<---", None))
        self.go_x_btn.setText(QCoreApplication.translate("MainWindow", u"Go", None))
        self.ref_x_btn.setText(QCoreApplication.translate("MainWindow", u"Reference", None))
        self.label_69.setText(QCoreApplication.translate("MainWindow", u"Actual Pos:", None))
        self.pos_x_l.setText(QCoreApplication.translate("MainWindow", u"???", None))
        self.n_label.setText(QCoreApplication.translate("MainWindow", u"n:", None))
        self.no_histogram_c.setText(QCoreApplication.translate("MainWindow", u"Exclude histogram data", None))
    # retranslateUi

