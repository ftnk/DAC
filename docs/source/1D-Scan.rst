
from experiment import Experiment


1-D Scan:
===========================================================

For a 1-D scan of using the 'Tektronix_DPO4102B' oscilloscope and thorlabs Kinesis stages, the instrument configuration section should look like following:

* First of all name the experiment:


.. code-block:: python

        
        experimentName = 'MyFirst1Dscan'


Here the name of the oscilloscope is **ScanOsc** , this can be any name , but it must be same throughout.

* **Oscilloscope** section:

**NOTE**: Make sure you are using the correct ip address for the oscilloscope.

.. code-block:: python


        model = 'Tektronix_DPO4102B'
        name= 'ScanOsc'
        ip_address = '192.168.1.5'
        data = [model,name,{'ipAddress':ip_address}] 
        instruments.append(data)


* **Stages** Section
**NOTE**: Make sure you are using the correct stage serial numbers and there is not repetition of axis-names as well as stage names.


.. code-block:: python


        stageData.append(['X-Stage','linear',c_char_p(b'27256338'),'X'])
        #stageData.append(['Y-Stage','linear',c_char_p(b'27255894'),'Y'])
        #stageData.append(['Z-Stage','linear',c_char_p(b'27005114'),'Z'])
        #stageData.append(['rotating','rotational',c_char_p(b'27005004'),'R'])
        #stageData.append(['wiregrid','rotational',c_char_p(b'27255354'),'R'])
        
        
It can be seen that only one stage name **X-Stage** is being used here. The same and axis can be changed as long as they follow the rules as stated in :doc:`non-progg-tutorial` .

Below is the overall configuration of stages.Here the stages are being homed.

.. code-block:: python


        stage_name = 'ScanStage'
        stage_type = 'KinesisController'
        home = True
        data = [stage_type,stage_name,{'createStages':True,'stageData':stageData,'homeStages':home}]  
        instruments.append(data)


Following is the scan configuration of the scan, the scan type is **"1DScan"**. The axis must be same as configured above i.e "X".
The start and end coordinates of the scan are as **[8,0,0]** and **[10,0,0]** respectively and the step size is **1** which means the 
coordinates of scan will be [8,0,0],[9,0,0] and [10,0,0]. 
if the step_size variable is changed to [0.1,0,0], the cooorrdinates of scan will be  :
[8,0,0],[8.1,0,0], [8.2,0,0]...... till [10,0,0]


Following is the scan configuration for the experiments, notice that the names of the scope and stage are the same as defined above.

.. code-block:: python


        moduleType = '1DScan'    
        axes = ['X']             
        startCoords = [8,0,0]      
        stopCoords = [10,0,0]     
        step_size =  [1,0,0]  
        scanStage = 'ScanStage'    
        scanReader = 'ScanOsc'       
        stageSettleTime = 0.5      
        resolution = 100          
        scanner = Scanner(moduleType,axes,startCoords,stopCoords,step_size,scanStage,scanReader,stageSettleTime,resolution)



* save the config file and run runExperiment.bat file.
