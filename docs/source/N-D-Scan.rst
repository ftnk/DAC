


N-D Scan:
===========================================================

* First of all name the experiment:


.. code-block:: python

        
        experimentName = 'MyFirstNDscan'

For a N-D scan using the 'Tektronix_DPO4102B' oscilloscope and thorlabs Kinesis stages, the instrument configuration section should look like following:
Here the name of the oscilloscope is **ScanOsc** , this can be any name , but it must be same throughout.


* **Oscilloscope** section:

.. code-block:: python


        model = 'Tektronix_DPO4102B'
        name= 'ScanOsc'
        ip_address = '192.168.1.5'
        data = [model,name,{'ipAddress':ip_address}] 
        instruments.append(data)


* **Stages** Section

.. code-block:: python


        stageData.append(['X-Stage','linear',c_char_p(b'27256338'),'X'])
        stageData.append(['Y-Stage','linear',c_char_p(b'27255894'),'Y'])
        stageData.append(['Z-Stage','linear',c_char_p(b'27005114'),'Z'])
        stageData.append(['rotating','rotational',c_char_p(b'27005004'),'R1'])
        stageData.append(['wiregrid','rotational',c_char_p(b'27255354'),'R2'])
        stageData.append(['wiregrid1','rotational',c_char_p(b'27255353'),'R3'])
        
        
Here we are using two axes **X** and **Y** axes and they can be changed as long as they follow the rules as stated in :doc:`non-progg-tutorial` .
The second stage can be **'linear'** as well as **'rotational'**.stage_name
**NOTE**: Make sure you are using the correct stage serial numbers and there is not repetition of axis-names as well as stage names.
For an N dimensional scan any nummber of stages can be added as long as they have unique serial numbers,names and axis names.

Name module type must have the correct value for example if you are running a 6D scan it module name must have a value of '6DScan'.

There can only be 1 of X,Y and Z axis and the rest of axis must be named as R1, R2, R3 ... uniquely.

Below is the overall configuration of stages.Here the stages are being homed.

.. code-block:: python


        stage_name = 'ScanStage'
        stage_type = 'KinesisController'
        home = True
        data = [stage_type,stage_name,{'createStages':True,'stageData':stageData,'homeStages':home}]  
        instruments.append(data)


Following is the scan configuration of the scan, the scan type is **"6DScan"** . The axes must be same as configured above.
The start and end coordinates of the scan are as **[8,13,10,0,0,0] ** and **[8,13,10,30,30,30] ** respectively and the step sizes are ** [0.5,0.5,0.5,10,10,10]** .
The effect of varying step_size will be the same  as shown in :doc:`1D-Scan`

.. code-block:: python


        moduleType = '6DScan'    
        axes = ['X','Y','Z','R1','R2','R3']             
        startCoords = [8,13,10,0,0,0]      
        stopCoords = [8,13,10,30,30,30]     
        step_size =  [0.5,0.5,0.5,10,10,10]  

        scanStage = 'ScanStage'    
        scanReader = 'ScanOsc'       
        stageSettleTime = 0.5      
        resolution = 100          
        scanner = Scanner(moduleType,axes,startCoords,stopCoords,step_size,scanStage,scanReader,stageSettleTime,resolution,stageData)


Here the major and minor axes follow the same notion as in the other scans.


* save the config file and run runExperiment.bat file.
