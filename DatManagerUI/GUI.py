# This Python file uses the following encoding: utf-8
import sys
from ctypes import * 

from PySide6.QtWidgets import QApplication, QMainWindow, QFileDialog, QMenu, QToolButton
from PySide6.QtCore import QRect, QThread, SIGNAL
from PySide6.QtCore import QRegularExpression as QRegExp
from PySide6.QtGui import QRegularExpressionValidator as QRegExpValidator
from PySide6.QtGui import QAction


# Important:
# You need to run the following command to generate the ui_form.py file
#     pyside6-uic form.ui -o ui_form.py, or
#     pyside2-uic form.ui -o ui_form.py
from ui_form_DM import Ui_MainWindow
from ExpSpecific.DataVisualizer import DataVisualizer
import json
import numpy as np
import os
import datetime
import time
import h5py
from config import readerType
readerType1 = readerType
if os.path.exists("./ui_data.json"):
    f = open("./ui_data.json", "r")
    UIData = json.load(f)
    f.close()
    readerType1 = UIData["reader_type"]
#if readerType1 == 1:
#    from ExpSpecific.DataHandlerLockIn import DataHandler,Accumulator
#if readerType1 == 0:
from ExpSpecific.DataHandler import DataHandler,Accumulator


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.setWindowTitle("DataManager")
        self.dh = DataHandler()
        self.dv = DataVisualizer(self.dh)
        self.onlyamp = False

        parentPath = os.getcwd()
        lastBackSlashIndex = parentPath.rfind("\\")

        self.expName = parentPath[lastBackSlashIndex + 1 :]
        self.ui.expname_l.setText(self.expName)

        self.oscExists = os.path.exists("OscData.hdf5")
        self.histExist = self.dh.histExist
        self.ampExists = os.path.exists("AmpData.hdf5")
        self.lockinExists = os.path.exists("LockinData.hdf5")
        if self.lockinExists:
            self.ui.sdp_radio.setVisible(False)
            self.ui.scopetrace_radio.setText("Lockin Data")
        self.ui.scopetrace_radio.setVisible(False)
        self.ui.histogram_radio.setVisible(False)
        try:
            self.ui.ammeter_radio.setVisible(False)
        except:
            print(f"Could not locate UI component, please copy the latest 'UI_form_DM.py'")
            input("Press any key to continue...")
        
        if self.oscExists:
            self.ui.scopetrace_radio.setVisible(True)
        if self.histExist:
            self.ui.histogram_radio.setVisible(True)
        if self.ampExists:
            self.ui.ammeter_radio.setVisible(True)

        self.openFiles()
        self.handleAmp()
        self.setMenus()
        self.createLists()
        self.connectSignals()

        if len(self.axes) == 1 and not self.histExist:
            self.ui.d2_radio.setVisible(False)
        
        self.ui.acc_scope_btn.setVisible(False)
        self.ui.acc_hist_btn.setVisible(False)

        
        reg_ex = QRegExp("-{0,1}[0-9]+(\.[0-9]+){1}$")
        for i in range(len(self.inpedits)):
            inpval = QRegExpValidator(reg_ex, self.inpedits[i])
            self.inpedits[i].setValidator(inpval)
            self.inpedits[i].textChanged.connect(self.checkinps)

        self.remainAxes = []
        for i in range(len(self.axes)):
            self.remainAxes.append(self.axes[i])
        
        self.currentAcc = None

        self.latestVoltage = -1

        self.ui.jap_radio.setText("日本")
        
        self.inpsNeeded = 0
        self.viewKeys()
        self.radioChange()
        self.resetInpLabels()
        self.sortMenus()
        self.detectInterrupt()
        self.miscSetup()

    def connectSignals(self):
        ''''
        Connects all the radio buttons to their appropriate methods
        '''
        self.ui.export_radio.toggled.connect(self.radioChange)
        self.ui.visualize_radio.toggled.connect(self.radioChange)

        self.ui.d1_radio.toggled.connect(self.resetBottom)
        self.ui.d1_radio.toggled.connect(self.resetAxisMenus)
        self.ui.d1_radio.toggled.connect(self.resetInpLabels)
        self.ui.d1_radio.toggled.connect(self.radioChange)

        self.ui.d2_radio.toggled.connect(self.resetBottom)
        self.ui.d2_radio.toggled.connect(self.resetAxisMenus)
        self.ui.d2_radio.toggled.connect(self.resetInpLabels)
        self.ui.d2_radio.toggled.connect(self.radioChange)

        self.ui.sdp_radio.toggled.connect(self.resetBottom)
        self.ui.sdp_radio.toggled.connect(self.resetAxisMenus)
        self.ui.sdp_radio.toggled.connect(self.radioChange)

        self.ui.mdp_radio.toggled.connect(self.resetBottom)
        self.ui.mdp_radio.toggled.connect(self.resetAxisMenus)
        self.ui.mdp_radio.toggled.connect(self.resetInpLabels)
        self.ui.mdp_radio.toggled.connect(self.radioChange)

        self.ui.scopetrace_radio.toggled.connect(self.resetBottom)
        self.ui.scopetrace_radio.toggled.connect(self.defaultScopeAcc)
        self.ui.scopetrace_radio.toggled.connect(self.radioChange)

        self.ui.histogram_radio.toggled.connect(self.resetBottom)
        self.ui.histogram_radio.toggled.connect(self.defaultHistAcc)
        self.ui.histogram_radio.toggled.connect(self.radioChange)

        self.ui.ammeter_radio.toggled.connect(self.resetBottom)
        self.ui.ammeter_radio.toggled.connect(self.radioChange)

        self.ui.e_hmap_radio.toggled.connect(self.resetBottom)
        self.ui.e_hmap_radio.toggled.connect(self.radioChange)
        self.ui.e_hmap_radio.toggled.connect(self.defaultVoltage)

        self.ui.r_hmap_radio.toggled.connect(self.resetBottom)
        self.ui.r_hmap_radio.toggled.connect(self.radioChange)

        self.ui.time_radio.toggled.connect(self.resetBottom)
        self.ui.time_radio.toggled.connect(self.radioChange)

        self.ui.energy_radio.toggled.connect(self.resetBottom)
        self.ui.energy_radio.toggled.connect(self.radioChange)
        self.ui.energy_radio.toggled.connect(self.defaultVoltage)
        
        self.ui.eng_radio.toggled.connect(self.changeLanguage)
        self.ui.jap_radio.toggled.connect(self.changeLanguage)
        
        self.ui.run_btn.clicked.connect(self.run)
        self.ax1menu.triggered.connect(self.axis1Chosen)
        self.ax2menu.triggered.connect(self.axis2Chosen)
        self.voltageMenu.triggered.connect(self.voltageChosen)
        self.accScopeMenu.triggered.connect(self.scopeAccChosen)
        self.accHistMenu.triggered.connect(self.histAccChosen)

        self.ui.custom_exp_btn.clicked.connect(self.custom_export)
        self.ui.custom_plot_btn.clicked.connect(self.custom_plot)
        

    def openFiles(self):
        ''''
        Opens the data-files and extracts data, such as valid keys,
        axis-names, start-coords, stop-coords and step-size
        '''
        if self.oscExists:
            self.oscData = h5py.File("OscData.hdf5", "r")
            dataKeys = list(self.oscData.keys())
        elif self.ampExists:
            self.onlyamp = True
            self.ampData = h5py.File("AmpData.hdf5", "r")
            dataKeys = list(self.ampData.keys())
        if self.ampExists:
            self.ampData = h5py.File("AmpData.hdf5", "r")
            dataKeys = list(self.ampData.keys())
        if self.histExist:
            self.histData = h5py.File("HistData.hdf5", "r")
            dataKeys = list(self.histData.keys())
        if self.lockinExists:
            self.lockinData = h5py.File("LockinData.hdf5", "r")
            dataKeys = list(self.lockinData.keys())
            
        if self.histExist:
            self.histData = h5py.File("HistData.hdf5", "r")
            
        self.axes, config = self.getAxInfo()
        self.axDict = {}
        self.revAxDict = {}
        for i in range(len(self.axes)):
            self.axDict[i] = self.axes[i]
            self.revAxDict[self.axes[i]] = i
        self.startCoords = config[0]
        self.stopCoords = config[1]
        self.step_size = config[2]
        self.validDict = {}
        for i in range(len(self.axes)):
            self.validDict[self.axes[i]] = []
        for j in range(len(dataKeys) - 2):
            key = self.keyToArr(dataKeys[j])
            for i in range(len(key)):
                if key[i] not in self.validDict[self.axDict[i]]:
                    self.validDict[self.axDict[i]].append(key[i])
    
    def handleAmp(self):
        if not self.onlyamp:
            return
        self.ui.sdp_radio.setVisible(False)
        self.ui.mdp_radio.setVisible(False)
        self.ui.scopetrace_radio.setVisible(False)
        self.ui.histogram_radio.setVisible(False)
        self.ui.expname_l.setText(self.expName + "\n(AMMETER ONLY)")

    def setMenus(self):
        ''''
        Creates the QMenus needed (1 for histogram acc, 1 for scope-trace acc and 1 for voltage).
        and their underlying QActions, and binds them together 
        '''
        self.voltageMenu = QMenu()
        self.voltageActions = []
        self.voltageOptions = [0, 50, 100, 200, 300, 500, 700, 1000, 1500, 2000]
        for i in self.voltageOptions:
            self.voltageActions.append(QAction(str(i), self))
            self.voltageMenu.addAction(self.voltageActions[-1])
        self.ui.voltage_btn.setMenu(self.voltageMenu)
        self.accScopeMenu = QMenu()
        self.accHistMenu = QMenu()
        integrateAction = QAction("Sum", self)
        maxAction = QAction("Max", self)
        sumAction = QAction("Sum", self)
        peakAction = QAction("Peak", self)
        self.accScopeMenu.addAction(integrateAction)
        self.accScopeMenu.addAction(maxAction)
        self.accHistMenu.addAction(sumAction)
        self.accHistMenu.addAction(peakAction)
        self.ui.acc_scope_btn.setMenu(self.accScopeMenu)
        self.ui.acc_hist_btn.setMenu(self.accHistMenu)

        self.ax1menu = QMenu()
        self.ax2menu = QMenu()
        self.actionsList = [{}, {}]
        for i in range(len(self.axes)):
            action1 = QAction(self.axes[i], self)
            action2 = QAction(self.axes[i], self)
            self.actionsList[0][self.axes[i]] = action1
            self.actionsList[1][self.axes[i]] = action2
            self.ax1menu.addAction(action1)
            self.ax2menu.addAction(action2)
        sep1 = self.ax1menu.addSeparator()
        self.actionsList[0]["sep"] = sep1
        self.ax1menu.addAction(QAction("Reset", self))
        sep2 = self.ax2menu.addSeparator()
        self.actionsList[1]["sep"] = sep2
        self.ax2menu.addAction(QAction("Reset", self))
        
        self.ui.axis_1_btn.setMenu(self.ax1menu)
        self.ui.axis_2_btn.setMenu(self.ax2menu)

        self.ui.acc_hist_btn.setPopupMode(QToolButton.ToolButtonPopupMode.InstantPopup)
        self.ui.acc_scope_btn.setPopupMode(QToolButton.ToolButtonPopupMode.InstantPopup)

        self.ui.axis_1_btn.setPopupMode(QToolButton.ToolButtonPopupMode.InstantPopup)
        self.ui.axis_2_btn.setPopupMode(QToolButton.ToolButtonPopupMode.InstantPopup)

        self.ui.voltage_btn.setPopupMode(QToolButton.ToolButtonPopupMode.InstantPopup)

    def createLists(self):
        ''''
        Creates all the lists needed by the rest of the program
        '''
        self.inpframes = [self.ui.inp_frame_1, self.ui.inp_frame_2, self.ui.inp_frame_3, 
                          self.ui.inp_frame_4, self.ui.inp_frame_5, self.ui.inp_frame_6, 
                          self.ui.inp_frame_7, self.ui.inp_frame_8, self.ui.inp_frame_9, 
                          self.ui.inp_frame_10, self.ui.inp_frame_11, self.ui.inp_frame_12]
        
        self.inplabels = [self.ui.inp_1_l, self.ui.inp_2_l, self.ui.inp_3_l, 
                          self.ui.inp_4_l, self.ui.inp_5_l, self.ui.inp_6_l, 
                          self.ui.inp_7_l, self.ui.inp_8_l, self.ui.inp_9_l, 
                          self.ui.inp_10_l, self.ui.inp_11_l, self.ui.inp_12_l]
        
        self.inpedits = [self.ui.inp_1_edit, self.ui.inp_2_edit, self.ui.inp_3_edit, 
                         self.ui.inp_4_edit, self.ui.inp_5_edit, self.ui.inp_6_edit, 
                         self.ui.inp_7_edit, self.ui.inp_8_edit, self.ui.inp_9_edit, 
                         self.ui.inp_10_edit, self.ui.inp_11_edit, self.ui.inp_12_edit]
        self.axis_sel = [None, None]

        self.axbuttons = [self.ui.axis_1_btn, self.ui.axis_2_btn]
        self.axmenus = [self.ax1menu, self.ax2menu]
        self.optionlist = [self.ui.d1_radio, self.ui.d2_radio, self.ui.sdp_radio, self.ui.mdp_radio]
        self.optionstrings = ["1D Data", "2D Data", "Single Data Point", "Multiple Data Points"]
        self.accMap = {"Integrate": 1, "Max": 2, "Sum": 3, "Peak": 4}
        self.hideList = [self.ui.r_hmap_radio, self.ui.e_hmap_radio, self.ui.time_radio, self.ui.energy_radio, self.ui.voltage_btn,
                         self.ui.acc_hist_btn, self.ui.acc_scope_btn, self.ui.mdp_radio, self.ui.axis_1_btn, self.ui.axis_2_btn,
                         self.ui.ax_and_l, self.ui.none_l, self.ui.range_l, self.ui.range_1_edit, self.ui.range_to_l, self.ui.range_2_edit,
                         self.ui.range_ax_1_l, self.ui.range_ax_1_start_inp, self.ui.range_1_to_l, self.ui.range_ax_1_stop_inp,
                         self.ui.range_ax_2_l, self.ui.range_ax_2_start_inp, self.ui.range_2_to_l, self.ui.range_ax_2_stop_inp]
        self.showAxDict = {"1D": [[self.ui.axis_1_btn, self.ui.range_ax_1_l, self.ui.range_ax_1_start_inp, self.ui.range_1_to_l, self.ui.range_ax_1_stop_inp], 1],
                           "2D": {"energy": [[self.ui.axis_1_btn, self.ui.range_ax_1_l, self.ui.range_ax_1_start_inp, self.ui.range_1_to_l, self.ui.range_ax_1_stop_inp], 1],
                                  "regular": [[self.ui.axis_1_btn, self.ui.ax_and_l, self.ui.axis_2_btn, self.ui.range_ax_1_l, self.ui.range_ax_1_start_inp, self.ui.range_1_to_l, self.ui.range_ax_1_stop_inp, self.ui.range_ax_2_l, self.ui.range_ax_2_start_inp, self.ui.range_2_to_l, self.ui.range_ax_2_stop_inp], 2]},
                           "sdp": [[self.ui.none_l], 0], "mdp": [[self.ui.axis_1_btn, self.ui.range_l, self.ui.range_1_edit, self.ui.range_to_l, self.ui.range_2_edit], 1]}
        self.showAccVoltage = {"scope": [self.ui.acc_scope_btn], "hist": [self.ui.acc_hist_btn], "energy": [self.ui.voltage_btn]}
        self.showRadio = {"1D": [], "2D": {"scope": [], "hist": [self.ui.e_hmap_radio, self.ui.r_hmap_radio], "amp": []}, "sdp": [self.ui.time_radio, self.ui.energy_radio]}
        self.englishWords = ["DataManager", "Export Data", "Visualize Data", "1 Dimension", "2 Dimensions", "Single Data Point", "Multiple Data Points",
                            "Time Axis", "Energy Axis",
                             "Voltage", "Accumulator", "Accumulator", "All Keys:", "All Axes:", "Axis to plot/export:", "Axis", "&", "Axis",
                             "None", "Range:", "to", "Axes Input:", "No axis input needed"]
        self.japaneseWords = ["データマナゲル", "データのエクスポート", "データの可視化", "一次元", "二次元", "単一のデータ ポイント", "複数のデータポイント",
                              "Time Axis", "Energy Axis",
                              "Voltage", "Accumulator", "Accumulator", "すべてのキー", "全軸", "プロット/エクスポートする軸", "Axis", "&", "Axis",
                              "None", "Range:", "to", "軸入力", "No axis input needed"]
        self.widgetsToTranslate= [self.ui.label, self.ui.export_radio, self.ui.visualize_radio, self.ui.d1_radio, self.ui.d2_radio, self.ui.sdp_radio, self.ui.mdp_radio,
                                  self.ui.time_radio, self.ui.energy_radio,
                                  self.ui.voltage_btn, self.ui.acc_scope_btn, self.ui.acc_hist_btn, self.ui.label_5, self.ui.label_2, self.ui.label_3, self.ui.axis_1_btn,
                                  self.ui.ax_and_l, self.ui.axis_2_btn, self.ui.none_l, self.ui.range_l, self.ui.range_to_l, self.ui.label_4, self.ui.no_axis_l]
        self.selAxis = {"eng": "Select axis...", "jap": "Select axis..."}
        self.redWidgets = []
        self.scopeAccs = ["Sum", "Max"]
        self.histAccs = ["Sum", "Peak"]
        self.range1 = [self.ui.range_ax_1_l, self.ui.range_ax_1_start_inp, self.ui.range_1_to_l, self.ui.range_ax_1_stop_inp]
        self.range2 = [self.ui.range_ax_2_l, self.ui.range_ax_2_start_inp, self.ui.range_2_to_l, self.ui.range_ax_2_stop_inp]

    def custom_export(self):
        self.dh.customExport()
    
    def custom_plot(self):
        self.dv.customPlot()
    
    def detectInterrupt(self):
        ''''
        Detects wether or not the experiment was interrupted, by looking in ui_data.json,
        and if it was, sets the new stop corrdinates, and displays them
        '''
        f = open("./ui_data.json", "r")
        UIData = json.load(f)
        f.close()
        if "interrupted" in list(UIData.keys()):
            if UIData["interrupted"]:
                txt = "This experiment was interrupted,\nsome axes might not have completed (see keys)"
                self.ui.interrupt_l.setText(txt)
                return True
        self.ui.interrupt_l.setVisible(False)
    
    def miscSetup(self):
        ''''
        The rest of the setup that didn't fit in the other methods
        '''
        for i, j in zip(self.range1, self.range2):
            i.setVisible(False)
            j.setVisible(False)
        

    def voltageChosen(self, s):
        ''''
        This is run, when ther user selects a voltage
        '''
        vol = s.text()
        self.ui.voltage_btn.setText(vol)
        self.latestVoltage = int(vol)

    def keyFunc(self, item):
        ''''
        Only used to view keys in the 'viewKeys' method
        '''
        lst = [i for i in item]
        return lst

    def viewKeys(self):
        ''''
        Views all the keys, and displays them in the GUI
        '''
        showCustom = False
        keyArr = []
        if self.onlyamp:
            keys = list(self.ampData.keys())
        elif self.oscExists:
            keys = list(self.oscData.keys())
        elif self.histExist:
            keys = list(self.histData.keys())
        elif self.lockinExists:
            keys = list(self.lockinData.keys())
                
        for i in range(len(keys)):
            if "_" in keys[i]:
                showCustom = True
            if keys[i] != "axes" and keys[i] != "config":
                keyArr.append(self.keyToArr(keys[i], True))
        keyArr.sort(key=self.keyFunc)
        keyStr = ""
        for i in keyArr:
            keyStr += str(i) + " "
        self.ui.keys_l.setText(keyStr)
        self.ui.custom_exp_btn.setVisible(showCustom)
        self.ui.custom_plot_btn.setVisible(showCustom)
    
    def defaultVoltage(self):
        ''''
        This method simulates that the user chose '0' voltage, and is run
        whenever the voltage-button is shown
        '''
        if self.ui.voltage_btn.text() not in self.voltageOptions:
            self.voltageChosen(QAction("0", self))
    
    def defaultScopeAcc(self):
        ''''
        This method simulates that the user chose 'Sum' accumulator, and is run
        whenever the scope-trace acc button is shown
        '''
        if self.ui.acc_scope_btn.text() not in self.scopeAccs:
            self.scopeAccChosen(QAction("Sum", self))
    
    def defaultHistAcc(self):
        ''''
        This method simulates that the user chose 'Sum' accumulator, and is run
        whenever the histogram acc button is shown
        '''
        if self.ui.acc_hist_btn.text() not in self.histAccs:
            self.histAccChosen(QAction("Sum", self))

    def scopeAccChosen(self, s):
        ''''
        This method runs when the user selects an accumulator for scope-trace data
        '''
        acc = s.text()
        self.ui.acc_scope_btn.setText(acc)
        self.currentAcc = acc
    
    def histAccChosen(self, s):
        ''''
        This method runs when the user selects an accumulator for histogram data
        '''
        acc = s.text()
        self.ui.acc_hist_btn.setText(acc)
        self.currentAcc = acc

    def findDigits(self,step_size):
        ''''
        Looks at the step-size and determines how many digits are needed
        '''
        maxDigits = -1
        for i in step_size:
            dotIndx = str(i).find(".")+1
            if len(str(i)[dotIndx:]) > maxDigits:
                maxDigits = len(str(i)[dotIndx:])
        return maxDigits
    
    def getInputVals(self):
        ''''
        Gets input-values for the fixed axes from the GUI
        '''
        axes = []
        inputvalues = []
        for i in range(len(self.inpframes)):
            if self.inpframes[i].isVisible():
                axes.append(self.inplabels[i].text())
                inputvalues.append(float(self.inpedits[i].text()))
        return axes, inputvalues
    
    def getDataSource(self):
        if self.lockinExists: return "lock"
        source = ""
        for i in [[self.ui.scopetrace_radio, "osc"], [self.ui.histogram_radio, "hist"], [self.ui.ammeter_radio, "amp"]]:
            if i[0].isChecked():
                source = i[1]
        return source

    def export(self, option, axes, inputvalues, varAxes):
        ''''
        Exports data.
        This method is run when the 'export' radio button is chosen, and the user presses 'GO'
        '''
        #for i in [option, axes, inputvalues, varAxes]: print(i)
        dSource = self.getDataSource()
        useHist = self.ui.histogram_radio.isChecked()
        digits = self.findDigits(self.step_size)
        useAmp = self.ui.ammeter_radio.isChecked()
        if self.onlyamp:
            self.currentAcc = "Sum"
        if readerType1 == 0:
            if self.ui.d1_radio.isChecked() or (self.ui.d2_radio.isChecked() and self.ui.scopetrace_radio.isChecked()):
                if not self.onlyamp and not self.lockinExists and not self.ui.ammeter_radio.isChecked():
                    self.dh.setAccumulator(self.accMap[self.currentAcc])
        if option == 0:
            rMap = {"start": float(self.ui.range_ax_1_start_inp.text()), "stop": float(self.ui.range_ax_1_stop_inp.text()), "index": self.revAxDict[self.ui.axis_1_btn.text()]}

            self.dh.save1Ddata(axes, self.axes, inputvalues, dSource, rangeMap=rMap)
        elif option == 1:  
            if self.ui.r_hmap_radio.isChecked() or self.ui.scopetrace_radio.isChecked() or self.ui.ammeter_radio.isChecked():
                if not self.lockinExists and not self.ui.ammeter_radio.isChecked():
                    self.dh.setAccumulator(self.accMap[self.currentAcc])
                rMap = {"start": [float(self.ui.range_ax_1_start_inp.text()), float(self.ui.range_ax_2_start_inp.text())],
                        "stop": [float(self.ui.range_ax_1_stop_inp.text()), float(self.ui.range_ax_2_stop_inp.text())],
                        "index": [self.revAxDict[self.ui.axis_1_btn.text()], self.revAxDict[self.ui.axis_2_btn.text()]]}
                self.dh.save2Ddata(axes, inputvalues, dSource, rangeMap=rMap, axisMap={"X": self.ui.axis_1_btn.text(), "Y": self.ui.axis_2_btn.text()})
            elif self.ui.e_hmap_radio.isChecked():
                axPlot = varAxes[0]
                axDict = {}
                for i in range(len(self.axes)):
                    axDict[self.axes[i]] = i
                #print(f"\naxPlot: {axPlot}\naxes: {axes}\naxdict: {axDict}\nvals: {inputvalues}\nsss: {[self.startCoords, self.stopCoords, self.step_size]}\nvoltage: {self.latestVoltage}\ndigits: {digits}\n")
                rMap = {"start": float(self.ui.range_ax_1_start_inp.text()), "stop": float(self.ui.range_ax_1_stop_inp.text()), "index": self.revAxDict[self.ui.axis_1_btn.text()]}
                self.dh.saveEnergyHeatMap(axPlot, axes, axDict, inputvalues, [self.startCoords, self.stopCoords, self.step_size], self.latestVoltage, digits, rMap)
        elif option == 2:
            self.dh.setAccumulator(0)
            if self.ui.time_radio.isChecked():
                self.dh.saveSingleData(inputvalues, dSource, digits)
            elif self.ui.energy_radio.isChecked():
                self.dh.saveSingleDataEnergyConv(inputvalues, dSource, digits, self.latestVoltage)
        elif option == 3:
            self.dh.save1D_Bulk(axes, inputvalues, float(self.ui.range_1_edit.text()), float(self.ui.range_2_edit.text()), dSource, digits)

    
    def visualize(self, option, axes, inputvalues, varAxes):
        ''''
        Visualizes data.
        This method is run when the 'visualize' radio button is chosen, and the user presses 'GO'
        '''
        useHist = self.ui.histogram_radio.isChecked()
        useAmp = self.ui.ammeter_radio.isChecked()
        dSource = self.getDataSource()
        
        digits = self.findDigits(self.step_size)
        if self.onlyamp:
            self.currentAcc = "Sum"
        if readerType1 == 0:
            if (self.ui.d1_radio.isChecked() or (self.ui.d2_radio.isChecked() and self.ui.scopetrace_radio.isChecked())) and not self.lockinExists:
                if not useAmp:
                    self.dh.setAccumulator(self.accMap[self.currentAcc])
        if option == 0:
            rMap = {"start": float(self.ui.range_ax_1_start_inp.text()), "stop": float(self.ui.range_ax_1_stop_inp.text()), "index": self.revAxDict[self.ui.axis_1_btn.text()]}
            self.dv.OneDPlot(axes, inputvalues, dSource, rMap)
        elif option == 1:
            if self.ui.r_hmap_radio.isChecked() or self.ui.scopetrace_radio.isChecked() or self.onlyamp or useAmp:
                if not self.lockinExists and not useAmp:
                    self.dh.setAccumulator(self.accMap[self.currentAcc])
                rMap = {"start": [float(self.ui.range_ax_1_start_inp.text()), float(self.ui.range_ax_2_start_inp.text())],
                        "stop": [float(self.ui.range_ax_1_stop_inp.text()), float(self.ui.range_ax_2_stop_inp.text())],
                        "index": [self.revAxDict[self.ui.axis_1_btn.text()], self.revAxDict[self.ui.axis_2_btn.text()]]}
                self.dv.heatMap(axes, inputvalues, dSource, rMap, {"X": self.ui.axis_1_btn.text(), "Y": self.ui.axis_2_btn.text()})
            elif self.ui.e_hmap_radio.isChecked():
                axPlot = varAxes[0]
                axDict = {}
                for i in range(len(self.axes)):
                    axDict[self.axes[i]] = i
                rMap = {"start": float(self.ui.range_ax_1_start_inp.text()), "stop": float(self.ui.range_ax_1_stop_inp.text()), "index": self.revAxDict[self.ui.axis_1_btn.text()]}
                self.dv.energyHeatMap(axPlot, axes, axDict, inputvalues, [self.startCoords, self.stopCoords, self.step_size], self.latestVoltage, digits, rMap)
        elif option == 2:
            self.dh.setAccumulator(0)
            self.dv.plotPoint(inputvalues, dSource)

    
    def reportError(self, err, widgets):
        ''''
        Used for reporting errors to the user, and marking the error with red
        '''
        print(f"ERROR: {err}")
        for i in widgets:
            if i not in self.redWidgets:
                self.redWidgets.append(i)
            i.setStyleSheet("color: red;")
        
    
    def validityCheck(self):
        ''''
        Checking that all input in the GUI is valid, and ready to export/visualize
        '''
        dims = [self.ui.d1_radio.isChecked(), self.ui.d2_radio.isChecked(), self.ui.sdp_radio.isChecked(), self.ui.mdp_radio.isChecked()]
        if not self.ui.export_radio.isChecked() and not self.ui.visualize_radio.isChecked():
            self.reportError("Choose Export or Visualize!", [self.ui.export_radio, self.ui.visualize_radio])
            return False
        if not any(dims):
            self.reportError("Choose '1D', '2D', 'Single Data Point' or 'Multiple Data Points'!", [self.ui.d1_radio, self.ui.d2_radio, self.ui.sdp_radio, self.ui.mdp_radio])
            return False
        if not self.ui.scopetrace_radio.isChecked() and not self.ui.histogram_radio.isChecked() and not self.ui.ammeter_radio.isChecked() and not self.onlyamp:
            self.reportError("Choose 'Scope-trace Data' or 'Histogram Data'!", [self.ui.scopetrace_radio, self.ui.histogram_radio])
            return False
        if self.ui.e_hmap_radio.isVisible() and self.ui.r_hmap_radio.isVisible():
            if not self.ui.e_hmap_radio.isChecked() and not self.ui.r_hmap_radio.isChecked():
                self.reportError("Choose 'Regular Heatmap' or 'Energy Heatmap'!", [self.ui.r_hmap_radio, self.ui.e_hmap_radio])
                return False
        if self.ui.time_radio.isVisible() and self.ui.energy_radio.isVisible():
            if not self.ui.time_radio.isChecked() and not self.ui.energy_radio.isChecked():
                self.reportError("Choose 'Time Axis' or 'Energy Axis'!", [self.ui.time_radio, self.ui.energy_radio])
                return False
        if self.ui.acc_scope_btn.isVisible():
            if self.currentAcc not in self.scopeAccs:
                #print(self.scopeAccs)
                #print(self.currentAcc)
                self.reportError("Choose an accumulator!", [self.ui.acc_scope_btn])
                return False
        if self.ui.acc_hist_btn.isVisible():
            if self.currentAcc not in self.histAccs:
                self.reportError("Choose an accumulator!", [self.ui.acc_hist_btn])
                return False
        if self.ui.voltage_btn.isVisible():
            if self.latestVoltage not in self.voltageOptions:
                self.reportError("Choose a voltage!", [self.ui.voltage_btn])
                return False
        if self.ui.axis_1_btn.isVisible():
            if self.ui.axis_1_btn.text() not in self.axes:
                self.reportError("Choose an axis!", [self.ui.axis_1_btn])
                return False
        if self.ui.axis_2_btn.isVisible():
            if self.ui.axis_2_btn.text() not in self.axes:
                self.reportError("Choose an axis!", [self.ui.axis_2_btn])
                return False
        if self.ui.range_1_edit.isVisible():
            if len(self.ui.range_1_edit.text()) == 0 or len(self.ui.range_2_edit.text()) == 0:
                self.reportError("Input range!", [self.ui.range_l])
                return False
        for i in range(self.inpsNeeded):
            if len(self.inpedits[i].text()) == 0:
                self.reportError("Input axis values!", [self.inplabels[i]])
                return False
        return True

    def run(self):
        ''''
        This method is run when the user presses 'GO'
        This method is responsible for calling visualize / export, depending on the radiobuttons
        and running a validity check
        '''
        for i in self.redWidgets:
            i.setStyleSheet("color: black;")
        if not self.validityCheck():
            return
        option = -1
        for i in range(len(self.optionlist)):
            if self.optionlist[i].isChecked():
                option = i
        axes, inputvalues = self.getInputVals()
        varAxes = self.getVarAxes()
        if self.ui.export_radio.isChecked():
            self.export(option, axes, inputvalues, varAxes)
        elif self.ui.visualize_radio.isChecked():
            self.visualize(option, axes, inputvalues, varAxes)
    
    def getVarAxes(self):
        ''''
        Gets variable axes (meaning the ones where the user doesn't input a fixed value)
        '''
        if self.ui.none_l.isVisible():
            return -1
        varAxes = []
        varAxes.append(self.ui.axis_1_btn.text())
        if self.ui.axis_2_btn.isVisible():
            varAxes.append(self.ui.axis_2_btn.text())
        return varAxes


    def keyToArr(self, key, asFloat=False):
        ''''
        Converts keys from the data-file to actual arrays instead of string
        '''
        if asFloat:
            keyArr = key.replace("[", "").replace("]", "").replace(" ", "").split(",")
            keyArr = [float(i) for i in keyArr]
        else:
            keyArr = key.replace("[", "").replace("]", "").replace(" ", "").split(",")
        return keyArr

    def resetBottom(self):
        ''''
        Resets the bottom half of the screen
        '''
        if not self.ui.sdp_radio.isChecked():
            self.axisChosen(QAction("Reset", self), 0)
            self.axisChosen(QAction("Reset", self), 1)
            self.resetAxisMenus()
        

    def checkinps(self):
        ''''
        Checks that the user have entered valid inputs for the fixed axes
        '''
        for i in range(self.inpsNeeded):
            self.inplabels[i].setStyleSheet("color: red;")
            if len(self.inpedits[i].text()) != 0:
                try:
                    inpTxt = self.inpedits[i].text()
                    if str(inpTxt) == "-":
                        continue
                    if str(float(inpTxt)) in self.validDict[self.inplabels[i].text()]:
                        self.inplabels[i].setStyleSheet("color: green;")
                except:
                    self.reportError("Couldn't convert input to float", [self.inplabels[i]])

    def axis1Chosen(self, s):
        self.axisChosen(s, 0)
    
    def axis2Chosen(self, s):
        self.axisChosen(s, 1)
    
    def axisChosen(self, s, n, defaultAxis=None):
        #print(f"axisChosen({s}, {n}, {defaultAxis})")
        if defaultAxis == None:
            axis = s.text()
        else:
            axis = defaultAxis
        other = (n + 1) % 2
        before = self.axis_sel[n]
        rangeAxLabels = [self.ui.range_ax_1_l, self.ui.range_ax_2_l]
        rangeStartInps = [self.ui.range_ax_1_start_inp, self.ui.range_ax_2_start_inp]
        rangeStopInps = [self.ui.range_ax_1_stop_inp, self.ui.range_ax_2_stop_inp]
        if axis == "Reset":
            self.axis_sel[n] = None
            self.axbuttons[n].setText("Axis")
            for i in self.inplabels:
                i.setText("Select axis...")
            rangeAxLabels[n].setText("Axis: ")
            rangeStartInps[n].setText("")
            rangeStopInps[n].setText("")
        else:
            self.axis_sel[n] = axis
            self.axbuttons[n].setText(axis)
            rangeAxLabels[n].setText(axis + ": ")
            rangeStartInps[n].setText(str(self.startCoords[self.revAxDict[axis]]))
            rangeStopInps[n].setText(str(self.stopCoords[self.revAxDict[axis]]))
        
        if before != None:
            self.remainAxes.append(before)
            self.axmenus[other].insertAction(self.actionsList[other]["sep"], self.actionsList[other][before])
            self.axmenus[n].insertAction(self.actionsList[n]["sep"], self.actionsList[n][before])
        if axis in self.remainAxes:
            self.remainAxes.remove(axis)
        if axis != "Reset":
            self.axmenus[other].removeAction(self.actionsList[other][axis])
            self.axmenus[n].removeAction(self.actionsList[n][axis])
        
        if self.ui.axis_2_btn.isVisible():
            if self.axis_sel[0] != None and self.axis_sel[1] != None:
                newLabels = []
                for i in self.axes:
                    if i not in self.axis_sel:
                        newLabels.append(i)
                self.updateInpLabels(newLabels)
        else:
            if self.axis_sel[0] != None:
                newLabels = []
                for i in self.axes:
                    if i not in self.axis_sel:
                        newLabels.append(i)
                self.updateInpLabels(newLabels)
        self.sortMenus()
    
    def sortMenus(self):
        action1Dict = {}
        action2Dict = {}
        for i in self.ax1menu.actions():
            if i.isSeparator():
                break
            action1Dict[i.text()] = i
        for i in self.ax2menu.actions():
            if i.isSeparator():
                break
            action2Dict[i.text()] = i
        sortedKeys1 = sorted(action1Dict.keys())
        sortedKeys2 = sorted(action2Dict.keys())
        for i in sortedKeys1:
            self.ax1menu.removeAction(action1Dict[i])
        for i in sortedKeys2:
            self.ax2menu.removeAction(action2Dict[i])
        for i in range(len(sortedKeys1)):
            self.ax1menu.insertAction(self.actionsList[0]["sep"], action1Dict[sortedKeys1[i]])
        for i in range(len(sortedKeys2)):
            self.ax2menu.insertAction(self.actionsList[1]["sep"], action2Dict[sortedKeys2[i]])
        
        
    def resetAxisMenus(self):
        for i in range(len(self.axbuttons)):
            self.axbuttons[i].setText("Axis")
            self.remainAxes = []
            for j in range(len(self.axes)):
                self.remainAxes.append(self.axes[j])
        for i in range(len(self.axis_sel)):
            if self.axis_sel[i] != None:
                for j in range(len(self.axmenus)):
                    self.axmenus[j].insertAction(self.actionsList[j]["sep"], self.actionsList[j][self.axis_sel[i]])
        self.axis_sel = [None, None]
        for i in range(len(self.inplabels)):
            self.inplabels[i].setStyleSheet("color: black;")
            self.inpedits[i].setText("")
    
    def resetInpLabels(self):
        for i in range(len(self.inplabels)):
            if self.ui.eng_radio.isChecked():
                self.inplabels[i].setText(self.selAxis["eng"])
            if self.ui.jap_radio.isChecked():
                self.inplabels[i].setText(self.selAxis["jap"])

    def hideAll(self):
        for i in self.hideList:
            i.setVisible(False)

    def radioChange(self):
        self.hideAll()
        showAxList = []
        showRadioList = []
        showButton = None
        usedAxis = len(self.axes)
        if self.ui.ammeter_radio.isChecked():
            self.ui.sdp_radio.setVisible(False)
            self.ui.sdp_radio.setChecked(False)
            self.ui.mdp_radio.setVisible(False)
            self.ui.mdp_radio.setChecked(False)
        if self.ui.histogram_radio.isChecked() or self.ui.scopetrace_radio.isChecked():
            self.ui.sdp_radio.setVisible(True)

        if self.ui.export_radio.isChecked() and not self.dh.lockinExp and not self.onlyamp:
            if not self.ui.ammeter_radio.isChecked():
                self.ui.mdp_radio.setVisible(True)
        if self.ui.d1_radio.isChecked():
            showAxList = self.showAxDict["1D"][0]
            usedAxis = self.showAxDict["1D"][1]
            showRadioList = self.showRadio["1D"]
        if self.ui.d2_radio.isChecked():
            showAxDict = self.showAxDict["2D"]
            if len(self.axes) < 2:
                self.ui.scopetrace_radio.setChecked(False)
                self.ui.histogram_radio.setChecked(True)
                self.ui.r_hmap_radio.setChecked(False)
                self.ui.e_hmap_radio.setChecked(True)
            if self.ui.scopetrace_radio.isChecked() or self.ui.r_hmap_radio.isChecked() or self.ui.ammeter_radio.isChecked():
                showAxList = showAxDict["regular"][0]
                usedAxis = showAxDict["regular"][1]
            if self.ui.histogram_radio.isChecked():
                showRadioList = self.showRadio["2D"]["hist"]
                if self.ui.e_hmap_radio.isChecked():
                    showAxList = showAxDict["energy"][0]
                    usedAxis = showAxDict["energy"][1]
                    if not self.ui.visualize_radio.isChecked():
                        showButton = self.ui.voltage_btn
        if self.ui.sdp_radio.isChecked():
            showAxList = self.showAxDict["sdp"][0]
            usedAxis = self.showAxDict["sdp"][1]
            if not self.ui.visualize_radio.isChecked():
                showRadioList = self.showRadio["sdp"]
            if not self.ui.time_radio.isChecked() and not self.ui.energy_radio.isChecked():
                self.ui.time_radio.setChecked(True)
            if self.ui.energy_radio.isChecked() and self.ui.voltage_btn not in showAxList and not self.ui.visualize_radio.isChecked():
                showAxList.append(self.ui.voltage_btn)
            elif self.ui.time_radio.isChecked() and self.ui.voltage_btn in showAxList:
                showAxList.remove(self.ui.voltage_btn)
            self.updateInpLabels(self.axes)
        
        if self.ui.mdp_radio.isChecked():
            showAxList = self.showAxDict["mdp"][0]
            usedAxis = self.showAxDict["mdp"][1]

        if showButton == None:
            if self.ui.scopetrace_radio.isChecked() and not self.ui.sdp_radio.isChecked() and not self.ui.visualize_radio.isChecked():
                showButton = self.ui.acc_scope_btn
            if self.ui.histogram_radio.isChecked() and not self.ui.sdp_radio.isChecked() and not self.ui.visualize_radio.isChecked():
                showButton = self.ui.acc_hist_btn
        if showButton != None and not self.onlyamp and not self.dh.lockinExp:
            showButton.setVisible(True)
        for i in showAxList:
            i.setVisible(True)
        for i in showRadioList:
            i.setVisible(True)
        
        axInps = 0

        if self.ui.axis_2_btn.isVisible():
            axInps = 2
        elif self.ui.axis_1_btn.isVisible():
            axInps = 1
        self.setDefaultAxes(axInps)
        self.updateInpFrames(len(self.axes) - usedAxis)
    
    def setDefaultAxes(self, n):
        if n == len(self.axes):
            for i in range(n):
                self.axisChosen(None, i, self.axes[i])

    def updateInpFrames(self, n_varAxis):
        self.inpsNeeded = n_varAxis
        for i in range(0, len(self.inpframes), 1):
            cond = i + 1 > n_varAxis
            self.inpframes[i].setVisible(not cond)
        self.ui.no_axis_l.setVisible(n_varAxis == 0)
    
    def updateInpLabels(self, labels):
        for i in range(len(labels)):
            self.inplabels[i].setText(labels[i])
        
    def getAxInfo(self):
        if self.onlyamp:
            data = self.ampData
        elif self.oscExists:
            data = self.oscData
        elif self.histExist:
            data = self.histData
        elif self.lockinExists:
            data = self.lockinData
        axes = [x.decode() for x in list(data.get("axes"))]
        config = list(data.get("config"))
        axes_str = ""
        for i in range(len(axes)):
            axes_str += axes[i] + ": [" + str(config[0][i]) + " : " + str(config[1][i]) + "] (" + str(config[2][i]) + ")     "
        self.ui.ax_vals_l.setText(axes_str)
        return (axes, config)

    def closeEvent(self, event):
        self.dv.closeAll()
        self.close()
    
    def changeLanguage(self):
        if self.ui.eng_radio.isChecked():
            self.setWindowTitle("DataManager")
            self.ui.scopetrace_radio.setVisible(True)
            if not self.lockinExists:
                self.ui.histogram_radio.setVisible(True)
            for i, j in zip(self.widgetsToTranslate, self.englishWords):
                i.setText(j)
        if self.ui.jap_radio.isChecked():
            self.ui.scopetrace_radio.setVisible(False)
            self.ui.histogram_radio.setVisible(False)
            self.ui.scopetrace_radio.setChecked(True)
            self.ui.histogram_radio.setChecked(False)
            self.setWindowTitle("データマナゲル")
            for i, j in zip(self.widgetsToTranslate, self.japaneseWords):
                i.setText(j)
    
if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = MainWindow()
    widget.show()
    sys.exit(app.exec())
