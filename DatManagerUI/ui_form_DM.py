# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form_DM.ui'
##
## Created by: Qt User Interface Compiler version 6.5.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractScrollArea, QApplication, QButtonGroup, QFrame,
    QGridLayout, QHBoxLayout, QLabel, QLineEdit,
    QMainWindow, QPushButton, QRadioButton, QScrollArea,
    QSizePolicy, QSpacerItem, QTextEdit, QToolButton,
    QVBoxLayout, QWidget)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(640, 632)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy1)
        self.gridLayout_3 = QGridLayout(self.centralwidget)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.gridLayout_3.setContentsMargins(0, 0, 0, 0)
        self.scrollArea_2 = QScrollArea(self.centralwidget)
        self.scrollArea_2.setObjectName(u"scrollArea_2")
        sizePolicy1.setHeightForWidth(self.scrollArea_2.sizePolicy().hasHeightForWidth())
        self.scrollArea_2.setSizePolicy(sizePolicy1)
        self.scrollArea_2.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollArea_2.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scrollArea_2.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.scrollArea_2.setWidgetResizable(False)
        self.scrollAreaWidgetContents_2 = QWidget()
        self.scrollAreaWidgetContents_2.setObjectName(u"scrollAreaWidgetContents_2")
        self.scrollAreaWidgetContents_2.setGeometry(QRect(0, 0, 624, 684))
        self.eng_radio = QRadioButton(self.scrollAreaWidgetContents_2)
        self.buttonGroup_5 = QButtonGroup(MainWindow)
        self.buttonGroup_5.setObjectName(u"buttonGroup_5")
        self.buttonGroup_5.addButton(self.eng_radio)
        self.eng_radio.setObjectName(u"eng_radio")
        self.eng_radio.setGeometry(QRect(50, 15, 91, 22))
        self.eng_radio.setChecked(True)
        self.jap_radio = QRadioButton(self.scrollAreaWidgetContents_2)
        self.buttonGroup_5.addButton(self.jap_radio)
        self.jap_radio.setObjectName(u"jap_radio")
        self.jap_radio.setGeometry(QRect(50, 45, 91, 22))
        self.frame = QFrame(self.scrollAreaWidgetContents_2)
        self.frame.setObjectName(u"frame")
        self.frame.setGeometry(QRect(40, 95, 281, 221))
        self.frame.setFrameShape(QFrame.Box)
        self.frame.setFrameShadow(QFrame.Plain)
        self.gridLayoutWidget = QWidget(self.frame)
        self.gridLayoutWidget.setObjectName(u"gridLayoutWidget")
        self.gridLayoutWidget.setGeometry(QRect(10, 10, 269, 211))
        self.gridLayout = QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.export_radio = QRadioButton(self.gridLayoutWidget)
        self.buttonGroup = QButtonGroup(MainWindow)
        self.buttonGroup.setObjectName(u"buttonGroup")
        self.buttonGroup.addButton(self.export_radio)
        self.export_radio.setObjectName(u"export_radio")

        self.verticalLayout.addWidget(self.export_radio)

        self.visualize_radio = QRadioButton(self.gridLayoutWidget)
        self.buttonGroup.addButton(self.visualize_radio)
        self.visualize_radio.setObjectName(u"visualize_radio")

        self.verticalLayout.addWidget(self.visualize_radio)

        self.verticalSpacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_4)

        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.d1_radio = QRadioButton(self.gridLayoutWidget)
        self.buttonGroup_2 = QButtonGroup(MainWindow)
        self.buttonGroup_2.setObjectName(u"buttonGroup_2")
        self.buttonGroup_2.addButton(self.d1_radio)
        self.d1_radio.setObjectName(u"d1_radio")

        self.verticalLayout_2.addWidget(self.d1_radio)

        self.d2_radio = QRadioButton(self.gridLayoutWidget)
        self.buttonGroup_2.addButton(self.d2_radio)
        self.d2_radio.setObjectName(u"d2_radio")

        self.verticalLayout_2.addWidget(self.d2_radio)

        self.sdp_radio = QRadioButton(self.gridLayoutWidget)
        self.buttonGroup_2.addButton(self.sdp_radio)
        self.sdp_radio.setObjectName(u"sdp_radio")

        self.verticalLayout_2.addWidget(self.sdp_radio)

        self.mdp_radio = QRadioButton(self.gridLayoutWidget)
        self.buttonGroup_2.addButton(self.mdp_radio)
        self.mdp_radio.setObjectName(u"mdp_radio")

        self.verticalLayout_2.addWidget(self.mdp_radio)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer_3)


        self.verticalLayout.addLayout(self.verticalLayout_2)


        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)

        self.verticalLayout_3 = QVBoxLayout()
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.scopetrace_radio = QRadioButton(self.gridLayoutWidget)
        self.buttonGroup_3 = QButtonGroup(MainWindow)
        self.buttonGroup_3.setObjectName(u"buttonGroup_3")
        self.buttonGroup_3.addButton(self.scopetrace_radio)
        self.scopetrace_radio.setObjectName(u"scopetrace_radio")

        self.verticalLayout_3.addWidget(self.scopetrace_radio)

        self.histogram_radio = QRadioButton(self.gridLayoutWidget)
        self.buttonGroup_3.addButton(self.histogram_radio)
        self.histogram_radio.setObjectName(u"histogram_radio")

        self.verticalLayout_3.addWidget(self.histogram_radio)

        self.ammeter_radio = QRadioButton(self.gridLayoutWidget)
        self.buttonGroup_3.addButton(self.ammeter_radio)
        self.ammeter_radio.setObjectName(u"ammeter_radio")

        self.verticalLayout_3.addWidget(self.ammeter_radio)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer)

        self.r_hmap_radio = QRadioButton(self.gridLayoutWidget)
        self.buttonGroup_4 = QButtonGroup(MainWindow)
        self.buttonGroup_4.setObjectName(u"buttonGroup_4")
        self.buttonGroup_4.addButton(self.r_hmap_radio)
        self.r_hmap_radio.setObjectName(u"r_hmap_radio")

        self.verticalLayout_3.addWidget(self.r_hmap_radio)

        self.e_hmap_radio = QRadioButton(self.gridLayoutWidget)
        self.buttonGroup_4.addButton(self.e_hmap_radio)
        self.e_hmap_radio.setObjectName(u"e_hmap_radio")

        self.verticalLayout_3.addWidget(self.e_hmap_radio)

        self.time_radio = QRadioButton(self.gridLayoutWidget)
        self.time_radio.setObjectName(u"time_radio")

        self.verticalLayout_3.addWidget(self.time_radio)

        self.energy_radio = QRadioButton(self.gridLayoutWidget)
        self.energy_radio.setObjectName(u"energy_radio")

        self.verticalLayout_3.addWidget(self.energy_radio)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer_2)

        self.voltage_btn = QToolButton(self.gridLayoutWidget)
        self.voltage_btn.setObjectName(u"voltage_btn")
        self.voltage_btn.setMinimumSize(QSize(95, 0))
        self.voltage_btn.setMaximumSize(QSize(95, 16777215))
        self.voltage_btn.setCheckable(False)
        self.voltage_btn.setPopupMode(QToolButton.MenuButtonPopup)
        self.voltage_btn.setToolButtonStyle(Qt.ToolButtonTextOnly)
        self.voltage_btn.setAutoRaise(False)

        self.verticalLayout_3.addWidget(self.voltage_btn)

        self.acc_scope_btn = QToolButton(self.gridLayoutWidget)
        self.acc_scope_btn.setObjectName(u"acc_scope_btn")
        self.acc_scope_btn.setMinimumSize(QSize(95, 0))
        self.acc_scope_btn.setMaximumSize(QSize(95, 16777215))
        self.acc_scope_btn.setCheckable(False)
        self.acc_scope_btn.setPopupMode(QToolButton.MenuButtonPopup)
        self.acc_scope_btn.setToolButtonStyle(Qt.ToolButtonTextOnly)
        self.acc_scope_btn.setAutoRaise(False)

        self.verticalLayout_3.addWidget(self.acc_scope_btn)

        self.acc_hist_btn = QToolButton(self.gridLayoutWidget)
        self.acc_hist_btn.setObjectName(u"acc_hist_btn")
        self.acc_hist_btn.setMinimumSize(QSize(95, 0))
        self.acc_hist_btn.setMaximumSize(QSize(95, 16777215))
        self.acc_hist_btn.setCheckable(False)
        self.acc_hist_btn.setPopupMode(QToolButton.MenuButtonPopup)
        self.acc_hist_btn.setToolButtonStyle(Qt.ToolButtonTextOnly)
        self.acc_hist_btn.setAutoRaise(False)

        self.verticalLayout_3.addWidget(self.acc_hist_btn)


        self.gridLayout.addLayout(self.verticalLayout_3, 0, 2, 1, 1)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout.addItem(self.horizontalSpacer_3, 0, 1, 1, 1)

        self.scrollArea = QScrollArea(self.scrollAreaWidgetContents_2)
        self.scrollArea.setObjectName(u"scrollArea")
        self.scrollArea.setGeometry(QRect(40, 325, 561, 266))
        self.scrollArea.setFrameShape(QFrame.Box)
        self.scrollArea.setFrameShadow(QFrame.Plain)
        self.scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.scrollArea.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scrollArea.setSizeAdjustPolicy(QAbstractScrollArea.AdjustIgnored)
        self.scrollArea.setWidgetResizable(False)
        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 979, 315))
        self.gridLayoutWidget_2 = QWidget(self.scrollAreaWidgetContents)
        self.gridLayoutWidget_2.setObjectName(u"gridLayoutWidget_2")
        self.gridLayoutWidget_2.setGeometry(QRect(0, 0, 1103, 246))
        self.gridLayout_2 = QGridLayout(self.gridLayoutWidget_2)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.gridLayout_2.setVerticalSpacing(0)
        self.gridLayout_2.setContentsMargins(10, 0, 0, 0)
        self.ax_vals_l = QLabel(self.gridLayoutWidget_2)
        self.ax_vals_l.setObjectName(u"ax_vals_l")
        font = QFont()
        font.setPointSize(12)
        self.ax_vals_l.setFont(font)

        self.gridLayout_2.addWidget(self.ax_vals_l, 1, 0, 1, 1)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.label_3 = QLabel(self.gridLayoutWidget_2)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setMaximumSize(QSize(16777215, 47))

        self.horizontalLayout_3.addWidget(self.label_3)

        self.axis_1_btn = QToolButton(self.gridLayoutWidget_2)
        self.axis_1_btn.setObjectName(u"axis_1_btn")
        self.axis_1_btn.setCheckable(False)
        self.axis_1_btn.setPopupMode(QToolButton.MenuButtonPopup)
        self.axis_1_btn.setToolButtonStyle(Qt.ToolButtonTextOnly)
        self.axis_1_btn.setAutoRaise(False)

        self.horizontalLayout_3.addWidget(self.axis_1_btn)

        self.ax_and_l = QLabel(self.gridLayoutWidget_2)
        self.ax_and_l.setObjectName(u"ax_and_l")

        self.horizontalLayout_3.addWidget(self.ax_and_l)

        self.axis_2_btn = QToolButton(self.gridLayoutWidget_2)
        self.axis_2_btn.setObjectName(u"axis_2_btn")
        self.axis_2_btn.setCheckable(False)
        self.axis_2_btn.setPopupMode(QToolButton.MenuButtonPopup)
        self.axis_2_btn.setToolButtonStyle(Qt.ToolButtonTextOnly)
        self.axis_2_btn.setAutoRaise(False)

        self.horizontalLayout_3.addWidget(self.axis_2_btn)

        self.none_l = QLabel(self.gridLayoutWidget_2)
        self.none_l.setObjectName(u"none_l")
        self.none_l.setMaximumSize(QSize(16777215, 47))

        self.horizontalLayout_3.addWidget(self.none_l)

        self.range_l = QLabel(self.gridLayoutWidget_2)
        self.range_l.setObjectName(u"range_l")

        self.horizontalLayout_3.addWidget(self.range_l)

        self.range_1_edit = QLineEdit(self.gridLayoutWidget_2)
        self.range_1_edit.setObjectName(u"range_1_edit")
        self.range_1_edit.setMaximumSize(QSize(50, 16777215))

        self.horizontalLayout_3.addWidget(self.range_1_edit)

        self.range_to_l = QLabel(self.gridLayoutWidget_2)
        self.range_to_l.setObjectName(u"range_to_l")

        self.horizontalLayout_3.addWidget(self.range_to_l)

        self.range_2_edit = QLineEdit(self.gridLayoutWidget_2)
        self.range_2_edit.setObjectName(u"range_2_edit")
        self.range_2_edit.setMaximumSize(QSize(50, 16777215))

        self.horizontalLayout_3.addWidget(self.range_2_edit)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer)


        self.gridLayout_2.addLayout(self.horizontalLayout_3, 3, 0, 1, 1)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.inp_frame_1 = QFrame(self.gridLayoutWidget_2)
        self.inp_frame_1.setObjectName(u"inp_frame_1")
        sizePolicy1.setHeightForWidth(self.inp_frame_1.sizePolicy().hasHeightForWidth())
        self.inp_frame_1.setSizePolicy(sizePolicy1)
        self.inp_frame_1.setMinimumSize(QSize(75, 50))
        self.inp_frame_1.setAutoFillBackground(False)
        self.inp_frame_1.setFrameShape(QFrame.StyledPanel)
        self.inp_frame_1.setFrameShadow(QFrame.Raised)
        self.verticalLayoutWidget_14 = QWidget(self.inp_frame_1)
        self.verticalLayoutWidget_14.setObjectName(u"verticalLayoutWidget_14")
        self.verticalLayoutWidget_14.setGeometry(QRect(0, 0, 71, 50))
        self.verticalLayout_14 = QVBoxLayout(self.verticalLayoutWidget_14)
        self.verticalLayout_14.setObjectName(u"verticalLayout_14")
        self.verticalLayout_14.setContentsMargins(0, 0, 0, 0)
        self.inp_1_l = QLabel(self.verticalLayoutWidget_14)
        self.inp_1_l.setObjectName(u"inp_1_l")
        self.inp_1_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_14.addWidget(self.inp_1_l)

        self.inp_1_edit = QLineEdit(self.verticalLayoutWidget_14)
        self.inp_1_edit.setObjectName(u"inp_1_edit")

        self.verticalLayout_14.addWidget(self.inp_1_edit)


        self.horizontalLayout_2.addWidget(self.inp_frame_1)

        self.inp_frame_2 = QFrame(self.gridLayoutWidget_2)
        self.inp_frame_2.setObjectName(u"inp_frame_2")
        sizePolicy1.setHeightForWidth(self.inp_frame_2.sizePolicy().hasHeightForWidth())
        self.inp_frame_2.setSizePolicy(sizePolicy1)
        self.inp_frame_2.setMinimumSize(QSize(75, 50))
        self.inp_frame_2.setAutoFillBackground(False)
        self.inp_frame_2.setFrameShape(QFrame.StyledPanel)
        self.inp_frame_2.setFrameShadow(QFrame.Raised)
        self.verticalLayoutWidget_17 = QWidget(self.inp_frame_2)
        self.verticalLayoutWidget_17.setObjectName(u"verticalLayoutWidget_17")
        self.verticalLayoutWidget_17.setGeometry(QRect(0, 0, 71, 50))
        self.verticalLayout_17 = QVBoxLayout(self.verticalLayoutWidget_17)
        self.verticalLayout_17.setObjectName(u"verticalLayout_17")
        self.verticalLayout_17.setContentsMargins(0, 0, 0, 0)
        self.inp_2_l = QLabel(self.verticalLayoutWidget_17)
        self.inp_2_l.setObjectName(u"inp_2_l")
        self.inp_2_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_17.addWidget(self.inp_2_l)

        self.inp_2_edit = QLineEdit(self.verticalLayoutWidget_17)
        self.inp_2_edit.setObjectName(u"inp_2_edit")

        self.verticalLayout_17.addWidget(self.inp_2_edit)


        self.horizontalLayout_2.addWidget(self.inp_frame_2)

        self.inp_frame_3 = QFrame(self.gridLayoutWidget_2)
        self.inp_frame_3.setObjectName(u"inp_frame_3")
        sizePolicy1.setHeightForWidth(self.inp_frame_3.sizePolicy().hasHeightForWidth())
        self.inp_frame_3.setSizePolicy(sizePolicy1)
        self.inp_frame_3.setMinimumSize(QSize(75, 50))
        self.inp_frame_3.setAutoFillBackground(False)
        self.inp_frame_3.setFrameShape(QFrame.StyledPanel)
        self.inp_frame_3.setFrameShadow(QFrame.Raised)
        self.verticalLayoutWidget_18 = QWidget(self.inp_frame_3)
        self.verticalLayoutWidget_18.setObjectName(u"verticalLayoutWidget_18")
        self.verticalLayoutWidget_18.setGeometry(QRect(0, 0, 71, 50))
        self.verticalLayout_18 = QVBoxLayout(self.verticalLayoutWidget_18)
        self.verticalLayout_18.setObjectName(u"verticalLayout_18")
        self.verticalLayout_18.setContentsMargins(0, 0, 0, 0)
        self.inp_3_l = QLabel(self.verticalLayoutWidget_18)
        self.inp_3_l.setObjectName(u"inp_3_l")
        self.inp_3_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_18.addWidget(self.inp_3_l)

        self.inp_3_edit = QLineEdit(self.verticalLayoutWidget_18)
        self.inp_3_edit.setObjectName(u"inp_3_edit")

        self.verticalLayout_18.addWidget(self.inp_3_edit)


        self.horizontalLayout_2.addWidget(self.inp_frame_3)

        self.inp_frame_4 = QFrame(self.gridLayoutWidget_2)
        self.inp_frame_4.setObjectName(u"inp_frame_4")
        sizePolicy1.setHeightForWidth(self.inp_frame_4.sizePolicy().hasHeightForWidth())
        self.inp_frame_4.setSizePolicy(sizePolicy1)
        self.inp_frame_4.setMinimumSize(QSize(75, 50))
        self.inp_frame_4.setAutoFillBackground(False)
        self.inp_frame_4.setFrameShape(QFrame.StyledPanel)
        self.inp_frame_4.setFrameShadow(QFrame.Raised)
        self.verticalLayoutWidget_19 = QWidget(self.inp_frame_4)
        self.verticalLayoutWidget_19.setObjectName(u"verticalLayoutWidget_19")
        self.verticalLayoutWidget_19.setGeometry(QRect(0, 0, 71, 50))
        self.verticalLayout_19 = QVBoxLayout(self.verticalLayoutWidget_19)
        self.verticalLayout_19.setObjectName(u"verticalLayout_19")
        self.verticalLayout_19.setContentsMargins(0, 0, 0, 0)
        self.inp_4_l = QLabel(self.verticalLayoutWidget_19)
        self.inp_4_l.setObjectName(u"inp_4_l")
        self.inp_4_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_19.addWidget(self.inp_4_l)

        self.inp_4_edit = QLineEdit(self.verticalLayoutWidget_19)
        self.inp_4_edit.setObjectName(u"inp_4_edit")

        self.verticalLayout_19.addWidget(self.inp_4_edit)


        self.horizontalLayout_2.addWidget(self.inp_frame_4)

        self.inp_frame_5 = QFrame(self.gridLayoutWidget_2)
        self.inp_frame_5.setObjectName(u"inp_frame_5")
        sizePolicy1.setHeightForWidth(self.inp_frame_5.sizePolicy().hasHeightForWidth())
        self.inp_frame_5.setSizePolicy(sizePolicy1)
        self.inp_frame_5.setMinimumSize(QSize(75, 50))
        self.inp_frame_5.setAutoFillBackground(False)
        self.inp_frame_5.setFrameShape(QFrame.StyledPanel)
        self.inp_frame_5.setFrameShadow(QFrame.Raised)
        self.verticalLayoutWidget_20 = QWidget(self.inp_frame_5)
        self.verticalLayoutWidget_20.setObjectName(u"verticalLayoutWidget_20")
        self.verticalLayoutWidget_20.setGeometry(QRect(0, 0, 71, 50))
        self.verticalLayout_20 = QVBoxLayout(self.verticalLayoutWidget_20)
        self.verticalLayout_20.setObjectName(u"verticalLayout_20")
        self.verticalLayout_20.setContentsMargins(0, 0, 0, 0)
        self.inp_5_l = QLabel(self.verticalLayoutWidget_20)
        self.inp_5_l.setObjectName(u"inp_5_l")
        self.inp_5_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_20.addWidget(self.inp_5_l)

        self.inp_5_edit = QLineEdit(self.verticalLayoutWidget_20)
        self.inp_5_edit.setObjectName(u"inp_5_edit")

        self.verticalLayout_20.addWidget(self.inp_5_edit)


        self.horizontalLayout_2.addWidget(self.inp_frame_5)

        self.inp_frame_6 = QFrame(self.gridLayoutWidget_2)
        self.inp_frame_6.setObjectName(u"inp_frame_6")
        sizePolicy1.setHeightForWidth(self.inp_frame_6.sizePolicy().hasHeightForWidth())
        self.inp_frame_6.setSizePolicy(sizePolicy1)
        self.inp_frame_6.setMinimumSize(QSize(75, 50))
        self.inp_frame_6.setAutoFillBackground(False)
        self.inp_frame_6.setFrameShape(QFrame.StyledPanel)
        self.inp_frame_6.setFrameShadow(QFrame.Raised)
        self.verticalLayoutWidget_21 = QWidget(self.inp_frame_6)
        self.verticalLayoutWidget_21.setObjectName(u"verticalLayoutWidget_21")
        self.verticalLayoutWidget_21.setGeometry(QRect(0, 0, 71, 50))
        self.verticalLayout_21 = QVBoxLayout(self.verticalLayoutWidget_21)
        self.verticalLayout_21.setObjectName(u"verticalLayout_21")
        self.verticalLayout_21.setContentsMargins(0, 0, 0, 0)
        self.inp_6_l = QLabel(self.verticalLayoutWidget_21)
        self.inp_6_l.setObjectName(u"inp_6_l")
        self.inp_6_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_21.addWidget(self.inp_6_l)

        self.inp_6_edit = QLineEdit(self.verticalLayoutWidget_21)
        self.inp_6_edit.setObjectName(u"inp_6_edit")

        self.verticalLayout_21.addWidget(self.inp_6_edit)


        self.horizontalLayout_2.addWidget(self.inp_frame_6)

        self.inp_frame_7 = QFrame(self.gridLayoutWidget_2)
        self.inp_frame_7.setObjectName(u"inp_frame_7")
        sizePolicy1.setHeightForWidth(self.inp_frame_7.sizePolicy().hasHeightForWidth())
        self.inp_frame_7.setSizePolicy(sizePolicy1)
        self.inp_frame_7.setMinimumSize(QSize(75, 50))
        self.inp_frame_7.setAutoFillBackground(False)
        self.inp_frame_7.setFrameShape(QFrame.StyledPanel)
        self.inp_frame_7.setFrameShadow(QFrame.Raised)
        self.verticalLayoutWidget_22 = QWidget(self.inp_frame_7)
        self.verticalLayoutWidget_22.setObjectName(u"verticalLayoutWidget_22")
        self.verticalLayoutWidget_22.setGeometry(QRect(0, 0, 71, 50))
        self.verticalLayout_22 = QVBoxLayout(self.verticalLayoutWidget_22)
        self.verticalLayout_22.setObjectName(u"verticalLayout_22")
        self.verticalLayout_22.setContentsMargins(0, 0, 0, 0)
        self.inp_7_l = QLabel(self.verticalLayoutWidget_22)
        self.inp_7_l.setObjectName(u"inp_7_l")
        self.inp_7_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_22.addWidget(self.inp_7_l)

        self.inp_7_edit = QLineEdit(self.verticalLayoutWidget_22)
        self.inp_7_edit.setObjectName(u"inp_7_edit")

        self.verticalLayout_22.addWidget(self.inp_7_edit)


        self.horizontalLayout_2.addWidget(self.inp_frame_7)

        self.inp_frame_8 = QFrame(self.gridLayoutWidget_2)
        self.inp_frame_8.setObjectName(u"inp_frame_8")
        sizePolicy1.setHeightForWidth(self.inp_frame_8.sizePolicy().hasHeightForWidth())
        self.inp_frame_8.setSizePolicy(sizePolicy1)
        self.inp_frame_8.setMinimumSize(QSize(75, 50))
        self.inp_frame_8.setAutoFillBackground(False)
        self.inp_frame_8.setFrameShape(QFrame.StyledPanel)
        self.inp_frame_8.setFrameShadow(QFrame.Raised)
        self.verticalLayoutWidget_23 = QWidget(self.inp_frame_8)
        self.verticalLayoutWidget_23.setObjectName(u"verticalLayoutWidget_23")
        self.verticalLayoutWidget_23.setGeometry(QRect(0, 0, 71, 50))
        self.verticalLayout_23 = QVBoxLayout(self.verticalLayoutWidget_23)
        self.verticalLayout_23.setObjectName(u"verticalLayout_23")
        self.verticalLayout_23.setContentsMargins(0, 0, 0, 0)
        self.inp_8_l = QLabel(self.verticalLayoutWidget_23)
        self.inp_8_l.setObjectName(u"inp_8_l")
        self.inp_8_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_23.addWidget(self.inp_8_l)

        self.inp_8_edit = QLineEdit(self.verticalLayoutWidget_23)
        self.inp_8_edit.setObjectName(u"inp_8_edit")

        self.verticalLayout_23.addWidget(self.inp_8_edit)


        self.horizontalLayout_2.addWidget(self.inp_frame_8)

        self.inp_frame_9 = QFrame(self.gridLayoutWidget_2)
        self.inp_frame_9.setObjectName(u"inp_frame_9")
        sizePolicy1.setHeightForWidth(self.inp_frame_9.sizePolicy().hasHeightForWidth())
        self.inp_frame_9.setSizePolicy(sizePolicy1)
        self.inp_frame_9.setMinimumSize(QSize(75, 50))
        self.inp_frame_9.setAutoFillBackground(False)
        self.inp_frame_9.setFrameShape(QFrame.StyledPanel)
        self.inp_frame_9.setFrameShadow(QFrame.Raised)
        self.verticalLayoutWidget_24 = QWidget(self.inp_frame_9)
        self.verticalLayoutWidget_24.setObjectName(u"verticalLayoutWidget_24")
        self.verticalLayoutWidget_24.setGeometry(QRect(0, 0, 71, 50))
        self.verticalLayout_24 = QVBoxLayout(self.verticalLayoutWidget_24)
        self.verticalLayout_24.setObjectName(u"verticalLayout_24")
        self.verticalLayout_24.setContentsMargins(0, 0, 0, 0)
        self.inp_9_l = QLabel(self.verticalLayoutWidget_24)
        self.inp_9_l.setObjectName(u"inp_9_l")
        self.inp_9_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_24.addWidget(self.inp_9_l)

        self.inp_9_edit = QLineEdit(self.verticalLayoutWidget_24)
        self.inp_9_edit.setObjectName(u"inp_9_edit")

        self.verticalLayout_24.addWidget(self.inp_9_edit)


        self.horizontalLayout_2.addWidget(self.inp_frame_9)

        self.inp_frame_10 = QFrame(self.gridLayoutWidget_2)
        self.inp_frame_10.setObjectName(u"inp_frame_10")
        sizePolicy1.setHeightForWidth(self.inp_frame_10.sizePolicy().hasHeightForWidth())
        self.inp_frame_10.setSizePolicy(sizePolicy1)
        self.inp_frame_10.setMinimumSize(QSize(75, 50))
        self.inp_frame_10.setAutoFillBackground(False)
        self.inp_frame_10.setFrameShape(QFrame.StyledPanel)
        self.inp_frame_10.setFrameShadow(QFrame.Raised)
        self.verticalLayoutWidget_25 = QWidget(self.inp_frame_10)
        self.verticalLayoutWidget_25.setObjectName(u"verticalLayoutWidget_25")
        self.verticalLayoutWidget_25.setGeometry(QRect(0, 0, 71, 50))
        self.verticalLayout_25 = QVBoxLayout(self.verticalLayoutWidget_25)
        self.verticalLayout_25.setObjectName(u"verticalLayout_25")
        self.verticalLayout_25.setContentsMargins(0, 0, 0, 0)
        self.inp_10_l = QLabel(self.verticalLayoutWidget_25)
        self.inp_10_l.setObjectName(u"inp_10_l")
        self.inp_10_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_25.addWidget(self.inp_10_l)

        self.inp_10_edit = QLineEdit(self.verticalLayoutWidget_25)
        self.inp_10_edit.setObjectName(u"inp_10_edit")

        self.verticalLayout_25.addWidget(self.inp_10_edit)


        self.horizontalLayout_2.addWidget(self.inp_frame_10)

        self.inp_frame_11 = QFrame(self.gridLayoutWidget_2)
        self.inp_frame_11.setObjectName(u"inp_frame_11")
        sizePolicy1.setHeightForWidth(self.inp_frame_11.sizePolicy().hasHeightForWidth())
        self.inp_frame_11.setSizePolicy(sizePolicy1)
        self.inp_frame_11.setMinimumSize(QSize(75, 50))
        self.inp_frame_11.setAutoFillBackground(False)
        self.inp_frame_11.setFrameShape(QFrame.StyledPanel)
        self.inp_frame_11.setFrameShadow(QFrame.Raised)
        self.verticalLayoutWidget_26 = QWidget(self.inp_frame_11)
        self.verticalLayoutWidget_26.setObjectName(u"verticalLayoutWidget_26")
        self.verticalLayoutWidget_26.setGeometry(QRect(0, 0, 71, 50))
        self.verticalLayout_26 = QVBoxLayout(self.verticalLayoutWidget_26)
        self.verticalLayout_26.setObjectName(u"verticalLayout_26")
        self.verticalLayout_26.setContentsMargins(0, 0, 0, 0)
        self.inp_11_l = QLabel(self.verticalLayoutWidget_26)
        self.inp_11_l.setObjectName(u"inp_11_l")
        self.inp_11_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_26.addWidget(self.inp_11_l)

        self.inp_11_edit = QLineEdit(self.verticalLayoutWidget_26)
        self.inp_11_edit.setObjectName(u"inp_11_edit")

        self.verticalLayout_26.addWidget(self.inp_11_edit)


        self.horizontalLayout_2.addWidget(self.inp_frame_11)

        self.inp_frame_12 = QFrame(self.gridLayoutWidget_2)
        self.inp_frame_12.setObjectName(u"inp_frame_12")
        sizePolicy1.setHeightForWidth(self.inp_frame_12.sizePolicy().hasHeightForWidth())
        self.inp_frame_12.setSizePolicy(sizePolicy1)
        self.inp_frame_12.setMinimumSize(QSize(75, 50))
        self.inp_frame_12.setAutoFillBackground(False)
        self.inp_frame_12.setFrameShape(QFrame.StyledPanel)
        self.inp_frame_12.setFrameShadow(QFrame.Raised)
        self.verticalLayoutWidget_27 = QWidget(self.inp_frame_12)
        self.verticalLayoutWidget_27.setObjectName(u"verticalLayoutWidget_27")
        self.verticalLayoutWidget_27.setGeometry(QRect(0, 0, 71, 50))
        self.verticalLayout_27 = QVBoxLayout(self.verticalLayoutWidget_27)
        self.verticalLayout_27.setObjectName(u"verticalLayout_27")
        self.verticalLayout_27.setContentsMargins(0, 0, 0, 0)
        self.inp_12_l = QLabel(self.verticalLayoutWidget_27)
        self.inp_12_l.setObjectName(u"inp_12_l")
        self.inp_12_l.setAlignment(Qt.AlignCenter)

        self.verticalLayout_27.addWidget(self.inp_12_l)

        self.inp_12_edit = QLineEdit(self.verticalLayoutWidget_27)
        self.inp_12_edit.setObjectName(u"inp_12_edit")

        self.verticalLayout_27.addWidget(self.inp_12_edit)


        self.horizontalLayout_2.addWidget(self.inp_frame_12)

        self.no_axis_l = QLabel(self.gridLayoutWidget_2)
        self.no_axis_l.setObjectName(u"no_axis_l")

        self.horizontalLayout_2.addWidget(self.no_axis_l)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_2)


        self.gridLayout_2.addLayout(self.horizontalLayout_2, 6, 0, 1, 1)

        self.label_2 = QLabel(self.gridLayoutWidget_2)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setMaximumSize(QSize(16777215, 20))
        font1 = QFont()
        font1.setPointSize(10)
        self.label_2.setFont(font1)

        self.gridLayout_2.addWidget(self.label_2, 0, 0, 1, 1)

        self.label_4 = QLabel(self.gridLayoutWidget_2)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setMaximumSize(QSize(16777215, 25))

        self.gridLayout_2.addWidget(self.label_4, 5, 0, 1, 1)

        self.interrupt_l = QLabel(self.gridLayoutWidget_2)
        self.interrupt_l.setObjectName(u"interrupt_l")

        self.gridLayout_2.addWidget(self.interrupt_l, 2, 0, 1, 1)

        self.verticalLayout_5 = QVBoxLayout()
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.range_ax_1_l = QLabel(self.gridLayoutWidget_2)
        self.range_ax_1_l.setObjectName(u"range_ax_1_l")

        self.horizontalLayout.addWidget(self.range_ax_1_l)

        self.range_ax_1_start_inp = QLineEdit(self.gridLayoutWidget_2)
        self.range_ax_1_start_inp.setObjectName(u"range_ax_1_start_inp")
        self.range_ax_1_start_inp.setMaximumSize(QSize(50, 16777215))

        self.horizontalLayout.addWidget(self.range_ax_1_start_inp)

        self.range_1_to_l = QLabel(self.gridLayoutWidget_2)
        self.range_1_to_l.setObjectName(u"range_1_to_l")

        self.horizontalLayout.addWidget(self.range_1_to_l)

        self.range_ax_1_stop_inp = QLineEdit(self.gridLayoutWidget_2)
        self.range_ax_1_stop_inp.setObjectName(u"range_ax_1_stop_inp")
        self.range_ax_1_stop_inp.setMaximumSize(QSize(50, 16777215))

        self.horizontalLayout.addWidget(self.range_ax_1_stop_inp)

        self.horizontalSpacer_8 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_8)

        self.horizontalSpacer_6 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_6)

        self.horizontalSpacer_4 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_4)


        self.verticalLayout_5.addLayout(self.horizontalLayout)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.range_ax_2_l = QLabel(self.gridLayoutWidget_2)
        self.range_ax_2_l.setObjectName(u"range_ax_2_l")

        self.horizontalLayout_4.addWidget(self.range_ax_2_l)

        self.range_ax_2_start_inp = QLineEdit(self.gridLayoutWidget_2)
        self.range_ax_2_start_inp.setObjectName(u"range_ax_2_start_inp")
        self.range_ax_2_start_inp.setMaximumSize(QSize(50, 16777215))

        self.horizontalLayout_4.addWidget(self.range_ax_2_start_inp)

        self.range_2_to_l = QLabel(self.gridLayoutWidget_2)
        self.range_2_to_l.setObjectName(u"range_2_to_l")

        self.horizontalLayout_4.addWidget(self.range_2_to_l)

        self.range_ax_2_stop_inp = QLineEdit(self.gridLayoutWidget_2)
        self.range_ax_2_stop_inp.setObjectName(u"range_ax_2_stop_inp")
        self.range_ax_2_stop_inp.setMaximumSize(QSize(50, 16777215))

        self.horizontalLayout_4.addWidget(self.range_ax_2_stop_inp)

        self.horizontalSpacer_9 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_4.addItem(self.horizontalSpacer_9)

        self.horizontalSpacer_7 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_4.addItem(self.horizontalSpacer_7)

        self.horizontalSpacer_5 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_4.addItem(self.horizontalSpacer_5)


        self.verticalLayout_5.addLayout(self.horizontalLayout_4)


        self.gridLayout_2.addLayout(self.verticalLayout_5, 4, 0, 1, 1)

        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.run_btn = QPushButton(self.scrollAreaWidgetContents_2)
        self.run_btn.setObjectName(u"run_btn")
        self.run_btn.setGeometry(QRect(280, 600, 80, 24))
        font2 = QFont()
        font2.setPointSize(15)
        self.run_btn.setFont(font2)
        self.label = QLabel(self.scrollAreaWidgetContents_2)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(235, 5, 170, 40))
        font3 = QFont()
        font3.setPointSize(20)
        self.label.setFont(font3)
        self.frame_2 = QFrame(self.scrollAreaWidgetContents_2)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setGeometry(QRect(330, 95, 271, 221))
        self.frame_2.setFrameShape(QFrame.Box)
        self.frame_2.setFrameShadow(QFrame.Plain)
        self.verticalLayoutWidget = QWidget(self.frame_2)
        self.verticalLayoutWidget.setObjectName(u"verticalLayoutWidget")
        self.verticalLayoutWidget.setGeometry(QRect(10, 10, 251, 201))
        self.verticalLayout_4 = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalLayout_4.setContentsMargins(0, 0, 0, 0)
        self.label_5 = QLabel(self.verticalLayoutWidget)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setFont(font)

        self.verticalLayout_4.addWidget(self.label_5)

        self.keys_l = QTextEdit(self.verticalLayoutWidget)
        self.keys_l.setObjectName(u"keys_l")
        self.keys_l.setFrameShape(QFrame.Box)
        self.keys_l.setFrameShadow(QFrame.Plain)
        self.keys_l.setReadOnly(True)

        self.verticalLayout_4.addWidget(self.keys_l)

        self.custom_exp_btn = QPushButton(self.scrollAreaWidgetContents_2)
        self.custom_exp_btn.setObjectName(u"custom_exp_btn")
        self.custom_exp_btn.setGeometry(QRect(490, 55, 101, 24))
        self.custom_plot_btn = QPushButton(self.scrollAreaWidgetContents_2)
        self.custom_plot_btn.setObjectName(u"custom_plot_btn")
        self.custom_plot_btn.setGeometry(QRect(490, 20, 101, 24))
        self.expname_l = QLabel(self.scrollAreaWidgetContents_2)
        self.expname_l.setObjectName(u"expname_l")
        self.expname_l.setGeometry(QRect(169, 50, 302, 40))
        self.expname_l.setFont(font)
        self.expname_l.setAlignment(Qt.AlignCenter)
        self.scrollArea_2.setWidget(self.scrollAreaWidgetContents_2)

        self.gridLayout_3.addWidget(self.scrollArea_2, 0, 0, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)

        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.eng_radio.setText(QCoreApplication.translate("MainWindow", u"English", None))
        self.jap_radio.setText(QCoreApplication.translate("MainWindow", u"Japanese", None))
        self.export_radio.setText(QCoreApplication.translate("MainWindow", u"Export Data", None))
        self.visualize_radio.setText(QCoreApplication.translate("MainWindow", u"Visualize Data", None))
        self.d1_radio.setText(QCoreApplication.translate("MainWindow", u"1 Dimension", None))
        self.d2_radio.setText(QCoreApplication.translate("MainWindow", u"2 Dimensions", None))
        self.sdp_radio.setText(QCoreApplication.translate("MainWindow", u"Single Data Point", None))
        self.mdp_radio.setText(QCoreApplication.translate("MainWindow", u"Multiple Data Points", None))
        self.scopetrace_radio.setText(QCoreApplication.translate("MainWindow", u"Scope-trace Data", None))
        self.histogram_radio.setText(QCoreApplication.translate("MainWindow", u"Histogram Data", None))
        self.ammeter_radio.setText(QCoreApplication.translate("MainWindow", u"Ammeter Data", None))
        self.r_hmap_radio.setText(QCoreApplication.translate("MainWindow", u"Regular Heatmap", None))
        self.e_hmap_radio.setText(QCoreApplication.translate("MainWindow", u"Energy Heatmap", None))
        self.time_radio.setText(QCoreApplication.translate("MainWindow", u"Time Axis", None))
        self.energy_radio.setText(QCoreApplication.translate("MainWindow", u"Energy Axis", None))
        self.voltage_btn.setText(QCoreApplication.translate("MainWindow", u"Voltage", None))
        self.acc_scope_btn.setText(QCoreApplication.translate("MainWindow", u"Accumulator", None))
        self.acc_hist_btn.setText(QCoreApplication.translate("MainWindow", u"Accumulator", None))
        self.ax_vals_l.setText("")
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Axis to plot/export: ", None))
        self.axis_1_btn.setText(QCoreApplication.translate("MainWindow", u"Axis", None))
        self.ax_and_l.setText(QCoreApplication.translate("MainWindow", u"&", None))
        self.axis_2_btn.setText(QCoreApplication.translate("MainWindow", u"Axis", None))
        self.none_l.setText(QCoreApplication.translate("MainWindow", u"None", None))
        self.range_l.setText(QCoreApplication.translate("MainWindow", u"Range:", None))
        self.range_to_l.setText(QCoreApplication.translate("MainWindow", u" to ", None))
        self.inp_1_l.setText(QCoreApplication.translate("MainWindow", u"Axis", None))
        self.inp_2_l.setText(QCoreApplication.translate("MainWindow", u"Axis", None))
        self.inp_3_l.setText(QCoreApplication.translate("MainWindow", u"Axis", None))
        self.inp_4_l.setText(QCoreApplication.translate("MainWindow", u"Axis", None))
        self.inp_5_l.setText(QCoreApplication.translate("MainWindow", u"Axis", None))
        self.inp_6_l.setText(QCoreApplication.translate("MainWindow", u"Axis", None))
        self.inp_7_l.setText(QCoreApplication.translate("MainWindow", u"Axis", None))
        self.inp_8_l.setText(QCoreApplication.translate("MainWindow", u"Axis", None))
        self.inp_9_l.setText(QCoreApplication.translate("MainWindow", u"Axis", None))
        self.inp_10_l.setText(QCoreApplication.translate("MainWindow", u"Axis", None))
        self.inp_11_l.setText(QCoreApplication.translate("MainWindow", u"Axis", None))
        self.inp_12_l.setText(QCoreApplication.translate("MainWindow", u"Axis", None))
        self.no_axis_l.setText(QCoreApplication.translate("MainWindow", u"No axis input needed", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"All Axes:", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"Axes Input:", None))
        self.interrupt_l.setText("")
        self.range_ax_1_l.setText(QCoreApplication.translate("MainWindow", u"Axis:", None))
        self.range_1_to_l.setText(QCoreApplication.translate("MainWindow", u"to", None))
        self.range_ax_2_l.setText(QCoreApplication.translate("MainWindow", u"Axis:", None))
        self.range_2_to_l.setText(QCoreApplication.translate("MainWindow", u"to", None))
        self.run_btn.setText(QCoreApplication.translate("MainWindow", u"Go", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"DataManager", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"All Keys:", None))
        self.custom_exp_btn.setText(QCoreApplication.translate("MainWindow", u"Custom Export", None))
        self.custom_plot_btn.setText(QCoreApplication.translate("MainWindow", u"Custom Plot", None))
        self.expname_l.setText("")
    # retranslateUi

